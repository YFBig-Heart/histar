//
//  UIView+layer.m
//  FOXLive
//
//  Created by yus on 15/12/17.
//  Copyright © 2015年 Foxconn. All rights reserved.
//

#import "UIView+Layer.h"

@implementation UIView (Layer)


- (void)layerWithBorderColor:(UIColor *)borderColor
                 borderWidth:(CGFloat)borderWidth
                cornerRadius:(CGFloat)cornerRadius
               masksToBounds:(BOOL)masksToBounds{
    self.layer.borderColor = borderColor.CGColor;
    self.layer.borderWidth = borderWidth;
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = masksToBounds;
}

///设置为圆形,不需要边框
- (void)layerCircle{
    CGFloat width = CGRectGetWidth(self.frame) / 2;
    [self layerWithBorderColor:nil
                   borderWidth:0
                  cornerRadius:width
                 masksToBounds:YES];
}


///设置为圆形,需要边框
- (void)layerCircleAnBorderWithColor:(UIColor *)color{
    CGFloat width = CGRectGetWidth(self.frame) / 2;
    [self layerWithBorderColor:color
                   borderWidth:1
                  cornerRadius:width
                 masksToBounds:YES];
}

///设置为扁圆形,不需要边框
- (void)layerWafering{
    CGFloat height = CGRectGetHeight(self.frame) / 2;
    [self layerWithBorderColor:[UIColor whiteColor]
                   borderWidth:0
                  cornerRadius:height
                 masksToBounds:YES];
}

///设置为扁圆形,需要边框
- (void)layerWaferingAnBorder{
    CGFloat height = CGRectGetHeight(self.frame) / 2;
    [self layerWithBorderColor:[UIColor whiteColor]
                   borderWidth:1
                  cornerRadius:height
                 masksToBounds:YES];
}

///设置圆角
- (void)layerFilletWithRadius:(CGFloat)cornerRadius{
    [self layerWithBorderColor:[UIColor clearColor]
                   borderWidth:0
                  cornerRadius:cornerRadius
                 masksToBounds:YES];
}

///边框
- (void)layerBorder{
    [self layerWithBorderColor:[UIColor whiteColor]
                   borderWidth:1
                  cornerRadius:0
                 masksToBounds:YES];
}
///边框，设置颜色
- (void)layerBorderWithColor:(UIColor *)borderColor
{
    [self layerWithBorderColor:borderColor
                   borderWidth:1
                  cornerRadius:0
                 masksToBounds:YES];
    
}

///设置为宽度的一半，设置颜色
- (void)layerWidthWithColor:(UIColor *)borderColor
{
    CGFloat width = CGRectGetWidth(self.frame) / 2;
    [self layerWithBorderColor:borderColor
                   borderWidth:1
                  cornerRadius:width
                 masksToBounds:YES];
}

///设置为高度的一半，设置颜色
- (void)layerHeightWithColor:(UIColor *)borderColor
{
    CGFloat height = CGRectGetHeight(self.frame) / 2;
    [self layerWithBorderColor:borderColor
                   borderWidth:1
                  cornerRadius:height
                 masksToBounds:YES];
}

///设置为宽度的一半，不需要边框
- (void)layerWidth{
    CGFloat height = CGRectGetWidth(self.frame) / 2;
    self.layer.cornerRadius = height;
    self.layer.masksToBounds = YES;
}

///设置为高度的一半，不需要边框
- (void)layerHeight{
    CGFloat height = CGRectGetHeight(self.frame) / 2;
    self.layer.cornerRadius = height;
    self.layer.masksToBounds = YES;
}

- (void)drawTextInRect:(CGRect)rect color:(UIColor *)color{
    
    UILabel * label = (UILabel *)self;
    
    CGSize shadowOffset = label.shadowOffset;
    UIColor *textColor = label.textColor;
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(c, 4);
    CGContextSetLineJoin(c, kCGLineJoinRound);
    
    CGContextSetTextDrawingMode(c, kCGTextStroke);
    label.textColor = color;
    [label drawTextInRect:rect];
    
    CGContextSetTextDrawingMode(c, kCGTextFill);
    label.textColor = textColor;
    label.shadowOffset = CGSizeMake(0, 0);
    [label drawTextInRect:rect];
    
    label.shadowOffset = shadowOffset;
}

- (void)iOS8blurView:(UIView *)view Frame:(CGRect)frame{
    
    if (IOS8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//        blurEffect.b
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.alpha = 0.8;
        visualEffectView.frame = frame;
        
        [view addSubview:visualEffectView];
    }
}

#pragma mark - 设置部分圆角
/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

#pragma mark - 生成渐变色的图片
+ (UIImage *)drawBackgroundImageWith:(CGRect)rect startColor:(UIColor *)startColor endColor:(UIColor *)endColor gridentH:(BOOL)horizontalGrident {
    // 导航栏颜色是 0xea6a62  0xdf4655 两个颜色渐变得出来的
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //创建CGMutablePathRef
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    
    CGPathAddLineToPoint(path, NULL,[UIScreen mainScreen].bounds.size.width, 0);
    CGPathAddLineToPoint(path, NULL,[UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, 0, CGRectGetMaxY(rect));
    
    [self drawLinearGradient:ctx path:path startColor:startColor.CGColor endColor:endColor.CGColor gridentH:horizontalGrident];
    
    CGPathRelease(path);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
    
}

// 生成两色上下渐变得背景图片
+ (UIImage *)drawBackgroundImageWith:(CGRect)rect startColor:(UIColor *)startColor endColor:(UIColor *)endColor {
    
    // 导航栏颜色是 0xea6a62  0xdf4655 两个颜色渐变得出来的
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //创建CGMutablePathRef
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    CGPathAddLineToPoint(path, NULL,[UIScreen mainScreen].bounds.size.width, 0);
    CGPathAddLineToPoint(path, NULL,[UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, 0, CGRectGetMaxY(rect));
    
    [self drawLinearGradient:ctx path:path startColor:startColor.CGColor endColor:endColor.CGColor];
    
    CGPathRelease(path);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}



+ (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor gridentH:(BOOL)horizontalGrident {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGRect pathRect = CGPathGetBoundingBox(path);
    
    CGPoint startPoint = CGPointZero;
    CGPoint endPoint = CGPointZero;
    //具体方向可根据需求修改
    if (horizontalGrident) {
        startPoint = CGPointMake(CGRectGetMinX(pathRect), CGRectGetMidY(pathRect));
        
        endPoint = CGPointMake(CGRectGetMaxX(pathRect), CGRectGetMidY(pathRect));
        
    }else {
        startPoint = CGPointMake(CGRectGetMidX(pathRect), CGRectGetMinY(pathRect));
        
        endPoint = CGPointMake(CGRectGetMidX(pathRect), CGRectGetMaxY(pathRect));
    }
    
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
}

+ (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor
{
    [self drawLinearGradient:context path:path startColor:startColor endColor:endColor gridentH:NO];
    
}

+ (UIImage *)drawBackgroundImageWith:(CGRect)rect gradiendtRect:(CGRect)gradiendtRect startColor:(UIColor *)startColor endColor:(UIColor *)endColor{
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //创建CGMutablePathRef
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, gradiendtRect.origin.x, gradiendtRect.origin.y);
    CGPathAddLineToPoint(path, NULL,CGRectGetMaxX(gradiendtRect),gradiendtRect.origin.y);
    CGPathAddLineToPoint(path, NULL,CGRectGetMaxX(gradiendtRect), CGRectGetMaxY(gradiendtRect));
    
    CGPathAddLineToPoint(path, NULL,gradiendtRect.origin.x, CGRectGetMaxY(rect));
    
    [self drawLinearGradient:ctx path:path startColor:startColor.CGColor endColor:endColor.CGColor];
    
    CGPathRelease(path);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
    
}

/**
 *  通过 CAShapeLayer 方式绘制虚线
 *
 *  param lineView:       需要绘制成虚线的view
 *  param lineLength:     虚线的宽度
 *  param lineSpacing:    虚线的间距
 *  param lineColor:      虚线的颜色
 *  param lineDirection   虚线的方向  YES 为水平方向， NO 为垂直方向
 *  param startAngle:     起始角度，默认为0
 *  param endAngle:   结束角度，默认为M_PI * 2,当startAngle和endAngle都为是0，怎么为默认值
 **/
- (CAShapeLayer *)drawLineOfDashByCAShapeLayerWithlegnth:(CGFloat)legnth spacing:(CGFloat)spacing circlePoint:(CGPoint)circlePoint radius:(CGFloat)radius lineWidth:(CGFloat)lineWidth lineColor:(UIColor *)lineColor drawDash:(BOOL)drawDash startAngle:(CGFloat)startAngle endAngle:(CGFloat)endAngle {

    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:self.bounds];
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    [shapeLayer setStrokeColor:lineColor.CGColor];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineCap:kCALineCapRound];
    [shapeLayer setLineWidth:lineWidth];
    if (drawDash) {
        [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithFloat:legnth], [NSNumber numberWithFloat:spacing], nil]];
    }

    CGMutablePathRef path = CGPathCreateMutable();
    if (startAngle == 0 && endAngle ==0) {
        endAngle = M_PI * 2;
    }
    CGPathAddArc(path, NULL, circlePoint.x, circlePoint.y, radius, startAngle,  endAngle, NO);

    [shapeLayer setPath:path];
    CGPathRelease(path);
    [self.layer addSublayer:shapeLayer];
    return shapeLayer;
}


#pragma mark - 获取当前控制器
//获取当前屏幕显示的viewcontroller
+(UIViewController *)getCurrentWindowVC
{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tempWindow in windows)
        {
            if (tempWindow.windowLevel == UIWindowLevelNormal)
            {
                window = tempWindow;
                break;
            }
        }
    }
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]])
    {
        result = nextResponder;
    }
    else
    {
        result = window.rootViewController;
    }
    return  result;
}
+(UIViewController *)getCurrentUIVC
{
    UIViewController  *superVC = [[self class]  getCurrentWindowVC ];

    if ([superVC isKindOfClass:[UITabBarController class]]) {

        UIViewController  *tabSelectVC = ((UITabBarController*)superVC).selectedViewController;

        if ([tabSelectVC isKindOfClass:[UINavigationController class]]) {

            return ((UINavigationController*)tabSelectVC).viewControllers.lastObject;
        }
        return tabSelectVC;
    }else
        if ([superVC isKindOfClass:[UINavigationController class]]) {
            return ((UINavigationController*)superVC).viewControllers.lastObject;
        }
    return superVC;
}

@end
