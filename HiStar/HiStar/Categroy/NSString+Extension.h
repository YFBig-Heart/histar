//
//  NSString+Extension.h
//  CoolTennisBall
//
//  Created by Coollang on 16/9/5.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

/**
 * obj 为nil时返回一个空的字符串
 */
+ (NSString *)checkIfNullWithString:(id)obj;
// APP 当前版本
+ (NSString *)appCurrentVersion;
+ (NSString *)appCurrentName;

/**
 * 获取中英文混合的字符长度
 */
+ (int)getCharLength:(NSString *)str;

- (BOOL)checkIsEnglishAndNumberWithInCounts:(NSInteger)count;
- (NSString *)trimmingSpecailSymbolWithOutUnderLine;
- (NSString *)trimmingSpecailSymbol;

/**
判断字符串是否为纯数字
 */
- (BOOL)isAllNum;

/**
 * 找出字符串中的数字,并按原有顺序返回
 */
- (NSString *)trimingAllNumber;

/**
 *  秒钟转小时
 *  @param trainTime 秒数
 *
 *  @return 不足一小时-返回类似：34:23，大于1小时-返回类似 01:34:23
 */
+ (NSString *)stringWithTrainTime:(int)trainTime mustShowHour:(BOOL)mustShowHour;

/**
 *  分钟转小时
 *  @param timeInterval 分钟数
 *
 *  @return 返回类似 0.9  1 的时间,单位小时
 */
+ (NSString *)stringHourAndMinuteWith:(NSInteger)timeInterval;

/**
 *  秒钟转分钟
 *  @param timeInterval 秒数
 *
 *  @return 返回类似 01:34
 */
+ (NSString *)stringMinuteAndSecondWith:(NSInteger)timeInterval;


/**
 *  字典转json
 */
+ (NSString*)dictionaryToJson:(id)dic;


/**
 *  json字符串转字典
 */
+ (id)dictionaryWithJsonString:(NSString *)jsonString;


/**
 *  计算文本大小
 *  @param maxSize     文本的最大尺寸
 *  @param font        文本字体大小
 */
- (CGSize)textSizeWithtextMaxSize:(CGSize)maxSize font:(UIFont *)font;

/**
 *
 *  @param currentVersion 服务器的版本号 如：v0.0.6
 *  @param version        硬件本地版本号 如：v0.0.4
 */
+ (BOOL)checkIsLastestVersion:(NSString *)currentVersion lastestVersion:(NSString *)version;

/*
 检查名字长度是否合法
 len:最大长度，包含最大长度
 hud:是否吐司提示
 **/
+ (BOOL)formatStr:(NSString *)string len:(int)len hud:(BOOL)hud;

@end
