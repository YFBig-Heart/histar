//
//  UIView+Frame.m
//  Coding_iOS
//
//  Created by pan Shiyu on 15/7/16.
//  Copyright (c) 2015年 Coding. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX
{
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY
{
    return self.center.y;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setTop:(CGFloat)t
{
    self.frame = CGRectMake(self.left, t, self.width, self.height);
}

- (CGFloat)top
{
    return self.frame.origin.y;
}

- (void)setBottom:(CGFloat)b
{
    self.frame = CGRectMake(self.left, b - self.height, self.width, self.height);
}

- (CGFloat)bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setLeft:(CGFloat)l
{
    self.frame = CGRectMake(l, self.top, self.width, self.height);
}

- (CGFloat)left
{
    return self.frame.origin.x;
}

- (void)setRight:(CGFloat)r
{
    self.frame = CGRectMake(r - self.width, self.top, self.width, self.height);
}

- (CGFloat)right
{
    return self.frame.origin.x + self.frame.size.width;
}


- (UIViewController *)viewController {

    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

- (UINavigationController *)navigationViewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            if ([nextResponder isKindOfClass:[UINavigationController class]]) {
                return (UINavigationController *)nextResponder;
            }

            UIViewController *viewController = (UIViewController *)nextResponder;
            if (viewController.navigationController) {
                return viewController.navigationController;
            }
        }
    }
    return nil;
}
#pragma mark - 工厂方法
+ (UIButton *)creatButtonTitle:(NSString *)title bgColor:(UIColor *__nullable)bgColor textFont:(UIFont *__nullable)textFont textColor:(UIColor *)textColor {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    if (bgColor){button.backgroundColor = bgColor;}
    if (textFont){button.titleLabel.font = textFont;}
    if (textColor) {
        [button setTitleColor:textColor forState:UIControlStateNormal];
    }
    return button;
}

+ (UIButton *)creatButtonTitle:(NSString * __nullable)title bgColor:(UIColor *__nullable)bgColor textFont:(UIFont *__nullable)textFont textColor:(UIColor *)textColor normalImage:(NSString *__nullable)normalImage selectImage:(NSString *__nullable)selectImage {
    UIButton *sender = [UIButton creatButtonTitle:title bgColor:bgColor textFont:textFont textColor:textColor];
    if (normalImage) {
        [sender setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
    }
    if (selectImage) {
        [sender setImage:[UIImage imageNamed:selectImage] forState:UIControlStateSelected];
    }

    return sender;
}
+ (UIButton *)creatBtnnormalImage:(NSString *__nullable)normalImage selectImage:(NSString *__nullable)selectImage target:(id)target action:(SEL)action {
    UIButton *sender = [UIButton buttonWithType:UIButtonTypeCustom];
    [sender setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
    if (normalImage) {
        [sender setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
    }
    if (selectImage) {
        [sender setImage:[UIImage imageNamed:selectImage] forState:UIControlStateSelected];
    }
    [sender addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return sender;
}

+ (UILabel *)CreatLabelText:(NSString *)text bgColor:(UIColor *)bgColor textFont:(UIFont *)textFont textColor:(UIColor *)textColor textAliment:(NSTextAlignment)textAliment {
    UILabel *label = [[UILabel alloc] init];
    if (bgColor){label.backgroundColor = bgColor;}
    label.text = text;
    if (textFont){label.font = textFont;}
    if (textColor){label.textColor = textColor;}
    label.textAlignment = textAliment;
    return label;
}


@end

