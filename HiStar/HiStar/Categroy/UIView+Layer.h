//
//  UIView+layer.h
//  FOXLive
//
//  Created by yus on 15/12/17.
//  Copyright © 2015年 Foxconn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Layer)

/**
 *  设置view及其子类的边框，圆角属性
 *
 *  @param borderColor   边框颜色
 *  @param borderWidth   边框宽度
 *  @param cornerRadius  圆角弧度
 *  @param masksToBounds
 */
- (void)layerWithBorderColor:(UIColor *)borderColor
                 borderWidth:(CGFloat)borderWidth
                cornerRadius:(CGFloat)cornerRadius
               masksToBounds:(BOOL)masksToBounds;

///设置圆角
- (void)layerFilletWithRadius:(CGFloat)cornerRadius;

///边框，设置颜色
- (void)layerBorderWithColor:(UIColor *)borderColor;

///设置为高度的一半，不需要边框
- (void)layerHeight;

///设置为高度的一半，设置边框颜色
- (void)layerHeightWithColor:(UIColor *)borderColor;

///设置为圆形,不需要边框
- (void)layerCircle;

///设置为圆形,需要边框
- (void)layerCircleAnBorderWithColor:(UIColor *)color;




///设置为高度的一半，不需要边框
- (void)layerWidth;

///设置为宽度的一半，设置边框颜色
- (void)layerWidthWithColor:(UIColor *)borderColor;

///设置为扁圆形,不需要边框
- (void)layerWafering;

///设置为扁圆形,需要边框
- (void)layerWaferingAnBorder;

///label字体描边
- (void)drawTextInRect:(CGRect)rect color:(UIColor *)color;

///设置iOS8以后毛玻璃效果
- (void)iOS8blurView:(UIView *)view Frame:(CGRect)frame;



/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii;
/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect;

#pragma mark - 画渐变图片
+ (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor;

+ (UIImage *)drawBackgroundImageWith:(CGRect)rect startColor:(UIColor *)startColor endColor:(UIColor *)endColor;

// horizontalGrident 是否为水平
+ (UIImage *)drawBackgroundImageWith:(CGRect)rect startColor:(UIColor *)startColor endColor:(UIColor *)endColor gridentH:(BOOL)horizontalGrident;

// rect: 图片的大小 gradiendtRect：绘制的渐变色区域
+ (UIImage *)drawBackgroundImageWith:(CGRect)rect gradiendtRect:(CGRect)gradiendtRect startColor:(UIColor *)startColor endColor:(UIColor *)endColor;

#pragma mark - 绘制圆弧
/**
 *  通过 CAShapeLayer 方式绘制虚线
 *
 *  param lineView:       需要绘制成虚线的view
 *  param lineLength:     虚线的宽度
 *  param lineSpacing:    虚线的间距
 *  param lineColor:      虚线的颜色
 *  param lineDirection   虚线的方向  YES 为水平方向， NO 为垂直方向
 *  param startAngle:     起始角度，默认为0
 *  param endAngle:   结束角度，默认为M_PI * 2,当startAngle和endAngle都为是0，怎么为默认值
 **/
- (CAShapeLayer *)drawLineOfDashByCAShapeLayerWithlegnth:(CGFloat)legnth spacing:(CGFloat)spacing circlePoint:(CGPoint)circlePoint radius:(CGFloat)radius lineWidth:(CGFloat)lineWidth lineColor:(UIColor *)lineColor drawDash:(BOOL)drawDash startAngle:(CGFloat)startAngle endAngle:(CGFloat)endAngle;

#pragma mark - 获取当前控制器
//获取当前屏幕显示的viewcontroller
+(UIViewController *)getCurrentWindowVC;
+(UIViewController *)getCurrentUIVC;

@end








