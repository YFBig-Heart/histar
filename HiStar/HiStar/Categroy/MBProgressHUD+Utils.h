//
//  MBProgressHUD+Utils.h
//  FOXAISpeaker
//
//  Created by answer on 2018/5/8.
//  Copyright © 2018年 answer. All rights reserved.
//

#import "MBProgressHUD.h"

#define MBProgressHUDShowTimeDefault 2.0

@interface MBProgressHUD (Utils)

+ (void)showTipMessageInWindow:(NSString*)message;
+ (void)showTipMessageInView:(NSString*)message;
+ (void)showTipMessageInWindow:(NSString*)message timer:(NSTimeInterval)aTimer;
+ (void)showTipMessageInView:(NSString*)message timer:(NSTimeInterval)aTimer;

+ (void)showActivityMessageInWindow:(NSString*)message;
+ (void)showActivityMessageInView:(NSString*)message;
+ (void)showActivityMessageInWindow:(NSString*)message timer:(NSTimeInterval)aTimer;
+ (void)showActivityMessageInView:(NSString*)message timer:(NSTimeInterval)aTimer;

+ (void)showSuccessMessage:(NSString *)Message timer:(NSTimeInterval)aTimer;
+ (void)showErrorMessage:(NSString *)Message timer:(NSTimeInterval)aTimer;

+ (void)hideHUD;

@end
