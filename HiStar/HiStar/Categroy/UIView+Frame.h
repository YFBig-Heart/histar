//
//  UIView+Frame.h
//  Coding_iOS
//
//  Created by pan Shiyu on 15/7/16.
//  Copyright (c) 2015年 Coding. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>

@interface UIView (Frame)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;

- (UIViewController *)viewController;

- (UINavigationController *)navigationViewController;

#pragma mark - 工厂方法
+ (UIButton *)creatButtonTitle:(NSString *)title bgColor:(UIColor *)bgColor textFont:(UIFont *)textFont textColor:(UIColor *)textColor;
+ (UIButton *)creatButtonTitle:(NSString * __nullable)title bgColor:(UIColor *__nullable)bgColor textFont:(UIFont *__nullable)textFont textColor:(UIColor *)textColor normalImage:(NSString *__nullable)normalImage selectImage:(NSString *__nullable)selectImage;
+ (UIButton *)creatBtnnormalImage:(NSString *__nullable)normalImage selectImage:(NSString *__nullable)selectImage target:(id)target action:(SEL)action;


+ (UILabel *)CreatLabelText:(NSString *)text bgColor:(UIColor *__nullable)bgColor textFont:(UIFont *__nullable)textFont textColor:(UIColor *)textColor textAliment:(NSTextAlignment)textAliment;

// 扩展View的响应范围,一般用于按钮



@end
