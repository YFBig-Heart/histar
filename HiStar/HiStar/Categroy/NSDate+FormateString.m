//
//  NSDate+FormateString.m
//  CoolMove
//
//  Created by CA on 15/4/26.
//  Copyright (c) 2015年 CA. All rights reserved.
//

#import "NSDate+FormateString.h"



@implementation NSDate (FormateString)

#pragma mark - Public Method

// 字符串转时间
+ (NSDate *)formateYearMonthDayString:(NSString *)string
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return [dateFormatter dateFromString:string];
}
+ (NSDate *)formateYearMonthString:(NSString *)string {
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setDateFormat:@"yyyy-MM"];
    }
    return [dateFormatter dateFromString:string];
}

+ (NSString *)formateUnixTimeStampTo24ClockString:(NSTimeInterval)unixTimeStamp
{
    NSDate *date = [NSDate dateWithTimeInterval:unixTimeStamp sinceDate:[self formateString:@"1970-01-01 00:00:00"]];
    return [self formateTo24ClockWithDate:date];
}

+ (NSString *)formateUnixTimeStampTo24ClockString:(NSTimeInterval)unixTimeStamp minutes:(NSUInteger)mintues
{
    NSTimeInterval newUnixTimeStamp = unixTimeStamp - (NSUInteger)unixTimeStamp%(60 * mintues);
    return [self formateUnixTimeStampTo24ClockString:newUnixTimeStamp];
}

+ (NSString *)formateUnixTimeStampTo24ClockNewString:(NSTimeInterval)unixTimeStamp withDateFormatter:(NSDateFormatter *)formatter
{
    NSDate *date = [NSDate dateWithTimeInterval:unixTimeStamp sinceDate:[self formateString:@"1970-01-01 00:00:00"]];
    return [self formateTo24ClockWithNewDate:date dateFormatter:formatter];
}

#pragma mark - Private Method

// 时间转字符串
+ (NSString *)formateDate:(NSDate *)date
{
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone systemTimeZone];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return [formatter stringFromDate:date];
}

// 字符串转时间
+ (NSDate *)formateString:(NSString *)string
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.timeZone = [NSTimeZone systemTimeZone];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return [dateFormatter dateFromString:string];
}
- (NSString *)formateYearAndMonth {
    
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone systemTimeZone];
        [formatter setDateFormat:@"yyyy-MM"];
    }
    return [formatter stringFromDate:self];
}
// 时间转字符串
+ (NSString *)formateTo24ClockWithDate:(NSDate *)date
{
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone systemTimeZone];
        [formatter setDateFormat:@"HH:mm"];
    }
    return [formatter stringFromDate:date];
}

- (NSString *)formateYearMonthDay
{
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone systemTimeZone];
        [formatter setDateFormat:@"yyyy-MM-dd"];
    }
    return [formatter stringFromDate:self];
}

// 时间转字符串
+ (NSString *)formateTo24ClockWithNewDate:(NSDate *)date dateFormatter:(NSDateFormatter *)dateFormatter
{
    return [dateFormatter stringFromDate:date];
}
// unixTimeStamp:必须是零时区的时间戳
+ (NSString *)formateUnixTimeStamp:(NSTimeInterval)unixTimeStamp formatString:(NSString *)format {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixTimeStamp];
    NSDateFormatter *dateform = [[NSDateFormatter alloc] init];
    dateform.timeZone = [NSTimeZone systemTimeZone];
    NSInteger secoend = [[NSTimeZone systemTimeZone]  secondsFromGMTForDate:date];
    unixTimeStamp += secoend;
    [dateform setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [NSDate formateUnixTimeStampTo24ClockNewString:unixTimeStamp withDateFormatter:dateform];
}

// 时间戳
- (NSTimeInterval)unixTimeStamp
{
   return [self timeIntervalSinceDate:[NSDate formateString:@"1970-01-01 00:00:00"]];
}

- (NSString *)unixTimeStampWithDateStr
{
    return [@([self timeIntervalSinceDate:[NSDate formateString:@"1970-01-01 00:00:00"]]) stringValue];
}

+ (NSDate *)dateWithUnixTimeStamp:(NSTimeInterval)timeInterval
{
    return [[NSDate alloc] initWithTimeInterval:timeInterval sinceDate:[NSDate formateString:@"1970-01-01 00:00:00"]];
}


/**
 计算两个时间相隔多久
 */
+ (NSDateComponents *)compareStartDate:(NSDate *)startDate endDate:(NSDate *)endDate calendarUnit:(NSCalendarUnit)calendarUnit {
    //利用NSCalendar比较日期的差异
    NSCalendar *calendar = [NSCalendar currentCalendar];
    /**
     * 要比较的时间单位,常用如下,可以同时传：
     *    NSCalendarUnitDay : 天
     *    NSCalendarUnitYear : 年
     *    NSCalendarUnitMonth : 月
     *    NSCalendarUnitHour : 时
     *    NSCalendarUnitMinute : 分
     *    NSCalendarUnitSecond : 秒
     */
    NSCalendarUnit unit = calendarUnit;//只比较天数差异
    //比较的结果是NSDateComponents类对象
    NSDateComponents *delta = [calendar components:unit fromDate:startDate toDate:endDate options:0];
    return delta;
}

+(NSDate *)getPriousorLaterDateFromDate:(NSDate *)date withMonth:(int)month {
    NSDateComponents *comps = [[NSDateComponents alloc]init];
    [comps setMonth:month];
    NSCalendar *calender = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];// NSGregorianCalendar、
    NSDate *mDate = [calender dateByAddingComponents:comps toDate:date options:0];
    return mDate;
}

@end
