//
//  UIImage+YFExtension.h
//  SquashSpark
//
//  Created by Coollang on 2017/11/3.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPhotoGroupView.h"

typedef void(^ThumbnailBlock)(BOOL success, UIImage *image);

@interface UIImage (YFExtension)

#pragma mark - 截屏
+ (UIImage *)imageFromView:(UIView *)theView;
+ (UIImage *)imageFromView:(UIView *)theView atFrame:(CGRect)r;
#pragma mark - 图片平铺 默认取图像中间一个像素平铺
+ (UIImage *)resizableImageName:(NSString *)imageName;
+ (UIImage *)resizableImage:(UIImage *)image;

#pragma mark - 颜色转换成图片(带圆角的)
+ (UIImage *)imageWithColor:(UIColor *)color redius:(CGFloat)redius size:(CGSize)size;

#pragma mark - 根据视频地址生成缩略图
+ (UIImage*)thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time;
+ (void)thumbnailImageForVideo:(NSURL *)videoURL withCompletionBlock:(ThumbnailBlock)block;

#pragma mark - 图片浏览
+ (void)browseURLImages:(NSArray <YYPhotoGroupItem *>*)groupItems fromView:(UIView *)fromView;


@end
