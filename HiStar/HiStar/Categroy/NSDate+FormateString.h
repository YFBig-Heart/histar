//
//  NSDate+FormateString.h
//  CoolMove
//
//  Created by CA on 15/4/26.
//  Copyright (c) 2015年 CA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (FormateString)

+ (NSDate *)formateYearMonthDayString:(NSString *)string;
+ (NSDate *)formateYearMonthString:(NSString *)string;
+ (NSDate *)formateString:(NSString *)string;
+ (NSDate *)dateWithUnixTimeStamp:(NSTimeInterval)timeInterval;


// 时间戳
- (NSTimeInterval)unixTimeStamp;
- (NSString *)unixTimeStampWithDateStr;

+ (NSString *)formateUnixTimeStampTo24ClockString:(NSTimeInterval)unixTimeStamp;
+ (NSString *)formateUnixTimeStampTo24ClockString:(NSTimeInterval)unixTimeStamp minutes:(NSUInteger)mintues;

// unixTimeStamp:必须是零时区的时间戳
+ (NSString *)formateUnixTimeStamp:(NSTimeInterval)unixTimeStamp formatString:(NSString *)format;

+ (NSString *)formateUnixTimeStampTo24ClockNewString:(NSTimeInterval)unixTimeStamp withDateFormatter:(NSDateFormatter *)formatter;

- (NSString *)formateYearMonthDay;
- (NSString *)formateYearAndMonth;


/**
 计算两个时间相隔多久
 */
+ (NSDateComponents *)compareStartDate:(NSDate *)startDate endDate:(NSDate *)endDate calendarUnit:(NSCalendarUnit)calendarUnit;

// 获取该时间的
/**
 date:传入的时间
 month: -1,上一个月，1下一个月
 */
+(NSDate *)getPriousorLaterDateFromDate:(NSDate *)date withMonth:(int)month;



@end
