//
//  AppDelegate.m
//  HiStar
//
//  Created by 晴天 on 2019/5/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "AppDelegate.h"
#import "HSHomeViewController.h"
#import "MyNavigationViewController.h"
#import "HSLoginNavigationController.h"
#import "LLFullScreenAdView.h"
#import "YFiAdTimeViewController.h"

#import <SMS_SDK/SMSSDK.h>
#import <Bugly/Bugly.h>

#import "SMSSDKUI.h"
#import <SMS_SDK/SMSSDK+ContactFriends.h>
#import <MOBFoundation/MOBFoundation.h>
#import <MobLinkPro/UIViewController+MLSDKRestore.h>
#import <MobLinkPro/MobLink.h>
#import <MobLinkPro/MLSDKScene.h>
#import <ShareSDK/ShareSDK.h>

@interface AppDelegate ()<IMLSDKRestoreDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    YFiAdTimeViewController *vc = [[YFiAdTimeViewController alloc] init];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [MobLink setDelegate:self];
    
    [self addADView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:@"kLoginAcionNotification" object:nil];
    [Bugly startWithAppId:kBuglyAppid];
    [self registerShareSdk];
    return YES;
}

/** 添加广告图 */
- (void)addADView
{
    LLFullScreenAdView *adView = [[LLFullScreenAdView alloc] init];
    adView.tag = 100;
    adView.duration = 3;
    adView.waitTime = 3;
    adView.skipType = SkipButtonTypeCircleAnimationTest;
    adView.adImageTapBlock = ^(NSString *content) {
        NSLog(@"%@", content);
    };
    [adView addLocationImage:[UIImage imageNamed:@"LaunchScreen_V"]];
    
    [adView setDismissBlock:^(LLFullScreenAdView *iadView) {
        UINavigationController *nav = [self getVerificationCodeWithLogin:![YFUserDefault shouldAutoLogin]];
        self.window.rootViewController = nav;
    }];
    [self.window addSubview:adView];
}

- (void)loginSuccess:(NSNotification *)note {
    UINavigationController *nav = [self getVerificationCodeWithLogin:![note.object boolValue]];
    self.window.rootViewController = nav;
    if ([note.userInfo.allKeys containsObject:@"phone"]) {
        [YFUserDefault saveLastLoginAccountID:note.object];
    }
}

- (UINavigationController *)getVerificationCodeWithLogin:(BOOL)isLogin
{
    if (isLogin) {
        SMSSDKUIGetCodeViewController *vc = [[SMSSDKUIGetCodeViewController alloc] initWithMethod:SMSGetCodeMethodSMS];
        HSLoginNavigationController *nav = [[HSLoginNavigationController alloc] initWithRootViewController:vc];
        nav.yfnavStyle = NavigationLogin;
        [YFUserDefault setAutoLogin:NO];
        vc.navigationItem.title = NSLocalizedString(@"Login", nil);
        vc.navigationItem.leftBarButtonItem = [UIBarButtonItem new];
        return nav;
    }else {
        HSHomeViewController *hoemVc = [HSHomeViewController homeViewController];
        MyNavigationViewController *nav = [[MyNavigationViewController alloc] initWithRootViewController:hoemVc];
        nav.yfnavStyle = NavigationNomal;
        [YFUserDefault setAutoLogin:YES];
        // 启动蓝牙
        [[YFCommunicationManager shareInstance] lgCenterManager];
        return nav;
    }
}

- (void)registerShareSdk {
    
    [ShareSDK registPlatforms:^(SSDKRegister *platformsRegister) {
      //QQ
//      [platformsRegister setupQQWithAppId:@"100371282" appkey:@"aed9b0303e3ed1e27bae87c33761161d"];
     //微信
     [platformsRegister setupWeChatWithAppId:kwechatAppkey appSecret:kwechatAppscreat universalLink:@"https://aixi.t4m.cn/"];
    }];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)IMLSDKWillRestoreScene:(MLSDKScene *)scene Restore:(void (^)(BOOL, RestoreStyle))restoreHandler
{
    NSLog(@"Will Restore Scene - Path:%@",scene.path);
//[[MLDTool shareInstance] showAlertWithTitle:nil
//                                    message:@"是否进行场景恢复？"
//                                cancelTitle:@"否"
//                                 otherTitle:@"是"
//                                 clickBlock:^(MLDButtonType type) {
//                                     type == MLDButtonTypeSure ? restoreHandler(YES, Default) : restoreHandler(NO, Default);
//                                 }];
}


@end
