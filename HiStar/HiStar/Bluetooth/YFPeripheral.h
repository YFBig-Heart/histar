//
//  YFPeripheral.h
//  FOXAISpeaker
//
//  Created by 刘云飞 on 2018/4/20.
//  Copyright © 2018年 answer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBluetoothConstant.h"
#import "LGBluetooth.h"



extern NSString * _Nullable const kLeashStateDidUpdateNotification;
extern NSString * _Nonnull const kLeashDeviceEventNotification;
extern NSString * _Nonnull const kFeederDeviceStatusNotification;

@class YFLeashDevModel,YFPeripheral;
@protocol YFPeripheralDelegate <NSObject>
@optional;
// 设备状态更新,更新的数据在devModel里面
- (void)peripheralStateUpdate:(YFPeripheral *_Nullable)per withLeashModel:(YFLeashDevModel *_Nullable)model;
// 通知事件，01 遛狗02 退出遛狗03 标记04 遛狗时间提醒(默认 30 分钟 提醒一次)
- (void)noticeEventPeripheral:(YFPeripheral *_Nonnull)per eventValue:(NSInteger)eventValue;
// 喂食器配网状态更新
- (void)peripheralFeederStateUpdate:(YFPeripheral *_Nullable)per withStauts:(NSInteger)stauts;

@end

@interface YFPeripheral : NSObject

@property (nonatomic, strong) LGPeripheral * _Nonnull lgPeripheral;
@property (nonatomic, assign, readonly) BOOL isDFUMode;        // 待定

@property (nullable, nonatomic, copy) NSString *version;       // 固件版本
@property (nullable, nonatomic, copy) NSString *serverVersion; //服务器上的固件版本
@property (nonatomic,strong)YFLeashDevModel * _Nullable leashStateModel;

@property (nonatomic,weak)id<YFPeripheralDelegate> _Nullable delegate;

@property (nonatomic, assign) BlueOemType oemType;

/** 是否开启自动重连：默认false */
@property (nonatomic, assign)BOOL shouldRetryConnect;
/** 是否连接 */
@property (nonatomic, assign,readonly)BOOL isPeripheralConnected;

/**
 创建设备
 @params peripheral  外设
 */
+(nullable instancetype)creatyfPeri:(nullable LGPeripheral *)peripheral;

/** 连接外设 */
- (void)connectPeripheralCompletion:(BleConnectBlock _Nullable )block;
/** 重连外设 */
- (void)reconnetDeviceCompletion:(BleConnectBlock _Nullable )block;
- (void)reConnectPeripheralUUID:(NSUUID *_Nullable)uuid completion:(BleConnectBlock _Nullable )block;
- (void)disconnectThePeripheralWithCompletionBlock:(BleConnectBlock _Nullable )block;
/*
 设备重连
 uuid:设备的唯一标识
 mac:设备的Mac地址,原因：重连时没走广播，拿不到对应的设备Mac
 */
+ (void)reConnectPeripheralUUID:(NSUUID *_Nullable)uuid mac:(NSString *_Nullable)mac completion:(BleConnectBlock _Nullable )block;

/**
 *operationType == OperationTypeChangeDeviceName
 Parameter object (E.g): @"PetDog"

 *operationType == OperationTypePhoneCallNotice
 Parameter object (E.g): @"0"  1->来电, 0-> 已接听、挂断

 *operationType == OperationTypeSetArgument
 Parameter object (E.g):@{@"walkTheDogTime":@(120),@"ringLightSwitch":@(1),@"ringLightModel":@(kRingLightModelBlink),@"ringLightColor":@(kRingLightRedColor),@"phoneCallNoticeSwitch":(YES)}
 return void
 */
- (void)requestOperationType:(BLEOperationType)operationType parmaObject:(id _Nullable )object progress:(void(^_Nullable)(CGFloat progress))progressBlock completionBlock:(BleHandleCompletionBlock _Nullable )block;

// 不带进度的
- (void)requestOperationType:(BLEOperationType)operationType parmaObject:(id _Nullable )object  completionBlock:(BleHandleCompletionBlock _Nullable )block;


@end
