//
//  YFBTHandler.h
//  Petcome
//
//  Created by petcome on 2019/1/7.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YFCommunicationManager.h"

// 数据解析
@interface YFBTHandler : NSObject

#pragma mark - private method
+ (void)handleVerifyData:(id)data peripheral:(YFPeripheral *)peripheral withCompletion:(BleHandleCompletionBlock)block;
#pragma mark - 一些数据计算
+ (NSString *)toValueStringWithValue:(u_int8_t)value;

+ (Byte)getFunctionCodeByOperationType:(BLEOperationType)operationType oemType:(BlueOemType)oemType;

// 获取校验和-所有数累计相加, 取模 0XFF 之后与 0XEE 相与
+ (Byte)modulusValue:(Byte *)bytes countOfBytes:(NSInteger)size;

/**
 计算Mac地址，
 range: mac 地址取值区间
 reversal: 是否需要对取值后的数据到序
 */
+ (NSString *)convertNewMacAddressWithData:(NSData *)data range:(NSRange)range isReversal:(BOOL)reversal;


@end


