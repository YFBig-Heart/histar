//
//  YFPeripheral.m
//  FOXAISpeaker
//
//  Created by 刘云飞 on 2018/4/20.
//  Copyright © 2018年 answer. All rights reserved.
//
#import "YFPeripheral.h"
#import "YFBTHandler.h"
#import "YFCommunicationManager.h"
#import "YFBluetoothConstant.h"
#import "YFLeashDevModel.h"
#import <YYKit/YYKit.h>

NSString * const kLeashStateDidUpdateNotification = @"kLeashStateDidUpdateNotification";
NSString * const kLeashDeviceEventNotification = @"kLeashDeviceEventNotification";

NSString * const kFeederDeviceStatusNotification = @"kFeederDeviceStatusNotification";

@interface YFPeripheral()

@property (nonatomic, strong) LGCharacteristic *deviceNewWriteCharacterstic;
@property (nonatomic, strong) LGCharacteristic *deviceNewNotifyCharacterstic;
@property (nonatomic, strong) LGCharacteristic *deviceNewReadCharacterstic;
@property (nonatomic, strong) LGCharacteristic *dfuMacReadCharacterstic;
@property (nonatomic, strong) LGCharacteristic *dfuControlPointCharacterstic;

@property (nonatomic, copy) BleConnectBlock connectBlock;
@property (nonatomic, copy) OperationBlock operationBlock;
/** 当前操作的回调 */
@property (nonatomic, copy)BleHandleCompletionBlock currenthandleBlock;

@end
@implementation YFPeripheral {
     NSMutableArray *audioSetNetworkDataArr; // 音响蓝牙配网返回的数据
}

- (instancetype)init {
    if (self = [super init]) {
        audioSetNetworkDataArr = @[].mutableCopy;
    }
    return self;
}
+(nullable instancetype)creatyfPeri:(nullable LGPeripheral *)peripheral {
    YFPeripheral *model = [[YFPeripheral alloc] init];
    model.lgPeripheral = peripheral;
    return model;
}
- (YFLeashDevModel *)leashStateModel {
    if (!_leashStateModel) {
        _leashStateModel = [YFLeashDevModel leashDevModelWithMac:self.lgPeripheral.macAddress];
        BOOL isNeedSav = NO;
        if (_leashStateModel.yfName.length <= 0) {
            _leashStateModel.yfName = self.lgPeripheral.name;
            isNeedSav = YES;
        }
        if(_leashStateModel.devUuid.length <= 0){
            _leashStateModel.devUuid = self.lgPeripheral.UUIDString;
            isNeedSav = YES;
        }
        if (isNeedSav) {
            [_leashStateModel bg_saveOrUpdate];
        }
    }
    return _leashStateModel;
}

- (BOOL)isPeripheralConnected {
    return self.lgPeripheral.isConnected;
}
- (BOOL)isDFUMode {
    return (self.dfuMacReadCharacterstic || self.dfuControlPointCharacterstic);
}

- (void)reConnectPeripheralUUID:(NSUUID *)uuid completion:(BleConnectBlock)block {
    if (uuid == nil) {
        if (block){
            block(NO,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleNotFoundUUIDErrorCode]);
        }
        return;
    }
    LGPeripheral * peripheral = [[[LGCentralManager sharedInstance] retrievePeripheralsWithIdentifiers:@[uuid]] lastObject];
    __weak YFPeripheral *weakSelf = self;
    [peripheral connectWithTimeout:kConnectTimeOut completion:^(NSError *error) {
        if (!error) {
            [[YFCommunicationManager shareInstance] addOrUpdatePeripheral:self];
            [YFCommunicationManager shareInstance].yfPeripheral = self;
            [weakSelf setupServicesWithPeripheral:self.lgPeripheral completion:block];
        }else{
            block(NO,error);
        }
    }];
}
- (void)reconnetDeviceCompletion:(BleConnectBlock)block {
    NSString *UUIDString = self.lgPeripheral.UUIDString;
    if (UUIDString.length <= 0) {
        if (block){
            block(NO,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleNotFoundUUIDErrorCode]);
        }
        return;
    }
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:UUIDString];
    [self reConnectPeripheralUUID:uuid completion:block];
}
- (void)disconnectThePeripheralWithCompletionBlock:(BleConnectBlock)block {
    if (self.lgPeripheral.isConnected) {
        [self.lgPeripheral disconnectWithCompletion:^(NSError *error) {
            if (!error) {
                block(YES,nil);
                [[NSNotificationCenter defaultCenter] postNotificationName:kLGPeripheralDidDisconnect object:nil];
            }
        }];
    } else {
        block(YES,nil);
    }
}
/*
 设备重连
 uuid:设备的唯一标识
 mac:设备的Mac地址,原因：重连时没走广播，拿不到对应的设备Mac
 */
+ (void)reConnectPeripheralUUID:(NSUUID *_Nullable)uuid mac:(NSString *)mac completion:(BleConnectBlock _Nullable )block {
    if (uuid == nil || mac.length <= 0) {
        if (block){
            block(NO,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleNotFoundUUIDErrorCode]);
        }
        return;
    }
    LGPeripheral * peripheral = [[[LGCentralManager sharedInstance] retrievePeripheralsWithIdentifiers:@[uuid]] lastObject];
    peripheral.macAddress = mac;
    [peripheral connectWithTimeout:kConnectTimeOut completion:^(NSError *error) {
        if (!error) {
            YFPeripheral *per = [YFPeripheral creatyfPeri:peripheral];
            [YFCommunicationManager shareInstance].yfPeripheral = per;
            [[YFCommunicationManager shareInstance] addOrUpdatePeripheral:per];
            [per setupServicesWithPeripheral:per.lgPeripheral completion:block];
        }else{
            block(NO,error);
        }
    }];
}

- (void)connectPeripheralCompletion:(BleConnectBlock _Nullable )block {
    self.connectBlock = block;
    __weak YFPeripheral *weakSelf = self;
    [self.lgPeripheral connectWithTimeout:kConnectTimeOut completion:^(NSError *error) {
        if (!error) {
            weakSelf.shouldRetryConnect = YES;
            [[YFCommunicationManager shareInstance] addOrUpdatePeripheral:self];
            [YFCommunicationManager shareInstance].yfPeripheral = self;
            [weakSelf setupServicesWithPeripheral:weakSelf.lgPeripheral completion:block];
        } else {
            block(NO,error);
        }
    }];
}
- (void)defaultCharacteristicSetting {
    self.deviceNewNotifyCharacterstic = nil;
    self.deviceNewWriteCharacterstic = nil;
    self.deviceNewReadCharacterstic = nil;
    self.dfuMacReadCharacterstic = nil;
    self.dfuControlPointCharacterstic = nil;
}

- (void)checkIfAllReady {
    if (self.deviceNewNotifyCharacterstic && self.deviceNewWriteCharacterstic  ) {
        if (self.connectBlock) {
            self.connectBlock(YES,nil);
            self.connectBlock = nil;
        }
        [[YFCommunicationManager shareInstance] stopScanningPeripherals];
        return;
    } else if (self.dfuMacReadCharacterstic && self.dfuControlPointCharacterstic) {
        if (self.connectBlock) {
            self.connectBlock(YES,nil);
            self.connectBlock = nil;
        }
        return;
    }
}

- (void)setupServicesWithPeripheral:(LGPeripheral *)peripheral completion:(BleConnectBlock)block {
    self.connectBlock = block;
    [self defaultCharacteristicSetting];
    self.lgPeripheral = peripheral;
    __weak YFPeripheral *weakSelf = self;
    // 设备连接成功将设备保存起来
    [peripheral discoverServicesWithCompletion:^(NSArray *services, NSError *error) {
        for (LGService *service in services) {
            NSString *servieUUIDString = [service.UUIDString lowercaseString];
            NSArray *servieArray = @[[kStarServiceUUIDLength lowercaseString],];
            // kStarServiceUUID180a,kStarServiceUUIDff00,kStarServiceUUI180f
            if ([servieArray containsObject:servieUUIDString]) {
                NSLog(@"serve uuidString:%@", service.UUIDString);
                [service discoverCharacteristicsWithCompletion:^(NSArray *characteristics, NSError *error) {
                    NSString *readUUid = nil;
                    NSString *wuuid = nil;
                    NSString *nuuid = nil;
                    for (LGCharacteristic *characteristic in characteristics) {
                        NSString *characterUuid = [characteristic.UUIDString lowercaseString];
                        if ([characterUuid isEqualToString:[kStarWriteCharacteristic lowercaseString]]) {
                            weakSelf.deviceNewWriteCharacterstic = characteristic;
                            [weakSelf checkIfAllReady];
                            wuuid = characteristic.UUIDString;
                        }
                        if ([characterUuid isEqualToString:[kStarNotifyCharacteristic lowercaseString]]) {
                            nuuid = characteristic.UUIDString;
                            weakSelf.deviceNewNotifyCharacterstic = characteristic;
                            [weakSelf checkIfAllReady];
                            [characteristic setNotifyValue:YES completion:nil onUpdate:^(NSData *data, NSError *error) {
                                NSLog(@"NotifyValue:%@-----%@",data,error);
                                [weakSelf handleData:data];
                            }];
                        }
                        
                        if ([[characteristic.UUIDString lowercaseString] isEqualToString:[kNewReadCharacteristic lowercaseString]]) {
                            self.deviceNewReadCharacterstic = characteristic;
                            readUUid = characteristic.UUIDString;
                            [characteristic readValueWithBlock:^(NSData *data, NSError *error) {
                                if (!error) {
                                    [YFBTHandler convertNewMacAddressWithData:data range:NSMakeRange(0, data.length) isReversal:YES];
                                    NSLog(@"mac data:%@", peripheral.macAddress);
                                    [self checkIfAllReady];
                                }
                            }];
                        }
                    }
                }];
                
            } else if ([service.UUIDString isEqualToString:dfuServiceUUIDString]) {
                [service discoverCharacteristicsWithCompletion:^(NSArray *characteristics, NSError *error) {
                    for (LGCharacteristic *characteristic in characteristics) {
                        NSLog(@"characteristic:%@", characteristic.UUIDString);
                        if ([characteristic.UUIDString isEqualToString:dfuMacReadUUIDString]) {
                            weakSelf.dfuMacReadCharacterstic = characteristic;
                            [weakSelf checkIfAllReady];
                        } else if ([characteristic.UUIDString isEqualToString:dfuControlPointCharacteristicUUIDString]) {
                            weakSelf.dfuControlPointCharacterstic = characteristic;
                            [weakSelf checkIfAllReady];
                        }
                    }
                }];
            }
        }
    }];
}

// 处理球拍返回的数据
- (void)handleData:(NSData *)data {
    NSLog(@"handle data:%@", data);
    [YFBTHandler handleVerifyData:data peripheral:self withCompletion:^(BOOL success, id response, NSError *error) {
        if (success) {
//            const u_int8_t *bytes = [data bytes];
//            Byte functionByte = bytes[2];
            if (self.oemType == BlueOemTypeHiStar) {
                self.operationBlock(YES, response, error);
                self.operationBlock = nil;
            }
        }
    }];
}
#pragma mark - 发送操作指令
// 不带进度的
- (void)requestOperationType:(BLEOperationType)operationType parmaObject:(id _Nullable )object  completionBlock:(BleHandleCompletionBlock _Nullable )block {
    [self requestOperationType:operationType parmaObject:object progress:nil completionBlock:block];
}

// 外部总方法
- (void)requestOperationType:(BLEOperationType)operationType parmaObject:(id _Nullable )object progress:(void(^_Nullable)(CGFloat progress))progressBlock completionBlock:(BleHandleCompletionBlock _Nullable )block {

    NSError *error = nil;
    NSArray *dataArr = [self dataWithOperationType:operationType object:object error:&error];
    for (int i = 0; i<dataArr.count; i++) {
        if (error != nil) {
            if (block) {
                block(NO, nil, [YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleOperationCommandOrParameterIsIncorrect]);
            }
            return;
        }
        NSData *data = [dataArr objectAtIndex:i];
        NSLog(@"bluewrite:%@--%lu",data,(unsigned long)operationType);
        [self performPeripheralOperationType:operationType Data:data completionBlock:^(BOOL success, id feedbackData,NSError *error) {
            if (progressBlock) {
                progressBlock((i*1.0)/dataArr.count);
            }
            if (operationType == BLEOperationTypeDwonload) {
                if (success) {
                    if ([feedbackData length] > 0) {
                        const u_int8_t *bytes = [feedbackData bytes];
                        if (bytes[0] == 0x06) {
                            // 成功
                            if (block) {
                                block(YES,feedbackData,nil);
                            }
                        }else if (bytes[0] == 0x38){
                            // 失败
                            if (block) {
                                block(NO,feedbackData,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleSensorNoDownloadStatusError]);
                            }
                        }
                    }
                }else {
                    block(NO,feedbackData,error);
                }
            }else if (operationType == BLEOperationTypeDwonloadData){
                if (success) {
                    const u_int8_t *bytes = [feedbackData bytes];
                    if (bytes[0] == 0x26) {
                        // 成功
                        if (block) {
                            block(YES,feedbackData,nil);
                        }
                    }else if (bytes[0] == 0x08){
                        // 失败
                        if (block) {
                            block(NO,feedbackData,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleSensorNoDownloadStatusError]);
                        }
                    }
                }else {
                    block(NO,feedbackData,error);
                }

            }else {
                if (block) {
                    block(success,feedbackData,nil);
                }
            }
        }];
    }

}
- (void)performPeripheralOperationType:(BLEOperationType)operationType Data:(NSData *)data completionBlock:(OperationBlock)block {
    [self.deviceNewWriteCharacterstic writeValue:data completion:^(NSError *error) {
        NSLog(@"error:%@",error);
        self.operationBlock = block;
        if (error) {
            NSLog(@"发送特征值数据:%@",data);
            if (self.operationBlock) {
                self.operationBlock(NO, nil, error);
            }
        }else {
            if (operationType == BLEOperationTypeDwonload || operationType == BLEOperationTypeDwonloadData) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (self.operationBlock) {
                        self.operationBlock(NO,nil,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleSensorNoDownloadStatusError]);
                        self.operationBlock = nil;
                    }
                });
            }
        }
    }];
}

- (Byte)getFunctionCodeByOperationType:(BLEOperationType)operationType {
    Byte functionByte = [YFBTHandler getFunctionCodeByOperationType:operationType oemType:self.oemType];
    return functionByte;
}
- (NSArray <NSData *> *)dataWithOperationType:(BLEOperationType)operationType object:(id)object error:(NSError **)error {
    Byte functionByte = [YFBTHandler getFunctionCodeByOperationType:operationType oemType:BlueOemTypeHiStar];
    switch (operationType) {
        case BLEOperationTypeDwonload:{
            Byte bytes[10] = {0x2c, 0x00,0xde, 0xad, 0xbe, 0xef,0xff,0x1b, 0xb4, 0x00};
            bytes[9]=functionByte;
            NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
            if (data == nil) {
                *error = [YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleOperationCommandOrParameterIsIncorrect];
            }
            return @[data];
        }
        case BLEOperationTypeDwonloadData:{
            if ([object isKindOfClass:[NSData class]]) {
                NSData *grameData = object;
                NSInteger count  = ((grameData.length + 20)/20);
                NSMutableArray *arrM = [NSMutableArray arrayWithCapacity:count];
                // 将数据分包
                for (int i = 0; i < count; i++) {
                    if (i == count-1) {
                        [arrM addObject:[grameData subdataWithRange:NSMakeRange(i*20, grameData.length-i*20)]];
                    }else {
                        [arrM addObject:[grameData subdataWithRange:NSMakeRange(i*20, 20)]];
                    }
                }
                return arrM.copy;
            }else {
                return @[];
            }
        }
            break;
        default:{
            Byte bytes[10] = {0x2c, 0x00,0xde, 0xad, 0xbe, 0xef,0xff,0x1b, 0xc8, 0x00};
            bytes[9]=functionByte;
            NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
            if (data == nil) {
                *error = [YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleOperationCommandOrParameterIsIncorrect];
            }
            return @[data];
        }
    }
}

// 发送多包数据
- (NSArray <NSData *> *)multiDataWithOperationType:(BLEOperationType)operationType object:(id)object {
    if (operationType == BLEOperationTypeDwonload && [object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dict = (NSDictionary *)object;
        NSString *ssid = dict[@"ssid"];
        NSString *wifiPwd = [dict objectForKey:@"wifiPwd"];
        NSData *ssidData = [ssid dataUsingEncoding:NSUTF8StringEncoding];
        NSData *wifiPwdData = [wifiPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableData *totalData = [NSMutableData dataWithData:ssidData];
        Byte empty[] = {0x00};
        [totalData appendBytes:empty length:1];
        [totalData appendData:wifiPwdData];
        NSInteger matchLength = 14; //单包最大长度
        const u_int8_t *totalDatabytes = [totalData bytes];
        NSMutableArray *arrM = [NSMutableArray array];
        for (int i = 0; i<(totalData.length+13)/matchLength; i++) {
            Byte bytes[20];
            bytes[0] = 0xaa;
            bytes[1] = 0xaa;
            bytes[2] = [self getFunctionCodeByOperationType:operationType];
            bytes[3] = (Byte)totalData.length;
            bytes[4] = (Byte)i;
            for (int j = 0; j<matchLength; j++) {
                if (j+i*matchLength < totalData.length) {
                    bytes[j+5] = totalDatabytes[j+i*matchLength];
                }else {
                    bytes[j+5] = 0x00;
                }
            }
            bytes[19] = [YFBTHandler modulusValue:bytes countOfBytes:sizeof(bytes)];
            NSString *temp = @"";
            for (int i = 0; i< 20; i++) {
               temp = [temp stringByAppendingFormat:@"%d ",(int)bytes[i]];
            }
            NSLog(@"%@",temp);
            NSData *dataT = [NSData dataWithBytes:bytes length:sizeof(bytes)];
            if (dataT) {
                [arrM addObject:dataT];
            }
        }
        return arrM.copy;
    }else {
        return @[];
    }
}


- (BOOL)checkModifyNameLength:(NSString *)name
{
    NSData *nameData = [name dataUsingEncoding:NSUTF8StringEncoding];
    if (nameData.length > 15) {
        return NO;
    }
    return YES;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"yf_peripheralDealloc");
}

@end
