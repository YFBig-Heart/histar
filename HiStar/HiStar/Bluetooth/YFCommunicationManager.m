//
//  YFCommunicationManager.m
//  FOXAISpeaker
//
//  Created by 刘云飞 on 2018/4/20.
//  Copyright © 2018年 answer. All rights reserved.
//

#import "YFCommunicationManager.h"
#import "YFBTHandler.h"
#import "YFLeashDevModel.h"

@interface YFCommunicationManager()

@property (nonatomic, strong) LGCharacteristic *deviceNewWriteCharacterstic;
@property (nonatomic, strong) LGCharacteristic *deviceNewNotifyCharacterstic;
@property (nonatomic, strong) LGCharacteristic *deviceNewReadCharacterstic;
@property (nonatomic, strong) LGCharacteristic *dfuMacReadCharacterstic;
@property (nonatomic, strong) LGCharacteristic *dfuControlPointCharacterstic;

@property (nonatomic,strong)NSMutableArray *scannedPeripherals;
@property (copy, nonatomic) DiscoverPerpheralsCallback scanBlock;

@property (nonatomic, strong) NSMutableArray *connetPeripheralArr;

@end
@implementation YFCommunicationManager {
    dispatch_source_t _timer; // GCD定时器
    dispatch_source_t _timerTemp; // GCD定时器
    NSInteger _tempOutTime; // 开启临时连接倒计时
}

- (instancetype)init{
    if (self = [super init]){
        _scannedPeripherals = [NSMutableArray new];
        _shouldRetryConnect = YES;
    }
    return self;
}
+ (void)initialize {
    // 开启蓝牙中心，不然第一次搜索不到蓝牙
    [[YFCommunicationManager shareInstance] lgCenterManager];
}
+ (instancetype)shareInstance {
    static id shareInstsance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstsance = [[self alloc] init];
    });
    return shareInstsance;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (LGCentralManager *)lgCenterManager {
    return [LGCentralManager sharedInstance];
}
- (BOOL)isCentralReady {
    return [LGCentralManager sharedInstance].isCentralReady;
}
#pragma mark - 搜索设备
- (void)startScanPeripheralsWithBlock:(DiscoverPerpheralsCallback)scanBlock {
    if (![self isCentralReady]) {
        if (scanBlock) {
            scanBlock(nil,[YFCommunicationManager errorDomainAndInfoWithErrorCode:kYFBleCentralNotReadyErrorCode]);
        }
        return;
    }
     __weak typeof(self) weakSelf = self;
    [[LGCentralManager sharedInstance] setFilterScanPeripherals:^BOOL(NSString *perName,NSDictionary *iadData, NSNumber *rssi) {
        return [weakSelf filterOEMIDAndOEMDataWithAdvertisementData:iadData];
    }];
    [self.scannedPeripherals removeAllObjects];
    self.scanBlock = scanBlock;
    [[LGCentralManager sharedInstance] scanForPeripheralsWithScanBlock:^(NSArray *peripherals) {
        [peripherals enumerateObjectsUsingBlock:^(LGPeripheral *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self wrapperByPeripheral:obj];
        }];
    }];
}
- (YFPeripheral *)wrapperByPeripheral:(LGPeripheral *)lgPeripheral
{
    YFPeripheral *wrapper = nil;
    for (YFPeripheral *scanned in self.scannedPeripherals) {
        if ([scanned.lgPeripheral.UUIDString isEqualToString:lgPeripheral.UUIDString]) {
            wrapper = scanned;
            break;
        }
    }
    if (!wrapper) {
        wrapper = [YFPeripheral creatyfPeri:lgPeripheral];
        [self setPer:wrapper AdvertisementData:wrapper.lgPeripheral.advertisingData];
        [self.scannedPeripherals addObject:wrapper];
    }
    if (self.scanBlock) {
        self.scanBlock(self.scannedPeripherals, nil);
    }
    return wrapper;
}

- (void)refreshScanPerpherals {
    // 间隔1秒
    GCDAfter(1, ^{
        if (self.lgCenterManager.isScanning) {
            if (self.scanBlock) {
                self.scanBlock(self.scannedPeripherals, nil);
            }
        }
    });
}

- (BOOL)filterOEMIDAndOEMDataWithAdvertisementData:(NSDictionary *)advertisementData
{
    NSString *perName = [[advertisementData objectForKey:CBAdvertisementDataLocalNameKey] lowercaseString];
    if ([[perName lowercaseString] containsString:@"histar"] || [[perName lowercaseString] containsString:@"hibot"]) {
        return YES;
    }
    return NO;
}
- (void)setPer:(YFPeripheral *)per AdvertisementData:(NSDictionary *)advertisementData {
    NSArray *advSerivceArr = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
    CBUUID *serviceUUid = nil;
    if (advSerivceArr) {
        serviceUUid = [advSerivceArr firstObject];
    }
#warning 暂时全部设置成小海星
    per.oemType = BlueOemTypeHiStar;
    // 设置oemType
    if (serviceUUid && [serviceUUid.UUIDString containsString:kStarAdvertisementDataServiceUUIDsKey]){
        per.oemType = BlueOemTypeHiStar;
        // 读取广播中的Mac值
        // 66000215 d5d3e710 144a 4d9f 89e7bf91 00000000 01020304 c3>
        NSData * manufacturerData = advertisementData[CBAdvertisementDataManufacturerDataKey];
        NSString *mac = [YFBTHandler convertNewMacAddressWithData:manufacturerData range:NSMakeRange(4, 6) isReversal:NO];
        per.lgPeripheral.macAddress = mac;
    }
}
- (void)stopScanningPeripherals {
    [[LGCentralManager sharedInstance] stopScanForPeripherals];
    if (self.scanBlock) {
        self.scanBlock(self.scannedPeripherals,nil);
    }
    self.scanBlock = nil;
}
#pragma mark - error
+ (NSError *)errorDomainAndInfoWithErrorCode:(kYFBleErrorCode)code {
    switch (code) {
        case kYFBleCentralNotReadyErrorCode:
            return [NSError errorWithDomain:@"BLENotReadyErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"Please turn on bluetooth and allow connection"}];
        case kYFBleNoServieErrorCode:
            return [NSError errorWithDomain:@"BLENoServieErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"No Servie"}];
        case kYFBleTimeOutErrorCode:
            return [NSError errorWithDomain:@"BLETimeOutErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"Time Out"}];
        case kYFBleNotFoundUUIDErrorCode:
            return [NSError errorWithDomain:@"BLENotFoundUUIDErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"Not found UUID"}];
        case kYFBleDeviceNameLengthMoreThan17Char:
            return [NSError errorWithDomain:@"BLEDeviceNameLengthMoreThan17CharErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"The input device name is too long"}];
        case kYFBleOperationCommandOrParameterIsIncorrect:
            return [NSError errorWithDomain:@"BLEOpcodeOrParameterIsIncorrectErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"Bluetooth opcode or parameter is incorrect"}];
        case kYFBleSensorResponseDataError:
            return [NSError errorWithDomain:@"BLESensorResponseDataErrorDomain" code:code userInfo:@{NSLocalizedDescriptionKey:@"BLE Sensor response data error,or response data equle null"}];
        case kYFBleSensorNoDownloadStatusError:
            return [NSError errorWithDomain:@"BleSensorNoDownloadStatusError" code:code userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Response timeout, please let the HIStar into the download mode", nil)}];
        case kYFBleSensorDataDownloadError:
            return [NSError errorWithDomain:@"BleSensorDataDownloadError" code:code userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Download failed, please try again", nil)}];
        default:
            break;
    }
}

- (NSArray <YFPeripheral *>*)getPeripheralWithOemType:(BlueOemType)oemType {
    NSMutableArray *arrM = nil;
    for (YFPeripheral *peripheal in self.connetPeripheralArr.reverseObjectEnumerator) {
        if (peripheal.oemType == oemType && peripheal.isPeripheralConnected) {
            if (arrM) {
                [arrM addObject:peripheal];
            }else {
                arrM = [NSMutableArray arrayWithObject:peripheal];
            }
        }
    }
    return arrM.copy;
}
- (void)addOrUpdatePeripheral:(YFPeripheral *)peripheral {
    if (peripheral == nil) {
        return;
    }
    BOOL isInclude = NO;
    NSMutableArray *array = [NSMutableArray array];
    for (YFPeripheral *per in self.connetPeripheralArr) {
        if ([per.lgPeripheral.UUIDString isEqualToString:peripheral.lgPeripheral.UUIDString]) {
            isInclude = YES;
            [self.connetPeripheralArr replaceObjectAtIndex:[self.connetPeripheralArr indexOfObject:per] withObject:peripheral];
            break;
        }
        if (per.isPeripheralConnected) {
            [array addObject:per];
        }
    }
    if (isInclude == NO) {
        [self.connetPeripheralArr addObject:peripheral];
    }
}
// 在绑定的设备中根据Mac找到对应的 设备
- (YFPeripheral *)getPeripheralWithMac:(NSString *)addressMac {
    for (YFPeripheral *peripheal in self.connetPeripheralArr) {
        if ([peripheal.lgPeripheral.macAddress isEqualToString:addressMac]) {
            return peripheal;
        }
    }
    return nil;
}

- (YFPeripheral *)getConnectPheriperWithLGPer:(LGPeripheral *)lgPer {
    for (YFPeripheral *peripheal in self.connetPeripheralArr) {
        if ([peripheal.lgPeripheral.UUIDString isEqualToString:lgPer.UUIDString]) {
            return peripheal;
        }
    }
    return nil;
}
- (NSMutableArray *)connetPeripheralArr {
    if (_connetPeripheralArr == nil) {
        _connetPeripheralArr = [NSMutableArray array];
    }
    return _connetPeripheralArr;
}

#pragma mark - 扫描自动重连
- (void)scanAutoReconnectWithArray:(NSArray <NSString *>*)devMacArr {
    [[YFCommunicationManager shareInstance] startScanPeripheralsWithBlock:^(NSArray<YFPeripheral *> *peripherals, NSError *error) {
        for (YFPeripheral *per in peripherals) {
            if ([devMacArr containsObject:per.lgPeripheral.macAddress]) {
                YFLeashDevModel *model = [YFLeashDevModel leashDevModelWithMac:per.lgPeripheral.macAddress];
                model.devUuid = per.lgPeripheral.UUIDString;
                [model bg_saveOrUpdate];
            }
        }
    }];
}

@end
