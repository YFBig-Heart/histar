//
//  YFBluetoothConstant.h
//  Petcome
//
//  Created by petcome on 2019/1/8.
//  Copyright © 2019 yunfei. All rights reserved.
//

#ifndef YFBluetoothConstant_h
#define YFBluetoothConstant_h

typedef NS_ENUM(NSInteger, kYFBleErrorCode) {
    kYFBleCentralNotReadyErrorCode = -3001,
    kYFBleNoServieErrorCode = -3002,
    kYFBleTimeOutErrorCode  = -3003,
    kYFBleNotFoundUUIDErrorCode = -3004,
    // The name is too long
    kYFBleDeviceNameLengthMoreThan17Char = -3005,
    // Operation command or parameter is incorrect
    kYFBleOperationCommandOrParameterIsIncorrect = -3006,
    // BLE Sensor response data error,or response data equle null
    kYFBleSensorResponseDataError = -3007,
    kYFBleSensorNoDownloadStatusError = -3008, // 设备没有进入下载状态
    kYFBleSensorDataDownloadError = -3009,    //程序数据下载失败
};

typedef NS_ENUM(NSUInteger, kRingLightModel) {
    kRingLightModelGradient = 0,       // 渐变
    kRingLightModelBlink = 1,              // 闪烁
    kRingLightModelAlwayLight = 2,         // 常亮
    kRingLightModelMulticolor = 3,         // 多色变化 (默认模式)
};

typedef NS_ENUM(NSUInteger, kRingLightColor) {
    kRingLightWhiteColor = 0, // 白色
    kRingLightRedColor = 1,   // 红色
    kRingLightGreenColor = 2, // 绿色
    kRingLightBlueColor = 3,  // 蓝色
    kRingLightPurpleColor = 4,// 紫色
    kRingLightYellowColor = 5,// 黄色
    kRingLightCyanColor = 6,  // 青色
};


#pragma mark - 设备常规操作
typedef NS_ENUM(NSUInteger, BLEOperationType) {
    
    BLEOperationTypeGoUp, // 前进
    BLEOperationTypeGodown, //后退
    BLEOperationTypeTurnLeft,// 左转
    BLEOperationTypeTurnRight, //右转
    BLEOperationTypeGoUpLeft,  // 左前方
    BLEOperationTypeGoUpRight, // 右前方
    BLEOperationTypeGoDownLeft, // 左后方
    BLEOperationTypeGoDownRight, // 右后方
    BLEOperationTypeRightRemote, // 右旋转
    BLEOperationTypeOpenLeftBlueLight, //左蓝亮
    BLEOperationTypeCloseLeftBlueLight, //左蓝灭
    BLEOperationTypeOpenRightRedLight, // 右红亮
    BLEOperationTypeCloseRightRedLight, // 右红灭
    BLEOperationTypeOpenSound, // 鸣笛
    BLEOperationTypeBiZhang, // 开启避障模式
    BLEOperationTypeXunXian, //s巡线模式
    BLEOperationTypeFollowlight,// 追光模式
    BLEOperationTypeApplause, // 声控
    BLEOperationTypeSumo,// 相扑模式
    BLEOperationTypeDance,// 跳舞
    BLEOperationTypeYaokong,// 遥控模式
    BLEOperationTypeCrawl, // 围栏模式
    BLEOperationTypeStopModel,//停止模式
    BLEOperationTypeAccelerate, //加速
    BLEOperationTypeStopAccelerate, //停止加速
    
    
    BLEOperationTypeGetVerison,// 获取设备版本
    BLEOperationTypeDwonload,// 下载指令,预备指令
    BLEOperationTypeDwonloadData,// 下载的真实数据
    
};


#define kConnectTimeOut 10 // 连接超时时间

// 牵引绳广播/服务 UUID 以此判断是否为牵引绳(广播和服务需包含)
static NSString * const kStarAdvertisementDataServiceUUIDsKey = @"8611-3253-013717033460";

static NSString * const kStarServiceUUID180a = @"180a";
static NSString * const kStarServiceUUIDff00 = @"FF00";
static NSString * const kStarServiceUUI180f = @"180f";

static NSString * const kStarServiceUUIDLength = @"f8c00001-159f-11e6-92f5-0002a5d5c51b";
// APP 下发至牵引绳硬件设备 写 特征
static NSString * const kStarWriteCharacteristic = @"f8c00002-159f-11e6-92f5-0002a5d5c51b";
// 小星设备上传至 APP 消息 特征
static NSString * const kStarNotifyCharacteristic = @"f8c00003-159f-11e6-92f5-0002a5d5c51b";


// APP 读取设备信息 读 特征 -- 暂时没有
static NSString * const kNewReadCharacteristic = @"0004";
#pragma mark - DFU ---- 暂时没有
static NSString * const dfuServiceUUIDString = @"FFFFFFFF";
static NSString * const dfuControlPointCharacteristicUUIDString = @"FFFFFFFF";
static NSString * const dfuMacReadUUIDString = @"FFFFFFFF";


@class YFPeripheral;
typedef void(^DiscoverPerpheralsCallback)(NSArray <YFPeripheral *>*peripherals,NSError *error);
typedef void(^OperationBlock)(BOOL success, id feedbackData,NSError *error);
typedef void(^BleConnectBlock)(BOOL success,NSError *error);

typedef void(^BleHandleCompletionBlock)(BOOL success,id response,NSError *error);
typedef void(^BleCompletionBlock)(BOOL success,NSError *error);

#endif /* YFBluetoothConstant_h */
