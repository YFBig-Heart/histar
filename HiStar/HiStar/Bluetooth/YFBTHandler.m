//
//  YFBTHandler.m
//  Petcome
//
//  Created by petcome on 2019/1/7.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFBTHandler.h"


@implementation YFBTHandler

+ (void)handleVerifyData:(id)data peripheral:(YFPeripheral *)peripheral withCompletion:(BleHandleCompletionBlock)block {
//    const u_int8_t *bytes = [data bytes];
//    Byte functionByte = bytes[2];
    if (peripheral.oemType == BlueOemTypeHiStar) {
        block(YES,data,nil);
    }
}
+ (id)handleLeashStateUpdateData:(NSData *)data {
    const u_int8_t *bytes = [data bytes];
    Byte functionByte = bytes[2];
    if (functionByte == 0x01 || (functionByte >= 0x80)) {
        // 状态更新
        int battery = (int)bytes[3];// 1-100% 200 为充电
        BOOL iswalkingDog = bytes[4]; // 当前状态，0待机，1遛狗
        int walkDogTime = bytes[5] * 0xff + bytes[6];//遛狗时长, 0~1200,单位分钟
        int ringlightSwitch = (int)bytes[7];
        kRingLightModel lightModel = (int)bytes[8];
        kRingLightColor lightColor = (int)bytes[9];
        BOOL phoneCallNotice = (int)bytes[10];
        int lightBrightness = (int)bytes[11]; // 1~5 五个档位
        int openLightHourTime = (int)bytes[12]; //夜间环灯，开始时间(小时)
        int openLightMinuteTime = (int)bytes[13];//夜间环灯，开始时间(分钟)

        int stopLightHourTime = (int)bytes[14];//夜间环灯，停止时间(小时)
        int stopLightMinuteTime = (int)bytes[15];//夜间环灯，停止时间(分钟)

        int version = (int)bytes[16]; // 版本信息 01~FF

        int devCurrentHour = (int)bytes[17];
        int devCurrentMinute = (int)bytes[18];
        NSDictionary *object = @{@"walkTheDogTime":@(walkDogTime),
                                 @"ringLightSwitch":@(ringlightSwitch),
                                 @"ringLightModel":@(lightModel),
                                 @"ringLightColor":@(lightColor),
                                 @"phoneCallNotice":@(phoneCallNotice),
                                 @"battery":@(battery),
                                 @"isWalkingDog":@(iswalkingDog),
                                 @"lightBrightness":@(lightBrightness),
                                 @"openLightHourTime":@(openLightHourTime),
                                 @"openLightMinuteTime":@(openLightMinuteTime),
                                 @"stopLightHourTime":@(stopLightHourTime),
                                 @"stopLightMinuteTime":@(stopLightMinuteTime),
                                 @"version":@(version),
                                 @"devCurrentHour":@(devCurrentHour),
                                 @"devCurrentMinute":@(devCurrentMinute)};
        return object;
    }else {
        return nil;
    }
}

+ (void)handleLeashStateUpdateData:(NSMutableArray <NSData *>*)datas completed:(void(^)(NSDictionary *response))block {
    if (![datas isKindOfClass:[NSArray class]]) {
        block(nil);
        return;
    }
}
+ (NSString *)handleFeederConfigWifiReponseData:(NSData *)data {
    const u_int8_t *bytes = [data bytes];
    Byte functionByte = bytes[2];
    if (functionByte == 0x01 &&  data.length == 20) {
        return [YFBTHandler convertNewMacAddressWithData:data range:NSMakeRange(3, 8) isReversal:NO];
    }else {
        return nil;
    }
}

#pragma mark - 一些数据计算
+ (NSString *)toValueStringWithValue:(u_int8_t)value {
    switch (value) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            return [@(value) stringValue];
        case 10:
            return @"A";
        case 11:
            return @"B";
        case 12:
            return @"C";
        case 13:
            return @"D";
        case 14:
            return @"E";
        case 15:
            return @"F";
        default:
            return @"";

    }
}

+ (Byte)getFunctionCodeByOperationType:(BLEOperationType)operationType oemType:(BlueOemType)oemType {
    Byte functionByte = '\0';
    switch (operationType) {
        case BLEOperationTypeGoUp:
            functionByte = 0x01;
            break;
        case BLEOperationTypeGodown:
            functionByte = 0x02;
            break;
        case BLEOperationTypeTurnLeft:
            functionByte = 0x03;
            break;
        case BLEOperationTypeTurnRight:
            functionByte = 0x04;
            break;
        case BLEOperationTypeGoUpLeft:
            functionByte = 0x0a;
            break;
        case BLEOperationTypeGoUpRight:
            functionByte = 0x0b;
            break;
        case BLEOperationTypeGoDownLeft:
            functionByte = 0x0c;
            break;
        case BLEOperationTypeGoDownRight:
            functionByte = 0x0d;
            break;
        case BLEOperationTypeRightRemote:
            functionByte = 0x0e;
            break;
        case BLEOperationTypeOpenLeftBlueLight:
            functionByte = 0x05;
            break;
        case BLEOperationTypeCloseLeftBlueLight:
            functionByte = 0x06;
            break;
        case BLEOperationTypeOpenRightRedLight:
            functionByte = 0x07;
            break;
        case BLEOperationTypeCloseRightRedLight:
            functionByte = 0x08;
            break;
        case BLEOperationTypeOpenSound:
            functionByte = 0x09;
            break;
        case BLEOperationTypeBiZhang:
            functionByte = 0x30;
            break;
        case BLEOperationTypeXunXian:
            functionByte = 0x31;
            break;
        case BLEOperationTypeFollowlight:
            functionByte = 0x32;
            break;
        case BLEOperationTypeApplause:
            functionByte = 0x33;
            break;
        case BLEOperationTypeSumo:
            functionByte = 0x34;
            break;
        case BLEOperationTypeDance:
            functionByte = 0x35;
            break;
        case BLEOperationTypeYaokong:
            functionByte = 0x36;
            break;
        case BLEOperationTypeCrawl:
            functionByte = 0x37;
            break;
        case BLEOperationTypeStopModel:
            functionByte = 0x3f;
            break;
        case BLEOperationTypeGetVerison:
            functionByte = 0x40;
            break;
        case BLEOperationTypeDwonload:
            functionByte = 0x20;
            break;
        case BLEOperationTypeAccelerate:
            functionByte = 0x51;
            break;
        case BLEOperationTypeStopAccelerate:
            functionByte = 0x52;
            break;
        case BLEOperationTypeDwonloadData:
            break;
    }
    return functionByte;
    
    
}

/**
 计算Mac地址，
 range: mac 地址取值区间
 reversal: 是否需要对取值后的数据到序
 */
+ (NSString *)convertNewMacAddressWithData:(NSData *)data range:(NSRange)range isReversal:(BOOL)reversal
{
    if (data.length < (range.length + range.location)) {
        return nil;
    }

    NSMutableString *valueString = [[NSMutableString alloc] initWithString:@""];
    NSData *subData = [data subdataWithRange:range];
    const u_int8_t *bytes = [subData bytes];

    if (reversal) {
        for (NSInteger index = [subData length] - 1; index >= 0; index--) {
            u_int8_t value = bytes[index];
            u_int8_t firstValue = value/16;
            u_int8_t secondValue = value%16;
            NSString *toFirstValueString = [YFBTHandler toValueStringWithValue:firstValue];
            NSString *toSecondValueString = [YFBTHandler toValueStringWithValue:secondValue];
            [valueString appendString:toFirstValueString];
            [valueString appendString:toSecondValueString];
        }
    }else {
        for (NSInteger index = 0; index < [subData length]; index++) {
            u_int8_t value = bytes[index];
            u_int8_t firstValue = value/16;
            u_int8_t secondValue = value%16;
            NSString *toFirstValueString = [YFBTHandler toValueStringWithValue:firstValue];
            NSString *toSecondValueString = [YFBTHandler toValueStringWithValue:secondValue];
            [valueString appendString:toFirstValueString];
            [valueString appendString:toSecondValueString];
        }
    }
    return [valueString uppercaseString];
}

// 获取校验和-所有数累计相加, 取模 0XFF 之后与 0XEE 相与
+ (Byte)modulusValue:(Byte *)bytes countOfBytes:(NSInteger)size
{
    NSUInteger total = 0;
    for (NSInteger index = 0; index < size - 1; index++) {
        total += bytes[index];
    }
    return (total % 0xff) & 0xEE;
}

@end
