//
//  YFCommunicationManager.h
//  FOXAISpeaker
//
//  Created by 刘云飞 on 2018/4/20.
//  Copyright © 2018年 answer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBluetoothConstant.h"
#import "LGBluetooth.h"
#import "YFPeripheral.h"
#import "YFBTHandler.h"



@interface YFCommunicationManager : NSObject

@property (nonatomic, strong) LGCentralManager * _Nullable lgCenterManager;

+ (instancetype)shareInstance;
@property (nonatomic,assign,readonly)BOOL isCentralReady;

+ (NSError *)errorDomainAndInfoWithErrorCode:(kYFBleErrorCode)code;
// 这个方法可以扩展正对特定设备进行Filter
- (void)startScanPeripheralsWithBlock:(DiscoverPerpheralsCallback)scanBlock;
- (void)stopScanningPeripherals;

// 当前连上的设备--目前只连一个，用这个
@property (nonatomic,strong)YFPeripheral *yfPeripheral;


/** 当前已连接上的设备 */
@property (nonatomic, strong,readonly) NSMutableArray *connetPeripheralArr;
// 根据外设OemType找到对应的外设
- (NSArray <YFPeripheral *>*)getPeripheralWithOemType:(BlueOemType)oemType;
// 在绑定的设备中根据Mac找到对应的 设备
- (YFPeripheral *)getPeripheralWithMac:(NSString *)addressMac;

- (YFPeripheral *)getConnectPheriperWithLGPer:(LGPeripheral *)lgPer;
// 添加或更新连接的设备
- (void)addOrUpdatePeripheral:(YFPeripheral *)peripheral;

@property (nonatomic,assign)BOOL shouldRetryConnect;


@end
