//
//  YFSheetAlertController.h
//  Petcome
//
//  Created by petcome on 2019/1/11.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "PopupViewController.h"
#import "YFCustomAlertCell.h"


NS_ASSUME_NONNULL_BEGIN
@class YFCustomAlertController;
// index: 不包含分享的cell 
typedef void(^CustomViewClickBlock)(YFCustomAlertController *alertVc, NSInteger index);
typedef void(^CustomViewShareClickBlock)(YFCustomAlertController *alertVc, kThirdpartType type);

@interface YFCustomAlertController : PopupViewController

/** -- 带分享视图
 flag：分享到:不要房子啊items里面，items里面放普通的选项
 @prama isSheet: YES-底部弹框，NO 中间弹框
 */
+ (instancetype)ShowCustomAlertViewWithPresentInVc:(UIViewController *_Nullable)parentVc isSheet:(BOOL)isSheet items:(NSArray <NSString *>*)items showShareView:(BOOL)showShare comeInAnimationType:(DetailViewControllerPresentAnimationType)comeInAnimation comeOutAnimationType:(DetailViewControllerAnimationType)comeOutAnimation clickBlock:(__nullable CustomViewClickBlock)clickBlock shareClick:(__nullable CustomViewShareClickBlock)shareClick;

// 不带分享视图
+ (instancetype)ShowCustomAlertViewWithPresentInVc:(UIViewController *_Nullable)parentVc isSheet:(BOOL)isSheet items:(NSArray <NSString *>*)items comeInAnimationType:(DetailViewControllerPresentAnimationType)comeInAnimation comeOutAnimationType:(DetailViewControllerAnimationType)comeOutAnimation clickBlock:(__nullable CustomViewClickBlock)clickBlock;

- (void)showRightSureImage:(BOOL)showRightSureImage selectIndex:(NSInteger)index;

/**
 有分享的实现该Block,
 点击分享的按钮
 tag: 50-wechat 51-Pyq 52-weibo 53-QQ, 54-Qzone 
 */
@property (nonatomic,copy)void(^shareBtnClick)(NSInteger tag);

@end




NS_ASSUME_NONNULL_END
