//
//  YFCustomTextFCell.h
//  Smart4U
//
//  Created by petcome on 2019/8/30.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class YFCustomTextfieldAction;
@interface YFCustomTextFCell : UITableViewCell

@property (nonatomic,copy)void(^textDidChange)(UITextField *textF);

#pragma mark - 下面的赋值方式和上面完全独立
@property (nonatomic,strong)YFCustomTextfieldAction *actionItem;

@end

NS_ASSUME_NONNULL_END
