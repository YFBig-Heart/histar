//
//  YFCustomTextfieldAction.h
//  Smart4U
//
//  Created by petcome on 2019/8/30.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomAlertAction.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^kTextViewDidChange)(UITextField *TF);

@interface YFCustomTextfieldAction : YFCustomAlertAction

/** 右侧限制文字 */
@property (nonatomic,copy)NSString *rightTipText;

@property (nonatomic,copy)NSString *placeholder;

@property (nonatomic,assign)UIKeyboardType keyType;

@property (nonatomic,copy)kTextViewDidChange textDidChange;

@property (nonatomic,copy)kTextViewDidChange textStartChange;
@property (nonatomic,copy)kTextViewDidChange textEndChange;

+ (instancetype)tfActuionWithT:(NSString *)title plh:(NSString *)plh textDidChange:(kTextViewDidChange)textDidChange;

@end

NS_ASSUME_NONNULL_END
