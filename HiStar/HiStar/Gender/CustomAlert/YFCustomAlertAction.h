//
//  YFCustomAlertAction.h
//  Petcome
//
//  Created by petcome on 2019/4/26.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// 下面是普通选线
@class YFCustomAlertAction;
typedef void(^kCustomAlertActionBlock)(YFCustomAlertAction *action);
// 留待以后扩展
@interface YFCustomAlertAction:NSObject

@property (nonatomic,copy)NSString *title;
// 有默认颜色
@property (nonatomic,strong)UIColor *titleColor;
// 所占高度,默认56
@property (nonatomic,assign)CGFloat itemHeight;
// 选中的图片
@property (nonatomic,copy)NSString *selectImage;
// 未选中的图片
@property (nonatomic,copy)NSString *normalImage;

@property (nonatomic,assign)BOOL isSelect;

/** 点击事件 */
@property (nonatomic,copy)kCustomAlertActionBlock action;

+ (instancetype)customAlertAction:(NSString *)title action:(kCustomAlertActionBlock)action;

@end

NS_ASSUME_NONNULL_END
