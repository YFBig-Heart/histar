//
//  YFBtnAlertController.h
//  Petcome
//
//  Created by petcome on 2019/4/26.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomAlertAction.h"
#import "PopupViewController.h"
#import "YFCustomTextfieldAction.h"

NS_ASSUME_NONNULL_BEGIN
@interface YFBtnAlertController : PopupViewController

@property (nonatomic,assign)BOOL isSheet;

- (void)dismissVC;

+ (instancetype)btnAlertVcWithActionItem:(NSArray <YFCustomAlertAction *>*)actions WithPresentInVc:(UIViewController *_Nullable)parentVc cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction;

+ (instancetype)btnAlertVcWithTitle:(NSString *)title actionItem:(NSArray <YFCustomAlertAction *>*)actions WithPresentInVc:(UIViewController *_Nullable)parentVc cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction;



@end


NS_ASSUME_NONNULL_END
