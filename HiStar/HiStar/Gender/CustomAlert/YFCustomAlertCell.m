//
//  YFCustomAlertCell.m
//  Petcome
//
//  Created by petcome on 2019/1/11.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomAlertCell.h"
#import <Masonry/Masonry.h>
#import "YFCustomAlertAction.h"

@interface YFCustomAlertCell ()
@property (weak, nonatomic) IBOutlet UIView *shareContentView;

@end
@implementation YFCustomAlertCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.shareContentView.layer.cornerRadius = 3;
    self.shareContentView.layer.masksToBounds = YES;
}
//  tag: 50-QQ, 51-Qzone 52-wechat 53-Pyq 54-weibo
- (IBAction)shareBtnClick:(UIButton *)sender {
    kThirdpartType type = kThirdpartWechatSession;
    if (sender.tag == 50) {
        type = kThirdpartWechatSession;
    }else if (sender.tag == 51){
        type = kThirdpartWechatTimeLine;
    }else if (sender.tag == 52){
        type = kThirdpartSina;
    }else if (sender.tag == 53){
        type = kThirdpartqq;
    }else if (sender.tag == 54){
        type = kThirdpartqqZone;
    }
    if (self.shareBtnClick) {
        self.shareBtnClick(type);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    self.titleLabel.textColor = selected ? kOrangeRedColor:kTextDrakGrayColor;
}
- (void)configCellWithTitle:(NSString *)title showShareView:(BOOL)showShareView {
    self.titleLabel.text = title;
    self.shareContentView.hidden = !showShareView;
}

- (void)setActionItem:(YFCustomAlertAction *)actionItem {
    _actionItem = actionItem;
    self.titleLabel.textColor = actionItem.titleColor;
    self.titleLabel.text = actionItem.title;
    self.selectImageView.hidden = NO;
    self.shareContentView.hidden = YES;
    if (actionItem.isSelect) {
        self.selectImageView.image = [UIImage imageNamed:actionItem.selectImage];
    }else {
        self.selectImageView.image = [UIImage imageNamed:actionItem.normalImage];
    }
}


@end
