//
//  YFSheetAlertController.m
//  Petcome
//
//  Created by petcome on 2019/1/11.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomAlertController.h"
#import "AppDelegate.h"

@interface YFCustomAlertController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,assign)BOOL showShare;
@property (nonatomic,assign)BOOL isSheet;
@property (nonatomic,assign)BOOL showRightSureImage;

@property (nonatomic,assign)NSInteger selectIndex;

@property (nonatomic,strong)NSArray *alertItems;
@property (nonatomic,copy)CustomViewClickBlock clickBlock;

@property (nonatomic,copy)CustomViewShareClickBlock shareBlock;

@end

@implementation YFCustomAlertController


/**
 flag：分享到:不要房子啊items里面，items里面放普通的选项
 @prama isSheet: YES-底部弹框，NO 中间弹框
 */
// 带分享视图
+ (instancetype)ShowCustomAlertViewWithPresentInVc:(UIViewController *_Nullable)parentVc isSheet:(BOOL)isSheet items:(NSArray <NSString *>*)items showShareView:(BOOL)showShare comeInAnimationType:(DetailViewControllerPresentAnimationType)comeInAnimation comeOutAnimationType:(DetailViewControllerAnimationType)comeOutAnimation clickBlock:(__nullable CustomViewClickBlock)clickBlock shareClick:(__nullable CustomViewShareClickBlock)shareClick {

    YFCustomAlertController *alertVc = [[YFCustomAlertController alloc] init];
    alertVc.clickBlock = clickBlock;
    alertVc.showShare  =showShare;
    alertVc.isSheet = isSheet;
    alertVc.alertItems = items;
    if (parentVc == nil) {
        AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        parentVc = appdelegate.window.rootViewController;
    }
    [alertVc presentInParentViewController:parentVc animationType:comeInAnimation];
    alertVc.comeOutAnimation = comeOutAnimation;
    alertVc.shareBlock = shareClick;
    return alertVc;
}

// 不带分享视图
+ (instancetype)ShowCustomAlertViewWithPresentInVc:(UIViewController *_Nullable)parentVc isSheet:(BOOL)isSheet items:(NSArray <NSString *>*)items comeInAnimationType:(DetailViewControllerPresentAnimationType)comeInAnimation comeOutAnimationType:(DetailViewControllerAnimationType)comeOutAnimation clickBlock:(__nullable CustomViewClickBlock)clickBlock {
    return [self ShowCustomAlertViewWithPresentInVc:parentVc isSheet:isSheet items:items showShareView:NO comeInAnimationType:comeInAnimation comeOutAnimationType:comeOutAnimation clickBlock:clickBlock shareClick:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatPopView];
}

- (void)showRightSureImage:(BOOL)showRightSureImage selectIndex:(NSInteger)index {
    self.showRightSureImage = showRightSureImage;
    if (index < 0 && index >= self.alertItems.count) {
        index = 0;
    }
    self.selectIndex = index;
    [self.tableView reloadData];
}

- (void)dismissVC {
    [self dismissFromParentViewControllerWithAnimationType:self.comeOutAnimation];
}

- (void)creatPopView {
    CGFloat viewH = _showShare ? 132:0;
    CGFloat maxH = _showShare ? 412:392;
    viewH += self.alertItems.count * 56;
    viewH = MIN(viewH, maxH);
    CGFloat viewW = _isSheet ?kYFScreenWidth:kAutoWid(325);
    // 计算高度
    self.popupView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewW, viewH)];
    [self.popupView addSubview:self.tableView];
    [self.view addSubview:self.popupView];
    if (self.isSheet) {
        self.popupView.y = self.view.height - self.popupView.height;
        self.isTapCoverViewDismiss = YES;
    }else {
        self.popupView.center = self.view.center;
        [self.popupView layerFilletWithRadius:5];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.alertItems.count + self.showShare;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    YFCustomAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YFCustomAlertCell" forIndexPath:indexPath];
    if (self.showShare && indexPath.row == 0) {
        [cell configCellWithTitle:@"分享到" showShareView:YES];
        [cell setShareBtnClick:self.shareBtnClick];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        __weak typeof(self) weakSelf = self;
        [cell setShareBtnClick:^(kThirdpartType type) {
            weakSelf.shareBlock(weakSelf, type);
            [weakSelf dismissVC];
        }];
    }else {
        NSString *title = @"";
        if (self.alertItems.count > indexPath.row-self.showShare) {
            title = self.alertItems[indexPath.row - self.showShare];
        }
        [cell configCellWithTitle:title showShareView:NO];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.showRightSureImage ) {
            cell.selectImageView.hidden = (self.selectIndex != indexPath.row - self.showShare);
        }else {
            cell.selectImageView.hidden = YES;
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.showShare && indexPath.row == 0) {
        return 132;
    }
    return 56;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.showShare == YES && indexPath.row == 0) {
        // 分享
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }else {
        self.tableView.userInteractionEnabled = NO;
        self.selectIndex = indexPath.row - self.showShare;
        if (self.clickBlock) {
            self.clickBlock(self, self.selectIndex);
        }
        [self dismissVC];
    }
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.popupView.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.bounces = NO;
        [_tableView registerNib:[UINib nibWithNibName:@"YFCustomAlertCell" bundle:nil] forCellReuseIdentifier:@"YFCustomAlertCell"];
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
        _tableView.backgroundColor = kLightGrayBgColor;
    }
    return _tableView;
}

@end

