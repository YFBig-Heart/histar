//
//  YFBtnAlertController.m
//  Petcome
//
//  Created by petcome on 2019/4/26.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFBtnAlertController.h"
#import "YFCustomAlertCell.h"
#import "AppDelegate.h"
#import "UIView+Frame.h"
#import "UIView+Layer.h"
#import "UIFont+YFFont.h"
#import "YFCustomTextFCell.h"

@interface YFBtnAlertController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)NSArray <YFCustomAlertAction *>*actionList;
@property (nonatomic,strong)UITableView *tableView;
/** leftBtn */
@property (nonatomic,copy)NSString *leftBtnTitle;
/** rightBtn */
@property (nonatomic,copy)NSString *rightBtnTitle;

@property (nonatomic,copy)void(^defalutAction)(void);
@property (nonatomic,copy)void(^cancelAction)(void);

/** 标题 */
@property (nonatomic,strong)NSString *titleString;
/** 是否有输入框 */
@property (nonatomic,assign)BOOL haveTF;

@end

@implementation YFBtnAlertController

+ (instancetype)btnAlertVcWithActionItem:(NSArray <YFCustomAlertAction *>*)actions WithPresentInVc:(UIViewController *_Nullable)parentVc cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction {

    return [self btnAlertVcWithTitle:@"" actionItem:actions WithPresentInVc:parentVc cancelTitle:cancelTitle defalutTitle:defaultTitle cancelAction:cancelAction defalutAction:defalutAction];
}

+ (instancetype)btnAlertVcWithTitle:(NSString *)title actionItem:(NSArray <YFCustomAlertAction *>*)actions WithPresentInVc:(UIViewController *_Nullable)parentVc cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction {

    YFBtnAlertController *vc = [[YFBtnAlertController alloc] init];
    vc.leftBtnTitle = cancelTitle;
    vc.rightBtnTitle = defaultTitle;
    vc.defalutAction = defalutAction;
    vc.cancelAction = cancelAction;
    vc.actionList = actions;
    vc.titleString = title;
    if (parentVc == nil) {
        AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        parentVc = appdelegate.window.rootViewController;
    }
    [vc presentInParentViewController:parentVc animationType:DetailViewControllerPresentAnimationTypeFade];

    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatPopView];
}

- (void)creatPopView {

    CGFloat btnH = 48;

    // 计算高度
    CGFloat viewH = btnH;
    for (YFCustomAlertAction *actionItem in self.actionList) {
        viewH += actionItem.itemHeight;
        if ([actionItem isKindOfClass:[YFCustomTextfieldAction class]]) {
            self.haveTF = YES;
        }
    }

    CGFloat viewW = _isSheet ?kYFScreenWidth:325;
    UILabel *titleLabel = nil;
    if (self.titleString.length > 0) {
        titleLabel = [[UILabel alloc] init];
        titleLabel.font = [UIFont pingFangSCFont:PingFangSCMedium size:15];
        titleLabel.textColor = RGB16TOCOLOR(0x141414);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.frame = CGRectMake(0, 0, viewW, btnH);
        titleLabel.backgroundColor = [UIColor whiteColor];
        titleLabel.text = self.titleString;
        viewH += btnH;
    }
    self.popupView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewW, viewH)];
    if (titleLabel) {
         [self.popupView addSubview:titleLabel];
    }

    self.popupView.backgroundColor = RGB16TOCOLOR(0xEFF3F6);
    [self.popupView addSubview:self.tableView];

    self.tableView.frame = CGRectMake(0, titleLabel.height, viewW, (viewH - titleLabel.height - btnH));

    [self.view addSubview:self.popupView];
    if (self.isSheet) {
        if (self.haveTF) {
            self.popupView.y = self.view.height - self.popupView.height - 50;
        }else {
            self.popupView.y = self.view.height - self.popupView.height;
        }

        self.isTapCoverViewDismiss = YES;
    }else {
        self.popupView.center = self.view.center;
        [self.popupView layerFilletWithRadius:5];
    }
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.popupView.userInteractionEnabled = YES;
    self.view.userInteractionEnabled = YES;
    self.view.superview.userInteractionEnabled = YES;
    // 创建底部按钮
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:self.rightBtnTitle ?self.rightBtnTitle:NSLocalizedString(@"确认", nil) forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setBackgroundColor:kMainThemeColor];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];

    [rightBtn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.popupView addSubview:rightBtn];
    if (self.leftBtnTitle.length > 0) {
//        UIButton *leftBtn = [UIButton creatButtonTitle:self.leftBtnTitle bgColor:[UIColor whiteColor] textFont:[UIFont pingFangSCFont:PingFangSCMedium size:15] textColor:kTextDrakGrayColor normalImage:nil selectImage:nil];
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setTitle:self.leftBtnTitle forState:UIControlStateNormal];
        [leftBtn setTitleColor:RGB16TOCOLOR(0x434053) forState:UIControlStateNormal];
        leftBtn.backgroundColor = [UIColor whiteColor];

        leftBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.popupView addSubview:leftBtn];
        leftBtn.frame = CGRectMake(0, viewH-btnH, viewW*0.5, btnH);
        rightBtn.frame = CGRectMake(viewW*0.5+0.5, viewH-btnH, viewW*0.5-0.5, btnH);
    }else {
        rightBtn.frame = CGRectMake(0, viewH-btnH, viewW, btnH);
    }
}
- (void)rightBtnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.defalutAction) {
        self.defalutAction();
    }
    [self dismissVC];
}
- (void)leftBtnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.cancelAction) {
        self.cancelAction();
    }
    [self dismissVC];
}
- (void)dismissVC {
    [self dismissFromParentViewControllerWithAnimationType:self.comeOutAnimation];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.actionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    YFCustomAlertAction *actionItem = self.actionList[indexPath.row];
    if ([actionItem isKindOfClass:[YFCustomTextfieldAction class]]) {
        YFCustomTextFCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YFCustomTextFCell" forIndexPath:indexPath];
        cell.actionItem = (YFCustomTextfieldAction *)actionItem;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else {
        YFCustomAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YFCustomAlertCell" forIndexPath:indexPath];
        cell.actionItem = actionItem;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    YFCustomAlertAction *actionItem = self.actionList[indexPath.row];
    return actionItem.itemHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    YFCustomAlertAction *actionItem = self.actionList[indexPath.row];
    if (actionItem.action) {
        actionItem.action(actionItem);
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.popupView.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.bounces = NO;
        [_tableView registerNib:[UINib nibWithNibName:@"YFCustomAlertCell" bundle:nil] forCellReuseIdentifier:@"YFCustomAlertCell"];

        [_tableView registerNib:[UINib nibWithNibName:@"YFCustomTextFCell" bundle:nil] forCellReuseIdentifier:@"YFCustomTextFCell"];

        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
        _tableView.backgroundColor = RGBA16TOCOLOR(0x434053, 0.5);

    }

    return _tableView;
}


@end


