//
//  YFCustomTextFCell.m
//  Smart4U
//
//  Created by petcome on 2019/8/30.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomTextFCell.h"
#import "YFCustomTextfieldAction.h"

@interface YFCustomTextFCell ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *contentV;
@property (weak, nonatomic) IBOutlet UILabel *rightLab;
@property (weak, nonatomic) IBOutlet UITextField *TF;

@end

@implementation YFCustomTextFCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentV.layer.cornerRadius = 3;
    [self.contentV.layer setMasksToBounds:YES];
    self.TF.delegate = self;
    self.contentV.backgroundColor = kLightGrayBgColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFDidchange:) name:UITextFieldTextDidChangeNotification object:nil];

}

- (void)setActionItem:(YFCustomTextfieldAction *)actionItem {
    _actionItem = actionItem;
    self.TF.text = actionItem.title;
    self.TF.textColor = actionItem.titleColor;
    self.TF.placeholder = actionItem.placeholder;
    self.TF.keyboardType = actionItem.keyType;
    self.rightLab.text = actionItem.rightTipText;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.actionItem.textStartChange) {
        self.actionItem.textStartChange(textField);
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (self.actionItem.textEndChange) {
        self.actionItem.textEndChange(textField);
    }
    return YES;
}
- (void)textFDidchange:(NSNotification *)note {
    if ([note.object isEqual:self.TF]) {
        if (self.actionItem.textDidChange) {
            self.actionItem.textDidChange(self.TF);
        }
        self.actionItem.title = self.TF.text;
    }
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
