//
//  YFCustomTextfieldAction.m
//  Smart4U
//
//  Created by petcome on 2019/8/30.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomTextfieldAction.h"

@implementation YFCustomTextfieldAction

+ (instancetype)tfActuionWithT:(NSString *)title plh:(NSString *)plh textDidChange:(kTextViewDidChange)textDidChange {

    YFCustomTextfieldAction *actionItem = [[YFCustomTextfieldAction alloc] init];
    actionItem.title = title;
    actionItem.textDidChange = textDidChange;
    actionItem.itemHeight = 76;
    actionItem.titleColor = kTextBlackColor;
    return actionItem;
}

@end
