//
//  YFCustomAlertAction.m
//  Petcome
//
//  Created by petcome on 2019/4/26.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFCustomAlertAction.h"

@implementation YFCustomAlertAction

+ (instancetype)customAlertAction:(NSString *)title action:(kCustomAlertActionBlock)action {
    YFCustomAlertAction *actionItem = [[YFCustomAlertAction alloc] init];
    actionItem.title = title;
    actionItem.action = action;
    return actionItem;
}
- (instancetype)init {
    if (self = [super init]) {
        self.itemHeight = 56;
        self.titleColor = kTextDrakGrayColor;
    }
    return self;
}

@end

