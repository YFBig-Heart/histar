//
//  YFCustomAlertCell.h
//  Petcome
//
//  Created by petcome on 2019/1/11.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, kThirdpartType) {
    kThirdpartWechatSession = 0, // 微信个人
    kThirdpartWechatTimeLine,// 朋友圈
    kThirdpartSina,  // 微博
    kThirdpartqq,    // QQ
    kThirdpartqqZone, //  QQ空间
};


@class YFCustomAlertAction;
@interface YFCustomAlertCell : UITableViewCell

- (void)configCellWithTitle:(NSString *)title showShareView:(BOOL)showShareView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectImageView;

/** 点击分享的按钮
 tag: 50-wechat 51-Pyq 52-weibo 53-QQ, 54-Qzone 
 */
@property (nonatomic,copy)void(^shareBtnClick)(kThirdpartType type);

#pragma mark - 下面的赋值方式和上面完全独立
@property (nonatomic,strong)YFCustomAlertAction *actionItem;


@end

NS_ASSUME_NONNULL_END
