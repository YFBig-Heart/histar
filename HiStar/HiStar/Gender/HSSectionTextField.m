//
//  HSSectionTextField.m
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSSectionTextField.h"
#import <Masonry/Masonry.h>
#import "PopView/PopView.h"

@interface HSSectionTextField ()

@property (nonatomic,strong)HSPopCutomTableView *popTableView;

@property (nonatomic,strong)UIImageView *bgImageView;
@end

@implementation HSSectionTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.rightImage = [UIImage imageNamed:@"element_tanlge"];
        _cornerRadius = 2;
        _sectionHeight = 40;
        [self setUpUi];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _cornerRadius = 2;
        _sectionHeight = 40;
        [self setUpUi];
    }
    return self;
}
- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    [self layerFilletWithRadius:_cornerRadius];
}
- (void)setUpUi {
    [self layerFilletWithRadius:_cornerRadius];
    
    UIImageView *bgImg = [[UIImageView alloc] initWithImage:self.bgImage];
    [self addSubview:bgImg];
    [bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    self.bgImageView = bgImg;
    
    _textField = [[UITextField alloc] init];
    _textField.textColor = RGB16TOCOLOR(0x030303);
    _textField.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.textField];
    
    _textField.inputView = [UIView new];
    _textField.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor whiteColor];

    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.rightButton];
    [self.rightButton setImage:self.rightImage forState:UIControlStateNormal];
   
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(6);
        make.top.bottom.equalTo(self);
        make.right.equalTo(self).offset(0);
    }];
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-8);
        make.centerY.equalTo(self);
    }];
    [self.rightButton addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *coverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    coverBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:coverBtn];
    [coverBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.textField);
    }];
    [coverBtn addTarget:self action:@selector(coverBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // 单词纠错
    [self.textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    // 首字母大写
    [self.textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    
    // [self addInputAccessoryView:self.textField];
    
}

- (void)addInputAccessoryView:(UITextField *)textfield {
    UIToolbar *assView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, kYFScreenWidth, 32)];
    [assView setBackgroundImage:[UIImage new] forToolbarPosition:(UIBarPositionTop) barMetrics:(UIBarMetricsDefault)];
    [assView setShadowImage:[UIImage new] forToolbarPosition:(UIBarPositionTop)];
    assView.clipsToBounds = YES;
    UIBarButtonItem *finishBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"zhcc_shouqi"] style:(UIBarButtonItemStyleDone) target:self action:@selector(doneBarItemAction:)];
    finishBtn.tintColor = [UIColor darkGrayColor];
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemFlexibleSpace) target:nil action:nil];
    [assView setItems:@[fixItem,fixItem,finishBtn]];
    textfield.inputAccessoryView = assView;
}

- (void)doneBarItemAction:(UIBarButtonItem *)item {
    [self.textField resignFirstResponder];
}

- (void)rightBtnClick:(UIButton *)sender {
    if (self.rightImageBtnBlock) {
        self.rightImageBtnBlock();
    }
}

- (void)setBgImage:(UIImage *)bgImage {
    _bgImage = bgImage;
    self.bgImageView.image = _bgImage;
}
- (void)setRightImage:(UIImage *)rightImage {
    _rightImage = rightImage;
    [self.rightButton setImage:_rightImage forState:UIControlStateNormal];
    [self.rightButton sizeToFit];
}

- (void)coverBtnClick:(UIButton *)sender {
    // 显示或隐藏tableview
    [self.textField becomeFirstResponder];
    sender.selected = !sender.selected;
    if (sender.selected) {
        // 显示下拉框
        UIView *view = [self.viewController view];
        CGRect rect = [self convertRect:sender.frame toView:view];
        CGRect tableViewFrame;
    
        CGFloat tableViewH = MIN(4,MAX(1, self.sectionArray.count))*self.sectionHeight;
        if (rect.origin.y > (view.height-rect.size.height)*0.5) {
            tableViewFrame = CGRectMake(rect.origin.x-6, rect.origin.y - tableViewH, self.width, tableViewH);
        }else {
            tableViewFrame = CGRectMake(rect.origin.x-6, rect.origin.y + rect.size.height, self.width, tableViewH);
        }
        __weak typeof(self) weakSelf = self;
        HSPopCutomTableView *tableView = [HSPopCutomTableView popCustomTableviewWithFrame:tableViewFrame sections:self.sectionArray current:self.textField.text didSelectBlock:^(NSIndexPath * _Nonnull indexPath, NSString * _Nonnull text, HSPopCutomTableView * _Nonnull view) {
            [view removeFromSuperview];
            weakSelf.textField.text = text;
            if ([weakSelf.delegate respondsToSelector:@selector(sectionTextfieldValueChange:)]) {
                [weakSelf.delegate sectionTextfieldValueChange:weakSelf];
            }
        }];
        tableView.sectionHeight = self.sectionHeight;
        [view addSubview:tableView];
        self.popTableView = tableView;
    }else {
        [self.popTableView removeFromSuperview];
    }
}

- (NSInteger)selectSectionIndex {
    return [self.sectionArray indexOfObject:self.textField.text];
}

@end
