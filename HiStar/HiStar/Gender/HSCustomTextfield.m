//
//  HSCustomTextfield.m
//  HiStar
//
//  Created by 晴天 on 2019/6/17.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSCustomTextfield.h"

@interface HSCustomTextfield()
@property (nonatomic,strong)NSValue *originRect;

@end
@implementation HSCustomTextfield

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)initUI {
    self.cornerRadius = 2;
    self.textColor = RGB16TOCOLOR(0x030303);
    self.font = [UIFont systemFontOfSize:14];
    [self resgisterNotification];
    // 单词纠错
    [self setAutocorrectionType:UITextAutocorrectionTypeNo];
    // 首字母大写
    [self setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [self addInputAccessoryView:self];
}


- (void)addInputAccessoryView:(UITextField *)textfield {
    UIToolbar *assView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, kYFScreenWidth, 32)];
    [assView setBackgroundImage:[UIImage new] forToolbarPosition:(UIBarPositionTop) barMetrics:(UIBarMetricsDefault)];
    [assView setShadowImage:[UIImage new] forToolbarPosition:(UIBarPositionTop)];
    assView.clipsToBounds = YES;
    UIBarButtonItem *finishBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"zhcc_shouqi"] style:(UIBarButtonItemStyleDone) target:self action:@selector(doneBarItemAction:)];
    finishBtn.tintColor = [UIColor darkGrayColor];
    UIBarButtonItem *fixItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemFlexibleSpace) target:nil action:nil];
    [assView setItems:@[fixItem,fixItem,finishBtn]];
    textfield.inputAccessoryView = assView;
}

- (void)doneBarItemAction:(UIBarButtonItem *)item {
    [self resignFirstResponder];
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    [self layerFilletWithRadius:_cornerRadius];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)resgisterNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:) name:UITextFieldTextDidChangeNotification object:self];
}

- (void)textFieldTextDidChange:(NSNotification *)note {
    if ([note.object isEqual:self]) {
        if ([self.customDelegate respondsToSelector:@selector(customTextFieldTextDidChange:)]) {
            [self.customDelegate customTextFieldTextDidChange:self];
        }
    }
}

- (void)keyboardWillChangeFrame:(NSNotification *)note {
    // 保存原始尺
    if (self.isFirstResponder == NO)
    {
        if (self.originRect) {
            self.originRect = nil;
            if ([self.customDelegate respondsToSelector:@selector(cusomTextFiledShouldEndEditing:)]) {
                [self.customDelegate cusomTextFiledShouldEndEditing:self];
            }
        }
    }else {
        CGFloat animationTime = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
        CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        [window convertRect:self.frame toWindow:window];
        CGRect rect = [window convertRect:self.frame fromView:self.superview];
        if (self.originRect == nil) {
            self.originRect = [NSValue valueWithCGRect:rect];
        }
        CGFloat offset = 0;
        if (endFrame.origin.y == kYFScreenHeight) {
            CGRect originRect = [self.originRect CGRectValue];
            offset = rect.origin.y - originRect.origin.y;
        }else {
            if ((rect.origin.y+rect.size.height) > rect.origin.y) {
                 offset = (rect.origin.y+rect.size.height) - endFrame.origin.y;
            }
        }
        if ([self.customDelegate respondsToSelector:@selector(customTextField:offSetY:keyboradShow:animiationTime:)]) {
            [self.customDelegate customTextField:self offSetY:offset keyboradShow:endFrame.origin.y != kYFScreenHeight animiationTime:animationTime];
        }
    }
}


@end
