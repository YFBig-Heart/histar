//
//  HSCustomTextfield.h
//  HiStar
//
//  Created by 晴天 on 2019/6/17.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HSCustomTextfield;
@protocol HSCustomTextfieldDelegate <NSObject>

@optional;
// offsetY:向上偏移多少
- (void)customTextField:(HSCustomTextfield *)textField offSetY:(CGFloat)offsetY keyboradShow:(BOOL)show animiationTime:(CGFloat)animationTime;

// 失去焦点
- (void)cusomTextFiledShouldEndEditing:(HSCustomTextfield *)textField;

- (void)customTextFieldTextDidChange:(HSCustomTextfield *)textField;

@end

@interface HSCustomTextfield : UITextField

@property (nonatomic,strong)id<HSCustomTextfieldDelegate> customDelegate;
// 圆角半径：默认2
@property (nonatomic,assign)CGFloat cornerRadius;

@end

NS_ASSUME_NONNULL_END
