//
//  HSShareManager.h
//  HiStar
//
//  Created by 晴天 on 2020/1/1.
//  Copyright © 2020 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
#import <WechatAuthSDK.h>
#import <WechatConnector/WechatConnector.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ShareResultBlock)(BOOL success);
typedef void(^ShareFinishBlock)(BOOL success,NSString *info);


@interface HSShareManager : NSObject



- (void)shareInView:(UIView *)view
        withContent:(NSString *)content
          withTitle:(NSString *)title
            withUrl:(NSString *)url
          withImage:(UIImage *)image
          shareType:(SSDKPlatformType)type
   shareResultBlock:(ShareResultBlock)block;

#pragma mark - 视频分享
- (void)sharewithContent:(NSString *)content withTitle:(NSString *)title withUrl:(NSString *)url shareType:(SSDKPlatformType)type withmage:(id)image thumbImage:(id)thumbImage shareResultBlock:(ShareResultBlock)block;


// 判断分享的平台（qq和微信）是否安装了
- (BOOL)showInstallApplicationPlatformTipsWithShareType:(SSDKPlatformType)type;

- (void)shareUIWithContent:(NSString *)content withTitle:(NSString *)title images:(id _Nullable)images thumbImage:(id _Nullable)thumbImage url:(NSURL * _Nullable )url shareResultBlock:(ShareResultBlock)block;


+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
