//
//  HSCustomBtn.m
//  HiStar
//
//  Created by petcome on 2019/5/17.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSCustomBtn.h"

@implementation HSCustomBtn
- (instancetype)init {
    if (self = [super init]) {
        self.imageToTextSpace = 3;
    }
    return self;
}

- (void)imageUpTitleDown:(UIButton *)button {
    [button.imageView sizeToFit];
    [button.titleLabel sizeToFit];
    //使图片和文字水平居中显示
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;  //文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
    [button setTitleEdgeInsets:UIEdgeInsetsMake(button.imageView.frame.size.height - self.imageToTextSpace,-button.imageView.frame.size.width, 0.0,0.0)];
    //图片距离右边框距离减少图片的宽度，其它不边
    [button setImageEdgeInsets:UIEdgeInsetsMake(-0.5*button.imageView.frame.size.height, 0.0,0.0, - button.titleLabel.bounds.size.width)];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self imageUpTitleDown:self];
}

@end
