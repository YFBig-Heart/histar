//
//  HSCustomBtn.h
//  HiStar
//
//  Created by petcome on 2019/5/17.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSCustomBtn : UIButton

/** 图片与文字下间隔，默认3 */
@property (nonatomic,assign)CGFloat imageToTextSpace;

@end

NS_ASSUME_NONNULL_END
