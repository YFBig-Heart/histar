//
//  YFHomeHelper.h
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/13.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YFHomeHelper : NSObject

/**
 1.GCD计时器,倒计时
 */
+ (dispatch_source_t)interiorDCDTimerManage:(CGFloat)maxTimeout span:(CGFloat)span reduseTimeBlock:(void(^)(CGFloat reduseTime))reduseTime cancelBlock:(void(^)(void))cancelBlock;

/*
2.GCD计时器，正计时
*/
+ (dispatch_source_t)interiorDCDTimeSpan:(CGFloat)span reduseTimeBlock:(void(^)(CGFloat reduseTime))reduseTime cancelBlock:(void(^)(void))cancelBlock;

// 设备未连接弹窗
+ (BOOL)checkBlueConnectWithTargetVc:(UIViewController *)targetVc;

@end

NS_ASSUME_NONNULL_END
