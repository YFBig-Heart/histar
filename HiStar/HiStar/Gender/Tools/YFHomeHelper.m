//
//  YFHomeHelper.m
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/13.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "YFHomeHelper.h"
#import "HSBlueConnectVc.h"

@implementation YFHomeHelper
/**
 1.GCD计时器
 */
+ (dispatch_source_t)interiorDCDTimerManage:(CGFloat)maxTimeout span:(CGFloat)span reduseTimeBlock:(void(^)(CGFloat reduseTime))reduseTime cancelBlock:(void(^)(void))cancelBlock {
    __block CGFloat timeout = maxTimeout;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), span * NSEC_PER_SEC, 0); // 每span执行一次
    dispatch_source_set_event_handler(timer, ^{ // 定时器执行
        if (timeout < 0) {
            dispatch_source_cancel(timer);
        } else {
            GCDMain(^{
                if (reduseTime) {
                    reduseTime(timeout);
                }
            });
        }
        timeout -= span;
    });
    dispatch_source_set_cancel_handler(timer, ^{
        // 关闭定时器执行方法
        GCDMain(^{
            if (cancelBlock) {
                cancelBlock();
            }
        });
    });
    // 启动
    dispatch_resume(timer);
    return timer;
}

/**
 1.GCD计时器
 */
+ (dispatch_source_t)interiorDCDTimeSpan:(CGFloat)span reduseTimeBlock:(void(^)(CGFloat reduseTime))reduseTime cancelBlock:(void(^)(void))cancelBlock {
    
    __block CGFloat time = 0;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), span * NSEC_PER_SEC, 0); // 每span执行一次
    dispatch_source_set_event_handler(timer, ^{ // 定时器执行
        GCDMain(^{
            if (reduseTime) {
                reduseTime(time);
            }
        });
        time +=span;
    });
    dispatch_source_set_cancel_handler(timer, ^{
        // 关闭定时器执行方法
        GCDMain(^{
            if (cancelBlock) {
                cancelBlock();
            }
        });
    });
    // 启动
    dispatch_resume(timer);
    return timer;
}


+ (BOOL)checkBlueConnectWithTargetVc:(UIViewController *)targetVc {
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    if ([peripheral isPeripheralConnected]==NO) {
        [YFAlertHelper showTwoAlertController:targetVc Title:NSLocalizedString(@"Bluetooth not connected", nil) message:NSLocalizedString(@"Please connect your device", nil) cancelTitle:NSLocalizedString(@"Later", nil) defalutTitle:NSLocalizedString(@"Connection", nil) defaultBtnStyle:UIAlertActionStyleDefault cancelAction:^(UIAlertAction * _Nonnull action) {
            
        } defalutAction:^(UIAlertAction * _Nonnull action) {
            HSBlueConnectVc *blueVc = [[HSBlueConnectVc alloc] initWithNibName:@"HSBlueConnectVc" bundle:nil];
            [targetVc.navigationController pushViewController:blueVc animated:YES];
        }];
        return NO;
    }
    return YES;
}



@end
