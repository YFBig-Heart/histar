//
//  HSShareManager.m
//  HiStar
//
//  Created by 晴天 on 2020/1/1.
//  Copyright © 2020 晴天. All rights reserved.
//

#import "HSShareManager.h"
#import "WXApi.h"
#import <ShareSDKUI/ShareSDKUI.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIShareSheetConfiguration.h>

@implementation HSShareManager

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)shareInView:(UIView *)view
        withContent:(NSString *)content
          withTitle:(NSString *)title
            withUrl:(NSString *)url
          withImage:(UIImage *)image
          shareType:(SSDKPlatformType)type
   shareResultBlock:(ShareResultBlock)block
{
    if (type == SSDKPlatformSubTypeWechatSession || type == SSDKPlatformSubTypeWechatTimeline) {
        if (![WXApi isWXAppInstalled]) {
            [self showInstallApplicationPlatformTipsWithShareType:type];
            return;
        }
    }
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSURL *urlShare = [NSURL URLWithString:url];
    switch (type) {
        case SSDKPlatformSubTypeWechatSession:
             [shareParams SSDKSetupWeChatParamsByText:content title:title url:urlShare thumbImage:nil image:image musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil sourceFileExtension:nil sourceFileData:nil type:SSDKContentTypeAuto forPlatformSubType:type];
             break;
        case SSDKPlatformSubTypeWechatTimeline:
            [shareParams SSDKSetupWeChatParamsByText:content title:title url:urlShare thumbImage:nil image:image musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil sourceFileExtension:nil sourceFileData:nil type:SSDKContentTypeAuto forPlatformSubType:type];
             break;
        default:
            break;
    }
    
    [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        switch (state) {
            case SSDKResponseStateSuccess:
            {
                block(YES);
                break;
            }
            case SSDKResponseStateFail:
            {
                block(NO);
                break;
            }
            default:
                block(NO);
                break;
        }
    }];
    
    
}

- (void)shareUIWithContent:(NSString *)content withTitle:(NSString *)title images:(id _Nullable)images thumbImage:(id _Nullable)thumbImage url:(NSURL * _Nullable )url shareResultBlock:(ShareResultBlock)block {
    
    SSUIShareSheetConfiguration *config = [[SSUIShareSheetConfiguration alloc] init];
    //设置分享菜单为简洁样式
     config.style = SSUIActionSheetStyleSimple;
    //设置竖屏有多少个item平台图标显示
    config.columnPortraitCount = 2;

    //设置横屏有多少个item平台图标显示
    config.columnLandscapeCount = 2;
    
    //设置取消按钮标签文本颜色
    config.cancelButtonTitleColor = RGB16TOCOLOR(0x333333);

    //设置对齐方式（简约版菜单无居中对齐）
    config.itemAlignment = SSUIItemAlignmentCenter;
    
    //设置标题文本颜色
    config.itemTitleColor = RGB16TOCOLOR(0x333333);

    //设置分享菜单栏状态栏风格
    config.statusBarStyle = UIStatusBarStyleLightContent;

    //设置支持的页面方向（单独控制分享菜单栏）
    config.interfaceOrientationMask = UIInterfaceOrientationMaskLandscape;
    //设置分享菜单栏的背景颜色
    config.menuBackgroundColor = RGB16TOCOLOR(0xffffff);
    //取消按钮是否隐藏，默认不隐藏
    config.cancelButtonHidden = YES;
    //设置直接分享的平台（不弹编辑界面）
    config.directSharePlatforms = @[@(SSDKPlatformSubTypeWechatSession),@(SSDKPlatformSubTypeWechatTimeline)];
    
    //2.弹出分享菜单
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic SSDKSetupShareParamsByText:title images:images url:url title:title type:(SSDKContentTypeAuto)];
    [ShareSDK showShareActionSheet:nil
                customItems:@[
                            @(SSDKPlatformSubTypeWechatSession),
                            @(SSDKPlatformSubTypeWechatTimeline),
                            ]
                   shareParams:dic
                   sheetConfiguration:config
    onStateChanged:^(SSDKResponseState state,
    SSDKPlatformType platformType, NSDictionary *userData,
    SSDKContentEntity *contentEntity, NSError *error, BOOL end)
    {
            switch (state) {
                       case SSDKResponseStateSuccess:
                       {
                           block(YES);
                           break;
                       }
                       case SSDKResponseStateFail:
                       {
                           block(NO);
                           break;
                       }
                       default:
                           block(NO);
                           break;
                   }
    }];
    
}


- (void)sharewithContent:(NSString *)content withTitle:(NSString *)title withUrl:(NSString *)url shareType:(SSDKPlatformType)type withmage:(id)image thumbImage:(id)thumbImage shareResultBlock:(ShareResultBlock)block {
    if (type == SSDKPlatformSubTypeWechatSession || type == SSDKPlatformSubTypeWechatTimeline) {
        if (![WXApi isWXAppInstalled]) {
            [self showInstallApplicationPlatformTipsWithShareType:type];
            return;
        }
    }
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    NSURL *urlShare = [NSURL URLWithString:url];
    switch (type) {
        
        case SSDKPlatformSubTypeWechatSession:
        case SSDKPlatformTypeWechat:
            [shareParams SSDKSetupWeChatParamsByText:content title:title url:urlShare thumbImage:thumbImage image:image musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil sourceFileExtension:nil sourceFileData:nil type:SSDKContentTypeAuto forPlatformSubType:type];
            break;
        case SSDKPlatformSubTypeWechatTimeline:
            [shareParams SSDKSetupWeChatParamsByText:content title:title url:urlShare thumbImage:thumbImage image:image musicFileURL:nil extInfo:nil fileData:nil emoticonData:nil sourceFileExtension:nil sourceFileData:nil type:SSDKContentTypeAuto forPlatformSubType:type];
            break;
        default:
            break;
    }
    
    [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        switch (state) {
            case SSDKResponseStateSuccess:
            {
                block(YES);
                break;
            }
            case SSDKResponseStateFail:
            {
                block(NO);
                break;
            }
            default:
                block(NO);
                break;
        }
    }];
}
#pragma mark - Private Method
/**
 * 判断是否安装的微信和QQ
 */
- (BOOL)showInstallApplicationPlatformTipsWithShareType:(SSDKPlatformType)type
{
    BOOL isInstall = YES;
    if (type == SSDKPlatformSubTypeWechatSession || type == SSDKPlatformSubTypeWechatTimeline) {
        if (![WXApi isWXAppInstalled]) {
            [YFAlertHelper showLeeAlertWithTitle:NSLocalizedString(@"prompt", nil) message:@"用户没有安装\"微信\"应用，请前去Appstore下载安装" cancelTitle:NSLocalizedString(@"Cancel", nil) defalutTitle:NSLocalizedString(@"Ok", nil) cancelAction:^{
                
            } defalutAction:^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/wei-xin/id414478124?mt=8"]];
            }];
            isInstall = NO;
        }
    }
    
    return isInstall;
}

@end
