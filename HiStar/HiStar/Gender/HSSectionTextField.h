//
//  HSSectionTextField.h
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSPopCutomTableView.h"

NS_ASSUME_NONNULL_BEGIN

@class HSTextfieldModel,HSSectionTextField;

@protocol HSSectionTextFieldDelegate <NSObject>
- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField;

@end


// 控制元素
@interface HSSectionTextField : UIView

@property (nonatomic,strong)UITextField *textField;
// 默认黑色的小三角形
@property (nonatomic,strong)UIButton *rightButton;

/** 右侧按钮图片 */
@property (nonatomic,strong)UIImage *rightImage;
// bgImage
@property (nonatomic,strong)UIImage *bgImage;

/** 右侧按钮点击事件 */
@property (nonatomic,copy)void(^rightImageBtnBlock)(void);
@property (nonatomic,strong)NSArray <NSString *>*sectionArray;
/** section高度,默认40 */
@property (nonatomic,assign)CGFloat sectionHeight;

/** 圆角半径：默认2 */
@property (nonatomic,assign)CGFloat cornerRadius;

@property (nonatomic,weak)id<HSSectionTextFieldDelegate> delegate;

// 当前选中元素的索引
- (NSInteger)selectSectionIndex;

@end


NS_ASSUME_NONNULL_END
