//
//  CustomSlider.h
//  CoolTennisBall
//
//  Created by Coollang on 16/7/30.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import <UIKit/UIKit.h>


#define UnitSpace(Total,span) (100.0/(Total/span))

@interface CustomSlider : UIView

// 设置初始值 [0 - 1]
@property (nonatomic, assign)CGFloat value;
/**
 *  当前的的值
 */
@property (nonatomic, assign,readonly)CGFloat curretTarget;
/**
 *  分度值,默认1
 */
@property (nonatomic, assign)CGFloat sliderSpan;

/** 最大值，默认100 */
@property (nonatomic, assign)NSInteger maxValue;

/** 滑块图片:默认home_slider */
@property (nonatomic,strong)UIImage *thumbImage;
/** 渐变开始颜色 */
@property (nonatomic,strong)UIColor *startColor;
@property (nonatomic,strong)UIColor *endColor;
/** 背景色 */
@property (nonatomic,strong)UIColor *bgColor;
/** 文字 */
@property (nonatomic,copy)NSString *centerText;

/** 横向滑动，还是竖向滑动，默认竖向 */
@property (nonatomic,assign)BOOL isHorizontalSlider;
/** 是否支持点击滑动,默认为YES */
@property (nonatomic,assign)BOOL isTapEnable;

// 圆角
@property (nonatomic,assign)CGFloat cornerRadius;

// 只要值变化就会调用
@property (nonatomic, copy)void (^valueChange)(CGFloat value);
// 只有拖动或点击手势g才会调用
@property (nonatomic,copy)void (^gesActionValueChange)(CGFloat value);
/** 开始滑动 */
@property (nonatomic,copy)void (^startSliderAction)(CGFloat value);
/** 结束滑动 */
@property (nonatomic,copy)void (^endSliderAction)(CGFloat value);

// 更新UI
- (void)updateDrawUI;

- (void)setSliderValueAndCurrentTarget:(CGFloat)CurrentTarget;

- (void)setSliderSpan:(CGFloat)span maxValue:(NSInteger)maxValue;



@end
