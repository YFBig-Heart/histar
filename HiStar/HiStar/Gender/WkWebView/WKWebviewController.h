//
//  WKWebviewController.h
//  KaizeOCR
//
//  Created by YYKit on 2017/6/16.
//  Copyright © 2017年 zl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBaseViewController.h"

@interface WKWebviewController : UIViewController
@property (nonatomic,copy) NSString *urlString;

/** 进度条颜色:默认白色 */
@property (nonatomic, strong) UIColor *progressColor;


@end
