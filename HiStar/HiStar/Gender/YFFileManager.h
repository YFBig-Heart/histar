//
//  YFFileManager.h
//  Petcome
//
//  Created by petcome on 2019/1/28.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YFFileManager : NSObject

+ (void)removeFile:(NSString *)filePath;

// 获取编程协议文件的文件夹
+ (NSString *)getProgramProtolFilePath;
// 获取截图文件夹
+ (NSString *)getCutImageFilePath;

// 默认忽略数据库文件
#pragma mark - 获取path路径下文件夹大小,ignoreFiles:忽略文件
+ (NSString *)getCacheSizeWithFilePath:(NSString *)path ignoreFile:(NSArray *)ignoreFiles;

#pragma mark - 清除path文件夹下缓存大小,ignoreFiles:忽略文件
+ (BOOL)clearCacheWithFilePath:(NSString *)path ignoreFile:(NSArray *)ignoreFiles;

@end

NS_ASSUME_NONNULL_END
