//
//  NSBundle+DAUtils.h
//  LanguageSettingsDemo
//
//  Created by DarkAngel on 2017/5/4.
//  Copyright © 2017年 暗の天使. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    KLanaguageSystem=0,
    kLanaguageHans=1,
    kLanaguageEn=2,
    kLanaguageHant=3,
} kLanaguageType;

@interface NSBundle (DAUtils)

+ (kLanaguageType)isChineseLanguage;

+ (NSString *)currentLanguage;

@end
