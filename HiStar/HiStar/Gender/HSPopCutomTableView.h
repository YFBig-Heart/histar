//
//  HSPopCutomTableView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HSPopCutomTableView;
typedef void(^SelectDidBlock)(NSIndexPath *indexPath,NSString *text,HSPopCutomTableView *view);

@interface HSPopCutomTableView : UIView

@property (nonatomic,strong)NSArray <NSString *>*sectionArray;
// 选中的颜色，默认 紫色
@property (nonatomic,strong)UIColor *selectCellBgColor;
/** section高度,默认44 */
@property (nonatomic,assign)CGFloat sectionHeight;
/** 能否多选 */
@property (nonatomic,assign)BOOL selectMulti;

@property (nonatomic,copy)SelectDidBlock selectBlock;

+ (instancetype)popCustomTableviewWithFrame:(CGRect)frame sections:(NSArray <NSString *>*)sections current:(NSString *__nullable)currentSelect didSelectBlock:(SelectDidBlock)selectBlock;

@end

@interface PopCustomTableCell : UITableViewCell

@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UIView *lineview;
    
@end


NS_ASSUME_NONNULL_END
