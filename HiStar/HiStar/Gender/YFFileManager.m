//
//  YFFileManager.m
//  Petcome
//
//  Created by petcome on 2019/1/28.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFFileManager.h"

@implementation YFFileManager
+ (void) removeFile:(NSString *)filePath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSError *error;
        [fileManager removeItemAtPath:filePath error:&error];
        if(error){
            NSLog(@"error removing file: %@", [error localizedDescription]);
        }
    }
}


// 获取编程协议文件的文件夹
+ (NSString *)getProgramProtolFilePath {
    NSString *document = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    document = [document stringByAppendingPathComponent:[NSString stringWithFormat:@"ProgramProtol"]];
    // 创建文件夹
    if (![[NSFileManager defaultManager] fileExistsAtPath:document]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:document withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return document;
}
// 获取截图文件夹
+ (NSString *)getCutImageFilePath {
    NSString *document = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    document = [document stringByAppendingPathComponent:@"cutImage"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:document]) {
         [[NSFileManager defaultManager] createDirectoryAtPath:document withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return document;
}

#pragma mark - 获取path路径下文件夹大小,ignoreFiles:忽略文件
+ (NSString *)getCacheSizeWithFilePath:(NSString *)path ignoreFile:(NSArray *)ignoreFiles {
    // 获取“path”文件夹下的所有文件
    NSArray *subPathArr = [[NSFileManager defaultManager] subpathsAtPath:path];
    NSString *filePath  = nil;
    NSInteger totleSize = 0;
    for (NSString *subPath in subPathArr){
        // 1. 拼接每一个文件的全路径
        filePath =[path stringByAppendingPathComponent:subPath];
        // 2. 是否是文件夹，默认不是
        BOOL isDirectory = NO;
        // 3. 判断文件是否存在
        BOOL isExist = [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        // 4. 以上判断目的是忽略不需要计算的文件
        if (!isExist || isDirectory || [filePath containsString:@".DS"] || [[filePath lowercaseString] containsString:@".db"]){
            // 过滤: 1. 文件夹不存在  2. 过滤文件夹  3. 隐藏文件
            continue;
        }
        if ([ignoreFiles containsObject:subPath]) {
            continue;
        }
        // 5. 指定路径，获取这个路径的属性
        NSDictionary *dict = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        /**
         attributesOfItemAtPath: 文件夹路径
         该方法只能获取文件的属性, 无法获取文件夹属性, 所以也是需要遍历文件夹的每一个文件的原因
         */
        // 6. 获取每一个文件的大小
        NSInteger size = [dict[@"NSFileSize"] integerValue];
        // 7. 计算总大小
        totleSize += size;
    }
    //8. 将文件夹大小转换为 M/KB/B
    NSString *totleStr = nil;
    if (totleSize > 1000 * 1000){
        totleStr = [NSString stringWithFormat:@"%.2fM",totleSize / 1000.00f /1000.00f];
    }else if (totleSize > 1000){
        totleStr = [NSString stringWithFormat:@"%.2fKB",totleSize / 1000.00f ];
    }else{
        totleStr = [NSString stringWithFormat:@"%.2fB",totleSize / 1.00f];
    }
    return totleStr;
}

#pragma mark - 清除path文件夹下缓存大小,ignoreFiles:忽略文件
+ (BOOL)clearCacheWithFilePath:(NSString *)path ignoreFile:(NSArray *)ignoreFiles {
    //拿到path路径的下一级目录的子文件夹
    NSArray *subPathArr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    NSString *filePath = nil;
    NSError *error = nil;
    for (NSString *subPath in subPathArr)
    {
        filePath = [path stringByAppendingPathComponent:subPath];
        if ([ignoreFiles containsObject:filePath]) {
            continue;
        }
        if ([filePath containsString:@".DS"] || [[filePath lowercaseString] containsString:@".db"]){
            // 过滤: 1. 文件夹不存在  2. 过滤文件夹  3. 隐藏文件
            continue;
        }
        //删除子文件夹
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        if (error) {
            return NO;
        }
    }
    return YES;
}

@end
