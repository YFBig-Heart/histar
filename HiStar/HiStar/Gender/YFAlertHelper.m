//
//  YFAlertHelper.m
//  Petcome
//
//  Created by 晴天 on 2019/1/3.
//  Copyright © 2019年 yunfei. All rights reserved.
//

#import "YFAlertHelper.h"
#import "AppDelegate.h"
#import "LEEAlert.h"
#import "UIFont+YFFont.h"

@implementation YFAlertHelper

#pragma mark - 自定义弹窗
+ (void)showLeeAlertWithTitle:(NSString *)title titleColor:(UIColor *)titleColor message:(NSString *__nullable)message cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction {

    [LEEAlert alert].config.LeeAddTitle(^(UILabel *label) {
        label.text = title;
        label.textColor = titleColor ? titleColor : kTextBlackColor;
        label.textAlignment = NSTextAlignmentLeft;
    })
    .LeeAddContent(^(UILabel *label) {
        label.text = message;
        label.textColor = kTextDrakGrayColor;
        label.textAlignment = NSTextAlignmentLeft;
    }).LeeAddAction(^(LEEAction *action) {
        action.type = LEEActionTypeCancel;
        action.title = cancelTitle;
        action.titleColor = kTextBlackColor;
        action.font = [UIFont pingFangSCFont:PingFangSCMedium size:16];
        action.borderColor = [UIColor clearColor];
        action.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 10, 10);
        action.height = 60.f;
        action.clickBlock = ^{
            // 点击事件Block
            if (cancelAction) {
                cancelAction();
            }
        };
    }).LeeAddAction(^(LEEAction *action) {
        action.type = LEEActionTypeDefault;
        action.title = defaultTitle;
        action.titleColor = kMainThemeColor;
        action.font = [UIFont pingFangSCFont:PingFangSCMedium size:16];
        action.borderColor = [UIColor clearColor];
        action.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 10, 10);
        action.height = 60.f;
        action.clickBlock = ^{
            // 点击事件Block
            if (defalutAction) {
                defalutAction();
            }
        };
    }).LeeShow();
}

// 左右两个按钮
+ (void)showLeeAlertWithTitle:(NSString *)title message:(NSString *__nullable)message cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction {
    
    [self showLeeAlertWithTitle:title titleColor:kTextBlackColor message:message cancelTitle:cancelTitle defalutTitle:defaultTitle cancelAction:cancelAction defalutAction:defalutAction];
}

// 只有一个按钮
+ (void)showLeeAlertWithTitle:(NSString *)title message:(NSString *)message defalutTitle:(NSString *)defaultTitle defalutAction:(void(^)(void))defalutAction {
    [LEEAlert alert].config.LeeAddTitle(^(UILabel *label) {
        label.text = title;
        label.textColor = kTextBlackColor;
        label.textAlignment = NSTextAlignmentLeft;
    })
    .LeeAddContent(^(UILabel *label) {
        label.text = message;
        label.textColor = kTextDrakGrayColor;
        label.textAlignment = NSTextAlignmentLeft;
    }).LeeAddAction(^(LEEAction *action) {
        action.type = LEEActionTypeDefault;
        action.title = defaultTitle;
        action.titleColor = kMainThemeColor;
        action.font = [UIFont pingFangSCFont:PingFangSCMedium size:16];
        action.borderColor = [UIColor clearColor];
        action.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 10, 10);
        action.height = 60.f;
        action.clickBlock = ^{
            // 点击事件Block
            if (defalutAction) {
                defalutAction();
            }
        };
    }).LeeShow();

}

// 隐藏LeeAlert
+ (void)hideLeeAlert {
    [LEEAlert closeWithCompletionBlock:^{

    }];
}

#pragma mark - 系统自带弹框
+ (UIAlertController *)showAlertController:(UIViewController *)controller Title:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:(void(^)(UIAlertAction *action))cancelAction defalutAction:(void(^)(UIAlertAction *action))defalutAction {
    
    return [self showTwoAlertController:controller Title:title message:message cancelTitle:cancelTitle defalutTitle:defaultTitle defaultBtnStyle:UIAlertActionStyleDefault cancelAction:cancelAction defalutAction:defalutAction];
}

+ (UIAlertController *)showTwoAlertController:(UIViewController *)controller Title:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle defalutTitle:(NSString *)defaultTitle defaultBtnStyle:(UIAlertActionStyle)defaultBtnStyle cancelAction:(void(^)(UIAlertAction *action))cancelAction defalutAction:(void(^)(UIAlertAction *action))defalutAction {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (cancelTitle.length <= 0 && defaultTitle.length <= 0) {
        cancelTitle = cancelTitle.length > 0 ? cancelTitle:@"OK";
    }
    
    if (cancelTitle) {
        UIAlertAction *cancelItem = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (cancelAction) {
                cancelAction(action);
            }
        }];
        [alertController addAction:cancelItem];
    }
    
    if (defaultTitle.length > 0) {
        UIAlertAction *defalut = [UIAlertAction actionWithTitle:defaultTitle style:defaultBtnStyle handler:^(UIAlertAction * _Nonnull action) {
            if (defalutAction) {
                defalutAction(action);
            }
        }];
        [alertController addAction:defalut];
    }
    
    if (controller == nil) {
        AppDelegate *appdelegate =  (AppDelegate *)[UIApplication sharedApplication].delegate;
        controller = appdelegate.window.rootViewController;
    }
    [controller presentViewController:alertController animated:YES completion:nil];
    
    return alertController;
}



@end
