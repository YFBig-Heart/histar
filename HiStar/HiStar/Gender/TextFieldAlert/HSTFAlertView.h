//
//  HSTFAlertView.h
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSTFAlertView : UIView


@property (weak, nonatomic) IBOutlet UITextField *textField;

+ (HSTFAlertView *)tfAlertViewTitle:(NSString *__nullable)title placeholder:(NSString *__nullable)placeholder tfText:(NSString *__nullable)tfText textDidChange:(void(^)(UITextField *text))textDidChange btnClickBlock:(void(^)(NSInteger index,HSTFAlertView *view))btnClickBlock;

@end

NS_ASSUME_NONNULL_END
