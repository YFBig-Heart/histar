//
//  HSTFAlertView.m
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSTFAlertView.h"
#import "UIView+Layer.h"

@interface HSTFAlertView ()

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYCon;

@property (nonatomic,copy)void (^btnClickBlock)(NSInteger index,HSTFAlertView *view);

@property (nonatomic,copy)void (^textDidChange)(UITextField *text);


@end

@implementation HSTFAlertView

+ (HSTFAlertView *)tfAlertViewTitle:(NSString *__nullable)title placeholder:(NSString *__nullable)placeholder tfText:(NSString *__nullable)tfText textDidChange:(void(^)(UITextField *text))textDidChange btnClickBlock:(void(^)(NSInteger index,HSTFAlertView *view))btnClickBlock {

    HSTFAlertView *view = [[NSBundle mainBundle] loadNibNamed:@"HSTFAlertView" owner:nil options:nil].firstObject;
    view.textDidChange = textDidChange;
    view.btnClickBlock = btnClickBlock;
    view.textField.placeholder = placeholder;
    view.titleLabel.text = title;
    view.textField.text = tfText;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    view.frame = window.bounds;
    [window addSubview:view];
    [view.textField becomeFirstResponder];
    return view;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.cancelBtn setTitle:klocCancel forState:UIControlStateNormal];
    [self.sureBtn setTitle:klocOk forState:UIControlStateNormal];
    [self.cancelBtn layerHeight];
    [self.sureBtn layerHeight];
    [self.contentView layerFilletWithRadius:5];
    [self.textField layerFilletWithRadius:5];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameUpdate:) name:UIKeyboardWillChangeFrameNotification object:nil];
}
- (void)keyboardFrameUpdate:(NSNotification *)note {
    CGFloat animationTime = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (endFrame.origin.y == kYFScreenHeight) {
        self.centerYCon.constant = 0;
    }else {
        CGFloat offset = CGRectGetMaxY(self.contentView.frame) - endFrame.origin.y - 20;
        self.centerYCon.constant = -offset;
    }
     __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationTime animations:^{
        [weakSelf.contentView layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

- (void)textDidChange:(NSNotification *)note {
    if ([note.object isEqual:self.textField]) {
        if (self.textDidChange) {
            self.textDidChange(_textField);
        }
    }
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)sureBtnClick:(id)sender {
    if (self.btnClickBlock) {
        self.btnClickBlock(1, self);
    }
}

- (IBAction)cancelBtnClick:(id)sender {
    if (self.btnClickBlock) {
        self.btnClickBlock(0, self);
    }
}


@end
