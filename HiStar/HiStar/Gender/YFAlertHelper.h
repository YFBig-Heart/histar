//
//  YFAlertHelper.h
//  Petcome
//
//  Created by 晴天 on 2019/1/3.
//  Copyright © 2019年 yunfei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopupViewController.h"
#import "YFCustomAlertController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YFAlertHelper : UIView

// 左右两个按钮
+ (void)showLeeAlertWithTitle:(NSString *)title message:(NSString *__nullable)message cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction;

+ (void)showLeeAlertWithTitle:(NSString *)title titleColor:(UIColor *)titleColor message:(NSString *__nullable)message cancelTitle:(NSString *__nullable)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:( void(^ _Nullable )(void))cancelAction defalutAction:( void(^ _Nullable )(void))defalutAction;


// 只有一个按钮
+ (void)showLeeAlertWithTitle:(NSString *)title message:(NSString *__nullable)message defalutTitle:(NSString *)defaultTitle defalutAction:(void(^)(void))defalutAction;

// 隐藏LeeAlert
+ (void)hideLeeAlert;

#pragma mark - 系统弹窗
+ (UIAlertController *)showAlertController:(UIViewController *)controller Title:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle defalutTitle:(NSString *)defaultTitle cancelAction:(void(^)(UIAlertAction *action))cancelAction defalutAction:(void(^)(UIAlertAction *action))defalutAction;

+ (UIAlertController *)showTwoAlertController:(UIViewController *)controller Title:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle defalutTitle:(NSString *)defaultTitle defaultBtnStyle:(UIAlertActionStyle)defaultBtnStyle cancelAction:(void(^)(UIAlertAction *action))cancelAction defalutAction:(void(^)(UIAlertAction *action))defalutAction;

@end

NS_ASSUME_NONNULL_END
