//
//  CustomProgressBarView.h
//  CoolTennisBall
//
//  Created by Coollang on 16/8/3.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProgressBarView : UIView

/** 渐变色 */
@property (nonatomic, strong)NSArray <UIColor *>*colors;

@property (nonatomic, assign, readonly)CGFloat value;

- (void)updateProgroess:(CGFloat)value;

@end
