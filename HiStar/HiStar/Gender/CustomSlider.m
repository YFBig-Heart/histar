//
//  CustomSlider.m
//  CoolTennisBall
//
//  Created by Coollang on 16/7/30.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import "CustomSlider.h"

@interface CustomSlider ()
/**
 *  当前的的值
 */
@property (nonatomic, assign)CGFloat curretTarget;

/** 滑块 */
@property (nonatomic, strong) CALayer *thumbImageLayer;
/** 渐变色块 */
@property (nonatomic, strong) CAGradientLayer *minTrackLayer;
@property (nonatomic, strong) CAGradientLayer *maxTrackLayer;
// 中间展示的文字
@property (nonatomic,strong)CATextLayer *textLayer;
@property (nonatomic,strong)UITapGestureRecognizer *tapGes;

// 上一个值
@property (nonatomic,assign)CGFloat lastVlaue;


@end

@implementation CustomSlider

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // 添加手势
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [self addGestureRecognizer:panGesture];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [self addGestureRecognizer:tapGesture];
        self.tapGes = tapGesture;
        self.startColor = [UIColor whiteColor];
        self.endColor = [UIColor whiteColor];
        self.isTapEnable = YES;
        self.maxValue = 100;
        self.sliderSpan = 1;
        self.curretTarget = 0;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        // 添加手势
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [self addGestureRecognizer:panGesture];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [self addGestureRecognizer:tapGesture];
        self.tapGes = tapGesture;
        self.isTapEnable = YES;
        self.maxValue = 100;
        self.sliderSpan = 1;
        self.curretTarget = 0;
    }
    return self;
}

- (void)tapGestureAction:(UITapGestureRecognizer *)tap {
    if (self.isHorizontalSlider) {
        CGPoint startP = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height);
        CGPoint curP = [tap locationInView:self];
        CGFloat postionY = startP.y - curP.y;
        if (postionY >= -2 && postionY <= self.bounds.size.height + 8) {
            [self updateProgroess:postionY];
        }
    }else {
        CGPoint startP = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height);
        CGPoint curP = [tap locationInView:self];
        CGFloat postionY = startP.y - curP.y;
        if (postionY >= -2 && postionY <= self.bounds.size.height + 8) {
            [self updateProgroess:postionY];
        }
    }
    self.lastVlaue = _value;
    if (self.gesActionValueChange) {
        self.gesActionValueChange(self.value);
    }
}
- (void)setIsTapEnable:(BOOL)isTapEnable {
    _isTapEnable = isTapEnable;
    self.tapGes.enabled = isTapEnable;
}

- (void)panGestureAction:(UIPanGestureRecognizer *)pan {
    CGPoint startP = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height);
    if (self.isHorizontalSlider) {
        startP = CGPointMake(self.thumbImage.size.width*0.5, self.bounds.size.height*0.5);
    }else {
        startP = CGPointMake(self.bounds.size.width * 0.5, self.thumbImage.size.height*0.5);
    }
    CGPoint curP = CGPointZero;
    curP = [pan locationInView:self];

    
    if (self.isHorizontalSlider) {
        CGFloat postionX = curP.x - startP.x;
        if (postionX >= -2 && postionX <= self.bounds.size.width + 8) {
            [self updateProgroess:postionX];
        }
    }else {
        CGFloat postionY = curP.y - startP.y;
        if (postionY >= -2 && postionY <= self.bounds.size.height + 8) {
            [self updateProgroess:postionY];
        }
    }
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{
            if (self.startSliderAction) {
                self.startSliderAction(self.value);
            }
        }
            break;
        case UIGestureRecognizerStateChanged:{
            if (self.gesActionValueChange) {
                self.gesActionValueChange(self.value);
            }
        }
        case UIGestureRecognizerStateEnded:{
            if (self.endSliderAction) {
                self.endSliderAction(self.value);
            }
        }
            break;
        default:
            break;
    }
}

- (void)updateProgroess:(CGFloat)postionY {
    if (postionY < 0) {
        postionY = 0;
    }
    if (self.isHorizontalSlider) {
        CGFloat maxPostion = (CGRectGetWidth(self.frame) - self.thumbImage.size.width*0.5);
        if (postionY > maxPostion) {
            postionY = maxPostion;
        }
        if (postionY < self.thumbImage.size.width*0.5) {
            postionY = self.thumbImage.size.width*0.5;
        }
        self.thumbImageLayer.position = CGPointMake(postionY,self.bounds.size.height * 0.5);
        self.minTrackLayer.frame = CGRectMake(0, 0,postionY, CGRectGetHeight(self.frame));
        self.minTrackLayer.startPoint = CGPointMake(postionY / CGRectGetWidth(self.frame),0.5);
        self.value = postionY/maxPostion;
    }else {
        CGFloat maxPostion = (CGRectGetHeight(self.frame) - self.thumbImage.size.height*0.5);
        if (postionY > maxPostion) {
            postionY = maxPostion;
        }
        if (postionY < self.thumbImage.size.height*0.5) {
            postionY = self.thumbImage.size.height*0.5;
        }
        self.thumbImageLayer.position = CGPointMake(self.bounds.size.width * 0.5, postionY);
        self.minTrackLayer.frame = CGRectMake(0, 0 , CGRectGetWidth(self.frame),postionY);
        self.value = postionY / (CGRectGetHeight(self.frame)-self.thumbImage.size.height*0.5);
        self.minTrackLayer.startPoint = CGPointMake(0.5,self.value);
    }
    if (self.valueChange) {
        self.valueChange(self.value);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setGradientColor];
    [self setUpUI];
}
- (void)updateDrawUI {
    [self setGradientColor];
    [self setUpUI];
}

// 设置渐变色
- (void)setGradientColor {
    if (self.maxTrackLayer) {
        [self.maxTrackLayer removeFromSuperlayer];
    }
    if (self.minTrackLayer) {
        [self.minTrackLayer removeFromSuperlayer];
    }
    CAGradientLayer *maxTrackLayer = [[CAGradientLayer alloc] init];
    maxTrackLayer.colors = @[(__bridge id)self.bgColor.CGColor,(__bridge id)self.bgColor.CGColor];
    maxTrackLayer.cornerRadius = self.cornerRadius;
    [maxTrackLayer setMasksToBounds:YES];
    maxTrackLayer.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    self.maxTrackLayer = maxTrackLayer;
    [self.layer addSublayer:maxTrackLayer];

    CAGradientLayer *minTrackLayer = [[CAGradientLayer alloc] init];
    minTrackLayer.colors = @[(__bridge id)self.startColor.CGColor,(__bridge id)self.endColor.CGColor];

    if (self.isHorizontalSlider) {
        maxTrackLayer.startPoint = CGPointMake(1, 0.5);
        maxTrackLayer.endPoint = CGPointMake(0, 0.5);

        minTrackLayer.startPoint = CGPointMake(1, 0.5);
        minTrackLayer.endPoint = CGPointMake(0, 0.5);
    }else {
        maxTrackLayer.startPoint = CGPointMake(0.5, 1);
        maxTrackLayer.endPoint = CGPointMake(0.5, 0);

        minTrackLayer.startPoint = CGPointMake(0.5, 1);
        minTrackLayer.endPoint = CGPointMake(0.5, 0);
    }
    
    minTrackLayer.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    minTrackLayer.cornerRadius = self.cornerRadius;
    [minTrackLayer setMasksToBounds:YES];
    self.minTrackLayer = minTrackLayer;
    [self.layer addSublayer:minTrackLayer];
}

- (void)setUpUI {
    if (self.textLayer) {
        [self.textLayer removeFromSuperlayer];
    }
    if (self.thumbImageLayer) {
        [self.thumbImageLayer removeFromSuperlayer];
    }
    if (self.centerText) {
        CATextLayer *textLayer = [CATextLayer layer];
        self.textLayer = textLayer;
        textLayer.foregroundColor = kMainThemeColor.CGColor;
        textLayer.string = self.centerText;
        textLayer.font = (__bridge CFTypeRef _Nullable)( [UIFont pingFangSCFont:PingFangSCMedium size:12]);
        textLayer.fontSize = [UIFont pingFangSCFont:PingFangSCMedium size:12].pointSize;
        textLayer.alignmentMode = kCAAlignmentCenter;
        CGSize textSize = [textLayer.string textSizeWithtextMaxSize:CGSizeMake(MAXFLOAT, 30) font:[UIFont pingFangSCFont:PingFangSCMedium size:12]];
        textLayer.bounds = CGRectMake(0, 0, textSize.width, textSize.height);
        textLayer.position = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height * 0.5);
        [self.layer addSublayer:textLayer];
    }
    CALayer *thumbImageLayer = [CALayer layer];
    thumbImageLayer.bounds = CGRectMake(0, 0, self.thumbImage.size.width, self.thumbImage.size.height);
    thumbImageLayer.position = CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height * 0.5);
    thumbImageLayer.contentsScale = [UIScreen mainScreen].scale;
    thumbImageLayer.contents = (__bridge id _Nullable)(_thumbImage.CGImage);
    self.thumbImageLayer = thumbImageLayer;
    [self.layer addSublayer:thumbImageLayer];

    if (self.isHorizontalSlider) {
        CGFloat maxPostion = (CGRectGetWidth(self.frame) - self.thumbImage.size.width*0.5);
        CGFloat postionX = maxPostion * self.value;
        [self updateProgroess:postionX];
    }else {
        CGFloat maxPostion = (CGRectGetWidth(self.frame) - self.thumbImage.size.height*0.5);
        CGFloat postionY = maxPostion * self.value;
        [self updateProgroess:postionY];
    }
}

- (void)setSliderValueAndCurrentTarget:(CGFloat)CurrentTarget {
    self.curretTarget = CurrentTarget;
    self.value = (self.curretTarget * UnitSpace(self.maxValue, self.sliderSpan)) / (100 * self.sliderSpan);
    if (self.isHorizontalSlider == YES) {
        [self updateProgroess:self.value * (CGRectGetWidth(self.frame) - self.thumbImage.size.width*0.5)];
    }else {
        [self updateProgroess:self.value * (CGRectGetHeight(self.frame) - self.thumbImage.size.height*0.5)];
    }
}

- (void)setSliderSpan:(CGFloat)span maxValue:(NSInteger)maxValue{
    self.maxValue = maxValue;
    self.sliderSpan = span;
}

- (UIImage *)thumbImage {
    if (!_thumbImage) {
        _thumbImage = [UIImage imageNamed:@"white_slider"];
    }
    return _thumbImage;
}
- (UIColor *)startColor {
    if (!_startColor) {
        _startColor = kMainThemeColor;
    }
    return _startColor;
}
- (UIColor *)endColor {
    if (!_endColor) {
        _endColor = kNavColor;
    }
    return _endColor;
}
- (UIColor *)bgColor {
    if (!_bgColor) {
        _bgColor = RGBA16TOCOLOR(0x434053, 1.0);
    }
    return _bgColor;
}


@end
