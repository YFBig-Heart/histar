//
//  HSPopCutomTableView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSPopCutomTableView.h"
#import <Masonry/Masonry.h>


@interface HSPopCutomTableView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic,copy)NSString *currentSelect;

@end
@implementation HSPopCutomTableView

+ (instancetype)popCustomTableviewWithFrame:(CGRect)frame sections:(NSArray <NSString *>*)sections current:(NSString *__nullable)currentSelect didSelectBlock:(SelectDidBlock)selectBlock {
    
    HSPopCutomTableView *view = [[HSPopCutomTableView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    view.sectionArray = sections;
    view.currentSelect = currentSelect;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:view action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = view.bounds;
    [view addSubview:btn];
    
    [view addSubview:view.tableView];
    view.selectBlock = selectBlock;
    view.tableView.frame = frame;
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.sectionHeight = 40;
        self.selectCellBgColor = kLightPurpleColor;
    }
    return self;
}
- (void)tapAction:(id *)tap {
    [self removeFromSuperview];
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] init];
        
        [_tableView registerClass:[PopCustomTableCell class] forCellReuseIdentifier:@"PopCustomTableCell"];
    }
    return _tableView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sectionArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *text = self.sectionArray[indexPath.row];
    self.currentSelect = text;
    [tableView reloadData];
    GCDAfter(0.15, ^{
        if (self.selectBlock) {
            self.selectBlock(indexPath,text,self);
        }
    });
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"PopCustomTableCell";
    PopCustomTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *text = self.sectionArray[indexPath.row];
    if (cell == nil) {
        cell = [[PopCustomTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.sectionHeight-0.5, tableView.width, 0.5)];
//        lineView.backgroundColor = [UIColor lightGrayColor];
//        [cell.contentView addSubview:lineView];
    }
//    cell.textLabel.text = [self.sectionArray objectAtIndex:indexPath.row];
//    cell.textLabel.adjustsFontSizeToFitWidth = YES;
//    cell.textLabel.minimumScaleFactor = 0.5;
//    cell.textLabel.numberOfLines = 2;
    cell.nameLabel.text = [self.sectionArray objectAtIndex:indexPath.row];
    if ([self.currentSelect isEqualToString:text]) {
        cell.backgroundColor = kLightPurpleColor;
    }else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.sectionHeight;
}



@end

@implementation PopCustomTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUi];
    }
    return self;
}
    
- (void)setupUi {
    UIView *line = [[UIView alloc] init];
    line.backgroundColor =[UIColor lightGrayColor];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
    self.lineview = line;
    
    UILabel *nameLabel = [[UILabel alloc] init];
    [self.contentView addSubview:nameLabel];
    nameLabel.adjustsFontSizeToFitWidth = YES;
    nameLabel.minimumScaleFactor = 0.5;
    nameLabel.font = [UIFont systemFontOfSize:12];
    nameLabel.textColor = kTextBlackColor;
    self.nameLabel = nameLabel;
    nameLabel.numberOfLines = 2;
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(6);
        make.top.equalTo(self.contentView).offset(3);
        make.right.equalTo(self.contentView).offset(-3);
        make.bottom.equalTo(self.contentView).offset(-0.5);
        make.height.mas_equalTo(0.5);
    }];
}

@end
