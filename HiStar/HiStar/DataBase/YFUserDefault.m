//
//  YFUserDefault.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "YFUserDefault.h"

@implementation YFUserDefault

// 1.最近登入的用户(手机号)
+ (void)saveLastLoginAccountID:(NSString *)accountID {
    
    [[NSUserDefaults standardUserDefaults] setObject:accountID forKey:@"lastLoginID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)lastLoginID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastLoginID"];
}

// 2.是否自动登录
+ (void)setAutoLogin:(BOOL)hasLogin
{
    [[NSUserDefaults standardUserDefaults] setBool:hasLogin forKey:@"hasLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (BOOL)shouldAutoLogin
{
    BOOL autoLogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"hasLogin"];
    return autoLogin;
}

// 3.显示新手指引
+ (void)setShowGuide:(BOOL)functionGuide {
    NSString *key = [NSString stringWithFormat:@"functionGuide_%@_%@",[self lastLoginID],[NSString appCurrentVersion]];
    [[NSUserDefaults standardUserDefaults] setBool:functionGuide forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (BOOL)showFunctionGuide {
    NSString *key = [NSString stringWithFormat:@"functionGuide_%@_%@",[self lastLoginID],[NSString appCurrentVersion]];
    BOOL guide = [[NSUserDefaults standardUserDefaults] boolForKey:key];
    return guide;
}

+ (void)setTestMode:(BOOL)testMode {
      [[NSUserDefaults standardUserDefaults] setBool:testMode forKey:@"testMode"];
      [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)getTestMode {
    BOOL testMode = [[NSUserDefaults standardUserDefaults] boolForKey:@"testMode"];
    return testMode;
}


@end
