//
//  HSMydesignItem.m
//  HiStar
//
//  Created by petcome on 2019/10/7.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSMydesignItem.h"
#import "YFFileManager.h"

@implementation HSMydesignItem

- (instancetype)init {
    self = [super init];
    if (self) {
        self.userId = [YFUserDefault lastLoginID];
    }
    return self;
}

+ (NSArray *)bg_unionPrimaryKeys {
    return @[@"creat_time",];
}

// 获取本地所有的我的设计
+ (NSArray *)getAllMyDesignes {
    NSArray *array = [HSMydesignItem bg_find:nil where:nil];
    return array;
}

// 删除
- (void)deleteMydesign {
    [YFFileManager removeFile:self.loadImagePath];
    NSString *where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"creat_time"),bg_sqlValue(self.creat_time)];
    [HSMydesignItem bg_delete:nil where:where];
}

- (NSString *)loadImagePath {
    NSString *document = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).lastObject;
    return [document stringByAppendingFormat:@"/myDesgin%@/%@",[YFUserDefault lastLoginID],self.imagePath];
}

@end
