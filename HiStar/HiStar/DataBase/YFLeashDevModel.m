//
//  YFLeashDevModel.m
//  Petcome
//
//  Created by petcome on 2019/1/9.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "YFLeashDevModel.h"
#import <YYKit/YYKit.h>

@implementation YFLeashDevModel


#pragma mark - 初始化和coping协议
- (instancetype)init {
    if (self = [super init]) {
        self.ringLightModel = kRingLightModelMulticolor;
    }
    return self;
}
- (id)copyWithZone:(NSZone *)zone {
    YFLeashDevModel *tempItem = [[YFLeashDevModel allocWithZone:zone] init];
    NSDictionary *data = [self modelToJSONObject];
    BOOL suc = [tempItem modelSetWithJSON:data];
    NSLog(@"%d",suc);
    return tempItem;
}

- (NSString *)openlightTimeString {
    NSString *openTime = [NSString stringWithFormat:@"%02d:%02d",self.openLightHourTime,self.openLightMinuteTime];
    return openTime;
}
- (NSString *)stoplightTimeString {
    NSString *stopTime = [NSString stringWithFormat:@"%02d:%02d",self.stopLightHourTime,self.stopLightMinuteTime];
    return stopTime;
}
- (NSString *)devCurrentTimeString {
    NSString *devCurrentTime = [NSString stringWithFormat:@"%02d:%02d",self.devCurrentHour,self.devCurrentHour];
    return devCurrentTime;
}

- (NSString *)ringLightModelString {
    NSString *modelStr = NSLocalizedString(@"Gradient/Blinking/Always bright/Colorful loop", nil);
    NSArray *modelArr = [modelStr componentsSeparatedByString:@"/"];
    switch (self.ringLightModel) {
        case kRingLightModelGradient:
            return modelArr[self.ringLightModel];//@"渐变";
        case kRingLightModelBlink:
            return modelArr[self.ringLightModel];//@"闪烁";
        case kRingLightModelAlwayLight:
            return modelArr[self.ringLightModel];//@"常亮";
        case kRingLightModelMulticolor:
            return modelArr[self.ringLightModel];//@"多彩循环";
    }
}
- (NSString *)ringLightColorString {
    NSString *colorStr = NSLocalizedString(@"White/Red/Green/Blue/Purple/Yellow/Cyan", nil);
    NSArray *colorArr = [colorStr componentsSeparatedByString:@"/"];
    return colorArr[self.ringLightColor];
}

+ (NSArray <NSString *>*)ringLightColorStingArray {
    NSString *colorStr = NSLocalizedString(@"White/Red/Green/Blue/Purple/Yellow/Cyan", nil);
    NSArray *colorArr = [colorStr componentsSeparatedByString:@"/"];
    return colorArr;
}
+ (NSArray <NSString *>*)ringLightModelStringArray {
    NSString *modelStr = NSLocalizedString(@"Gradient/Blinking/Always bright/Colorful loop", nil);
    NSArray *modelArr = [modelStr componentsSeparatedByString:@"/"];
    return modelArr;
}
- (void)setbatteryBtn:(UIButton *)btn {
    
    NSString *batterImgN = @"dev_battery_3";
    UIColor *batterColor = RGB16TOCOLOR(0x3BCF02);
    [btn setTitle:[NSString stringWithFormat:@" %ld%%",(long)self.battery] forState:UIControlStateNormal];
    if (self.battery <30) {
        batterImgN = @"dev_battery_1";
        batterColor = RGBCOLOR(239, 47, 48);;
    }else if (self.battery < 80){
        batterImgN = @"dev_battery_2";
        batterColor = RGBCOLOR(87, 184, 171);;
    }else if (self.battery <= 100) {
        batterImgN = @"dev_battery_3";
        batterColor = RGB16TOCOLOR(0x3BCF02);
    }else if (self.battery == 200){
        batterImgN = @"dev_battery_charing";
        batterColor = RGB16TOCOLOR(0x3BCF02);
        [btn setTitle:NSLocalizedString(@"Charging", nil) forState:UIControlStateNormal];
    }
    [btn setImage:[UIImage imageNamed:batterImgN] forState:UIControlStateNormal];
    [btn setTitleColor:batterColor forState:UIControlStateNormal];
}

// 遛狗时间数据
+ (NSArray <NSString *>*)walkNoticeTimes {
    NSMutableArray *arrM = [NSMutableArray array];
    for (int i=0;i <= 240;i++) {
        if (i %5 == 0) {
            [arrM addObject:@(i).stringValue];
        }
    }
    return arrM.copy;
}
#pragma mark 数据存储相关
+ (NSArray *)bg_unionPrimaryKeys {
    return @[@"mac"];
}
+ (YFLeashDevModel *)leashDevModelWithMac:(NSString *)mac {
    // 首先判断本地是否有
    NSString *where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"mac"),bg_sqlValue(mac)];
    NSArray *array = [YFLeashDevModel bg_find:nil where:where];
    if (array.count >= 1){
        return array.firstObject;
    }else {
        YFLeashDevModel *dev = [[YFLeashDevModel alloc] init];
        dev.mac = mac;
        return dev;
    }
}
- (void)updateLeashDevState:(NSDictionary *)dict {
    BOOL suc = [self modelSetWithDictionary:dict];
    if (suc) {
        [self bg_saveOrUpdate];
    }
}

@end
