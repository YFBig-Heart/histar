//
//  HSVariableItem.h
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSVariableItem : NSObject<NSCopying>

// 0用户添加,1系统自带
@property (nonatomic,assign)BOOL isSystemVar;
/** name:变量Name必须唯一 */
@property (nonatomic,copy)NSString *name;
/** value */
@property (nonatomic,assign)NSInteger value;
/** 类型:YES-256,NO-32767 */
@property (nonatomic,assign)BOOL isByteOrange;

+ (void)creatSystemVariable;

+ (instancetype)variableWithName:(NSString *)name value:(NSInteger)value isbyteOrange:(BOOL)isByteOrange;

// 获取自带的元素
+ (HSVariableItem *)getVariable1;
+ (HSVariableItem *)getVariable2;

// 获取本地所有变量
+ (NSArray *)getAllVariables;
// 0:不区分,1只需要256 2：只需要32767
+ (NSArray <NSString *>*)variablesWithByteOrange:(NSInteger)orange;

// 2.根据name获取变量
+ (HSVariableItem *)getVariableItemWithName:(NSString *)name;

// 删除
+ (void)deleteAllVariableWithName:(NSString *)name;

// 当返回Yes,代表添加/更新成功
- (BOOL)saveCustomVariableIsUpdate:(BOOL)isupdate Match:(NSString **)matchError;

@end

NS_ASSUME_NONNULL_END
