//
//  YFUserDefault.h
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YFUserDefault : NSObject

// 1.最近登入的用户(手机号)
+ (void)saveLastLoginAccountID:(NSString *)accountID;
+ (NSString *)lastLoginID;

// 2.是否自动登录
+ (void)setAutoLogin:(BOOL)hasLogin;
+ (BOOL)shouldAutoLogin;

// 3.显示新手指引
+ (void)setShowGuide:(BOOL)functionGuide;
+ (BOOL)showFunctionGuide;

// 4.测试模式
+ (void)setTestMode:(BOOL)testMode;
+ (BOOL)getTestMode;


@end

NS_ASSUME_NONNULL_END
