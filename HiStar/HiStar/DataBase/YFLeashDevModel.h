//
//  YFLeashDevModel.h
//  Petcome
//
//  Created by petcome on 2019/1/9.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YFCommunicationManager.h"
#import <BGFMDB/BGFMDB.h>//添加该头文件,本类就具有了存储功能.

NS_ASSUME_NONNULL_BEGIN

/*------------------------------------
牵引绳设备状态信息数据,当设备未连接的时候展示本地数据
 -------------------------------------*/
@interface YFLeashDevModel : NSObject<NSCopying>

// 两个字节 遛狗时长, 0~1200 单位为分钟 高位在前，003C =60 分钟
@property (nonatomic,assign)NSInteger walkTheDogTime;
//修改了 状态数据的 第 7 个字节，环灯开关值 0 关闭 1 开启,2 设置页面为开，遛弯界面为关
@property (nonatomic,assign)NSInteger ringLightSwitch;
// 一个字节 灯环模式 00 渐变.01 闪烁,02 常亮,03 多色变化 (默认模式) 当模式为 3 时，颜色参数无作用
@property (nonatomic,assign)kRingLightModel ringLightModel;
//一个字节 灯环颜色 00:白色,01:红色,02:绿色,03:蓝色,04:紫色 05:黄色 06:青色
@property (nonatomic,assign)kRingLightColor ringLightColor;
// 一个字节 来电提醒 00:关, 01:开
@property (nonatomic,assign)BOOL phoneCallNotice;
// 当前电量,200 为充电
@property (nonatomic,assign) NSInteger battery;
/** 当前状态:NO待机 YES 正在遛狗 */
@property (nonatomic,assign)BOOL isWalkingDog;
/** 亮度：1~5 五个档位 */
@property (nonatomic,assign)int lightBrightness;
/** 版本 */
@property (nonatomic,assign)NSInteger version;

/** 环灯开启时间 */
@property (nonatomic,assign)int openLightHourTime;
@property (nonatomic,assign)int openLightMinuteTime;
/** 环灯停止时间 */
@property (nonatomic,assign)int stopLightHourTime;
@property (nonatomic,assign)int stopLightMinuteTime;
/** 设备当前时间 */
@property (nonatomic,assign)int devCurrentHour;
@property (nonatomic,assign)int devCurrentMinute;

/** 设备名字 */
@property (nonatomic,copy)NSString * _Nonnull yfName;
@property (nonatomic,copy)NSString *mac;

/** 是否关闭自动自动重连 */
@property (nonatomic,assign)BOOL isCloseAutoRetryConnect;
@property (nonatomic,copy)NSString *devUuid;

- (NSString *)openlightTimeString;
- (NSString *)stoplightTimeString;
- (NSString *)devCurrentTimeString;

- (NSString *)ringLightModelString;
- (NSString *)ringLightColorString;

// 设置电量
- (void)setbatteryBtn:(UIButton *)btn;
// 遛狗时间数据
+ (NSArray <NSString *>*)walkNoticeTimes;

+ (NSArray <NSString *>*)ringLightColorStingArray;
+ (NSArray <NSString *>*)ringLightModelStringArray;

+ (YFLeashDevModel *)leashDevModelWithMac:(NSString *)mac;
// 更新并存储数据
- (void)updateLeashDevState:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
