//
//  HSVariableItem.m
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSVariableItem.h"
#import <YYKit/YYKit.h>

@implementation HSVariableItem

- (id)copyWithZone:(NSZone *)zone {
    HSVariableItem *tempItem = [[HSVariableItem allocWithZone:zone] init];
    NSDictionary *data = [self modelToJSONObject];
    BOOL suc = [tempItem modelSetWithJSON:data];
    NSLog(@"%d",suc);
    return tempItem;
}

+ (NSArray *)bg_unionPrimaryKeys {
    return @[@"name",];
}

// 获取本地所有变量
+ (NSArray *)getAllVariables {
    NSArray *array = [HSVariableItem bg_find:nil where:nil];
    return array;
}

+ (NSArray <NSString *>*)variablesWithByteOrange:(NSInteger)orange {
    NSMutableArray *arrM = [NSMutableArray array];
    for (HSVariableItem *item in [HSVariableItem getAllVariables]) {
        if (orange == 0) {
            [arrM addObject:item.name];
        }else if (orange == 1){
            if (item.isByteOrange) {
                [arrM addObject:item.name];
            }
        }else if (orange == 2){
            if (item.isByteOrange == NO) {
                [arrM addObject:item.name];
            }
        }
    }
    return arrM.copy;
}

// 2.根据name获取变量
+ (HSVariableItem *)getVariableItemWithName:(NSString *)name {
    NSString *where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"name"),bg_sqlValue(name)];
    NSArray *array = [HSVariableItem bg_find:nil where:where];
    if (array.count > 0) {
        return array.firstObject;
    }
    return nil;
}

// 删除
+ (void)deleteAllVariableWithName:(NSString *)name {
    if ([HSVariableItem getVariableItemWithName:name]) {
        NSString *where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"name"),bg_sqlValue(name)];
        [HSVariableItem bg_delete:nil where:where];
    }
}

+ (instancetype)variableWithName:(NSString *)name value:(NSInteger)value isbyteOrange:(BOOL)isByteOrange {
    HSVariableItem *item = [[HSVariableItem alloc] init];
    item.name = name;
    item.value = value;
    item.isByteOrange = isByteOrange;
    return item;
}

// 获取自带的元素
+ (HSVariableItem *)getVariable1 {
    HSVariableItem *variable1 = [HSVariableItem getVariableItemWithName:NSLocalizedString(@"variable1", nil)];
    if (variable1 == nil) {
        variable1 = [HSVariableItem variableWithName:NSLocalizedString(@"variable1", nil) value:0 isbyteOrange:YES];
        variable1.isSystemVar = YES;
        [variable1 bg_saveOrUpdate];
    }
    
    return variable1;
}

+ (HSVariableItem *)getVariable2 {
    HSVariableItem *variable2 = [HSVariableItem getVariableItemWithName: NSLocalizedString(@"variable2", nil)];
    if (variable2 == nil) {
        variable2 = [HSVariableItem variableWithName: NSLocalizedString(@"variable2", nil) value:0 isbyteOrange:YES];
        variable2.isSystemVar = YES;
        variable2.isByteOrange = NO;
        [variable2 bg_saveOrUpdate];
    }
    
    return variable2;
}

+ (void)creatSystemVariable {
    if (![self getVariableItemWithName: NSLocalizedString(@"variable1", nil)]) {
        HSVariableItem *variavle1 = [HSVariableItem variableWithName: NSLocalizedString(@"variable1", nil) value:0 isbyteOrange:YES];
        variavle1.isSystemVar = YES;
        [variavle1 bg_saveOrUpdate];
    }
    if (![self getVariableItemWithName: NSLocalizedString(@"variable2", nil)]) {
        HSVariableItem *variavle2 = [HSVariableItem variableWithName: NSLocalizedString(@"variable2", nil) value:0 isbyteOrange:YES];
        variavle2.isSystemVar = YES;
        variavle2.isByteOrange = NO;
        [variavle2 bg_saveOrUpdate];
    }
}

// 当返回Yes,代表添加成功
- (BOOL)saveCustomVariableIsUpdate:(BOOL)isupdate Match:(NSString **)matchError {
    if (isupdate == NO && [HSVariableItem getVariableItemWithName:self.name]) {
        *matchError = NSLocalizedString(@"Duplicate variable name", nil);
        return NO;
    }
    // 检查名称是否符合规格
    if (![HSVariableItem checkVariableName:self.name errorMessage:&*matchError]) {
        return NO;
    }
    [self bg_saveOrUpdate];
    return YES;
}
+ (BOOL)checkVariableName:(NSString *)string
       errorMessage:(NSString **)errorMsg
{
    //未填写
    if (![string length]) {
        if (errorMsg) {
            *errorMsg = NSLocalizedString(@"Please enter a variable name", @"请输入变量名");
        }
        return NO;
    }
    return YES;
    
    //长度不符合要求
//    NSString *regex = @"^[a-zA-Z][a-zA-Z0-9_]*$";
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
//    BOOL ismatch = [pred evaluateWithObject:string];
//    if (ismatch) {
//        return YES;
//    }else {
//        if (errorMsg) {
//            *errorMsg = NSLocalizedString(@"Variable names must start with an English letter and can only contain English letters, numbers, and underscores.", @"变量名需英文字母开头，只能包含英文字母、数字、下划线");
//        }
//        return NO;
//    }
}




@end
