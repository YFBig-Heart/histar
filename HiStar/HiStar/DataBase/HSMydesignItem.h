//
//  HSMydesignItem.h
//  HiStar
//
//  Created by petcome on 2019/10/7.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSMydesignItem : NSObject

/** UID */
@property (nonatomic,copy)NSString *userId;

// 创建的时间戳
@property (nonatomic,copy)NSString *creat_time;
/** 名称 */
@property (nonatomic,copy)NSString *designName;
/** 图片地址 */
@property (nonatomic,copy)NSString *imagePath;

// 获取本地所有的我的设计
+ (NSArray *)getAllMyDesignes;

// 删除
- (void)deleteMydesign;

- (NSString *)loadImagePath;

@end

NS_ASSUME_NONNULL_END
