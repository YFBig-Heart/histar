//
//  HSBlueSearchPerpareView.m
//  HiStar
//
//  Created by 晴天 on 2019/5/26.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSBlueSearchPerpareView.h"

@interface HSBlueSearchPerpareView ()

@property (weak, nonatomic) IBOutlet UIView *noOpenBlueView;
@property (weak, nonatomic) IBOutlet UIButton *tipQPButton;
@property (weak, nonatomic) IBOutlet UILabel *stepTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *stempThreeLabel;
@property (nonatomic,strong)UITapGestureRecognizer *tapStartScan;

@end

@implementation HSBlueSearchPerpareView

+ (instancetype)blueSearchPerpareView {
    HSBlueSearchPerpareView *view = [[NSBundle mainBundle] loadNibNamed:@"HSBlueSearchPerpareView" owner:nil options:0].firstObject;
    return view;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpUi];
}

- (void)setUpUi {
    self.stepOneLabel.textColor = RGB16TOCOLOR(0x00A0E9);
    self.stepTwoLabel.textColor = RGB16TOCOLOR(0x00A0E9);
    self.stepTitleLabel.textColor = RGB16TOCOLOR(0x00A0E9);
    self.stempThreeLabel.textColor = RGB16TOCOLOR(0x00A0E9);
    
    self.stepOneLabel.text = NSLocalizedString(@"1. Turn on mobile/pad Bluetooth", nil);
    self.stepTitleLabel.text = NSLocalizedString(@"Connection steps:", nil);
    
    self.stempThreeLabel.text = NSLocalizedString(@"3. If the blue light is flashing, please click on the screen", nil);
    NSString *stepOneStr = NSLocalizedString(@"2. Please press HiBot Bluetooth button# for 2 seconds before turning on Bluetooth.", nil);
    NSMutableAttributedString *attrM = [[NSMutableAttributedString alloc] initWithString:stepOneStr attributes:@{NSForegroundColorAttributeName:RGB16TOCOLOR(0x00A0E9)}];
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"Rectangle"];
    attach.bounds = CGRectMake(0, 0, attach.image.size.width, attach.image.size.height);
    
    NSAttributedString *imageAttr = [NSAttributedString attributedStringWithAttachment:attach];
    NSRange range = [stepOneStr rangeOfString:@"#"];
    [attrM replaceCharactersInRange:range withAttributedString:imageAttr];
    self.stepTwoLabel.attributedText = attrM;
    UITapGestureRecognizer *tapStartScan = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapStartScanPer:)];
    self.tapStartScan = tapStartScan;
    [self addGestureRecognizer:tapStartScan];
    
}
- (void)tapStartScanPer:(UITapGestureRecognizer *)tap {
    // 点击开始进入搜索小星页面
    if (self.tapStartScanPer) {
        self.tapStartScanPer();
    }
}
- (void)changePerpareViewWithIPhoneOpenBlue:(BOOL)openBlue {
    self.noOpenBlueView.hidden = openBlue;
    self.tapStartScan.enabled = openBlue;
}

@end
