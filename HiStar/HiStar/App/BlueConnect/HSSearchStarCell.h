//
//  HSSearchStarCell.h
//  HiStar
//
//  Created by 晴天 on 2019/5/26.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSSearchStarCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
