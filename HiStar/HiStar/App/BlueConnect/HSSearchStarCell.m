//
//  HSSearchStarCell.m
//  HiStar
//
//  Created by 晴天 on 2019/5/26.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSSearchStarCell.h"

@implementation HSSearchStarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
}

@end
