//
//  HSBlueSearchPerpareView.h
//  HiStar
//
//  Created by 晴天 on 2019/5/26.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface HSBlueSearchPerpareView : UIView

@property (nonatomic,copy)void (^tapStartScanPer)(void);

+ (instancetype)blueSearchPerpareView;

- (void)changePerpareViewWithIPhoneOpenBlue:(BOOL)openBlue;

@end

NS_ASSUME_NONNULL_END
