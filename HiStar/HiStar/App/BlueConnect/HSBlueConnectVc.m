//
//  HSBlueConnectVc.m
//  HiStar
//
//  Created by 晴天 on 2019/5/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSBlueConnectVc.h"
#import "HSBlueSearchPerpareView.h"
#import <Masonry/Masonry.h>
#import "HSSearchStarCell.h"

typedef enum : NSUInteger {
    kSearchStateNoBlue,// 手机蓝牙未打开
    kSearchStatePerpare,// 准备搜索蓝牙界面
    kSearchStateScaning, // 正在搜索中
    kSearchStateScanStop,// 停止搜索
} kSearchState;

@interface HSBlueConnectVc ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) HSBlueSearchPerpareView *perpareView;
@property (weak, nonatomic) IBOutlet UILabel *scanTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *reSearchBtn;
@property (weak, nonatomic) IBOutlet UILabel *connectTiplabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet UIImageView *activityImageview;
@property (weak, nonatomic) IBOutlet UIImageView *littleStarImageview;

@property (weak, nonatomic) IBOutlet UIButton *leftArrowBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightArrowBtn;
@property (nonatomic,assign)kSearchState searchState;
@property (weak, nonatomic) IBOutlet UIView *skipView;
@property (weak, nonatomic) IBOutlet UILabel *skipLabel;
@property (weak, nonatomic) IBOutlet UIImageView *skipArrowImg;

/** 搜索到蓝牙外设 */
@property (nonatomic, strong) NSArray *peripheralArray;
// 单次搜索的时间,20秒
@property (nonatomic,assign)CGFloat searchTime;

/** 当前正在连接的设备 */
@property (nonatomic,strong)YFPeripheral *currentPeripheral;

@end

@implementation HSBlueConnectVc{
    __block BOOL _shouldRefresh;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    self.skipView.hidden = YES;
    [self setUpUi];
    [self configCollectionView];
//    [self.perpareView changePerpareViewWithIPhoneOpenBlue:[YFCommunicationManager shareInstance].isCentralReady];
    [self.perpareView changePerpareViewWithIPhoneOpenBlue:YES];
    [self registerNotification];
}
- (void)dealloc {
    [self endScanPeripherals];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setUpUi {
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    if ([YFCommunicationManager shareInstance].isCentralReady == NO) {
        self.searchState = kSearchStateNoBlue;
    }else {
        self.searchState = kSearchStatePerpare;
    }
    self.skipLabel.text = NSLocalizedString(@"Skip", nil);
}

- (void)setSearchState:(kSearchState)searchState {
    _searchState = searchState;
    self.reSearchBtn.hidden = YES;
    self.leftArrowBtn.hidden = YES;
    self.rightArrowBtn.hidden = YES;
    self.littleStarImageview.hidden = YES;
    self.activityImageview.hidden = YES;
    switch (searchState) {
        case kSearchStateNoBlue:
        case kSearchStatePerpare:{
            self.perpareView.hidden = NO;
            [self.perpareView changePerpareViewWithIPhoneOpenBlue:YES];
            [self checkBlueCenter];
        }
            break;
        case kSearchStateScaning:{
            self.perpareView.hidden = YES;
            self.scanTitleLabel.text = NSLocalizedString(@"Searching for HiBots", nil);
            self.connectTiplabel.text = NSLocalizedString(@"Make sure your HiBot power is turned on", nil);
            self.leftArrowBtn.hidden = NO;
            self.rightArrowBtn.hidden = NO;
            self.collectionView.hidden = NO;
            self.activityImageview.hidden = NO;
        }
            break;
        case kSearchStateScanStop:{
            
            if (self.peripheralArray.count == 0) {
                self.scanTitleLabel.text = NSLocalizedString(@"No HiBot was found.", nil);
                NSString *tipStr = [NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"No Robot Found", nil),NSLocalizedString(@"Make sure your HiBot power is turned on", nil)];
                self.connectTiplabel.text = tipStr;
                self.collectionView.hidden = YES;
                self.littleStarImageview.hidden = NO;
//                self.skipView.hidden = NO;
                [self.reSearchBtn setTitle:NSLocalizedString(@"Re-search", nil) forState:UIControlStateNormal];
                self.reSearchBtn.hidden = NO;
            }else {
                self.scanTitleLabel.text =NSLocalizedString(@"Search for HiBots", nil);
                self.connectTiplabel.text = NSLocalizedString(@"Hold down the Little Warrior Picture Connection", nil);
                self.leftArrowBtn.hidden = self.peripheralArray.count <= 3;
                self.rightArrowBtn.hidden = self.peripheralArray.count <= 3;
                self.collectionView.hidden = NO;
                self.reSearchBtn.hidden = YES;
            }
        }
            break;
        default:
            break;
    }
}
- (IBAction)skipBtnclick:(id)sender {
    [self closeButtonClick:self.closeBtn];
}

-(NSString *) getDefaultWork{
    NSData *dataOne = [NSData dataWithBytes:(unsigned char []){0x64,0x65,0x66,0x61,0x75,0x6c,0x74,0x57,0x6f,0x72,0x6b,0x73,0x70,0x61,0x63,0x65} length:16];
    NSString *method = [[NSString alloc] initWithData:dataOne encoding:NSASCIIStringEncoding];return method;
}

- (void)startRotaionImageView {
    if (self.searchState != kSearchStateScaning) {
        self.searchTime = 0;
        return;
    }
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.05 animations:^{
        weakSelf.activityImageview.transform = CGAffineTransformRotate(weakSelf.activityImageview.transform, M_PI_4/6);
    } completion:^(BOOL finished) {
        weakSelf.searchTime += 0.05;
        if (weakSelf.searchTime > 15) {
            [weakSelf endScanPeripherals];
        }
        [weakSelf startRotaionImageView];
    }];
}

- (void)configCollectionView {
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.flowLayout.minimumLineSpacing = 0;
    self.flowLayout.minimumInteritemSpacing = 0;
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [self.collectionView registerNib:[UINib nibWithNibName:@"HSSearchStarCell" bundle:nil] forCellWithReuseIdentifier:@"HSSearchStarCell"];
}

- (void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRefreshRssi:) name:kLGCentralManagerDidRefreshRssiNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(centerPowerOnOffNoti:) name:kLGCentralManagerStatePoweredOffNotification object:nil];
}
- (void)centerPowerOnOffNoti:(NSNotification *)note {
    if ([YFCommunicationManager shareInstance].isCentralReady == NO) {
        self.peripheralArray = nil;
        [self.collectionView reloadData];
        self.searchState = kSearchStateNoBlue;
    }else {
        // 重新开始搜索
        self.searchState = kSearchStateScaning;
        [self startScanPeripherals];
    }
}
- (void)handleRefreshRssi:(NSNotification *)note {
    if (_shouldRefresh) {
        _shouldRefresh = NO;
        self.peripheralArray = [self.peripheralArray sortedArrayUsingComparator:^NSComparisonResult(YFPeripheral *obj1, YFPeripheral *obj2) {
            return obj1.lgPeripheral.countOfSignal < obj2.lgPeripheral.countOfSignal;
        }];
        [self.collectionView reloadData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self->_shouldRefresh = YES;
        });
    }
}

- (BOOL)checkBlueCenter {
    if ([YFCommunicationManager shareInstance].isCentralReady) {
        return YES;
    }
    [YFAlertHelper showAlertController:self Title:[NSString stringWithFormat:NSLocalizedString(@"Turn on bluetooth to allow \"%@\" to connect to the accessory", nil),[NSString appCurrentName]] message:@"" cancelTitle:NSLocalizedString(@"OK", nil) defalutTitle:@"" cancelAction:^(UIAlertAction * _Nonnull action) {
        if (@available(iOS 10.0, *)) {
            // @"App-Prefs:root=Bluetooth"
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{UIApplicationOpenURLOptionsSourceApplicationKey:@YES} completionHandler:^(BOOL success) {
//
//            }];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationLaunchOptionsBluetoothCentralsKey]];
        }
    } defalutAction:^(UIAlertAction * _Nonnull action) {

    }];
    return NO;
}

// 开始搜索
- (void)startScanPeripherals {
    if ([self checkBlueCenter] == NO) {
        return;
    }
    self.searchState = kSearchStateScaning;
    self.peripheralArray = @[];
    self.searchTime = 0;
    [self startRotaionImageView];
    WS(weakSelf);
    [[YFCommunicationManager shareInstance] startScanPeripheralsWithBlock:^(NSArray<YFPeripheral *> *peripherals, NSError *error) {
        weakSelf.peripheralArray = peripherals;
        [weakSelf.collectionView reloadData];
    }];
}
- (void)endScanPeripherals {
    self.searchState = kSearchStateScanStop;
    [[YFCommunicationManager shareInstance] stopScanningPeripherals];
}

- (IBAction)closeButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)researchBtnClick:(id)sender {
    [self startScanPeripherals];
}
// 切换页面
- (IBAction)leftBtnClick:(id)sender {
    if (self.collectionView.contentOffset.x <= _collectionView.width) {
      [self.collectionView setContentOffset:CGPointMake(MAX(self.collectionView.contentOffset.x-self.collectionView.width, 0), 0) animated:YES];
    }else {
//        [MBProgressHUD showTipMessageInView:@"已经是第一页" timer:1.0];
    }
}
- (IBAction)rightBtnClick:(id)sender {
    if (self.collectionView.contentOffset.x < (self.collectionView.contentSize.width -self.collectionView.width)) {
        // 不是最后一页
        [self.collectionView setContentOffset:CGPointMake(MIN(self.collectionView.contentOffset.x+self.collectionView.width, self.collectionView.contentSize.width), 0) animated:YES];
    }else {
//        [MBProgressHUD showTipMessageInView:@"已经是最后一页" timer:1.0];
    }
}
- (void)setPeripheralArray:(NSArray *)peripheralArray {
    _peripheralArray = peripheralArray;
    if (self.searchState != kSearchStateScaning) {
        return;
    }
    self.collectionView.hidden = peripheralArray.count == 0;
    self.leftArrowBtn.hidden = peripheralArray.count <= 3;
    self.rightArrowBtn.hidden = peripheralArray.count <= 3;
    if (peripheralArray.count == 0) {
        self.connectTiplabel.text = @"";
        
    }else {
        self.connectTiplabel.text = NSLocalizedString(@"Hold down the Little Warrior Picture Connection", nil);
    }
}

#pragma mark - collectionView 代理
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.peripheralArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HSSearchStarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HSSearchStarCell" forIndexPath:indexPath];
    if (self.peripheralArray.count > indexPath.item) {
        YFPeripheral *per = self.peripheralArray[indexPath.item];
        cell.nameLabel.text = per.lgPeripheral.name;
    }
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger col = MAX(self.peripheralArray.count, 1);
    col = MIN(3, col);
    CGFloat width = (collectionView.width - self.flowLayout.minimumInteritemSpacing*2)/col;
    return CGSizeMake(width, collectionView.height);
    
}
// 点击连接小海星
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (self.peripheralArray.count > indexPath.item) {
        YFPeripheral *per = self.peripheralArray[indexPath.item];
        self.currentPeripheral = per;
        [MBProgressHUD showActivityMessageInView:NSLocalizedString(@"In connection...", nil) timer:10];
        collectionView.userInteractionEnabled = NO;
        [self endScanPeripherals];
        [per connectPeripheralCompletion:^(BOOL success, NSError *error) {
            collectionView.userInteractionEnabled = YES;
            [MBProgressHUD hideHUD];
            if (success) {
                [self closeButtonClick:self.closeBtn];
            }else {
                [self startScanPeripherals];
            }
        }];
    }
}

- (HSBlueSearchPerpareView *)perpareView {
    if (!_perpareView) {
        _perpareView = [HSBlueSearchPerpareView blueSearchPerpareView];
        [self.view insertSubview:_perpareView belowSubview:self.closeBtn];
        [_perpareView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        WS(weakSelf);
        [_perpareView setTapStartScanPer:^{
            [weakSelf startScanPeripherals];
        }];
    }
    return _perpareView;
}

@end
