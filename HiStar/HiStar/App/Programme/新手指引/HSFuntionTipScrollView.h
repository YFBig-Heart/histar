//
//  HSFuntionTipScrollView.h
//  HiStar
//
//  Created by 晴天 on 2019/10/31.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSFuntionTipScrollView : UIView

//显示功能指引
+ (void)showFunctionTipView;

@end

NS_ASSUME_NONNULL_END
