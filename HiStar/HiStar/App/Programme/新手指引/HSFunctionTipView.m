//
//  HSFunctionTipOneView.m
//  HiStar
//
//  Created by 晴天 on 2019/9/22.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSFunctionTipView.h"

@interface HSFunctionTipView ()
@property (weak, nonatomic) IBOutlet UILabel *oneTipLable;
@property (weak, nonatomic) IBOutlet UIButton *oneNextBTn;

@property (weak, nonatomic) IBOutlet UILabel *twoTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *twoNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *threeTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *threeNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *fourTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *fourNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *fiveTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *fiveNextBtn;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveLabel;

@property (weak, nonatomic) IBOutlet UILabel *sixTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *sixNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *sevenTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *sevenNextBtn;
@property (weak, nonatomic) IBOutlet UIButton *varBtn;

@end

@implementation HSFunctionTipView


+ (instancetype)functionTipViewWithTag:(NSInteger)tag {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"HSFunctionTipView" owner:nil options:nil];
    for (HSFunctionTipView *tipView in views) {
        if (tipView.tag == tag) {
            return tipView;
        }
    }
    return nil;
}

//显示功能指引
+ (void)showFunctionTipView {
    HSFunctionTipView *tipView = [HSFunctionTipView functionTipViewWithTag:1];
    [tipView updateText];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    tipView.frame = window.bounds;
    [window addSubview:tipView];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)updateText {
    switch (self.tag) {
        case 1:{
            self.oneTipLable.text = NSLocalizedString(@"Click here to expand the module\ngroup classification list", nil);
            [self.oneNextBTn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        case 2:{
            self.twoTipLabel.text = NSLocalizedString(@"Drag this button to zoom in or out\non the programmed program", nil);
            [self.twoNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        case 3:{
            self.threeTipLabel.text = NSLocalizedString(@"Drag the programming module to the\narrow to place it for programming", nil);
            [self.threeNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        case 4:{
            self.fourTipLabel.text = NSLocalizedString(@"Click to download to HiBot", nil);
            [self.fourNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        case 5:{
            self.fiveTipLabel.text = NSLocalizedString(@"Open and save the programming file", nil);
            [self.fiveNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        case 6:{
            self.sixTipLabel.text = NSLocalizedString(@"Click to delete the module with the\nminus sign, then click again\nto Cancel", nil);
            [self.sixNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        case 7:{
             self.sevenTipLabel.text = NSLocalizedString(@"Variable addition and operation", nil);
            [self.varBtn setTitle:NSLocalizedString(@"Variable", nil) forState:UIControlStateNormal];
            [self.sevenNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
}

- (IBAction)nextBtnClick:(id)sender {
    if (self.tag >= 7) {
        [self removeFromSuperview];
    }else {
        HSFunctionTipView *tipView = [HSFunctionTipView functionTipViewWithTag:self.tag+1];
        [tipView updateText];
        [self removeFromSuperview];
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        tipView.frame = window.bounds;
        [window addSubview:tipView];
    }
}


@end
