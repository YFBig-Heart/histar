//
//  HSFuntionTipScrollView.m
//  HiStar
//
//  Created by 晴天 on 2019/10/31.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSFuntionTipScrollView.h"

@interface HSFuntionTipScrollView ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *oneTipLable;
@property (weak, nonatomic) IBOutlet UIButton *oneNextBTn;

@property (weak, nonatomic) IBOutlet UILabel *twoTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *twoNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *threeTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *threeNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *fourTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *fourNextBtn;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;

@property (weak, nonatomic) IBOutlet UILabel *fiveTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *fiveNextBtn;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveLabel;

@property (weak, nonatomic) IBOutlet UILabel *sixTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *sixNextBtn;

@property (weak, nonatomic) IBOutlet UILabel *sevenTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *sevenNextBtn;
@property (weak, nonatomic) IBOutlet UIButton *varBtn;


@end

@implementation HSFuntionTipScrollView


+ (instancetype)funtionTipScrollView{
    HSFuntionTipScrollView *view = [[NSBundle mainBundle] loadNibNamed:@"HSFuntionTipScrollView" owner:nil options:nil].firstObject;
    return view;
}

//显示功能指引
+ (void)showFunctionTipView {
    HSFuntionTipScrollView *tipView = [HSFuntionTipScrollView funtionTipScrollView];
    [tipView updateText];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    tipView.frame = window.bounds;
    [window addSubview:tipView];
}

- (void)awakeFromNib {
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.bounces = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.scrollEnabled = NO;
    
    for (NSInteger i = 1; i<=7; i++) {
        UIView *view = [self.scrollView viewWithTag:i];
        view.backgroundColor = [UIColor clearColor];
    }
}

- (void)updateText {
    self.oneTipLable.text = NSLocalizedString(@"Click here to expand the module\ngroup classification list", nil);
    [self.oneNextBTn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
    
    self.twoTipLabel.text = NSLocalizedString(@"Drag this button to zoom in or out\non the programmed program", nil);
    [self.twoNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
    
    self.threeTipLabel.text = NSLocalizedString(@"Drag the programming module to the\narrow to place it for programming", nil);
    [self.threeNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
    
    self.fourTipLabel.text = NSLocalizedString(@"Click to download to HiBot", nil);
    [self.fourNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
    [self.downloadBtn setTitle:NSLocalizedString(@"Download", nil) forState:UIControlStateNormal];
    
    
    self.fiveTipLabel.text = NSLocalizedString(@"Open and save the programming file", nil);
    [self.fiveNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
    self.openLabel.text = NSLocalizedString(@"Open", nil);
    self.saveLabel.text = NSLocalizedString(@"Save", nil);
    
    self.sixTipLabel.text = NSLocalizedString(@"Click to delete the module with the\nminus sign, then click again\nto Cancel", nil);
    [self.sixNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
    
    self.sevenTipLabel.text = NSLocalizedString(@"Variable addition and operation", nil);
    [self.varBtn setTitle:NSLocalizedString(@"Var", nil) forState:UIControlStateNormal];
    [self.sevenNextBtn setTitle:NSLocalizedString(@"Next", nil) forState:UIControlStateNormal];
}

- (IBAction)nextBtnClick:(UIButton *)sender {
    if (sender.tag >= 17) {
        [self removeFromSuperview];
    }else {
        self.scrollView.contentOffset = CGPointMake((sender.tag-10)*self.scrollView.width, 0);
    }
}


@end
