//
//  HSProgrammeController.h
//  HiStar
//
//  Created by petcome on 2019/5/31.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "YFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSProgrammeController : YFBaseViewController

+ (instancetype)programmeContrller;

@end

NS_ASSUME_NONNULL_END
