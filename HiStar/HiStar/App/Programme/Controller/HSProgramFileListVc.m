//
//  HSProgramFileListVc.m
//  HiStar
//
//  Created by 晴天 on 2019/7/14.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSProgramFileListVc.h"
#import "YFFileManager.h"
#import "HSProgramFileReadVc.h"

@interface HSProgramFileListVc ()

@property (nonatomic,strong)NSArray *fileArray;

@property (nonatomic,strong)NSString *fileDocument;

@end

@implementation HSProgramFileListVc

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getprogameprotolfiledatas];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}
- (BOOL)shouldAutorotate {
    return YES;
}

//支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft |UIInterfaceOrientationMaskLandscapeRight;
}
//默认方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

// 获取数据
-(void)getprogameprotolfiledatas {
    NSString *filePath = [YFFileManager getProgramProtolFilePath];
    self.fileDocument = filePath;
    NSError *error = nil;
    NSArray *fileArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath error:&error];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self endswith 'txt'"];
    NSArray *array = [fileArray filteredArrayUsingPredicate:predicate];
    self.fileArray = array;
    [self.tableView reloadData];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kYFScreenWidth, 50)];
    self.tableView.backgroundColor = kLightGrayBgColor;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"fileCell"];
    
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fileArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fileCell" forIndexPath:indexPath];
    NSString *filePath = [self.fileArray objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [filePath substringToIndex:filePath.length-4];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HSProgramFileReadVc *readVc = [[HSProgramFileReadVc alloc] initWithNibName:@"HSProgramFileReadVc" bundle:nil];
    NSString *filePath = [self.fileArray objectAtIndex:indexPath.row];
    NSString *path = [NSString stringWithFormat:@"%@/%@",self.fileDocument,filePath];
    readVc.fileText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];;
    
    [self.navigationController pushViewController:readVc animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *action = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDefault) title:NSLocalizedString(@"Delete", nil) handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        NSString *filePath = [self.fileArray objectAtIndex:indexPath.row];
        NSString *path = [NSString stringWithFormat:@"%@/%@",self.fileDocument,filePath];
        [YFFileManager removeFile:path];
        [self getprogameprotolfiledatas];
    }];
    return @[action];
    
}


@end
