//
//  HSProgramFileReadVc.h
//  HiStar
//
//  Created by 晴天 on 2019/7/14.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSProgramFileReadVc : UIViewController

@property (nonatomic,strong)NSString *fileText;

@end

NS_ASSUME_NONNULL_END
