    //
    //  HSProgrammeController.m
    //  HiStar
    //
    //  Created by petcome on 2019/5/31.
    //  Copyright © 2019 晴天. All rights reserved.
    //

#import "HSProgrammeController.h"
#import <Masonry/Masonry.h>

#import "HSTypeElementView.h"
#import "HSElememtView.h"
#import "HSConditionView.h"
#import "HSElementImageView.h"

#import "HSFuntionTipScrollView.h"

#import "HSElementAlert.h"
#import "HSTFAlertView.h"
#import "HSProgrameSaveAlert.h"
#import "HSDownloadAlertView.h"
#import "CustomSlider.h"

#import "HSProgrammerViewModel.h"
#import "HSElementModel.h"
#import "HSProgramSaveModel.h"

#import "HSProgramFileListVc.h"
#import "HSBlueConnectVc.h"


int staic_bric_id = 1;

@interface HSProgrammeController ()<HSTypeElementViewDelegate,HSConditionViewDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *variableBtn;
@property (strong, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIButton *openFileBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *leftModuleBtns;
@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fileNameLabelConH;

@property (weak, nonatomic) IBOutlet UIView *scrollSuperView;

@property (weak, nonatomic) IBOutlet UIView *fixeableView;
@property (weak, nonatomic) IBOutlet CustomSlider *sliderView;

@property (nonatomic,strong)HSProgrammerViewModel *programmerVM;
@property (nonatomic,strong)HSTypeElementView *elementView;

// 当前
@property (nonatomic,strong)HSMoudleModel *currentModel;

/** 所有的流程元素:里面是集合 */
@property (nonatomic,strong)NSMutableArray *totalElementArray;

@property (nonatomic,strong)NSMutableArray <UIImageView *>*arrowImgs;

@property (nonatomic,strong)HSElementModel *startItem;
@property (nonatomic,strong)HSElementModel *endItem;
/** 新事件 */
@property (nonatomic,strong)HSElementModel *eventnewItem;

/** 新事件的ConditionView */
@property (nonatomic,strong)HSConditionView *eventNewView;

/** 能被添加 */
@property (nonatomic,strong)UIImageView *moveAboveImg;

/** 从侧边栏移动的过来的视图 */
@property (nonatomic,strong)UIView *movingView;

/** 调整位置时移动的视图 */
@property (nonatomic,strong)HSConditionView *movingConditionView;
/** 调整位置时,初始位置 */
@property (nonatomic,assign)CGPoint movingConditionViewbeginPoint;

/** 为保持增加新元素位置不乱飘动 */
@property (nonatomic,assign)CGPoint originContentOffset;
@property (nonatomic,assign)CGSize originContentSize;

@end

@implementation HSProgrammeController

+ (instancetype)programmeContrller {
    HSProgrammeController *vc = [[HSProgrammeController alloc] initWithNibName:@"HSProgrammeController" bundle:nil];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([YFUserDefault showFunctionGuide] == NO) {
        [YFUserDefault setShowGuide:YES];
        [HSFuntionTipScrollView showFunctionTipView];
    }
    
    self.hiddenNavBar = YES;
    self.programmerVM = [HSProgrammerViewModel shareProgrammerVM];
    self.programmerVM.elementWH = 55;
    
    if (self.programmerVM.showDeleteBtn) {
        self.programmerVM.showDeleteBtn = NO;
    }
    [self setUpUi];
    [self configLocalized];
    [self registerNotification];
    
    [YFHomeHelper checkBlueConnectWithTargetVc:self];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (self.programmerVM.showDeleteBtn) {
        self.programmerVM.showDeleteBtn = NO;
    }
}

- (void)setUpUi {
    self.elementView = [HSTypeElementView typeElementView];
    [HSVariableItem creatSystemVariable];
    [self.view addSubview:self.elementView];
    [self.elementView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(15);
        make.left.equalTo(self.leftView.mas_right);
        make.width.mas_equalTo(90);
    }];
    self.elementView.delegate = self;
    self.elementView.hidden = YES;
    
    self.startItem = [HSElementModel elementItemWithMoudle:kMoudleOther funtionType:k_ft_start name:ElementString(@"start") norImage:@"start_event"];
    
    self.endItem = [HSElementModel elementItemWithMoudle:kMoudleOther funtionType:k_ft_end name:ElementString(@"end") norImage:@"end_event"];
    
    self.eventnewItem = [HSElementModel elementItemWithMoudle:kMoudleOther funtionType:k_ft_addEvent name:ElementString(@"new event") norImage:@"add_newEvent"];
    [self setUpSliderView];
    [self reDrawFlowView];
}

- (void)configLocalized {
    [self.openFileBtn setTitle:NSLocalizedString(@"Open", nil) forState:UIControlStateNormal];
    self.openFileBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.openFileBtn.titleLabel.minimumScaleFactor = 0.5;
    [self.saveBtn setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    self.saveBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.saveBtn.titleLabel.minimumScaleFactor = 0.5;
    [_downloadBtn setTitle:NSLocalizedString(@"Download", nil) forState:UIControlStateNormal];
    self.downloadBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.downloadBtn.titleLabel.minimumScaleFactor = 0.5;
    [self.variableBtn setTitle:NSLocalizedString(@"Var", nil) forState:UIControlStateNormal];
    
    NSArray *moduleSts = @[NSLocalizedString(@"Control module", nil),NSLocalizedString(@"Read module", nil),NSLocalizedString(@"Data module", nil),NSLocalizedString(@"Flow module", nil)];
    for (UIButton *btn in self.leftModuleBtns) {
        if (moduleSts.count > btn.tag - 20000) {
            [btn setTitle:moduleSts[btn.tag - 20000] forState:UIControlStateNormal];
        }
        btn.titleLabel.adjustsFontSizeToFitWidth = YES;
        btn.titleLabel.minimumScaleFactor = 0.5;
    }
}

// 设置伸缩控件
- (void)setUpSliderView {
    self.sliderView.cornerRadius = 0;
    self.sliderView.isHorizontalSlider = NO;
    self.sliderView.bgColor = [UIColor clearColor];
    self.sliderView.startColor = [UIColor clearColor];
    self.sliderView.endColor = [UIColor clearColor];
    self.sliderView.thumbImage = [UIImage imageNamed:@"programme_clycle"];
    [self.sliderView layerFilletWithRadius:self.sliderView.cornerRadius];
    [self.sliderView setSliderSpan:4 maxValue:100];
    [self.sliderView updateDrawUI];
    GCDAfter(0.25, ^{
        [self.sliderView setSliderValueAndCurrentTarget:50];
    });
    __weak typeof(self) weakSelf = self;
    [self.sliderView setGesActionValueChange:^(CGFloat value) {
        if (value < 0.5) {
            weakSelf.programmerVM.elementWH = 55 - 40 * (0.5-value);
        }else {
            weakSelf.programmerVM.elementWH = 55 + 40 * (value-0.5);
        }
        [weakSelf reDrawFlowView];
    }];
}

//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    return self.scrollContentV;
//}


#pragma mark - Notification
- (void)registerNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDeleteBtnNotice:) name:yfElementViewDeleteBtnHiddenStateChange object:nil];
}
- (void)showDeleteBtnNotice:(NSNotification *)note {
    self.deleteBtn.selected = self.programmerVM.showDeleteBtn;
}

#pragma mark - action
- (void)tapWhiteArea:(UITapGestureRecognizer *)tap {
    if (self.programmerVM.showDeleteBtn) {
            //        self.programmerVM.showDeleteBtn = NO;
    }else {
        if (self.elementView.hidden == NO) {
            self.elementView.hidden = YES;
        }
    }
}

- (IBAction)rightDownBtnsAction:(UIButton *)sender {
    self.elementView.hidden = YES;
    if (sender.tag == 10) {
            // 删除
        self.programmerVM.showDeleteBtn = !self.programmerVM.showDeleteBtn;
    }else if (sender.tag == 11){
            // 变量
        [HSVariableAlertView showVariableAlertViewWithVc:self];
    }else if (sender.tag == 12){
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)saveBtnClick:(id)sender {
    if ([YFUserDefault getTestMode]) {
            // 保存
        [HSProduceFileManager produceProgrameFileEntranceWithVc:self isSave:YES];
    }else {
        if (self.totalElementArray.count == 1 && [self.totalElementArray.firstObject count] == 0) {
            return;
        }
        [HSTFAlertView tfAlertViewTitle:NSLocalizedString(@"Please enter a file name", nil) placeholder:@"" tfText:@"" textDidChange:^(UITextField * _Nonnull text) {
            
        } btnClickBlock:^(NSInteger index, HSTFAlertView * _Nonnull view) {
            if (index == 0) {
                [view removeFromSuperview];
                return;
            }
            NSString *text = [view.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (text.length == 0) {
                [MBProgressHUD showTipMessageInView:NSLocalizedString(@"Please enter a file name", nil)];
                return;
            }
            HSProgramSaveModel *saveModel = [HSProgramSaveModel programSaveModelWithName:text];
            BOOL suc = [self.totalElementArray bg_saveArrayWithName:saveModel.saveId];
            if (suc) {
                [saveModel bg_saveOrUpdate];
                [MBProgressHUD showSuccessMessage:NSLocalizedString(@"Save Success", nil) timer:1.5];
            }
            [view removeFromSuperview];
        }];
    }
    
}

- (IBAction)openFileBtnClick:(id)sender {
    if ([YFUserDefault getTestMode]) {
        HSProgramFileListVc *fileListVc = [[HSProgramFileListVc alloc] initWithStyle:UITableViewStylePlain];
        [self.navigationController pushViewController:fileListVc animated:YES];
    }else {
        HSProgrameSaveAlert *saveAlert = [HSProgrameSaveAlert showProgramSaveAlertView];
        __weak typeof(self) weakSelf = self;
        [saveAlert setAddProgramFile:^{
            [weakSelf.totalElementArray removeAllObjects];
            weakSelf.totalElementArray = nil;
            weakSelf.fileNameLabel.text = @"";
            weakSelf.fileNameLabelConH.constant = 16;
            [weakSelf reDrawFlowView];
        }];
        [saveAlert setOpenHistoryFile:^(HSProgramSaveModel * _Nonnull model) {
                // 加载历史
            weakSelf.totalElementArray = [model getHistoryProgramElements];
            weakSelf.fileNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Current file: %@", nil),model.saveName];
            weakSelf.fileNameLabelConH.constant = 16;
            [weakSelf reDrawFlowView];
        }];
    }
}

- (IBAction)downLoadFieldBtnClick:(id)sender {
        // 下载
    YFPeripheral *per = [YFCommunicationManager shareInstance].yfPeripheral;
    if ([YFHomeHelper checkBlueConnectWithTargetVc:self]) {
        if ([self.totalElementArray.firstObject count] == 0 && self.totalElementArray.count == 1) {
                //            [MBProgressHUD showErrorMessage:@"空的程序" timer:1.5];
            return;
        }
        NSString *fileText = [HSProduceFileManager produceProgrameFileEntranceWithVc:self isSave:NO];
        NSArray *results = [HSProduceFileManager parperProgrameFileText:fileText isDownLoad:YES];
        NSData *data = results[1];
        HSDownloadAlertView *downloadView = [HSDownloadAlertView showDownloadAlertViewStart:^(HSDownloadAlertView * _Nonnull alertView,UIButton *sender) {
            sender.userInteractionEnabled = NO;
            [alertView proressAnimationTime:2 endAniBlock:^(HSDownloadAlertView * _Nonnull alertView) {
                
            }];
            [per requestOperationType:BLEOperationTypeDwonload parmaObject:nil completionBlock:^(BOOL success, id response, NSError *error) {
                sender.userInteractionEnabled = YES;
                alertView.downloadTipLabel.textColor = RGB16TOCOLOR(0x030303);
                if (success) {
                    [per requestOperationType:BLEOperationTypeDwonloadData parmaObject:data progress:^(CGFloat progress) {
                        
                    } completionBlock:^(BOOL success, id response, NSError *error) {
                        if (success) {
                            [MBProgressHUD showSuccessMessage:NSLocalizedString(@"Success", nil) timer:1.5];
                            [alertView removeFromSuperview];
                        }else {
                            alertView.downloadTipLabel.text = NSLocalizedString(@"Download failed, please try again", nil);
                            alertView.downloadTipLabel.textColor = RGB16TOCOLOR(0xE13A3A);
                            [alertView crearProgress];
                        }
                    }];
                    [per requestOperationType:BLEOperationTypeDwonloadData parmaObject:data completionBlock:^(BOOL success, id response, NSError *error) {
                        if (success) {
                            [MBProgressHUD showSuccessMessage:NSLocalizedString(@"Success", nil) timer:1.5];
                            [alertView removeFromSuperview];
                        }else {
                            alertView.downloadTipLabel.text = NSLocalizedString(@"Download failed, please try again", nil);
                            alertView.downloadTipLabel.textColor = RGB16TOCOLOR(0xE13A3A);
                            [alertView crearProgress];
                        }
                    }];
                }else {
                    alertView.downloadTipLabel.text = error.userInfo[NSLocalizedDescriptionKey];
                    alertView.downloadTipLabel.textColor = RGB16TOCOLOR(0xE13A3A);
                    [alertView crearProgress];
                }
            }];
        } cancelAction:^(HSDownloadAlertView * _Nonnull alertView,UIButton *sender) {
            
        }];
        NSString *bytesInfo = [NSString stringWithFormat:NSLocalizedString(@"The programmed size is %ld bytes", nil),(unsigned long)data.length];
        downloadView.downloadTipLabel.text = bytesInfo;
        downloadView.downloadTipLabel.textColor = RGB16TOCOLOR(0x030303); 
    }
}

- (IBAction)leftModuleBtnsAction:(UIButton *)sender {
    if (self.programmerVM.showDeleteBtn) {
        self.programmerVM.showDeleteBtn = NO;
    }
    HSMoudleModel *model = self.programmerVM.moudleModels[sender.tag - 20000];
    if (self.currentModel && model.type == self.currentModel.type) {
        self.elementView.hidden = !self.elementView.hidden;
    }else {
        self.elementView.hidden = NO;
        [self.elementView reloadDataWithMoudleModel:model];
        self.currentModel = model;
    }
}

#pragma mark - HSTypeElementViewDelegate
- (void)cellPanActionWithCell:(HSTypeElementCell *)cell longGes:(UILongPressGestureRecognizer *)longGes {
    CGPoint point = [longGes locationInView:self.view];
    switch (longGes.state) {
        case UIGestureRecognizerStateBegan:
        {
            self.movingView = [HSElememtView elementViewWithFrame:CGRectMake(0,0, kElementWH, kElementWH) model:cell.elementModel];
            self.movingView.center = point;
            [self.view addSubview:self.movingView];
            self.elementView.hidden = YES;
        }
            break;
        case UIGestureRecognizerStateChanged:{
            self.movingView.center = point;
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:{
            [self.movingView removeFromSuperview];
            CGPoint sPoint = [longGes locationInView:self.scrollView];
            HSElementModel *model = [cell.elementModel copy];
                // 判断是能有效添加
            [self arrowAnimationWithEndMovePoint:sPoint withNewModel:model longGes:longGes];
        }
            break;
        default:
            break;
    }
}

- (void)arrowAnimationWithMovePoint:(CGPoint)point {
    NSLog(@"%@",NSStringFromCGPoint(point));
    self.moveAboveImg = nil;
    for (HSElementImageView *arrow in self.arrowImgs) {
        if (point.x < arrow.centerX + 16 && point.x > arrow.centerX - 16 && point.y < arrow.bottom + 6 && point.y > arrow.top - 6) {
            arrow.transform = CGAffineTransformMakeScale(1.2, 1.2);
            self.moveAboveImg = arrow;
        }else {
            arrow.transform = CGAffineTransformIdentity;
        }
    }
}

    // 最后结束的时候看是否能被添加
- (void)arrowAnimationWithEndMovePoint:(CGPoint)point withNewModel:(HSElementModel *)elementModel longGes:(UILongPressGestureRecognizer *)longGes {
    
    if ([elementModel.funtction_type isEqualToString:k_ft_startEvent]) {
            // 新事件
        CGPoint subPoint = point;
        if ((subPoint.x < (_eventNewView.center.x + kElementWH*0.5) && (subPoint.x > _eventNewView.center.x - kElementWH*0.5) && (subPoint.y < _eventNewView.bottom + kElementWH*0.5) && subPoint.y > _eventNewView.top - kElementWH*0.5)) {
            elementModel.isIf_noBranch = NO;
            [self.totalElementArray addObject:[NSMutableArray arrayWithObject:elementModel]];
            [self reDrawFlowView];
            return;
        }
    }else {
        NSLog(@"%@",NSStringFromCGPoint(point));
        for (HSElementImageView *arrow in self.arrowImgs) {
            if ([arrow.superview isKindOfClass:[HSConditionView class]]) {
                if (arrow.if_noCondition) {
                    HSConditionView *conditionView = (HSConditionView *)arrow.superview;
                    CGPoint subPoint = [conditionView convertPoint:point fromView:self.scrollView];
                    if ((subPoint.x < arrow.centerX + 16 && subPoint.x > arrow.centerX - 16 && subPoint.y < arrow.bottom + 6 && subPoint.y > arrow.top - 6)) {
                        elementModel.isIf_noBranch = YES;
                        [conditionView.matserModel.if_noElementItems insertObject:elementModel atIndex:arrow.insetIndex];
                        [self reDrawFlowView];
                        return;
                    }
                }else {
                    HSConditionView *conditionView = (HSConditionView *)arrow.superview;
                    CGPoint subPoint = [conditionView convertPoint:point fromView:self.scrollView];
                    if ((subPoint.x < arrow.centerX + 16 && subPoint.x > arrow.centerX - 16 && subPoint.y < arrow.bottom + 6 && subPoint.y > arrow.top - 6)) {
                        elementModel.isIf_noBranch = NO;
                        [conditionView.matserModel.subElementItems insertObject:elementModel atIndex:arrow.insetIndex];
                        [self reDrawFlowView];
                        return;
                    }
                }
                
            }else {
                if ((point.x < arrow.centerX + 16 && point.x > arrow.centerX - 16 && point.y < arrow.bottom + 6 && point.y > arrow.top - 6)) {
                    NSMutableArray *array = [self.totalElementArray objectAtIndex:arrow.sectionIndex];
                    elementModel.isIf_noBranch = NO;
                    [array insertObject:elementModel atIndex:arrow.insetIndex];

                    [self reDrawFlowView];
                    
                    return;
                }
            }
        }
    }
}

#pragma mark - HSConditionViewDelegate
    // 删除流程元素
- (void)deleteConitionView:(HSConditionView *)conditionView {
    if (self.totalElementArray.count > conditionView.matserModel.sectionIndex) {
        if ([conditionView.matserModel.funtction_type isEqualToString:k_ft_startEvent]) {
            [self.totalElementArray removeObjectAtIndex:conditionView.matserModel.sectionIndex];
        }else {
            NSMutableArray *arrM = self.totalElementArray[conditionView.matserModel.sectionIndex];
            [arrM removeObject:conditionView.matserModel];
        }
        [self reDrawFlowView];
    }
}
    // 流程元素删除里面的子元素
- (void)deleteOnlySubEelementViewOrSubConitionView {
    [self reDrawFlowView];
}

- (void)conditionView:(HSConditionView *)conditionView tapElementView:(HSElememtView *)elementView {
    HSElementModel *model = elementView.elementModel;
    [HSNorAlertView showNorAlertViewWithElementModel:model inVc:self sureBtnAction:^(HSNorAlertView * _Nonnull view, UIButton * _Nonnull sender) {
        elementView.elementModel = model;
        [view disMissAlertView];
    }];
}
- (void)conditionView:(HSConditionView *)conditionView longGes:(UILongPressGestureRecognizer *)longGes {
    CGPoint point = [longGes locationInView:self.view];
    CGRect frame = [self.scrollView convertRect:conditionView.frame fromView:conditionView.superview];
    
    switch (longGes.state) {
        case UIGestureRecognizerStateBegan:
        {
            self.movingConditionViewbeginPoint = point;
            self.movingConditionView = [HSConditionView conditionViewModel:conditionView.matserModel];
            [self.scrollView addSubview:self.movingConditionView];
            self.movingConditionView.frame = frame;
            conditionView.hidden = YES;
        }
            break;
        case UIGestureRecognizerStateChanged:{
            self.movingConditionView.x = frame.origin.x + (point.x - self.movingConditionViewbeginPoint.x);
            self.movingConditionView.y = frame.origin.y + (point.y - self.movingConditionViewbeginPoint.y);
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:{
                // 判断是能有效添加
            [self movingConditionView:conditionView endMovePoint:self.movingConditionView.center longGes:longGes];
            
        }
            break;
        default:
            break;
    }
}

    // 最后结束的时候看是否能被添加
- (void)movingConditionView:(HSConditionView *)conditionView endMovePoint:(CGPoint)point longGes:(UILongPressGestureRecognizer *)longGes {
        // 找到主View
    NSLog(@"%@",NSStringFromCGPoint(point));
    HSElementModel *masterModel = conditionView.matserModel;
    
    NSMutableArray *oldElementArrM = [self.totalElementArray objectAtIndex:conditionView.matserModel.sectionIndex];
    
    for (HSElementImageView *arrow in self.arrowImgs) {
        if ([conditionView.arrowImageviews containsObject:arrow]) {
            continue;
        }
        if ([arrow.superview isKindOfClass:[HSConditionView class]]) {
            
            HSConditionView *tempConditionView = (HSConditionView *)arrow.superview;
                // 转到相同的坐标系下面
            CGRect arrowInscrFrame = [self.scrollView convertRect:arrow.frame fromView:tempConditionView];
            CGPoint subPoint = [self.scrollView convertPoint:self.movingConditionView.matserElementView.frame.origin fromView:self.movingConditionView];
            
                // 将arrow
            if ((subPoint.x < arrowInscrFrame.origin.x + 32 && subPoint.x > arrowInscrFrame.origin.x - 32 && subPoint.y < arrow.bottom + 6 && subPoint.y > arrow.top - 6)) {
                
                NSMutableArray *subElemantsArrM = nil;
                if (arrow.if_noCondition) {
                    subElemantsArrM = tempConditionView.matserModel.if_noElementItems;
                }else {
                    subElemantsArrM = tempConditionView.matserModel.subElementItems;
                }
                if ([subElemantsArrM containsObject:masterModel] == NO) {
                        // 不包含
                    [subElemantsArrM insertObject:masterModel atIndex:arrow.insetIndex];
                    if ([conditionView.superview isKindOfClass:[HSConditionView class]]) {
                        HSConditionView *superCondition = (HSConditionView *)conditionView.superview;
                        if (masterModel.isIf_noBranch) {
                            [superCondition.matserModel.if_noElementItems removeObject:masterModel];
                        }else {
                            [superCondition.matserModel.subElementItems removeObject:masterModel];
                        }
                    }else {
                        [oldElementArrM removeObject:masterModel];
                    }
                    masterModel.isIf_noBranch = arrow.if_noCondition;
                }else {
                    NSInteger index = [subElemantsArrM indexOfObject:masterModel];
                    NSInteger changeIndex = arrow.insetIndex;
                    [subElemantsArrM removeObject:masterModel];
                    if (changeIndex > index) {
                        [subElemantsArrM insertObject:masterModel atIndex:changeIndex-1];
                    }else {
                        [subElemantsArrM insertObject:masterModel atIndex:changeIndex];
                    }
                    masterModel.isIf_noBranch = arrow.if_noCondition;
                }
                break;
            }
        }else {
            if ((point.x < arrow.centerX + 16 && point.x > arrow.centerX - 16 && point.y < arrow.bottom + 6 && point.y > arrow.top - 6)) {
                
                if ([conditionView.superview isKindOfClass:[HSConditionView class]]) {
                    
                    if (self.totalElementArray.count > arrow.sectionIndex) {
                        NSMutableArray *newEelementArrM = self.totalElementArray[arrow.sectionIndex];
                        HSConditionView *superCondition = (HSConditionView *)conditionView.superview;
                        [superCondition.matserModel.subElementItems removeObject:masterModel];
                        [newEelementArrM insertObject:masterModel atIndex:arrow.insetIndex];
                        masterModel.isIf_noBranch = NO;
                    }
                }else {
                    
                    NSInteger index = [oldElementArrM indexOfObject:masterModel];
                    NSInteger changeIndex = arrow.insetIndex;
                    [oldElementArrM removeObject:masterModel];
                    
                    if (conditionView.matserModel.sectionIndex == arrow.sectionIndex) {
                        if (changeIndex > index) {
                            [oldElementArrM insertObject:masterModel atIndex:changeIndex-1];
                        }else {
                            [oldElementArrM insertObject:masterModel atIndex:changeIndex];
                        }
                        
                    }else {
                        if (self.totalElementArray.count > arrow.sectionIndex) {
                            NSMutableArray *newEelementArrM = self.totalElementArray[arrow.sectionIndex];
                            [newEelementArrM insertObject:masterModel atIndex:arrow.sectionIndex];
                        }
                    }
                }
                break;
            }
        }
    }
    [self.movingConditionView removeFromSuperview];
    [self reDrawFlowView];
}


#pragma mark - 初始化
    // 重新搭建流程UI
-(void)reDrawFlowView {
    self.originContentSize = self.scrollView.contentSize;
    self.originContentOffset = self.scrollView.contentOffset;
    [self creatScrollView];
    CGFloat last_row_y = 0;
    self.arrowImgs = [NSMutableArray array];
    self.programmerVM.allElementArray = [NSMutableArray array];
    CGFloat maxX = 0;
    for (int j = 0; j<=self.totalElementArray.count; j++) {
        NSMutableArray *arrM = [NSMutableArray array];
        if (j < self.totalElementArray.count) {
            [arrM addObjectsFromArray:self.totalElementArray[j]];
            if (j == 0) {
                [arrM insertObject:self.startItem atIndex:0];
                [arrM addObject:self.endItem];
            }else {
                HSElementModel *endItem = [HSElementModel elementItemWithMoudle:kMoudleOther funtionType:k_ft_endEvent name:ElementString(@"end event") norImage:@"end_event"];
                [arrM addObject:endItem];
                HSElememtView *startItem = [[arrM filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"funtction_type=%@",k_ft_startEvent]] firstObject];
                if (startItem) {
                    [arrM removeObject:startItem];
                    [arrM insertObject:startItem atIndex:0];
                }
            }
        }else {
            [arrM addObject:self.eventnewItem];
        }
        
        CGFloat x = 10;
        CGFloat y = 20 + last_row_y;
        CGFloat margin = 6;
            // 找出最大的高度
        CGFloat maxH = 0;
        for (HSElementModel *model in arrM) {
            CGSize size = [model calculateHeightWidth];
            if ( maxH < size.height) {
                maxH = size.height;
            }
        }
        staic_bric_id = 1;
        
        CGFloat centerY = y + maxH * 0.5;
        last_row_y = maxH + y;
        for (int i=0; i<arrM.count; i++) {
            HSElementModel *model = arrM[i];
            HSConditionView *conditionView = [HSConditionView conditionViewModel:model];
            conditionView.x =x;
            x += conditionView.width+margin;
            conditionView.centerY = centerY;
            conditionView.matserModel.sectionIndex = j;
            [self.scrollView addSubview:conditionView];
            conditionView.delegate = self;
            [_arrowImgs addObjectsFromArray:conditionView.arrowImageviews];
            
            if (i != arrM.count-1) {
                    // 创建一个箭头
                HSElementImageView *arrow = [[HSElementImageView  alloc] initWithImage:[UIImage imageNamed:@"flow_arrow"]];
                arrow.centerY = centerY;
                arrow.x = x;
                x += arrow.width+margin;
                arrow.insetIndex = i;
                arrow.sectionIndex = j;
                [self.scrollView addSubview:arrow];
                [self.arrowImgs addObject:arrow];
            }else {
                if (maxX < x) {
                    maxX = x;
                }
                if (j == self.totalElementArray.count-1) {
//                    self.scrollView.contentSize = CGSizeMake(maxX, centerY * 2 + kElementWH*2);
                }
                model.bric_id = -1; // 结束
                if (j == self.totalElementArray.count) {
                    self.eventNewView = conditionView;
                    self.scrollView.contentSize = CGSizeMake(maxX, CGRectGetMaxY(conditionView.frame) + kElementWH);
                }
            }
        }
    }
    
    self.programmerVM.vmElementTArray = self.totalElementArray;
    if (self.scrollView.contentSize.width > self.scrollView.width || self.scrollView.contentSize.height > self.scrollView.height) {
        self.scrollView.contentOffset = self.originContentOffset;
    }
}

- (NSMutableArray *)totalElementArray {
    if (!_totalElementArray) {
        _totalElementArray = [NSMutableArray arrayWithObject:[NSMutableArray array]];
    }
    return _totalElementArray;
}

- (void)creatScrollView {
    [self.scrollView removeFromSuperview];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.scrollSuperView.bounds];
//    [self.scrollView setMinimumZoomScale:0.5];
//    [self.scrollView setMinimumZoomScale:2];
//    scrollView.delegate = self;
    self.scrollView = scrollView;
    
    UITapGestureRecognizer *tapWhite = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapWhiteArea:)];
    [self.scrollView addGestureRecognizer:tapWhite];
    
    [self.scrollSuperView addSubview:scrollView];
    scrollView.backgroundColor = [UIColor clearColor];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollSuperView);
        make.width.height.equalTo(self.scrollSuperView);
    }];

}


@end
