//
//  HSProgramFileReadVc.m
//  HiStar
//
//  Created by 晴天 on 2019/7/14.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSProgramFileReadVc.h"
#import "HSProduceFileManager.h"

@interface HSProgramFileReadVc ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textviewOne;
@property (weak, nonatomic) IBOutlet UITextView *textViewTwo;

@end

@implementation HSProgramFileReadVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textviewOne.text = self.fileText;
    self.textviewOne.delegate = self;
    self.textViewTwo.delegate = self;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}
- (BOOL)shouldAutorotate {
    return YES;
}

//支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft |UIInterfaceOrientationMaskLandscapeRight;
}
//默认方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}
- (IBAction)get_data_bytesAction:(id)sender {
    NSArray *arr = [HSProduceFileManager parperProgrameFileText:self.textviewOne.text isDownLoad:NO];
    NSString *countStr = [[NSString stringWithFormat:@"%@",arr[0]] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    self.textViewTwo.text = [NSString stringWithFormat:@"%@\n\n%@",arr[1],countStr];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

@end
