//
//  HSProgrammerViewModel.h
//  HiStar
//
//  Created by 晴天 on 2019/6/3.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HSMoudleModel.h"
#import "HSProduceFileManager.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString * const yfElementViewDeleteBtnHiddenStateChange;
extern NSString * const yfElementViewTapActionNotice;

@interface HSProgrammerViewModel : NSObject

+ (instancetype)shareProgrammerVM;

- (void)updateMoudleModels;
// 模组类型
@property (nonatomic,strong)NSArray <HSMoudleModel *>*moudleModels;

/** 是否显示元素的删除按钮:默认NO */
@property (nonatomic,assign)BOOL showDeleteBtn;

/** 流程元素集合:不包含开始和结束元素,主元素和子元素抽到同一个数组里面 */
@property (nonatomic,strong)NSMutableArray *allElementArray;
/** 流程元素集合 */
@property (nonatomic,strong)NSMutableArray *vmElementTArray;

@property (nonatomic,strong)NSMutableArray <HSFlowPregram *>*programs;

// 获取开始事件元素可用的事件集-- （废弃）
- (NSArray *)getStartEventSectionsWithModel:(HSElementModel *__nullable)oldEventElement;

/*
 获取开始事件元素可用的事件集
 oldEventElement:当前元素之前选择的
 filterKeys:需要屏蔽哪些选项
 */
- (NSDictionary *)getStartEvent_dictWithModel:(HSElementModel *__nullable)oldEventElement filter:(NSArray *)filterKeys;

/** 方块的大小,默认55 */
@property (nonatomic,assign)CGFloat elementWH;

@end

NS_ASSUME_NONNULL_END
