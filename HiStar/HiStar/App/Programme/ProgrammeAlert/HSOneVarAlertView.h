//
//  HSOneVarAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSOneVarAlertView : HSNorAlertView

@property (nonatomic,strong)HSSectionTextField *varSection;
// 默认文字变量
@property (nonatomic,strong)UILabel *onelabel;

// 单一变量选择框
+ (HSOneVarAlertView *)oneAlertViewWithElement:(HSElementModel *)model sections:(NSArray *)sections;

@end

NS_ASSUME_NONNULL_END
