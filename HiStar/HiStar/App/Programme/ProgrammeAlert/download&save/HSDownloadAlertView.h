//
//  HSDownloadAlertView.h
//  HiStar
//
//  Created by petcome on 2019/10/11.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HSDownloadAlertView;
typedef void(^kDownloadActionBlock)(HSDownloadAlertView *alertView,UIButton *sender);

@interface HSDownloadAlertView : UIView

+ (instancetype)showDownloadAlertViewStart:(kDownloadActionBlock)startAction cancelAction:(kDownloadActionBlock)cancelAction;
    
@property (weak, nonatomic) IBOutlet UILabel *downloadTipLabel;

- (void)proressAnimationTime:(CGFloat)totalTime endAniBlock:(void(^)(HSDownloadAlertView *alertView))endAniBlock;
// 清除下載進度
- (void)crearProgress;

@end

NS_ASSUME_NONNULL_END
