//
//  HSProgramSaveCell.m
//  HiStar
//
//  Created by petcome on 2019/10/11.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSProgramSaveCell.h"
#import "HSProgramSaveModel.h"

@interface HSProgramSaveCell ()

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *trashBtn;
@property (weak, nonatomic) IBOutlet UIButton *pencilBtn;
@property (weak, nonatomic) IBOutlet UIImageView *addImageview;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageview;

@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

/** 当前编辑状态:0-没有编辑，1-修改名字,2-删除 */
@property (nonatomic,assign)NSInteger editState;

@property (nonatomic,strong)HSProgramSaveModel *saveModel;


@end

@implementation HSProgramSaveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.textField.userInteractionEnabled = NO;
    [self.cancelBtn setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    self.cancelBtn.backgroundColor = RGB16TOCOLOR(0xB5B5B5);
}

- (void)configProgramSvaeCellWithModel:(HSProgramSaveModel *__nullable)model isAdd:(BOOL)isadd {
    self.saveModel = model;
    self.addImageview.hidden = !isadd;
    self.pencilBtn.hidden = isadd;
    self.trashBtn.hidden = isadd;
    if (isadd) {
        self.textField.text = @"";
        self.bgImageview.image = [UIImage imageNamed:@"dash_tfBg"];
    }else {
        self.textField.text = model.saveName;
        self.bgImageview.image = [UIImage imageNamed:@"saveTableViewBg"];
    }
}

- (IBAction)pencilBtnClick:(id)sender {
    self.editState = 1;
}

- (IBAction)deleteBtnClick:(id)sender {
    self.editState = 2;
}

- (IBAction)cancelBtnClick:(id)sender {
    self.editState = 0;
}
- (IBAction)editBtnClick:(id)sender {
    if (self.editState == 1) {
        if ([NSString formatStr:self.textField.text len:20 hud:YES]) {
            self.saveModel.saveName = self.textField.text;
            [self.saveModel bg_saveOrUpdate];
            if (self.pencilBtnActionBlock) {
                self.pencilBtnActionBlock();
            }
        }else {
            self.textField.text = self.saveModel.saveName;
        }
    }else if (self.editState == 2){
        if (self.deleteBtnActionBlock) {
            self.deleteBtnActionBlock();
        }
    }
    self.editState = 0;
}

- (void)setEditState:(NSInteger)editState {
    _editState = editState;
    self.editBtn.hidden = !editState;
    self.cancelBtn.hidden = !editState;
    self.bgImageview.image = [UIImage imageNamed:@"saveTableViewBg"];
    self.textField.userInteractionEnabled = NO;
    if (editState == 0) {

    }else if (editState == 1){
        [self.editBtn setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
        self.editBtn.backgroundColor = RGB16TOCOLOR(0x13B5B1);
        self.bgImageview.image = [UIImage imageNamed:@"dash_tfBg"];
        self.textField.userInteractionEnabled = YES;
        [self.textField becomeFirstResponder];
    }else if (editState == 2){
        [self.editBtn setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateNormal];
        self.editBtn.backgroundColor = RGB16TOCOLOR(0xE13A3A);
    }
}




@end
