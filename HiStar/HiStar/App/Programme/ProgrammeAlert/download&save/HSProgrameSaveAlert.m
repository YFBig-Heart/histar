//
//  HSProgrameSaveAlert.m
//  HiStar
//
//  Created by petcome on 2019/10/11.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSProgrameSaveAlert.h"
#import "HSProgramSaveCell.h"
#import "HSProgramSaveModel.h"

@interface HSProgrameSaveAlert ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray <HSProgramSaveModel *>*dataSource;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYCon;

@end

@implementation HSProgrameSaveAlert

+ (instancetype)showProgramSaveAlertView {
    HSProgrameSaveAlert *alertView = [[NSBundle mainBundle] loadNibNamed:@"HSProgrameSaveAlert" owner:nil options:nil].firstObject;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    alertView.frame = window.bounds;
    [window addSubview:alertView];
    return alertView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpUi];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)maskBtnClick:(id)sender {
    [self endEditing:YES];
}
- (IBAction)closeBtnClick:(id)sender {
    [self endEditing:YES];
    [self removeFromSuperview];
}

- (void)setUpUi {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.bounces = NO;
    [self.tableView registerNib:[UINib nibWithNibName:@"HSProgramSaveCell" bundle:nil] forCellReuseIdentifier:@"HSProgramSaveCell"];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 60;
    self.titleLabel.text = NSLocalizedString(@"Please select the file to open", nil);

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameUpdate:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardFrameUpdate:(NSNotification *)note {
    CGFloat animationTime = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (endFrame.origin.y == kYFScreenHeight) {
        self.centerYCon.constant = 0;
    }else {
        CGFloat offset = CGRectGetMaxY(self.contentView.frame) - endFrame.origin.y;
        self.centerYCon.constant = -offset;
    }
     __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationTime animations:^{
        [weakSelf.contentView layoutIfNeeded];
    } completion:^(BOOL finished) {

    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count + 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HSProgramSaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HSProgramSaveCell" forIndexPath:indexPath];
    if (indexPath.row == 0) {
        [cell configProgramSvaeCellWithModel:nil isAdd:YES];
    }else {
        HSProgramSaveModel *model = self.dataSource[indexPath.row-1];
        [cell configProgramSvaeCellWithModel:model isAdd:NO];
        __weak typeof(self) weakSelf = self;
        [cell setPencilBtnActionBlock:^{
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
        [cell setDeleteBtnActionBlock:^{
            [weakSelf.dataSource removeObject:model];
            [model deleteSaveModel];
            [tableView reloadData];
        }];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.row == 0) {
        if (self.addProgramFile) {
            self.addProgramFile();
        }
    }else {
        HSProgramSaveModel *model = self.dataSource[indexPath.row-1];
        if (self.openHistoryFile) {
            self.openHistoryFile(model);
        }
    }
    [self removeFromSuperview];
}

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        NSArray *array = [HSProgramSaveModel allProgramSaveModels];
        _dataSource = [NSMutableArray arrayWithArray:array];
    }
    return _dataSource;
}

@end
