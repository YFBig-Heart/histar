//
//  HSProgrameSaveAlert.h
//  HiStar
//
//  Created by petcome on 2019/10/11.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HSProgramSaveModel;
@interface HSProgrameSaveAlert : UIView
    
+ (instancetype)showProgramSaveAlertView;

// 打开历史编程记录
@property (nonatomic,copy)void (^openHistoryFile)(HSProgramSaveModel *model);
// 添加一个新的
@property (nonatomic,copy)void (^addProgramFile)(void);

@end

NS_ASSUME_NONNULL_END
