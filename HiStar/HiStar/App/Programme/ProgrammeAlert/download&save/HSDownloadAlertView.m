//
//  HSDownloadAlertView.m
//  HiStar
//
//  Created by petcome on 2019/10/11.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSDownloadAlertView.h"
#import "CustomProgressBarView.h"
#import "YFHomeHelper.h"

@interface HSDownloadAlertView ()
    
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;

@property (weak, nonatomic) IBOutlet CustomProgressBarView *progressView;

@property (nonatomic,copy)kDownloadActionBlock startAction;
@property (nonatomic,copy)kDownloadActionBlock cancelAction;

@end

@implementation HSDownloadAlertView

+ (instancetype)showDownloadAlertViewStart:(kDownloadActionBlock)startAction cancelAction:(kDownloadActionBlock)cancelAction {
    HSDownloadAlertView *alertView = [[NSBundle mainBundle] loadNibNamed:@"HSDownloadAlertView" owner:nil options:nil].firstObject;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    alertView.frame = window.bounds;
    [window addSubview:alertView];
    alertView.startAction = startAction;
    alertView.cancelAction = cancelAction;
    return alertView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.progressView.colors = @[RGB16TOCOLOR(0x13B5B1),RGB16TOCOLOR(0x13B5B1)];
    [self.progressView updateProgroess:0];
    self.titleLabel.text = NSLocalizedString(@"Download programming", nil);
    self.progressLabel.text = NSLocalizedString(@"Download progress", nil);
    [self.startBtn setTitle:NSLocalizedString(@"Start download", nil) forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"Cancel download", nil) forState:UIControlStateNormal];
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self endEditing:YES];
    [self removeFromSuperview];
}
- (IBAction)maskBtnClick:(id)sender {
    
}
// 取消下载
- (IBAction)cancelBtnClick:(id)sender {
    [self removeFromSuperview];
    if (self.cancelAction) {
        self.cancelAction(self,sender);
    }
}
// 开始下载
- (IBAction)startBtnClick:(id)sender {
    if (self.startAction) {
        self.startAction(self,sender);
    }
   [self crearProgress];
}

- (void)proressAnimationTime:(CGFloat)totalTime endAniBlock:(void(^)(HSDownloadAlertView *alertView))endAniBlock {
    __weak typeof(self) weakSelf = self;
    [YFHomeHelper interiorDCDTimerManage:totalTime span:0.1 reduseTimeBlock:^(CGFloat reduseTime) {
        weakSelf.progressLabel.text = [NSString stringWithFormat:@"%@:%0.f%%",NSLocalizedString(@"Download progress", nil),(totalTime - reduseTime)/totalTime * 100];
        [weakSelf.progressView updateProgroess:(totalTime - reduseTime)/totalTime];
    } cancelBlock:^{
        
    }];

}
// 清除下載進度
- (void)crearProgress {
    self.progressLabel.text = NSLocalizedString(@"Download progress", nil);
    [self.progressView updateProgroess:0];
}


@end
