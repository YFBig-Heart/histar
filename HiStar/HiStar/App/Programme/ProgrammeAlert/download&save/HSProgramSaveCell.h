//
//  HSProgramSaveCell.h
//  HiStar
//
//  Created by petcome on 2019/10/11.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HSProgramSaveModel;
@interface HSProgramSaveCell : UITableViewCell

- (void)configProgramSvaeCellWithModel:(HSProgramSaveModel *__nullable)model isAdd:(BOOL)isadd;

@property (nonatomic,copy)void(^pencilBtnActionBlock)(void);

@property (nonatomic,copy)void(^deleteBtnActionBlock)(void);

@end

NS_ASSUME_NONNULL_END
