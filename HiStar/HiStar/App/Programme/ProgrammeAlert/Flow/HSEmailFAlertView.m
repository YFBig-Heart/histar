//
//  HSEmailFAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/10/6.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSEmailFAlertView.h"

@interface HSEmailFAlertView ()<HSSectionTextFieldDelegate>

@end
@implementation HSEmailFAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // [self creatFirstgroupOtherViews];
    }
    return self;
}

//第一组 基本
- (void)creatContentViewSubViews {
    NSDictionary *originDict = self.elementModel.convert_dict;
    
    UILabel *chooseLabel = [UILabel CreatLabelText:ElementString(@"Choose a letter") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentLeft];
    [self.contentView addSubview:chooseLabel];
    
    UILabel *sendLabel = [UILabel CreatLabelText:ElementString(@"Send a letter") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentLeft];
    [self.contentView addSubview:sendLabel];
    
    NSArray *letterArrays = [HSParserHelper all_EmailTypes];
    NSMutableArray *allLetters = [NSMutableArray array];
    for (NSArray *item in letterArrays) {
        [allLetters addObject:item[1]];
    }
    NSInteger letterIndex = [[originDict objectForKey:@"Letter"] integerValue];
    if (letterIndex < 0 || letterIndex >= allLetters.count) {
        letterIndex = 0;
    }
    HSSectionTextField *letterSection = [[HSSectionTextField alloc] init];
    letterSection.sectionArray = allLetters.copy;
    letterSection.textField.text = letterSection.sectionArray[letterIndex];
    [self.contentView addSubview:letterSection];
    letterSection.delegate = self;
    
    [chooseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(12);
    }];
    [sendLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(chooseLabel.mas_bottom).offset(60);
        make.left.equalTo(chooseLabel.mas_left);
        make.width.mas_equalTo(kAutoWid(50));
        make.height.mas_equalTo(kAutoHei(40));
    }];
    
    [letterSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(sendLabel);
        make.width.mas_equalTo(kAutoWid(80));
        make.height.mas_equalTo(kAutoHei(40));
        make.left.equalTo(sendLabel.mas_right).offset(15);
    }];
    self.emailTypeSection = letterSection;
    
}
- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.emailTypeSection]) {
        NSInteger index = [self.emailTypeSection.sectionArray indexOfObject:self.emailTypeSection.textField.text];
        NSArray *items = [[HSParserHelper all_EmailTypes] objectAtIndex:index];
        self.elementModel.norImageName = [NSString stringWithFormat:@"%@_letter",items[3]];
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    NSInteger index = [self.emailTypeSection.sectionArray indexOfObject:self.emailTypeSection.textField.text];
    [dict setObject:@(index) forKey:@"Letter"];
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}





@end
