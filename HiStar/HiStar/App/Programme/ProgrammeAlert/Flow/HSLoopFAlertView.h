//
//  HSLoopFAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSLoopFAlertView : HSNorAlertView

@property (nonatomic,strong)UIButton *selectBtnOne;
@property (nonatomic,strong)UIButton *selectBtnTwo;
@property (nonatomic,strong)UIButton *selectBtnThree;

@property (nonatomic,strong)HSSectionTextField *conditionvarSection;
@property (nonatomic,strong)HSSectionTextField *equationSection;
@property (nonatomic,strong)HSCustomTextfield *conditionValueTextField;
@property (nonatomic,strong)HSSectionTextField *eventSection;
@property (nonatomic,strong)HSSectionTextField *timeSection;

@end

NS_ASSUME_NONNULL_END
