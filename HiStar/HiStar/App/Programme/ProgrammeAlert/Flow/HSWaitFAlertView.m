//
//  HSWaitFAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSWaitFAlertView.h"

@interface HSWaitFAlertView ()<HSSectionTextFieldDelegate,UITextFieldDelegate>

/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,assign)NSInteger selectSection;

@end

@implementation HSWaitFAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // [self creatFirstgroupOtherViews];
    }
    return self;
}

//第一组 基本
- (void)creatContentViewSubViews {
    NSDictionary *originDict = self.elementModel.convert_dict;
    
    UILabel *conditionUntilLabel = [UILabel CreatLabelText:ElementString(@"Wait Until") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:conditionUntilLabel];
    
    HSCustomTextfield *delayTextField = [[HSCustomTextfield alloc] init];
    delayTextField.text = [originDict objectForKey:@"Wait_time"];
    delayTextField.keyboardType = UIKeyboardTypeDecimalPad;
    delayTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    [self.contentView addSubview:delayTextField];
    delayTextField.delegate = self;
    
    NSMutableArray *allVars = [NSMutableArray arrayWithObject:klocConstant];
    [allVars addObjectsFromArray:[HSVariableItem variablesWithByteOrange:2]];
    HSSectionTextField *conditionvarSection = [[HSSectionTextField alloc] init];
    conditionvarSection.sectionArray = allVars;
    conditionvarSection.textField.text = conditionvarSection.sectionArray.firstObject;
    [self.contentView addSubview:conditionvarSection];
    conditionvarSection.delegate = self;
    id var = [originDict objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        conditionvarSection.textField.text = varItem.name;
        self.varItem = varItem;
        delayTextField.backgroundColor = [UIColor lightGrayColor];
        delayTextField.userInteractionEnabled = NO;
    }else {
        self.varItem = nil;
        conditionvarSection.textField.text = conditionvarSection.sectionArray.firstObject;
        delayTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
        delayTextField.userInteractionEnabled = YES;
    }
    
    [self.selectBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(3);
        make.right.equalTo(delayTextField.mas_left).offset(-3);
        make.centerY.equalTo(delayTextField);
        make.height.mas_equalTo(30);
    }];
    
    [conditionUntilLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(delayTextField);
    }];
    [delayTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(conditionUntilLabel.mas_bottom).offset(10);
        make.width.mas_equalTo(kAutoWid(50));
        make.height.mas_equalTo(kAutoHei(40));
    }];
    
    [conditionvarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(delayTextField);
        make.width.mas_equalTo(kAutoWid(80));
        make.height.mas_equalTo(kAutoHei(40));
        make.left.equalTo(delayTextField.mas_right).offset(15);
    }];
    
    // 第二排
    HSSectionTextField *eventSection = [[HSSectionTextField alloc] init];
    //  @[@"倒计时",@"信号检测",@"其他小星",@"掌声检测",@"障碍检测",@"电机驱动",@"按键",@"沿轨检测"];
    NSArray *eventArray = [HSParserHelper EVENT_DICTAllkeysfilter:@[ElementString(@"Receiving letter"),ElementString(@"Music")]];
    eventSection.sectionArray = eventArray;
    
    NSString *key = [originDict objectForKey:@"Event_type"];
    eventSection.textField.text = key;
    [self.contentView addSubview:eventSection];
    eventSection.delegate = self;
    
    HSSectionTextField *timeSection = [[HSSectionTextField alloc] init];
    NSArray *timeSectionArray = [HSParserHelper getEventValueFirstWithEventKey:eventSection.textField.text eventDict:[HSParserHelper EVENT_DICT]];
    timeSection.sectionArray = timeSectionArray;
    if ([timeSection.sectionArray containsObject:[originDict objectForKey:@"Event_op"]]) {
        timeSection.textField.text = [originDict objectForKey:@"Event_op"];
    }else {
        timeSection.textField.text = timeSection.sectionArray.firstObject;
    }
    [self.contentView addSubview:timeSection];
    
    [self.selectBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(3);
        make.right.equalTo(eventSection.mas_left).offset(-3);
        make.centerY.equalTo(eventSection);
        make.height.mas_equalTo(30);
    }];
    
    [eventSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(delayTextField.mas_bottom).offset(10);
        make.left.equalTo(delayTextField);
        make.height.width.equalTo(delayTextField);
    }];
    
    [timeSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(eventSection);
        make.left.equalTo(eventSection.mas_right).offset(15);
        make.height.width.equalTo(conditionvarSection);
    }];
    
    self.conditionvarSection = conditionvarSection;
    self.delayTextField = delayTextField;
    self.eventSection = eventSection;
    self.timeSection = timeSection;
    
    if ([[originDict objectForKey:@"Section"] boolValue]) {
        [self selectBtnClick:self.selectBtnTwo];
        self.selectSection = 1;
    }else {
        [self selectBtnClick:self.selectBtnOne];
        self.selectSection = 0;
    }
    
}


- (UIButton *)selectBtnOne {
    if (!_selectBtnOne) {
        UIButton *selectBtnOne = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnOne setTitle:ElementString(@"Seconds pass") forState:UIControlStateNormal];
        [self.contentView addSubview:selectBtnOne];
        _selectBtnOne = selectBtnOne;
        [selectBtnOne setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnOne.titleLabel.font = [UIFont systemFontOfSize:12];
    }
    return _selectBtnOne;
}
- (UIButton *)selectBtnTwo {
    if (!_selectBtnTwo) {
        UIButton *selectBtnTwo = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnTwo setTitle:ElementString(@"Event happens") forState:UIControlStateNormal];
        [self.contentView addSubview:selectBtnTwo];
        [selectBtnTwo setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnTwo.titleLabel.font = [UIFont systemFontOfSize:12];
        _selectBtnTwo = selectBtnTwo;
    }
    return _selectBtnTwo;
}

- (void)selectBtnClick:(UIButton *)sender {
    
    if ([sender isEqual:_selectBtnOne]) {
        _selectBtnOne.selected = YES;
        _selectSection = 0;
        self.conditionvarSection.backgroundColor = [UIColor whiteColor];
        self.conditionvarSection.userInteractionEnabled = YES;
        
        NSInteger index = [self.conditionvarSection.sectionArray indexOfObject:self.conditionvarSection.textField.text];
        if (index == 0) {
            self.delayTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.delayTextField.userInteractionEnabled = YES;
        }else {
            self.delayTextField.backgroundColor = [UIColor lightGrayColor];
            self.delayTextField.userInteractionEnabled = NO;
        }
    }else {
        _selectBtnOne.selected = NO;
        self.conditionvarSection.backgroundColor = [UIColor lightGrayColor];
        self.conditionvarSection.userInteractionEnabled = NO;
        
        self.delayTextField.backgroundColor = [UIColor lightGrayColor];
        self.delayTextField.userInteractionEnabled = NO;
    }
    
    if ([sender isEqual:_selectBtnTwo]) {
        _selectBtnTwo.selected = YES;
        _selectSection = 1;
        self.eventSection.backgroundColor = [UIColor whiteColor];
        self.timeSection.backgroundColor = [UIColor whiteColor];
        self.eventSection.userInteractionEnabled = YES;
        self.timeSection.userInteractionEnabled = YES;
    }else {
        _selectBtnTwo.selected = NO;
        self.eventSection.backgroundColor = [UIColor lightGrayColor];
        self.timeSection.backgroundColor = [UIColor lightGrayColor];
        self.eventSection.userInteractionEnabled = NO;
        self.timeSection.userInteractionEnabled = NO;
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    //新输入的
    if (string.length == 0) {
        return YES;
    }
   //第一个参数，被替换字符串的range
   //第二个参数，即将键入或者粘贴的string
   //返回的是改变过后的新str，即textfield的新的文本内容
    NSString *checkStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //正则表达式（只支持两位小数）
    NSString *regex = @"^\\-?([1-9]\\d*|0)(\\.\\d{0,2})?$";
   //判断新的文本内容是否符合要求
    return [self isValid:checkStr withRegex:regex];
}

//检测改变过的文本是否匹配正则表达式，如果匹配表示可以键入，否则不能键入
- (BOOL) isValid:(NSString*)checkStr withRegex:(NSString*)regex
{
    NSPredicate *predicte = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicte evaluateWithObject:checkStr];
}


- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.conditionvarSection]) {
        if ([sectionTextField.textField.text isEqualToString:self.conditionvarSection.sectionArray.firstObject]) {
            self.varItem = nil;
            self.delayTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.delayTextField.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem = varItem.copy;
            self.delayTextField.backgroundColor = [UIColor lightGrayColor];
            self.delayTextField.userInteractionEnabled = NO;
        }
    }else if ([sectionTextField isEqual:self.eventSection]){
        self.timeSection.sectionArray = [HSParserHelper getEventValueFirstWithEventKey:sectionTextField.textField.text eventDict:[HSParserHelper EVENT_DICT]];
        self.timeSection.textField.text = self.timeSection.sectionArray.firstObject;
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    //    当前选中的是哪一组
    [dict setObject:@(_selectSection).stringValue forKey:@"Section"];
    
    NSString *delay = @([self.delayTextField.text floatValue]).stringValue;
    [dict setObject:delay forKey:@"Wait_time"];
    
    if ([self.varItem isKindOfClass:[HSVariableItem class]]) {
        [dict setObject:self.varItem forKey:@"Var"];
        if (self.selectBtnOne.selected == YES) {
            [self.elementModel.userVaritems addObject:self.varItem];
        }
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    
    [dict setObject:self.eventSection.textField.text forKey:@"Event_type"];
    
    [dict setObject:self.timeSection.textField.text forKey:@"Event_op"];
    
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}





@end
