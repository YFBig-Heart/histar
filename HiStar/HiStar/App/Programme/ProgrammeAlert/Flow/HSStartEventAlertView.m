//
//  HSStartEventAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/8/20.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSStartEventAlertView.h"
#import "HSProgrammerViewModel.h"

@interface HSStartEventAlertView ()<HSSectionTextFieldDelegate>

@property (nonatomic,strong)NSDictionary *event_dict;

@end

@implementation HSStartEventAlertView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
       
    }
    return self;
}

//第一组 基本
- (void)creatContentViewSubViews {
    NSDictionary *originDict = self.elementModel.convert_dict;
    
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Event happens") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    _event_dict = [[HSProgrammerViewModel shareProgrammerVM] getStartEvent_dictWithModel:self.elementModel filter:@[ElementString(@"Music"),ElementString(@"Detect clap")]];
    
    HSSectionTextField *eventSection = [[HSSectionTextField alloc] init];
    eventSection.sectionArray = _event_dict.allKeys;
    eventSection.textField.text = [originDict objectForKey:@"Event_type"];
    [self.contentView addSubview:eventSection];
    eventSection.delegate = self;
    
    HSSectionTextField *timeSection = [[HSSectionTextField alloc] init];
    NSArray *timeSectionArray = [HSParserHelper getEventValueFirstWithEventKey:eventSection.textField.text eventDict:self.event_dict];
    timeSection.sectionArray = timeSectionArray;
    if ([timeSection.sectionArray containsObject:[originDict objectForKey:@"Event_op"]]) {
        timeSection.textField.text = [originDict objectForKey:@"Event_op"];
    }else {
        timeSection.textField.text = timeSection.sectionArray.firstObject;
    }
    [self.contentView addSubview:timeSection];
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(12);
        make.top.equalTo(self.contentView).offset(20);
        make.height.mas_equalTo(30);
    }];
    
    [eventSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oneLabel.mas_bottom).offset(6);
        make.left.equalTo(self.contentView).offset(12);
        make.right.equalTo(timeSection.mas_left).offset(-15);
        make.height.mas_equalTo(kAutoHei(40));
    }];
    
    [timeSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(eventSection);
        make.left.equalTo(eventSection.mas_right).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-12);
        make.height.width.equalTo(eventSection);
    }];
    
    self.eventSection = eventSection;
    self.timeSection = timeSection;
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
     if ([sectionTextField isEqual:self.eventSection]){
        self.timeSection.sectionArray = [HSParserHelper getEventValueFirstWithEventKey:sectionTextField.textField.text eventDict:self.event_dict];
         self.timeSection.textField.text = self.timeSection.sectionArray.firstObject;
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    [dict setObject:self.eventSection.textField.text forKey:@"Event_type"];
    [dict setObject:self.timeSection.textField.text forKey:@"Event_op"];
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}



@end
