//
//  HSWaitFAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSWaitFAlertView : HSNorAlertView

@property (nonatomic,strong)UIButton *selectBtnOne;
@property (nonatomic,strong)UIButton *selectBtnTwo;

@property (nonatomic,strong)HSSectionTextField *conditionvarSection;
@property (nonatomic,strong)HSCustomTextfield *delayTextField;

@property (nonatomic,strong)HSSectionTextField *eventSection;
@property (nonatomic,strong)HSSectionTextField *timeSection;


@end

NS_ASSUME_NONNULL_END
