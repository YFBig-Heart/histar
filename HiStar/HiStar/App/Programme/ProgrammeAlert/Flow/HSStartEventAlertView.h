//
//  HSStartEventAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/8/20.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSStartEventAlertView : HSNorAlertView

@property (nonatomic,strong)HSSectionTextField *eventSection;
@property (nonatomic,strong)HSSectionTextField *timeSection;

@end

NS_ASSUME_NONNULL_END
