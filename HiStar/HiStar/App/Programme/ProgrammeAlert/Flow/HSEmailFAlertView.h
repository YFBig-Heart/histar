//
//  HSEmailFAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/10/6.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSEmailFAlertView : HSNorAlertView

@property (nonatomic,strong)HSSectionTextField *emailTypeSection;

@end

NS_ASSUME_NONNULL_END
