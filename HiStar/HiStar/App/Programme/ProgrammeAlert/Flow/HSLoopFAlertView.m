//
//  HSLoopFAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSLoopFAlertView.h"

@interface HSLoopFAlertView ()<HSSectionTextFieldDelegate>
/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,assign)NSInteger selectSection;

@end

@implementation HSLoopFAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self creatFirstgroupOtherViews];
    }
    return self;
}

//第一组 基本
- (void)creatContentViewSubViews {
    
    NSDictionary *originDict = self.elementModel.convert_dict;
    
    UILabel *conditionUntilLabel = [UILabel CreatLabelText:ElementString(@"Loop UNTIL") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:conditionUntilLabel];
    
    
    NSMutableArray *allVars = [NSMutableArray arrayWithObject:NO_VAR];
    [allVars addObjectsFromArray:[HSVariableItem variablesWithByteOrange:0]];
    HSSectionTextField *conditionvarSection = [[HSSectionTextField alloc] init];
    conditionvarSection.sectionArray = allVars;
    [self.contentView addSubview:conditionvarSection];
    
    conditionvarSection.delegate = self;
    id var = [originDict objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        conditionvarSection.textField.text = varItem.name;
        self.varItem = varItem;
    }else {
        self.varItem = nil;
        conditionvarSection.textField.text = conditionvarSection.sectionArray.firstObject;
    }
    
    
    HSSectionTextField *equationSection = [[HSSectionTextField alloc] init];
    equationSection.sectionArray = @[@"=",@"!=",@">",@"<",@"<=",@">="];
    equationSection.textField.text = [originDict objectForKey:@"Equation"];
    [self.contentView addSubview:equationSection];
    
    HSCustomTextfield *conditionValueTextField = [[HSCustomTextfield alloc] init];
    conditionValueTextField.text = [originDict objectForKey:@"Value_text"];
    conditionValueTextField.keyboardType = UIKeyboardTypeNumberPad;
    conditionValueTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    conditionValueTextField.customDelegate = self;
    [self.contentView addSubview:conditionValueTextField];
    
    //    一直循环
    [self.selectBtnThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(3);
        make.top.equalTo(conditionUntilLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(kAutoHei(40));
    }];
    
    [self.selectBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(3);
        make.right.equalTo(conditionvarSection.mas_left).offset(-3);
        make.centerY.equalTo(conditionvarSection);
        make.height.mas_equalTo(30);
    }];
    
    [conditionUntilLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(conditionvarSection);
    }];
    [conditionvarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.selectBtnThree.mas_bottom).offset(10);
        make.width.mas_equalTo(kAutoWid(65));
        make.height.mas_equalTo(kAutoHei(40));
    }];
    [equationSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(conditionvarSection);
        make.left.equalTo(conditionvarSection.mas_right).offset(15);
        make.height.equalTo(conditionvarSection);
        make.width.mas_equalTo(55);
    }];
    [conditionValueTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(conditionvarSection);
        make.left.equalTo(equationSection.mas_right).offset(15);
        make.height.equalTo(equationSection);
        make.width.mas_equalTo(60);
    }];
    
    // 第二排
    HSSectionTextField *eventSection = [[HSSectionTextField alloc] init];
//  @[@"倒计时",@"信号检测",@"其他小星",@"掌声检测",@"障碍检测",@"电机驱动",@"按键",@"沿轨检测"];
    NSArray *eventArray = [HSParserHelper EVENT_DICTAllkeysfilter:@[ElementString(@"Receiving letter"),ElementString(@"Music"),ElementString(@"Detect clap")]];
    eventSection.sectionArray = eventArray;
    
    NSString *key = [originDict objectForKey:@"Event_type"];
    eventSection.textField.text = key;
    [self.contentView addSubview:eventSection];
    eventSection.delegate = self;
    
    HSSectionTextField *timeSection = [[HSSectionTextField alloc] init];
    NSArray *timeSectionArray = [HSParserHelper getEventValueFirstWithEventKey:eventSection.textField.text eventDict:[HSParserHelper EVENT_DICT]];
    
    
    timeSection.sectionArray = timeSectionArray;
    if ([timeSection.sectionArray containsObject:[originDict objectForKey:@"Event_op"]]) {
        timeSection.textField.text = [originDict objectForKey:@"Event_op"];
    }else {
        timeSection.textField.text = timeSection.sectionArray.firstObject;
    }
    [self.contentView addSubview:timeSection];
    
    
    [self.selectBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(3);
        make.right.equalTo(eventSection.mas_left).offset(-3);
        make.centerY.equalTo(eventSection);
        make.height.mas_equalTo(30);
    }];
    
    [eventSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(conditionvarSection.mas_bottom).offset(10);
        make.left.equalTo(conditionvarSection);
        make.height.width.equalTo(conditionvarSection);
    }];
    
    [timeSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(eventSection);
        make.left.equalTo(eventSection.mas_right).offset(15);
        make.height.width.equalTo(conditionvarSection);
    }];
    
    self.conditionvarSection = conditionvarSection;
    self.equationSection = equationSection;
    self.conditionValueTextField = conditionValueTextField;
    self.eventSection = eventSection;
    self.timeSection = timeSection;
    
    if ([[originDict objectForKey:@"Section"] intValue] == 2) {
        [self selectBtnClick:self.selectBtnThree];
    }else if ([[originDict objectForKey:@"Section"] intValue] == 0){
        [self selectBtnClick:self.selectBtnOne];
    }else {
        [self selectBtnClick:self.selectBtnTwo];
    }
    
}


- (UIButton *)selectBtnOne {
    if (!_selectBtnOne) {
        UIButton *selectBtnOne = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnOne setTitle:ElementString(@"Test passes") forState:UIControlStateNormal];
        [self.contentView addSubview:selectBtnOne];
        _selectBtnOne = selectBtnOne;
        [selectBtnOne setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnOne.titleLabel.font = [UIFont systemFontOfSize:12];
    }
    return _selectBtnOne;
}
- (UIButton *)selectBtnTwo {
    if (!_selectBtnTwo) {
        UIButton *selectBtnTwo = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnTwo setTitle:ElementString(@"Event happens") forState:UIControlStateNormal];
        [self.contentView addSubview:selectBtnTwo];
        [selectBtnTwo setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnTwo.titleLabel.font = [UIFont systemFontOfSize:12];
        _selectBtnTwo = selectBtnTwo;
    }
    return _selectBtnTwo;
}

- (UIButton *)selectBtnThree {
    if (!_selectBtnThree) {
        UIButton *selectBtnThree = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnThree setTitle:ElementString(@"loop forever") forState:UIControlStateNormal];
        [self.contentView addSubview:selectBtnThree];
        [selectBtnThree setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnThree.titleLabel.font = [UIFont systemFontOfSize:12];
        _selectBtnThree = selectBtnThree;
    }
    return _selectBtnThree;
}


- (void)selectBtnClick:(UIButton *)sender {
    
    if ([sender isEqual:_selectBtnOne]) {
        _selectBtnOne.selected = YES;
        _selectSection = 0;
        self.conditionvarSection.backgroundColor = [UIColor whiteColor];
        self.conditionvarSection.userInteractionEnabled = YES;
        
        self.equationSection.backgroundColor = [UIColor whiteColor];
        self.equationSection.userInteractionEnabled = YES;
        
        self.conditionValueTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
        self.conditionValueTextField.userInteractionEnabled = YES;
        
    }else {
        _selectBtnOne.selected = NO;
        self.conditionvarSection.backgroundColor = [UIColor lightGrayColor];
        self.conditionvarSection.userInteractionEnabled = NO;
        
        self.equationSection.backgroundColor = [UIColor lightGrayColor];
        self.equationSection.userInteractionEnabled = NO;
        
        self.conditionValueTextField.backgroundColor = [UIColor lightGrayColor];
        self.conditionValueTextField.userInteractionEnabled = NO;
        
    }
    
    if ([sender isEqual:_selectBtnTwo]) {
        _selectBtnTwo.selected = YES;
        _selectSection = 1;
        self.eventSection.backgroundColor = [UIColor whiteColor];
        self.timeSection.backgroundColor = [UIColor whiteColor];
        self.eventSection.userInteractionEnabled = YES;
        self.timeSection.userInteractionEnabled = YES;
    }else {
        _selectBtnTwo.selected = NO;
        self.eventSection.backgroundColor = [UIColor lightGrayColor];
        self.timeSection.backgroundColor = [UIColor lightGrayColor];
        self.eventSection.userInteractionEnabled = NO;
        self.timeSection.userInteractionEnabled = NO;
    }
    
    if ([sender isEqual:_selectBtnThree]) {
        _selectBtnThree.selected = YES;
        _selectSection = 2;
    }else {
        _selectBtnThree.selected = NO;
    }
    
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.eventSection]){
        self.timeSection.sectionArray = [HSParserHelper getEventValueFirstWithEventKey:sectionTextField.textField.text eventDict:[HSParserHelper EVENT_DICT]];
        self.timeSection.textField.text = self.timeSection.sectionArray.firstObject;
    }else if ([sectionTextField isEqual:self.conditionvarSection]){
        
        if ([sectionTextField.textField.text isEqualToString:self.conditionvarSection.sectionArray.firstObject]) {
            self.varItem = nil;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem = varItem.copy;
        }
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    //    当前选中的是哪一组
    [dict setObject:@(_selectSection).stringValue forKey:@"Section"];
    
    if ([self.varItem isKindOfClass:[HSVariableItem class]]) {
        [dict setObject:self.varItem forKey:@"Var"];
        if (self.selectBtnOne.selected == YES) {
            [self.elementModel.userVaritems addObject:self.varItem];
        }
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    
    NSString *delay = @([self.conditionValueTextField.text integerValue]).stringValue;
    [dict setObject:delay forKey:@"Value_text"];
    [dict setObject:self.equationSection.textField.text forKey:@"Equation"];
    
    
    [dict setObject:self.eventSection.textField.text forKey:@"Event_type"];
    
    [dict setObject:self.timeSection.textField.text forKey:@"Event_op"];
    
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
    
}




@end
