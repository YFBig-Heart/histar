//
//  HSOneVarAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSOneVarAlertView.h"

@interface HSOneVarAlertView ()<HSSectionTextFieldDelegate>

/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,strong)NSArray *varSections;

@end

@implementation HSOneVarAlertView

// 单一变量选择框
+ (HSOneVarAlertView *)oneAlertViewWithElement:(HSElementModel *)model sections:(NSArray *)sections {
    HSOneVarAlertView *alertView = [[HSOneVarAlertView alloc] init];
    
    alertView.varSection.sectionArray = sections;
    alertView.elementModel = model;
    alertView.varSections = sections;
    
    return alertView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self creatLightAlertView];
    }
    return self;
}
- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    NSString *lableText = klocVariable;
    if ([self.elementModel.codeName isEqualToString:@"Increment"]) {
        lableText = ElementString(@"Plus 1 to variable");
    }else if ([self.elementModel.codeName isEqualToString:@"Decrement"]){
        lableText = ElementString(@"Minus 1 from variable");
    }
    
    // 第一排
    UILabel *oneLabel = [UILabel CreatLabelText:lableText bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    HSSectionTextField *varSection = [[HSSectionTextField alloc] init];
    varSection.sectionArray = self.varSections;
    [varSection layerFilletWithRadius:2];
    varSection.delegate = self;
    [self.contentView addSubview:varSection];
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        varSection.textField.text = varItem.name;
        self.varItem = varItem;
    }else {
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:varSection.sectionArray.firstObject];
        self.varItem = varItem;
        varSection.textField.text = varSection.sectionArray.firstObject;
    }
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(varSection);
        make.width.mas_lessThanOrEqualTo(80);
    }];
    
    [varSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(65));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    self.varSection = varSection;

}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.varSection] ) {
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
        self.varItem = varItem;
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    if (self.varItem) {
        [dict setObject:self.varItem forKey:@"Var"];
        [self.elementModel.userVaritems addObject:self.varItem];
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}



@end
