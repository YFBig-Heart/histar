//
//  HSVariableCell.m
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSVariableCell.h"

@interface HSVariableCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLable;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (nonatomic,assign)BOOL isHeadCell;

@property (nonatomic,strong)HSVariableItem *item;

@end
@implementation HSVariableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}
// 编辑按钮
- (IBAction)editBtnClick:(id)sender {
    if (self.editBtnClickBlock) {
        self.editBtnClickBlock(self.item);
    }
}
- (void)isheadCell:(BOOL)isHeadCell {
    self.isHeadCell = isHeadCell;
    UIView *topLine = [self.contentView viewWithTag:123];
    topLine.hidden = !isHeadCell;
    if (isHeadCell) {
        self.editBtn.enabled = NO;
        [self.editBtn setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
        [self.editBtn setImage:[UIImage new] forState:UIControlStateNormal];
        [self.editBtn setImage:[UIImage new] forState:UIControlStateDisabled];
        self.nameLabel.text = NSLocalizedString(@"Parameter", nil);
        self.valueLable.text = NSLocalizedString(@"Initial Value", nil);
        self.rangeLabel.text = NSLocalizedString(@"Range", nil);
        self.backgroundColor = RGB16TOCOLOR(0xC5F1FA);
    }else {
        [self.editBtn setTitle:@"" forState:UIControlStateNormal];
        [self.editBtn setImage:[UIImage new] forState:UIControlStateNormal];
        self.nameLabel.text = @"";
        self.valueLable.text = @"";
        self.rangeLabel.text = @"";
        self.editBtn.enabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self.editBtn setImage:[UIImage imageNamed:@"variable_pencilUnable"] forState:UIControlStateDisabled];
        [self.editBtn setImage:[UIImage imageNamed:@"variable_pancil"] forState:UIControlStateNormal];
    }
}
- (void)configVariable:(HSVariableItem *)item {
    if (self.isHeadCell) {
        return;
    }
    self.editBtn.enabled = !item.isSystemVar;
    self.nameLabel.text = item.name;
    self.rangeLabel.text = item.isByteOrange ? @"0-255":@"+/-32767";
    self.valueLable.text = @(item.value).stringValue;
    self.item = item;
}


@end
