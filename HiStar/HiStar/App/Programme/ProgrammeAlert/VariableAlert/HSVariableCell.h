//
//  HSVariableCell.h
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSVariableCell : UITableViewCell

- (void)isheadCell:(BOOL)isHeadCell;

- (void)configVariable:(HSVariableItem *)item;


@property (nonatomic,copy)void (^editBtnClickBlock)(HSVariableItem *item);

@end

NS_ASSUME_NONNULL_END
