//
//  HSVariableAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSVariableAlertView : UIView

+ (instancetype)showVariableAlertViewWithVc:(UIViewController *)vc;

@end

NS_ASSUME_NONNULL_END
