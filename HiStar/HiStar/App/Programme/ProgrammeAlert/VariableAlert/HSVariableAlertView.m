//
//  HSVariableAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/16.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSVariableAlertView.h"
#import "HSSectionTextField.h"
#import "HSVariableCell.h"
#import "HSCustomTextfield.h"

@interface HSVariableAlertView ()<UITableViewDelegate,UITableViewDataSource,HSCustomTextfieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *oneView;
@property (weak, nonatomic) IBOutlet UILabel *oneTitleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *oneViewSureButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneViewCenterYCon;

@property (weak, nonatomic) IBOutlet UIView *setVariableView;
@property (weak, nonatomic) IBOutlet UILabel *twoTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *veriableNameLabel;
@property (weak, nonatomic) IBOutlet HSCustomTextfield *variableNameTex;
@property (weak, nonatomic) IBOutlet UILabel *variableTypeLabel;
@property (weak, nonatomic) IBOutlet HSSectionTextField *variableTypeSectionText;
@property (weak, nonatomic) IBOutlet UILabel *variableValueLabel;
@property (weak, nonatomic) IBOutlet HSCustomTextfield *variableValueTex;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (nonatomic,strong)NSMutableArray *variableItems;
// 当前修改变量
@property (nonatomic,strong)HSVariableItem *currentChangeVarItem;

@end

@implementation HSVariableAlertView

+ (instancetype)showVariableAlertViewWithVc:(UIViewController *)vc {
    HSVariableAlertView *view = [[NSBundle mainBundle] loadNibNamed:@"HSVariableAlertView" owner:nil options:nil].firstObject;
    if (vc == nil) {
        vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    }
    view.frame = vc.view.bounds;
    [vc.view addSubview:view];
    return view;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpUi];
    [self configLocalized];
}
- (IBAction)coverButtonClick:(id)sender {
    [self endEditing:YES];
}
- (void)setUpUi {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"HSVariableCell" bundle:nil] forCellReuseIdentifier:@"HSVariableCell"];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.rowHeight = 40;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.variableTypeSectionText.sectionArray = @[@"0-255",@"+/-32767"];
    self.variableTypeSectionText.textField.text = @"0-255";
    self.variableNameTex.customDelegate = self;
    self.variableValueTex.customDelegate = self;
    self.variableTypeSectionText.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
}
- (void)configLocalized {
    self.oneTitleLabel.text = NSLocalizedString(@"Modify variable", nil);
    [self.addBtn setTitle:NSLocalizedString(@"Add", nil) forState:UIControlStateNormal];
    [self.oneViewSureButton setTitle:NSLocalizedString(@"Ok", nil) forState:UIControlStateNormal];
    
    self.twoTitleLabel.text = NSLocalizedString(@"New variable", nil);
    
    self.veriableNameLabel.text = NSLocalizedString(@"Variable name", nil);
    self.variableTypeLabel.text = NSLocalizedString(@"Range of variables", nil);
    self.variableValueLabel.text = NSLocalizedString(@"Initial value (optional)", nil);
    [self.deleteBtn setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [self.sureAddBtn setTitle:NSLocalizedString(@"Confirm", nil) forState:UIControlStateNormal];
    
}
    
- (void)textFieldTextDidChange:(NSNotification *)note {
    [self updateAddBtnState];
}
- (void)customTextField:(HSCustomTextfield *)textField offSetY:(CGFloat)offsetY keyboradShow:(BOOL)show animiationTime:(CGFloat)animationTime {
    self.oneViewCenterYCon.constant = show ? -offsetY:0;
    [UIView animateWithDuration:animationTime animations:^{
        [self setNeedsLayout];
    } completion:^(BOOL finished) {
        
    }];
}
- (void)cusomTextFiledShouldEndEditing:(HSCustomTextfield *)textField {
    self.oneViewCenterYCon.constant = 0;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.variableItems.count+1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HSVariableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HSVariableCell" forIndexPath:indexPath];
    [cell isheadCell:indexPath.row == 0];
    if (indexPath.row != 0 ) {
        if (self.variableItems.count > indexPath.row-1) {
            HSVariableItem *item = self.variableItems[indexPath.row-1];
            [cell configVariable:item];
        }
    }
    __weak typeof(self) weakSelf = self;
    [cell setEditBtnClickBlock:^(HSVariableItem * _Nonnull item) {
        // 展示
        [weakSelf showAddOrUpdateViewWithItem:item];
    }];
    return cell;
}
- (void)showAddOrUpdateViewWithItem:(HSVariableItem *)item {
    self.setVariableView.hidden = NO;
    if (item) {
        self.currentChangeVarItem = item;
        self.twoTitleLabel.text = NSLocalizedString(@"Modify variable", nil);
        self.deleteBtn.enabled = YES;
        self.deleteBtn.backgroundColor = RGB16TOCOLOR(0x9083BE);
        self.variableNameTex.text = item.name;
        self.variableValueTex.text = @(item.value).stringValue;
    }else {
        self.twoTitleLabel.text = NSLocalizedString(@"New variable", nil);
        self.deleteBtn.enabled = NO;
        self.deleteBtn.backgroundColor = RGB16TOCOLOR(0xd3d3d3);
        self.variableNameTex.text = @"";
        self.variableValueTex.text = @"";
        self.currentChangeVarItem = nil;
    }
    [self updateAddBtnState];
}
    
#pragma mark - action
- (IBAction)colseBtnClick:(id)sender {
    [self removeFromSuperview];
}
// 新增变量
- (IBAction)addBtnClick:(id)sender {
    [self showAddOrUpdateViewWithItem:nil];
}
- (IBAction)oneSureBtnClick:(id)sender {
    [self removeFromSuperview];
}
- (IBAction)deleteBtnClick:(id)sender {
    [self.variableItems containsObject:self.currentChangeVarItem];
    [HSVariableItem deleteAllVariableWithName:self.currentChangeVarItem.name];
    [self.variableItems removeObject:self.currentChangeVarItem];
    [self.tableView reloadData];
    self.setVariableView.hidden = YES;
}
- (IBAction)sureAddBtnClick:(id)sender {
    // 添加
    if (self.currentChangeVarItem) {
        // 更新
        [HSVariableItem deleteAllVariableWithName:self.currentChangeVarItem.name];
        [self.variableItems removeObject:self.currentChangeVarItem];
        self.currentChangeVarItem.value = [self.variableValueTex.text integerValue];
        self.currentChangeVarItem.name = self.variableNameTex.text;
        self.currentChangeVarItem.isByteOrange = [self.variableTypeSectionText.textField.text isEqualToString:@"0-255"];
        NSString *errorMsg;
        if (![self.currentChangeVarItem saveCustomVariableIsUpdate:YES Match:&errorMsg]) {
            [MBProgressHUD showErrorMessage:errorMsg timer:1.5];
        }else {
            [self.variableItems addObject:self.currentChangeVarItem];
            [self.tableView reloadData];
            self.setVariableView.hidden = YES;
            self.currentChangeVarItem = nil;
        }
    }else {
        HSVariableItem *item = [HSVariableItem variableWithName:self.variableNameTex.text value:[self.variableValueTex.text integerValue] isbyteOrange:[self.variableTypeSectionText.textField.text isEqualToString:@"0-255"]];
        NSString *errorMsg;
        if (![item saveCustomVariableIsUpdate:NO Match:&errorMsg]) {
            [MBProgressHUD showErrorMessage:errorMsg timer:1.5];
        }else {
            [self.variableItems addObject:item];
            [self.tableView reloadData];
            self.setVariableView.hidden = YES;
        }
    }
}
    
- (void)updateAddBtnState {
    if (self.variableTypeSectionText.textField.text.length * self.variableNameTex.text.length *self.variableValueTex.text.length > 0) {
        self.sureAddBtn.enabled = YES;
        self.sureAddBtn.backgroundColor = RGB16TOCOLOR(0x9083BE);
    }else {
        self.sureAddBtn.enabled = NO;
        self.sureAddBtn.backgroundColor = RGB16TOCOLOR(0xd3d3d3);
    }
}
    
// 取消
- (IBAction)cancelBtnClick:(id)sender {
    self.setVariableView.hidden = YES;
}
    
- (NSMutableArray *)variableItems {
    if (!_variableItems) {
        _variableItems = [NSMutableArray arrayWithArray:[HSVariableItem getAllVariables]];
    }
    return _variableItems;
}


@end
