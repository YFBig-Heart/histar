//
//  HSByteDAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/25.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSByteDAlertView.h"

@interface HSByteDAlertView ()<HSSectionTextFieldDelegate,HSCustomTextfieldDelegate>

@property (nonatomic,strong)HSVariableItem *varItem1;
@property (nonatomic,strong)HSVariableItem *varItemBase;
@property (nonatomic,strong)HSVariableItem *varItemDivide;// 除和位移
@property (nonatomic,strong)HSVariableItem *varItemLogical; //逻辑里面的变量

// 当前是哪一组:0,1,2
@property (nonatomic,assign)NSInteger sectionIndex;

@end

@implementation HSByteDAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self creatFirstgroupOtherViews];
//        [self creatSecondgroupOtherViewsn];
//        [self creatThirdgroupOtherViewsn];
    }
    return self;
}

- (void)creatContentViewSubViews {
    [self creatFirstgroupOtherViews];
    [self creatSecondgroupOtherViewsn];
    [self creatThirdgroupOtherViewsn];
}

//第一组 基本
- (void)creatFirstgroupOtherViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    UILabel *oneLabel = [UILabel CreatLabelText:klocVariable bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:oneLabel];
    // 字变量 数组
    NSArray *characterVarArr = [HSVariableItem variablesWithByteOrange:1];
    HSSectionTextField *oneVarSection = [[HSSectionTextField alloc] init];
    oneVarSection.sectionArray = characterVarArr;
    [self.scrollView addSubview:oneVarSection];
    oneVarSection.delegate = self;
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]] == NO) {
        self.varItem1 = [HSVariableItem getVariable1];
    }else {
        self.varItem1 = var;
    }
    oneVarSection.textField.text = self.varItem1.name;
    
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.selectBtnOne.mas_right).offset(10);
        make.centerY.equalTo(oneVarSection);
    }];
    [oneVarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(60));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(self.scrollView).offset(10);
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    //    第二排
    UILabel *twoLabel = [UILabel CreatLabelText:ElementString(@"Operation") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:twoLabel];
    
    
    HSSectionTextField *operatorGroup1Section = [[HSSectionTextField alloc] init];
    operatorGroup1Section.sectionArray = @[ElementString(@"plus"),ElementString(@"minus"),ElementString(@"multiply"),ElementString(@"not(bitwise)")];
    [self.scrollView addSubview:operatorGroup1Section];
    if ([[HSParserHelper basic_ops] containsObject:[originValue objectForKey:@"Basic_op"]]) {
        NSInteger index = [[HSParserHelper basic_ops] indexOfObject:[originValue objectForKey:@"Basic_op"]];
        operatorGroup1Section.textField.text = operatorGroup1Section.sectionArray[index];
    }else {
        operatorGroup1Section.textField.text = operatorGroup1Section.sectionArray.firstObject;
    }
    
    
    [self.selectBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(twoLabel);
        make.left.equalTo(self.scrollView).offset(3);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(65);
    }];
    
    [twoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(oneLabel);
        make.centerY.equalTo(operatorGroup1Section);
    }];
    
    [operatorGroup1Section mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(oneVarSection);
        make.top.equalTo(oneVarSection.mas_bottom).offset(10);
        make.left.equalTo(twoLabel.mas_right).offset(10);
    }];
    
    //    第三排
    UILabel *threeLabel = [UILabel CreatLabelText:ElementString(@"Argument") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:threeLabel];
    
    HSCustomTextfield *argGroup1TextField = [[HSCustomTextfield alloc] init];
    argGroup1TextField.text = [originValue objectForKey:@"Basic_arg"];
    argGroup1TextField.keyboardType = UIKeyboardTypeNumberPad;
    argGroup1TextField.textColor = RGB16TOCOLOR(0x030303);
    argGroup1TextField.font = [UIFont systemFontOfSize:14];
    argGroup1TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    [self.scrollView addSubview:argGroup1TextField];
    argGroup1TextField.customDelegate = self;
    
    
    NSMutableArray *allVars = [NSMutableArray arrayWithObject:klocConstant];
    [allVars addObjectsFromArray:[HSVariableItem variablesWithByteOrange:1]];
    HSSectionTextField *twoVarSection = [[HSSectionTextField alloc] init];
    twoVarSection.sectionArray = allVars;
    [twoVarSection layerFilletWithRadius:2];
    [self.scrollView addSubview:twoVarSection];
    twoVarSection.delegate = self;
    id var_basic = [originValue objectForKey:@"Basic_var"];
    if ([var_basic isKindOfClass:[HSVariableItem class]]) {
        self.varItemBase = var_basic;
        twoVarSection.textField.text = self.varItemBase.name;
        argGroup1TextField.backgroundColor = [UIColor lightGrayColor];
        argGroup1TextField.userInteractionEnabled = NO;
    }else {
        self.varItemBase = nil;
        twoVarSection.textField.text = twoVarSection.sectionArray.firstObject;
        argGroup1TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
        argGroup1TextField.userInteractionEnabled = YES;
    }
    
    
    [threeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(oneLabel);
        make.centerY.equalTo(argGroup1TextField);
    }];
    
    [argGroup1TextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(oneVarSection);
        make.top.equalTo(operatorGroup1Section.mas_bottom).offset(10);
        make.left.equalTo(threeLabel.mas_right).offset(10);
    }];
    
    [twoVarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(oneVarSection);
        make.centerY.equalTo(argGroup1TextField);
        make.left.equalTo(argGroup1TextField.mas_right).offset(10);
    }];
    
    self.oneLabel = oneLabel;
    self.oneVarSection = oneVarSection;
    self.operatorGroup1Section = operatorGroup1Section;
    self.argGroup1TextField = argGroup1TextField;
    self.twoVarSection = twoVarSection;
    
    
}
// 第二组 除/移位
- (void)creatSecondgroupOtherViewsn {
    UILabel *fourLabel = [UILabel CreatLabelText:ElementString(@"Operation") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:fourLabel];
    
    
    HSSectionTextField *operatorGroup2Section = [[HSSectionTextField alloc] init];
    operatorGroup2Section.sectionArray = @[ElementString(@"divide"),ElementString(@"modulus"),ElementString(@"left shift"),ElementString(@"right shift"),];
    
    [self.scrollView addSubview:operatorGroup2Section];
    if ([[HSParserHelper shift_ops] containsObject:[self.elementModel.convert_dict objectForKey:@"Divide_op"]]) {
        NSInteger index = [[HSParserHelper shift_ops] indexOfObject:[self.elementModel.convert_dict objectForKey:@"Divide_op"]];
        operatorGroup2Section.textField.text = operatorGroup2Section.sectionArray[index];
    }else {
        operatorGroup2Section.textField.text = operatorGroup2Section.sectionArray.firstObject;
    }
    
    
    [self.selectBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(fourLabel);
        make.left.equalTo(self.scrollView).offset(3);
        make.height.width.equalTo(self.selectBtnOne);
    }];
    
    [fourLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.oneLabel);
        make.centerY.equalTo(operatorGroup2Section);
    }];
    
    [operatorGroup2Section mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(self.oneVarSection);
        make.top.equalTo(self.argGroup1TextField.mas_bottom).offset(10);
        make.left.equalTo(fourLabel.mas_right).offset(10);
    }];
    
    //    第二排
    UILabel *fiveLabel = [UILabel CreatLabelText:ElementString(@"Argument") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:fiveLabel];
    
    HSCustomTextfield *argGroup2TextField = [[HSCustomTextfield alloc] init];
    argGroup2TextField.text = [self.elementModel.convert_dict objectForKey:@"Divide_arg"];
    argGroup2TextField.keyboardType = UIKeyboardTypeNumberPad;
    argGroup2TextField.textColor = RGB16TOCOLOR(0x030303);
    argGroup2TextField.font = [UIFont systemFontOfSize:14];
    argGroup2TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    argGroup2TextField.customDelegate = self;
    [self.scrollView addSubview:argGroup2TextField];
    
    //    第二组第二排
    NSMutableArray *allVars = [NSMutableArray arrayWithObject:klocConstant];
    [allVars addObjectsFromArray:[HSVariableItem variablesWithByteOrange:1]];
    HSSectionTextField *threeVarSection = [[HSSectionTextField alloc] init];
    threeVarSection.sectionArray = allVars;
    threeVarSection.textField.text = threeVarSection.sectionArray.firstObject;
    [self.scrollView addSubview:threeVarSection];
    threeVarSection.delegate = self;
    id var_divide = [self.elementModel.convert_dict objectForKey:@"Divide_var"];
    if ([var_divide isKindOfClass:[HSVariableItem class]]) {
        self.varItemDivide = var_divide;
        threeVarSection.textField.text = self.varItemDivide.name;
        argGroup2TextField.backgroundColor = [UIColor lightGrayColor];
        argGroup2TextField.userInteractionEnabled = NO;
    }else {
        self.varItemDivide = nil;
        threeVarSection.textField.text = threeVarSection.sectionArray.firstObject;
        argGroup2TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
        argGroup2TextField.userInteractionEnabled = YES;
    }
    
    
    [fiveLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(fourLabel);
        make.centerY.equalTo(argGroup2TextField);
    }];
    
    [argGroup2TextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(self.oneVarSection);
        make.top.equalTo(operatorGroup2Section.mas_bottom).offset(10);
        make.left.equalTo(fiveLabel.mas_right).offset(10);
    }];
    
    [threeVarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(self.oneVarSection);
        make.centerY.equalTo(argGroup2TextField);
        make.left.equalTo(argGroup2TextField.mas_right).offset(10);
    }];
    
    self.operatorGroup2Section = operatorGroup2Section;
    self.argGroup2TextField = argGroup2TextField;
    self.threeVarSection = threeVarSection;
    
}


// 第三组 逻辑
- (void)creatThirdgroupOtherViewsn {
    UILabel *sixLabel = [UILabel CreatLabelText:ElementString(@"Operation") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:sixLabel];
    
    
    HSSectionTextField *operatorGroup3Section = [[HSSectionTextField alloc] init];
    operatorGroup3Section.sectionArray = @[ElementString(@"and(bitwise)"),ElementString(@"or (bitwise)"),ElementString(@"xor(bitwise)"),];
    operatorGroup3Section.textField.text = operatorGroup3Section.sectionArray.firstObject;
    [self.scrollView addSubview:operatorGroup3Section];
    if ([[HSParserHelper logical_ops] containsObject:[self.elementModel.convert_dict objectForKey:@"Logical_op"]]) {
        NSInteger index = [[HSParserHelper logical_ops] indexOfObject:[self.elementModel.convert_dict objectForKey:@"Logical_op"]];
        operatorGroup3Section.textField.text = operatorGroup3Section.sectionArray[index];
    }else {
        operatorGroup3Section.textField.text = operatorGroup3Section.sectionArray.firstObject;
    }
    
    
    [self.selectBtnThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(sixLabel);
        make.left.equalTo(self.scrollView).offset(3);
        make.height.width.equalTo(self.selectBtnOne);
    }];
    
    [sixLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.oneLabel);
        make.centerY.equalTo(operatorGroup3Section);
    }];
    
    [operatorGroup3Section mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(self.oneVarSection);
        make.top.equalTo(self.argGroup2TextField.mas_bottom).offset(10);
        make.left.equalTo(sixLabel.mas_right).offset(10);
    }];
    
    //    第三组第二排
    UILabel *sevenLabel = [UILabel CreatLabelText:ElementString(@"Argument") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.scrollView addSubview:sevenLabel];
    
    HSCustomTextfield *argGroup3TextField = [[HSCustomTextfield alloc] init];
    argGroup3TextField.text = [self.elementModel.convert_dict objectForKey:@"Logical_arg"];
    argGroup3TextField.keyboardType = UIKeyboardTypeNumberPad;
    argGroup3TextField.textColor = RGB16TOCOLOR(0x030303);
    argGroup3TextField.font = [UIFont systemFontOfSize:14];
    argGroup3TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    [self.scrollView addSubview:argGroup3TextField];
    argGroup3TextField.customDelegate = self;
    
    
    NSMutableArray *allVars = [NSMutableArray arrayWithObject:klocConstant];
    [allVars addObjectsFromArray:[HSVariableItem variablesWithByteOrange:1]];
    HSSectionTextField *fourVarSection = [[HSSectionTextField alloc] init];
    fourVarSection.sectionArray = allVars;
    fourVarSection.textField.text = fourVarSection.sectionArray.firstObject;
    [self.scrollView addSubview:fourVarSection];
    
    fourVarSection.delegate = self;
    id var_divide = [self.elementModel.convert_dict objectForKey:@"Divide_var"];
    if ([var_divide isKindOfClass:[HSVariableItem class]]) {
        self.varItemLogical = var_divide;
        fourVarSection.textField.text = self.varItemDivide.name;
        argGroup3TextField.backgroundColor = [UIColor lightGrayColor];
        argGroup3TextField.userInteractionEnabled = NO;
    }else {
        self.varItemLogical = nil;
        fourVarSection.textField.text = fourVarSection.sectionArray.firstObject;
        argGroup3TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
        argGroup3TextField.userInteractionEnabled = YES;
    }
    
    
    [sevenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(sixLabel);
        make.centerY.equalTo(argGroup3TextField);
    }];
    
    [argGroup3TextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(self.oneVarSection);
        make.top.equalTo(operatorGroup3Section.mas_bottom).offset(10);
        make.left.equalTo(sevenLabel.mas_right).offset(10);
    }];
    
    [fourVarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(self.oneVarSection);
        make.centerY.equalTo(argGroup3TextField);
        make.left.equalTo(argGroup3TextField.mas_right).offset(10);
//        make.bottom.equalTo(self.scrollView).offset(-30);
    }];
    
    self.operatorGroup3Section = operatorGroup3Section;
    self.argGroup3TextField = argGroup3TextField;
    self.fourVarSection = fourVarSection;
    
    
    self.scrollView.contentSize = CGSizeMake(0, (kAutoHei(40)+10)*7 + 30);
    
    NSInteger section = [self.elementModel.convert_dict[@"Section"] intValue];
    
    [self selectBtnClick:[self.scrollView viewWithTag:section+10]];
    
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        [self.contentView addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return _scrollView;
}

- (UIButton *)selectBtnOne {
    if (!_selectBtnOne) {
        UIButton *selectBtnOne = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnOne setTitle:ElementString(@"Basic") forState:UIControlStateNormal];
        [self.scrollView addSubview:selectBtnOne];
        _selectBtnOne = selectBtnOne;
        [selectBtnOne setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnOne.titleLabel.font = [UIFont systemFontOfSize:12];
        selectBtnOne.tag = 10;
    }
    return _selectBtnOne;
}
- (UIButton *)selectBtnTwo {
    if (!_selectBtnTwo) {
        UIButton *selectBtnTwo = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnTwo setTitle:ElementString(@"Divide") forState:UIControlStateNormal];
        [self.scrollView addSubview:selectBtnTwo];
        [selectBtnTwo setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnTwo.titleLabel.font = [UIFont systemFontOfSize:12];
        _selectBtnTwo = selectBtnTwo;
        _selectBtnTwo.tag = 11;
    }
    return _selectBtnTwo;
}
- (UIButton *)selectBtnThree {
    if (!_selectBtnThree) {
        UIButton *selectBtnThree = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnClick:)];
        [selectBtnThree setTitle:ElementString(@"Logical") forState:UIControlStateNormal];
        [self.scrollView addSubview:selectBtnThree];
        [selectBtnThree setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        selectBtnThree.titleLabel.font = [UIFont systemFontOfSize:12];
        _selectBtnThree = selectBtnThree;
        _selectBtnThree.tag = 12;
    }
    return _selectBtnThree;
}


- (void)selectBtnClick:(UIButton *)sender {
    

    if (sender.tag == 10) {
        self.sectionIndex = 0;
        self.selectBtnOne.selected = YES;
        self.operatorGroup1Section.backgroundColor = [UIColor whiteColor];
        self.operatorGroup1Section.userInteractionEnabled = YES;
        NSInteger index = [self.twoVarSection.sectionArray indexOfObject:self.twoVarSection.textField.text];
        if (index == 0) {
            self.argGroup1TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.argGroup1TextField.userInteractionEnabled = YES;
        }else {
            self.argGroup1TextField.backgroundColor = [UIColor lightGrayColor];
            self.argGroup1TextField.userInteractionEnabled = NO;
        }
        self.twoVarSection.backgroundColor = [UIColor whiteColor];
        self.twoVarSection.userInteractionEnabled = YES;
    }else {
        self.selectBtnOne.selected = NO;
        self.operatorGroup1Section.backgroundColor = [UIColor lightGrayColor];
        self.operatorGroup1Section.userInteractionEnabled = NO;
        
        self.argGroup1TextField.backgroundColor = [UIColor lightGrayColor];
        self.argGroup1TextField.userInteractionEnabled = NO;
        
        self.twoVarSection.backgroundColor = [UIColor lightGrayColor];
        self.twoVarSection.userInteractionEnabled = NO;
    }
    
    if (sender.tag == 11) {
        self.sectionIndex = 1;
        self.selectBtnTwo.selected = YES;
        self.operatorGroup2Section.backgroundColor = [UIColor whiteColor];
        self.operatorGroup2Section.userInteractionEnabled = YES;
        NSInteger index = [self.threeVarSection.sectionArray indexOfObject:self.threeVarSection.textField.text];
        if (index == 0) {
            self.argGroup2TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.argGroup2TextField.userInteractionEnabled = YES;
        }else {
            self.argGroup2TextField.backgroundColor = [UIColor lightGrayColor];
            self.argGroup2TextField.userInteractionEnabled = NO;
        }
        self.threeVarSection.backgroundColor = [UIColor whiteColor];
        self.threeVarSection.userInteractionEnabled = YES;
        
    }else {
        self.selectBtnTwo.selected = NO;
        self.operatorGroup2Section.backgroundColor = [UIColor lightGrayColor];
        self.operatorGroup2Section.userInteractionEnabled = NO;
        
        self.argGroup2TextField.backgroundColor = [UIColor lightGrayColor];
        self.argGroup2TextField.userInteractionEnabled = NO;
        
        self.threeVarSection.backgroundColor = [UIColor lightGrayColor];
        self.threeVarSection.userInteractionEnabled = NO;
    }
    
    if (sender.tag == 12) {
        self.sectionIndex = 2;
        self.selectBtnThree.selected = YES;
        self.operatorGroup3Section.backgroundColor = [UIColor whiteColor];
        self.operatorGroup3Section.userInteractionEnabled = YES;
        NSInteger index = [self.fourVarSection.sectionArray indexOfObject:self.fourVarSection.textField.text];
        if (index == 0) {
            self.argGroup3TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.argGroup3TextField.userInteractionEnabled = YES;
        }else {
            self.argGroup3TextField.backgroundColor = [UIColor lightGrayColor];
            self.argGroup3TextField.userInteractionEnabled = NO;
        }
        self.fourVarSection.backgroundColor = [UIColor whiteColor];
        self.fourVarSection.userInteractionEnabled = YES;
    }else {
        self.selectBtnThree.selected = NO;
        self.operatorGroup3Section.backgroundColor = [UIColor lightGrayColor];
        self.operatorGroup3Section.userInteractionEnabled = NO;
        
        self.argGroup3TextField.backgroundColor = [UIColor lightGrayColor];
        self.argGroup3TextField.userInteractionEnabled = NO;
        
        self.fourVarSection.backgroundColor = [UIColor lightGrayColor];
        self.fourVarSection.userInteractionEnabled = NO;
    }
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.oneVarSection]) {
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
        self.varItem1 = varItem.copy;
    }else if ([sectionTextField isEqual:self.twoVarSection]) {
        if ([sectionTextField.textField.text isEqualToString:sectionTextField.sectionArray.firstObject]) {
            self.varItemBase = nil;
            self.argGroup1TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.argGroup1TextField.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItemBase = varItem.copy;
            self.argGroup1TextField.backgroundColor = [UIColor lightGrayColor];
            self.argGroup1TextField.userInteractionEnabled = NO;
        }
    }else if ([sectionTextField isEqual:self.threeVarSection]) {
        
        if ([sectionTextField.textField.text isEqualToString:sectionTextField.sectionArray.firstObject]) {
            self.varItemDivide = nil;
            self.argGroup2TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.argGroup2TextField.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItemDivide = varItem.copy;
            self.argGroup2TextField.backgroundColor = [UIColor lightGrayColor];
            self.argGroup2TextField.userInteractionEnabled = NO;
        }
        
    }else if ([sectionTextField isEqual:self.fourVarSection]){
        if ([sectionTextField.textField.text isEqualToString:sectionTextField.sectionArray.firstObject]) {
            self.varItemLogical = nil;
            self.argGroup3TextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
            self.argGroup3TextField.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItemLogical = varItem.copy;
            self.argGroup3TextField.backgroundColor = [UIColor lightGrayColor];
            self.argGroup3TextField.userInteractionEnabled = NO;
        }
    }
}

- (void)customTextFieldTextDidChange:(HSCustomTextfield *)textField {
    if ([textField isEqual:self.argGroup1TextField] || [textField isEqual:self.argGroup1TextField] || [textField isEqual:self.argGroup1TextField]) {
        if ([textField.text integerValue] > MAX_BYTE) {
            textField.text = [textField.text substringToIndex:textField.text.length-1];
        }
        if ([textField.text integerValue] < 0) {
            textField.text = @"0";
        }
    }
}


- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    if (self.varItem1 == nil) {
        self.varItem1 = [HSVariableItem getVariable1];
    }
    [self.elementModel.userVaritems addObject:self.varItem1];
    
    [dict setObject:self.varItem1 forKey:@"Var"];
    
//    当前选中的是哪一组
    [dict setObject:@(self.sectionIndex).stringValue forKey:@"Section"];
    
//    第一组
    NSUInteger index_bop =[self.operatorGroup1Section.sectionArray indexOfObject:self.operatorGroup1Section.textField.text];
    [dict setObject:[HSParserHelper basic_ops][index_bop] forKey:@"Basic_op"];
    
    [dict setObject:@([self.argGroup1TextField.text intValue]).stringValue forKey:@"Basic_arg"];
    
    if (self.varItemBase) {
        [dict setObject:self.varItemBase forKey:@"Basic_var"];
        if (self.sectionIndex == 0) {
            [self.elementModel.userVaritems addObject:self.varItemBase];
        }
    }else {
        [dict setObject:@"" forKey:@"Basic_var"];
    }
//    第二组
    NSUInteger index_dop = [self.operatorGroup2Section.sectionArray indexOfObject:self.operatorGroup2Section.textField.text];
    [dict setObject:[HSParserHelper shift_ops][index_dop] forKey:@"Divide_op"];
    
    [dict setObject:@([self.argGroup2TextField.text intValue]).stringValue forKey:@"Divide_arg"];
    
    if (self.varItemDivide) {
        [dict setObject:self.varItemDivide forKey:@"Divide_var"];
        if (self.sectionIndex == 1) {
            [self.elementModel.userVaritems addObject:self.varItemDivide];
        }
    }else {
        [dict setObject:@"" forKey:@"Divide_var"];
    }
    
//    第三组
    NSUInteger index_lop =[self.operatorGroup3Section.sectionArray indexOfObject:self.operatorGroup3Section.textField.text];
    [dict setObject:[HSParserHelper logical_ops][index_lop] forKey:@"Logical_op"];
    
    [dict setObject:@([self.argGroup3TextField.text intValue]).stringValue forKey:@"Logical_arg"];
    
    if (self.varItemLogical) {
        [dict setObject:self.varItemLogical forKey:@"Logical_var"];
        if (self.sectionIndex == 2) {
            [self.elementModel.userVaritems addObject:self.varItemLogical];
        }
    }else {
        [dict setObject:@"" forKey:@"Logical_var"];
    }
    
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}




@end
