//
//  HSByteDAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/25.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSByteDAlertView : HSNorAlertView

@property (nonatomic,strong)UIScrollView *scrollView;

@property (nonatomic,strong)UIButton *selectBtnOne;
@property (nonatomic,strong)UIButton *selectBtnTwo;
@property (nonatomic,strong)UIButton *selectBtnThree;

// 第一组第一排的label
@property (nonatomic,strong)UILabel *oneLabel;

@property (nonatomic,strong)HSSectionTextField *oneVarSection;
@property (nonatomic,strong)HSSectionTextField *operatorGroup1Section;
@property (nonatomic,strong)HSCustomTextfield *argGroup1TextField;
@property (nonatomic,strong)HSSectionTextField *twoVarSection;


@property (nonatomic,strong)HSSectionTextField *operatorGroup2Section;
@property (nonatomic,strong)HSCustomTextfield *argGroup2TextField;
@property (nonatomic,strong)HSSectionTextField *threeVarSection;


// 第三组--逻辑
@property (nonatomic,strong)HSSectionTextField *operatorGroup3Section;
@property (nonatomic,strong)HSCustomTextfield *argGroup3TextField;
@property (nonatomic,strong)HSSectionTextField *fourVarSection;

@end

NS_ASSUME_NONNULL_END
