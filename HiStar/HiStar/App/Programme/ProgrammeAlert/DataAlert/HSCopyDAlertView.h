//
//  HSCopyDAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/25.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSCopyDAlertView : HSNorAlertView

// 选择左/右侧感光灯
@property (nonatomic,strong)HSSectionTextField *formSection;
@property (nonatomic,strong)HSSectionTextField *toSection;

@end

NS_ASSUME_NONNULL_END
