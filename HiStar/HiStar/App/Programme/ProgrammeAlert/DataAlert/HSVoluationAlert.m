//
//  HSVoluationAlert.m
//  HiStar
//
//  Created by 晴天 on 2019/6/25.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSVoluationAlert.h"

@interface HSVoluationAlert ()<HSCustomTextfieldDelegate,HSSectionTextFieldDelegate>

@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,strong)NSArray *variableArray;

@end

@implementation HSVoluationAlert

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self creatOtherAlertView];
        NSMutableArray *variableArrM = [NSMutableArray array];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:0]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}
- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    //  第一排
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Value") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    HSCustomTextfield *valueTextField = [[HSCustomTextfield alloc] init];
    valueTextField.text = [[originValue objectForKey:@"Value"] stringValue];
    valueTextField.keyboardType = UIKeyboardTypeNumberPad;
    valueTextField.textColor = RGB16TOCOLOR(0x030303);
    valueTextField.font = [UIFont systemFontOfSize:14];
    valueTextField.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    [valueTextField layerFilletWithRadius:2];
    valueTextField.customDelegate = self;
    [self.contentView addSubview:valueTextField];
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(valueTextField);
    }];
    [valueTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(65));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    
    // 第二排
    UILabel *twoLabel = [UILabel CreatLabelText:ElementString(@"Variable to read into") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:twoLabel];
    
    HSSectionTextField *varSection = [[HSSectionTextField alloc] init];
    varSection.sectionArray = _variableArray;
    [varSection layerFilletWithRadius:2];
    [self.contentView addSubview:varSection];
    varSection.delegate = self;
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        varSection.textField.text = varItem.name;
        self.varItem = varItem;
    }else {
        varSection.textField.text = _variableArray.firstObject;
        self.varItem = [HSVariableItem getVariableItemWithName:_variableArray.firstObject];
    }
    
    
    [twoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(varSection);
        make.right.equalTo(varSection.mas_left).offset(-10);
    }];
    
    [varSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(valueTextField);
        make.top.equalTo(valueTextField.mas_bottom).offset(10);
        make.left.equalTo(valueTextField.mas_left);
    }];
    
    self.valueTextField = valueTextField;
    self.varSection = varSection;
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.varSection]) {
        if ([sectionTextField.textField.text isEqualToString:_variableArray.firstObject]) {
            self.varItem = nil;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem = varItem.copy;
        }
        [self checkValueInvalied];
    }
}

- (void)customTextFieldTextDidChange:(HSCustomTextfield *)textField {
    if ([textField isEqual:self.valueTextField]) {
        if ([self checkValueInvalied] == NO) {
            textField.text = [textField.text substringToIndex:textField.text.length-1];
        }
    }
}

// 检测输入的值是否有效
- (BOOL)checkValueInvalied {
    NSInteger value = [self.valueTextField.text integerValue];
    if (self.varItem.isByteOrange) {
        if (value > MAX_WORD || value < MIN_WORD) {
            return NO;
        }
    }else {
        if (value > MAX_SBYTE || value < MIN_SBYTE) {
            return NO;
        }
    }
    return YES;
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    [dict setObject:@([self.valueTextField.text intValue]) forKey:@"Value"];
    
    if (self.varItem) {
        [dict setObject:self.varItem forKey:@"Var"];
        [self.elementModel.userVaritems addObject:self.varItem];
    }else {
        [dict setObject:[HSVariableItem getVariable1] forKey:@"Var"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}


@end
