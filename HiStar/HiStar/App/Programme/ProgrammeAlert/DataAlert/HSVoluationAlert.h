//
//  HSVoluationAlert.h
//  HiStar
//
//  Created by 晴天 on 2019/6/25.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSVoluationAlert : HSNorAlertView

// 选择左/右侧感光灯
@property (nonatomic,strong)HSCustomTextfield *valueTextField;
@property (nonatomic,strong)HSSectionTextField *varSection;

@end

NS_ASSUME_NONNULL_END
