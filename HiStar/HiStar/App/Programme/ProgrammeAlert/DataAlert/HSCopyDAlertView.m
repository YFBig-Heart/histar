//
//  HSCopyDAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/25.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSCopyDAlertView.h"

@interface HSCopyDAlertView ()<HSSectionTextFieldDelegate>

@property (nonatomic,strong)NSArray *variableArray;

@property (nonatomic,strong)HSVariableItem *varItem1;
@property (nonatomic,strong)HSVariableItem *varItem2;

@end

@implementation HSCopyDAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self creatLightAlertView];
        NSMutableArray *variableArrM = [NSMutableArray array];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:0]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}
- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    //    第一排
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Copy data from variable") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    HSSectionTextField *sensitiveSection = [[HSSectionTextField alloc] init];
    sensitiveSection.sectionArray = _variableArray;
    [sensitiveSection layerFilletWithRadius:2];
    [self.contentView addSubview:sensitiveSection];
    sensitiveSection.delegate = self;
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        sensitiveSection.textField.text = varItem.name;
        self.varItem1 = varItem;
    }else {
        sensitiveSection.textField.text = _variableArray.firstObject;
        self.varItem1 = [HSVariableItem getVariableItemWithName:_variableArray.firstObject];
    }
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(sensitiveSection);
    }];
    [sensitiveSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(65));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    
    // 第二排
    UILabel *twoLabel = [UILabel CreatLabelText:ElementString(@"Copy data to variable") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:twoLabel];
    
    HSSectionTextField *varSection = [[HSSectionTextField alloc] init];
    varSection.sectionArray = _variableArray;
    [varSection layerFilletWithRadius:2];
    [self.contentView addSubview:varSection];
    id var2 = [originValue objectForKey:@"Var2"];
    varSection.delegate = self;
    if ([var2 isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var2;
        varSection.textField.text = varItem.name;
        self.varItem2 = varItem;
    }else {
        varSection.textField.text = _variableArray.firstObject;
        self.varItem2 = [HSVariableItem getVariableItemWithName:_variableArray.firstObject];
    }
    
    
    [twoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(varSection);
        make.right.equalTo(varSection.mas_left).offset(-10);
    }];
    
    [varSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(sensitiveSection);
        make.top.equalTo(sensitiveSection.mas_bottom).offset(10);
        make.left.equalTo(sensitiveSection.mas_left);
    }];
    
    self.formSection = sensitiveSection;
    self.toSection = varSection;
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.formSection]) {
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
        self.varItem1 = varItem.copy;
        
    }else if ([sectionTextField isEqual:self.toSection]){
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
        self.varItem2 = varItem.copy;
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    if (self.varItem1 == nil) {
        self.varItem1 = [HSVariableItem getVariable1];
    }
    if (self.varItem2 == nil) {
        self.varItem2 = [HSVariableItem getVariable2];
    }
    [self.elementModel.userVaritems addObjectsFromArray:@[_varItem1,_varItem2]];
    
    [dict setObject:self.varItem1 forKey:@"Var"];
    [dict setObject:self.varItem2 forKey:@"Var2"];
    
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}


@end
