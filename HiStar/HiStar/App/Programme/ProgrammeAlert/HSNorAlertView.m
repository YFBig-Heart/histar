//
//  HSNorAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/18.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"
#import "HSElementModel.h"

#import "HSLightCAlertView.h"
#import "HSMusicCAlertView.h"
#import "HSObstacleCAlertView.h"
#import "HSSingleMotorCAlertView.h"
#import "HSDoubleMotorCAlertView.h"
#import "HSXunXianCAlertView.h"
#import "HSSignCAlertView.h"
#import "HSTimerCAlertView.h"
#import "HSOneVarAlertView.h"
#import "HSSensitiveLightRAlert.h"
#import "HSVoluationAlert.h"
#import "HSCopyDAlertView.h"
#import "HSCharacterAlertView.h"
#import "HSByteDAlertView.h"

#import "HSifFAlertView.h"
#import "HSLoopFAlertView.h"
#import "HSWaitFAlertView.h"
#import "HSStartEventAlertView.h"
#import "HSEmailFAlertView.h"

@interface HSNorAlertView ()
@property (nonatomic, weak) IBOutlet UIView *view;
@property (nonatomic,weak)UIView *coverView;
@property (nonatomic,weak)UIViewController *inVc;

@end

@implementation HSNorAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatBaseUi];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self creatBaseUi];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

// 创建控件
- (void)creatBaseUi {
    self.backgroundColor = RGB16TOCOLOR(0xD2D2D2);
    
    self.leftTitleLabel = [UILabel CreatLabelText:self.elementModel.proptitle bgColor:nil textFont:[UIFont pingFangSCFont:PingFangSCMedium size:16] textColor:RGB16TOCOLOR(0x030303) textAliment:NSTextAlignmentCenter];
    self.rightTitleLabel = [UILabel CreatLabelText:NSLocalizedString(@"Programming information", nil) bgColor:nil textFont:[UIFont pingFangSCFont:PingFangSCMedium size:16] textColor:RGB16TOCOLOR(0x030303) textAliment:NSTextAlignmentCenter];
    self.rightTitleLabel.adjustsFontSizeToFitWidth = YES;
    self.rightTitleLabel.minimumScaleFactor = 0.5;
    [self addSubview:_leftTitleLabel];
    [self addSubview:_rightTitleLabel];
    
    UIView *hLine = [[UIView alloc] init];
    hLine.backgroundColor = RGB16TOCOLOR(0x9083BE);
    [self addSubview:hLine];
    
    UIView *vLine = [[UIView alloc] init];
    vLine.backgroundColor = RGB16TOCOLOR(0x9083BE);
    [self addSubview:vLine];
    
    [self.leftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.right.equalTo(vLine.mas_left);
        make.height.mas_equalTo(44);
    }];
    [_rightTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.height.equalTo(self.leftTitleLabel);
        make.left.equalTo(vLine.mas_right);
        make.width.equalTo(self.leftTitleLabel).multipliedBy(138/310.0);
    }];
    [vLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(2);
        make.top.bottom.equalTo(self);
    }];
    
    [hLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftTitleLabel.mas_bottom);
        make.left.right.equalTo(self);
        make.height.mas_equalTo(2);
    }];
    
    self.textView = [[UITextView alloc] init];
    self.textView.inputView = [UIView new];
    self.textView.editable = NO;
    self.textView.selectable = NO;
    self.textView.textColor = [UIColor darkGrayColor];
    self.textView.font = [UIFont systemFontOfSize:14];
    self.textView.backgroundColor = RGB16TOCOLOR(0xd2d2d2);
    self.textView.inputAccessoryView = [UIView new];
    [self addSubview:self.textView];
    self.textView.text = self.elementModel.propText;
    self.textView.contentInset = UIEdgeInsetsMake(6, 6, 6, 6);
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(vLine.mas_right);
        make.top.equalTo(hLine.mas_bottom);
        make.right.bottom.equalTo(self);
    }];
    
    self.sureButton = [UIButton creatButtonTitle:klocOk bgColor:RGB16TOCOLOR(0xd2d2d2) textFont:[UIFont pingFangSCFont:PingFangSCRegular size:14] textColor:RGB16TOCOLOR(0x030303)];
    [self addSubview:_sureButton];
    [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(6);
        make.bottom.equalTo(self).offset(-6);
        make.right.equalTo(vLine.mas_left).offset(-6);
        make.height.mas_equalTo(40);
    }];
    [self.sureButton layerWithBorderColor:[UIColor lightGrayColor] borderWidth:0.5 cornerRadius:3 masksToBounds:NO];
    self.sureButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.sureButton.layer.shadowOpacity = 0.015;
    self.sureButton.layer.shadowRadius = 2;
    self.sureButton.layer.shadowOffset = CGSizeMake(2, 2);
    [self.sureButton addTarget:self action:@selector(sureButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.contentView = [[UIView alloc] init];
    [self addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.bottom.equalTo(self.sureButton.mas_top);
        make.right.equalTo(vLine.mas_left);
        make.top.equalTo(hLine.mas_bottom);
    }];
    
    self.layer.cornerRadius = 2;
    self.layer.masksToBounds = YES;
}

- (void)creatContentViewSubViews {
    if (self.elementModel.moduleType == kModuleControl && [self.elementModel.funtction_type isEqualToString:k_ft_trumpet]) {
        
        [self creatThumptOtherView];
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    if (self.sureBtnAction) {
        self.sureBtnAction(self, sender);
    }
}

+ (instancetype)showNorAlertViewWithElementModel:(HSElementModel *)elementModel inVc:(UIViewController *)inVc sureBtnAction:(kSureButtonClickBlock)sureBtnBlock {
    // 根据元素创建对于补充视图
    HSNorAlertView *alertView = [HSNorAlertView creatOtherViewsWithModle:elementModel];
    if (alertView == nil) {
        return nil;
    }
    alertView.inVc = inVc;
    alertView.sureBtnAction = sureBtnBlock;
    alertView.elementModel = elementModel;
    alertView.coverView = [alertView showAlertInVc:inVc];
    [alertView creatContentViewSubViews];
    return alertView;
}

- (void)setElementModel:(HSElementModel *)elementModel {
    _elementModel = elementModel;
    self.leftTitleLabel.text = elementModel.proptitle;
    self.textView.text = elementModel.propText;
}

- (UIView *)showAlertInVc:(UIViewController *__nullable)inVc {
    CGFloat width = kYFScreenWidth*500/667;
    CGFloat height = kYFScreenHeight * 318.0/360;
    if (MIN(kYFScreenWidth, kYFScreenHeight) <= 667) {
        width = kYFScreenWidth*560/667;
        height = kYFScreenHeight * 318.0/360;
    }
    
    UIView *coverView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UIButton *coverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [coverView addSubview:coverBtn];
    coverBtn.backgroundColor = [UIColor clearColor];
    [coverBtn addTarget:self action:@selector(coverBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    coverBtn.backgroundColor = [UIColor clearColor];
    coverBtn.frame = coverView.bounds;
    self.bounds = CGRectMake(0, 0, width, height);
    self.center = coverView.center;
    [coverView addSubview:self];
    if (inVc == nil) {
        inVc = [UIApplication sharedApplication].keyWindow.rootViewController;
    }
    [inVc.view addSubview:coverView];
    return coverView;
}
- (void)coverBtnAction:(UIButton *)coverBtn {
    [self endEditing:YES];
}

- (void)disMissAlertView {
    if (self.coverView) {
        [UIView animateWithDuration:0.25 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self.coverView removeFromSuperview];
        }];
    }else {
        [self removeFromSuperview];
    }
}

- (void)customTextField:(HSCustomTextfield *)textField offSetY:(CGFloat)offsetY keyboradShow:(BOOL)show animiationTime:(CGFloat)animationTime {
    [UIView animateWithDuration:animationTime animations:^{
        self.centerY = self.coverView.centerY + (show ? -offsetY:0);
    } completion:^(BOOL finished) {
        
    }];
}
- (void)cusomTextFiledShouldEndEditing:(HSCustomTextfield *)textField {
    self.centerY = self.coverView.centerY;
}

#pragma mark - 补充视图
+ (HSNorAlertView *)creatOtherViewsWithModle:(HSElementModel *)model {
    if (model.moduleType == kModuleControl) {
        return [self creatControlElementAlert:model];
    }else if (model.moduleType == kModuleRead){
        return [self creatReadElementAlert:model];
    }else if (model.moduleType == kModuleData){
        return [self creatDataElementAlert:model];
    }else if (model.moduleType == kModuleFlow){
        return [self creatFlowElementAlert:model];
    }
    return nil;
}

// 创建喇叭中的子视图
- (void)creatThumptOtherView {
    NSString *text = self.elementModel.propText;//@"蜂鸣器响一下\n频率：1KH\n时间：100毫秒（0.1秒）";
    
    UILabel *trumpetIntroLabel = [UILabel CreatLabelText:text bgColor:nil textFont:[UIFont pingFangSCFont:PingFangSCRegular size:12] textColor:RGB16TOCOLOR(0x030303) textAliment:NSTextAlignmentLeft];
    trumpetIntroLabel.numberOfLines = 0;
    [trumpetIntroLabel sizeToFit];
    [self.contentView addSubview:trumpetIntroLabel];
    trumpetIntroLabel.x = 10;
    trumpetIntroLabel.y = 8;
    
}

// 创建控住元素的弹窗
+ (HSNorAlertView *)creatControlElementAlert:(HSElementModel *)model {
    if (model.moduleType != kModuleControl) {
        return nil;
    }
    if ([model.funtction_type isEqualToString:k_ft_trumpet]) {
        HSNorAlertView *alertView = [[HSNorAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_light]){
        HSLightCAlertView *lightAlertView = [[HSLightCAlertView alloc] init];
        return lightAlertView;
    }else if ([model.funtction_type isEqualToString:k_ft_music]){
        HSMusicCAlertView *musicAlertView = [[HSMusicCAlertView alloc] init];
        return musicAlertView;
    }else if ([model.funtction_type isEqualToString:k_ft_obstacle]){
        HSObstacleCAlertView *alertView = [[HSObstacleCAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_singleMotor]){
        HSSingleMotorCAlertView *alertView = [[HSSingleMotorCAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_doubleMotor]){
        HSDoubleMotorCAlertView *alertView = [[HSDoubleMotorCAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_xunXian]){
        HSXunXianCAlertView *alertView = [[HSXunXianCAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_sign]){
        HSSignCAlertView *alertView = [[HSSignCAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_timer]){
        HSTimerCAlertView *alertView = [[HSTimerCAlertView alloc] init];
        return alertView;
    }
    return nil;
}


// 创建读取元素的弹框
+(HSNorAlertView *)creatReadElementAlert:(HSElementModel *)model {
    if (model.moduleType != kModuleRead) {
        return nil;
    }
    if ([model.funtction_type isEqualToString:k_ft_xunXian] ||
        [model.funtction_type isEqualToString:k_ft_obstacle] ||
        [model.funtction_type isEqualToString:k_ft_keyBtn] ||
        [model.funtction_type isEqualToString:k_ft_applouse] ||
        [model.funtction_type isEqualToString:k_ft_receiveData] ||
        [model.funtction_type isEqualToString:k_ft_yaoKong]) {
        NSArray *section1s = [HSVariableItem variablesWithByteOrange:1];
        NSString *current1Section = @"";
        if (section1s) {
            current1Section = section1s.firstObject;
        }
        HSOneVarAlertView *alertView = [HSOneVarAlertView oneAlertViewWithElement:model sections:section1s];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_timer]){
        NSArray *section2s = [HSVariableItem variablesWithByteOrange:2];
        HSOneVarAlertView *alertView = [HSOneVarAlertView oneAlertViewWithElement:model sections:section2s];
        return alertView;
    }else if ( [model.funtction_type isEqualToString:k_ft_sensitiveLight]){
        HSSensitiveLightRAlert *alertView = [[HSSensitiveLightRAlert alloc] init];
        return alertView;
    }
    return nil;
}

+ (instancetype)creatDataElementAlert:(HSElementModel *)model {
    if (model.moduleType != kModuleData) {
        return nil;
    }
    if ([model.funtction_type isEqualToString:k_ft_add] ||
        [model.funtction_type isEqualToString:k_ft_reduce]){
        NSArray *section1s = [HSVariableItem variablesWithByteOrange:0];
        NSString *current1Section = @"";
        if (section1s) {
            current1Section = section1s.firstObject;
        }
        HSOneVarAlertView *alertView = [HSOneVarAlertView oneAlertViewWithElement:model sections:section1s];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_variable]){
        HSVoluationAlert *alertView = [[HSVoluationAlert alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_copy]){
        HSCopyDAlertView *alertView = [[HSCopyDAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_character]){
        HSCharacterAlertView *alertView = [[HSCharacterAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_byte]){
        HSByteDAlertView *alertView = [[HSByteDAlertView alloc] init];
        return alertView;
    }
    
    return nil;
}

+ (instancetype)creatFlowElementAlert:(HSElementModel *)model {
    if (model.moduleType != kModuleFlow) {
        return nil;
    }
    if ([model isIfConditionItem]){
        HSifFAlertView *alertView = [[HSifFAlertView alloc] init];
        return alertView;
    }else if ([model isLoopItem]){
        HSLoopFAlertView *alertView = [[HSLoopFAlertView alloc] init];
        return alertView;
    }else if ([model.codeName isEqualToString:@"Wait"]){
        HSWaitFAlertView *alertView = [[HSWaitFAlertView alloc] init];
        return alertView;
    }else if ([model.funtction_type isEqualToString:k_ft_startEvent]){
        HSStartEventAlertView *alertView = [[HSStartEventAlertView alloc] init];
        return alertView;
    }else if ([model.codeName isEqualToString:@"Email"]){
        HSEmailFAlertView *alertView = [[HSEmailFAlertView alloc] init];
        return alertView;
    }
    return nil;
}






@end
