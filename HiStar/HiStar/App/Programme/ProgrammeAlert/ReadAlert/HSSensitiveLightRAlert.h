//
//  HSSensitiveLightRAlert.h
//  HiStar
//
//  Created by 晴天 on 2019/6/24.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSSensitiveLightRAlert : HSNorAlertView

// 选择左/右侧感光灯
@property (nonatomic,strong)HSSectionTextField *sensitiveSection;

@property (nonatomic,strong)HSSectionTextField *varSection;

@end

NS_ASSUME_NONNULL_END
