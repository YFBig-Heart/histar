//
//  HSSensitiveLightRAlert.m
//  HiStar
//
//  Created by 晴天 on 2019/6/24.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSSensitiveLightRAlert.h"

@interface HSSensitiveLightRAlert ()<HSSectionTextFieldDelegate>

/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,strong)NSArray *variableArray;

@end

@implementation HSSensitiveLightRAlert

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self creatLightAlertView];
        NSMutableArray *variableArrM = [NSMutableArray array];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:2]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}
- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    //    第一排
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Sense") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    HSSectionTextField *sensitiveSection = [[HSSectionTextField alloc] init];
    sensitiveSection.sectionArray = @[ElementString(@"Left light level"),ElementString(@"Right light level")];
    if ([[originValue objectForKey:@"ReadLight"] isEqualToString:ElementString(@"Right light level")]){
        sensitiveSection.textField.text = sensitiveSection.sectionArray.lastObject;
    }else {
        sensitiveSection.textField.text = sensitiveSection.sectionArray.firstObject;
    }
    [sensitiveSection layerFilletWithRadius:2];
    [self.contentView addSubview:sensitiveSection];
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(sensitiveSection);
    }];
    
    [sensitiveSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(65));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    
// 第二排
    UILabel *twoLabel = [UILabel CreatLabelText:ElementString(@"Variable to read into") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:twoLabel];
    
    HSSectionTextField *varSection = [[HSSectionTextField alloc] init];
    varSection.sectionArray = self.variableArray;
    [varSection layerFilletWithRadius:2];
    [self.contentView addSubview:varSection];
    varSection.delegate = self;
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        varSection.textField.text = varItem.name;
        self.varItem = varItem;
    }else {
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:varSection.sectionArray.firstObject];
        self.varItem = varItem;
        varSection.textField.text = varSection.sectionArray.firstObject;
    }
    
    
    [twoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(varSection);
        make.right.equalTo(varSection.mas_left).offset(-10);
    }];
    
    [varSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(sensitiveSection);
        make.top.equalTo(sensitiveSection.mas_bottom).offset(10);
        make.left.equalTo(sensitiveSection.mas_left);
    }];
    
    self.sensitiveSection = sensitiveSection;
    self.varSection = varSection;
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.varSection]) {
        HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
        self.varItem = varItem;
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    if ([self.sensitiveSection.textField.text isEqualToString:self.sensitiveSection.sectionArray.firstObject]) {
        [dict setObject:ElementString(@"Left light level") forKey:@"ReadLight"];
    }else {
        [dict setObject:ElementString(@"Right light level") forKey:@"ReadLight"];
    }
    
    if (self.varItem) {
        [dict setObject:self.varItem forKey:@"Var"];
        [self.elementModel.userVaritems addObject:self.varItem];
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}

@end
