//
//  HSMusicAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSMusicCAlertView.h"

@interface HSMusicCAlertView ()<HSCustomTextfieldDelegate,UITextFieldDelegate>

@end

@implementation HSMusicCAlertView


- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
//    @{@"ToneType":@(0),@"Tone":@"C",@"Time":@"sixteenth",@"Text":@""};//默认值
    
    UIButton *selectBtnOne = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnOneClick:)];
    [self.contentView addSubview:selectBtnOne];
    
    UILabel *musicNoteLabel = [UILabel CreatLabelText:ElementString(@"Musical note") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [musicNoteLabel sizeToFit];
    [self.contentView addSubview:musicNoteLabel];
    
    HSSectionTextField *musicNoteSection = [[HSSectionTextField alloc] init];
    // @[@"A (6th)", @"A# (6th)", @"B (6th)", @"C", @"C#", @"D", @"D#", @"E", @"F", @"F#",@"G", @"G#", @"A", @"A#", @"B", @"C (8th)", @"Rest"];
    musicNoteSection.sectionArray = @[@"C-do",@"D-re",@"E-mi",@"F-fa",@"G-so",@"A-la",@"B-si",@"Rest-暂停"];//[HSParserHelper tone_notes];
    
    NSInteger toneIndex = [[HSParserHelper tone_notes] indexOfObject:[originValue objectForKey:@"Tone"]];
    musicNoteSection.textField.text = musicNoteSection.sectionArray[toneIndex];
    [musicNoteSection layerFilletWithRadius:2];
    [self.contentView addSubview:musicNoteSection];
    
    HSSectionTextField *musicNoteDBSection = [[HSSectionTextField alloc] init];
    musicNoteDBSection.sectionArray = @[ElementString(@"sixteenth"),ElementString(@"eighth"),ElementString(@"quarter"),ElementString(@"half"),ElementString(@"whole")];
    
    NSInteger index = [[HSParserHelper tone_durations] indexOfObject:[originValue objectForKey:@"Time"]];
    musicNoteDBSection.textField.text = musicNoteDBSection.sectionArray[index];
    [musicNoteDBSection layerFilletWithRadius:2];
    [self.contentView addSubview:musicNoteDBSection];
    
    [selectBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(30);
        make.left.equalTo(self.contentView).offset(3);
        make.centerY.equalTo(musicNoteLabel);
    }];
    
    [musicNoteLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(selectBtnOne.mas_right).offset(8);
        make.right.equalTo(musicNoteSection.mas_left).offset(-8);
        make.centerY.equalTo(musicNoteSection);
    }];
    
    [musicNoteSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.width.mas_equalTo(kAutoWid(60));
        make.height.mas_equalTo(kAutoHei(40));
        make.right.equalTo(musicNoteDBSection.mas_left).offset(-8);
        
    }];
    [musicNoteDBSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(musicNoteSection);
        make.width.mas_equalTo(kAutoWid(60));
    }];
    
    // 第二排
    UIButton *selectBtnTwo = [UIButton creatBtnnormalImage:@"msg_box_choose_before" selectImage:@"msg_box_choose_now" target:self action:@selector(selectBtnTwoClick:)];
    [selectBtnTwo sizeToFit];
    [self.contentView addSubview:selectBtnTwo];
    
    UILabel *tuneStringLabel = [UILabel CreatLabelText:ElementString(@"Tune string") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [tuneStringLabel sizeToFit];
    [self.contentView addSubview:tuneStringLabel];
    
    
    HSCustomTextfield *musicTextfield = [[HSCustomTextfield alloc] init];
    musicTextfield.textColor = RGB16TOCOLOR(0x030303);
    musicTextfield.font = [UIFont systemFontOfSize:14];
    musicTextfield.text = [originValue objectForKey:@"Text"];
    [self.contentView addSubview:musicTextfield];
    musicTextfield.customDelegate = self;
    musicTextfield.delegate = self;
    musicTextfield.keyboardType = UIKeyboardTypeEmailAddress;
    
    [selectBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.height.equalTo(selectBtnOne);
        make.centerY.equalTo(musicTextfield);
    }];
    
    [tuneStringLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(selectBtnTwo.mas_right).offset(8);
        make.centerY.equalTo(musicTextfield);
    }];
    
    [musicTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(tuneStringLabel.mas_right).offset(8);
        make.width.mas_equalTo(kAutoWid(80));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(musicNoteSection.mas_bottom).offset(12);
    }];
    
    self.selectBtnOne = selectBtnOne;
    self.selectBtnTwo = selectBtnTwo;
    self.musicNoteSection = musicNoteSection;
    self.musicNoteDBSection = musicNoteDBSection;
    self.musicTextfield = musicTextfield;
    [self selectBtnOneClick:self.selectBtnOne];
    
    if ([[originValue objectForKey:@"ToneType"] boolValue]) {
        [self selectBtnTwoClick:self.selectBtnTwo];
    }else {
        [self selectBtnOneClick:self.selectBtnOne];
    }
    
}

- (void)selectBtnOneClick:(UIButton *)sender {
    _selectBtnOne.selected = YES;
    _selectBtnTwo.selected = NO;
    
    self.musicNoteSection.backgroundColor = [UIColor whiteColor];
    self.musicNoteSection.userInteractionEnabled = YES;
    
    self.musicNoteDBSection.backgroundColor = [UIColor whiteColor];
    self.musicNoteDBSection.userInteractionEnabled = YES;
    
    self.musicTextfield.backgroundColor = [UIColor lightGrayColor];
    self.musicTextfield.enabled = NO;
    
}

- (void)selectBtnTwoClick:(UIButton *)sender {
    _selectBtnTwo.selected = YES;
    _selectBtnOne.selected = NO;
   
    self.musicNoteSection.backgroundColor = [UIColor lightGrayColor];
    self.musicNoteSection.userInteractionEnabled = NO;
    [self.musicNoteSection endEditing:YES];
    
    self.musicNoteDBSection.backgroundColor = [UIColor lightGrayColor];
    self.musicNoteDBSection.userInteractionEnabled = NO;
    [self.musicNoteDBSection endEditing:YES];
    
    self.musicTextfield.backgroundColor = [UIColor whiteColor];
    self.musicTextfield.enabled = YES;
}
- (void)customTextFieldTextDidChange:(HSCustomTextfield *)textField {
        
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL suc = [self checkMusicWithIndex:textField.text.length string:string];
    textField.text = [[textField text] uppercaseString];
    return suc;
}

- (BOOL)checkMusicWithIndex:(NSInteger)index string:(NSString *)text {
    text = [[text uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (text.length == 0) {
        return YES;
    }
    for (int i = 0; i<text.length; i++) {
        NSString *t = [text substringWithRange:NSMakeRange(i, 1)];
        if ((index + 1) % 2) {
            // 音符
            if ([notes containsString:t]) {
                return YES;
            }else{
                [MBProgressHUD showTipMessageInView:ElementString(@"Note does not meet the requirements.") timer:1.5];
                return NO;
            }
        }else {
            // 音符时长
            if ([octdigits containsString:t]) {
                return YES;
            }else {
                [MBProgressHUD showTipMessageInView:ElementString(@"Note does not meet the requirements.") timer:1.5];
                return NO;
            }
        }
    }
    return NO;
}

- (void)sureButtonClick:(UIButton *)sender {
    self.musicTextfield.text = [self.musicTextfield.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (self.selectBtnTwo.selected == YES && self.musicTextfield.text.length % 2 > 0) {
        [MBProgressHUD showTipMessageInView:ElementString(@"The last note is missing the time.") timer:1.5];
        return;
    }
    
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    BOOL toneType = self.selectBtnTwo.selected;
    [dict setObject:@(toneType) forKey:@"ToneType"];
    
    NSString *tone = self.musicNoteSection.textField.text;
    NSInteger toneIndex = [self.musicNoteSection.sectionArray indexOfObject:tone];
    [dict setObject:[HSParserHelper tone_notes][toneIndex] forKey:@"Tone"];
    
    NSInteger index = [self.musicNoteDBSection.sectionArray indexOfObject:self.musicNoteDBSection.textField.text];
    NSString *time = [[HSParserHelper tone_durations] objectAtIndex:index];
    [dict setObject:time forKey:@"Time"];
    
//    text
    [dict setObject:[NSString checkIfNullWithString:self.musicTextfield.text] forKey:@"Text"];
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}



@end
