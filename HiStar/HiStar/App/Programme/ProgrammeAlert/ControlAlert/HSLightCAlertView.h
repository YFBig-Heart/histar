//
//  HSLightAlertOtherV.h
//  HiStar
//
//  Created by 晴天 on 2019/6/22.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSLightCAlertView :HSNorAlertView

@property (nonatomic,strong)HSSectionTextField *lightSection;
@property (nonatomic,strong)HSSectionTextField *lightColorSection;
@property (nonatomic,strong)HSSectionTextField *switchSection;
@property (nonatomic,strong)HSSectionTextField *variableSection;

@end

NS_ASSUME_NONNULL_END
