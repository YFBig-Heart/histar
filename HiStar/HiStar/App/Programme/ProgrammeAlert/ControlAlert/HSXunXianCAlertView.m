//
//  HSXunXianCAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSXunXianCAlertView.h"

@interface HSXunXianCAlertView ()<HSSectionTextFieldDelegate>
/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,strong)NSArray *variableArray;

@end

@implementation HSXunXianCAlertView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        [self creatOtherAlertView];
        NSMutableArray *variableArrM = [NSMutableArray arrayWithObject:klocConstant];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:1]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}

- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    UILabel *constLabel = [UILabel CreatLabelText:klocConstant bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:constLabel];
    UILabel *variableLabel = [UILabel CreatLabelText:klocVariable bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:variableLabel];
    
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Line Tracker LED") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    HSSectionTextField *switchSection = [[HSSectionTextField alloc] init];
    switchSection.sectionArray = @[ElementString(@"Off"),ElementString(@"On")];
    switchSection.textField.text = switchSection.sectionArray[[[originValue objectForKey:@"Switch"] boolValue]];
    [switchSection layerFilletWithRadius:2];
    [self.contentView addSubview:switchSection];
    
    
    HSSectionTextField *variableSection = [[HSSectionTextField alloc] init];
    variableSection.sectionArray = _variableArray;
    [variableSection layerFilletWithRadius:2];
    variableSection.delegate = self;
    [self.contentView addSubview:variableSection];
    
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        variableSection.textField.text = varItem.name;
        self.varItem = varItem;
        switchSection.backgroundColor = [UIColor lightGrayColor];
        switchSection.userInteractionEnabled = NO;
    }else {
        self.varItem = nil;
        variableSection.textField.text = _variableArray.firstObject;
        switchSection.backgroundColor = [UIColor whiteColor];
        switchSection.userInteractionEnabled = YES;
    }
    
    
    [constLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(switchSection).offset(-20);
        make.top.equalTo(self.contentView).offset(8);
        make.bottom.equalTo(switchSection.mas_top).offset(-8);
    }];
    [variableLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(variableSection).offset(-20);
        make.centerY.equalTo(constLabel);
    }];
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(switchSection.mas_left).offset(-10);
        make.centerY.equalTo(switchSection);
    }];
    
    [switchSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(50));
        make.height.mas_equalTo(kAutoHei(40));
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    [variableSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(switchSection);
        make.centerY.equalTo(switchSection);
        make.width.mas_equalTo(kAutoWid(65));
        make.left.equalTo(switchSection.mas_right).offset(8);
    }];
    
    self.switchSection = switchSection;
    self.variableSection = variableSection;
    
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if (sectionTextField == self.variableSection) {
        if ([sectionTextField.textField.text isEqualToString:_variableArray.firstObject]) {
            self.varItem = nil;
            self.switchSection.backgroundColor = [UIColor whiteColor];
            self.switchSection.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem = varItem.copy;
            self.switchSection.backgroundColor = [UIColor lightGrayColor];
            self.switchSection.userInteractionEnabled = NO;
        }
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    BOOL onOff = ![self.switchSection.textField.text isEqualToString:self.switchSection.sectionArray.firstObject];
    [dict setObject:@(onOff).stringValue forKey:@"Switch"];
    
    if (self.varItem) {
        [dict setObject:self.varItem forKey:@"Var"];
        [self.elementModel.userVaritems addObject:self.varItem];
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}



@end
