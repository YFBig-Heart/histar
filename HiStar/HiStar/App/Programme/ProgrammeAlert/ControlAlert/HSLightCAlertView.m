//
//  HSLightAlertOtherV.m
//  HiStar
//
//  Created by 晴天 on 2019/6/22.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSLightCAlertView.h"


@interface HSLightCAlertView ()<HSSectionTextFieldDelegate>
/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,strong)NSArray *variableArray;

@end
@implementation HSLightCAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSMutableArray *variableArrM = [NSMutableArray arrayWithObject:klocConstant];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:1]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}

- (void)creatContentViewSubViews {
    // @{@"LED":@"Left_LED",@"Switch":@"0",@"Color":@"Blue",@"Var":@""};
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    UILabel *controlLabel = [UILabel CreatLabelText:ElementString(@"Control") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:controlLabel];
    HSSectionTextField *lightSection = [[HSSectionTextField alloc] init];
    lightSection.sectionArray = @[ElementString(@"left_LED"),ElementString(@"Right_LED")];

    lightSection.textField.text = [[originValue objectForKey:@"LED"] isEqualToString:@"Left_LED"] ? lightSection.sectionArray.firstObject:lightSection.sectionArray.lastObject;

    [lightSection layerFilletWithRadius:2];
    [self.contentView addSubview:lightSection];
    
    HSSectionTextField *lightColorSection = [[HSSectionTextField alloc] init];
    lightColorSection.sectionArray = @[ElementString(@"Blue"),ElementString(@"Red")];
    lightColorSection.textField.text = [[originValue objectForKey:@"Color"] isEqualToString:@"Blue"] ? lightColorSection.sectionArray.firstObject:lightColorSection.sectionArray.lastObject;
    
    [lightColorSection layerFilletWithRadius:2];
    [self.contentView addSubview:lightColorSection];
    
    [controlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(8);
        make.width.mas_equalTo(kAutoWid(40));
        make.right.equalTo(lightSection.mas_left).offset(-8);
        make.centerY.equalTo(lightSection);
    }];
    
    [lightSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.width.mas_equalTo(kAutoWid(60));
        make.right.equalTo(lightColorSection.mas_left).offset(-8);
        make.height.mas_equalTo(kAutoHei(40));
    }];
    
    [lightColorSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.width.equalTo(lightSection);
    }];
    
//    第二排
    UILabel *constLabel = [UILabel CreatLabelText:klocConstant bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:constLabel];
    UILabel *variableLabel = [UILabel CreatLabelText:klocVariable bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    
    [self.contentView addSubview:variableLabel];
    
    UILabel *ledControlLabel = [UILabel CreatLabelText:ElementString(@"LED Setting") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:ledControlLabel];
    
    HSSectionTextField *switchSection = [[HSSectionTextField alloc] init];
    switchSection.sectionArray = @[ElementString(@"Off"),ElementString(@"On")];
    switchSection.textField.text = [[originValue objectForKey:@"Switch"] intValue] == 0 ? switchSection.sectionArray.firstObject:switchSection.sectionArray.lastObject;
    [switchSection layerFilletWithRadius:2];
    [self.contentView addSubview:switchSection];
    
    
    HSSectionTextField *variableSection = [[HSSectionTextField alloc] init];
    variableSection.sectionArray = _variableArray;
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        variableSection.textField.text = varItem.name;
        self.varItem = varItem;
        switchSection.backgroundColor = [UIColor lightGrayColor];
        switchSection.userInteractionEnabled = NO;
        
    }else {
        self.varItem = nil;
        variableSection.textField.text = _variableArray.firstObject;
        switchSection.backgroundColor = [UIColor whiteColor];
        switchSection.userInteractionEnabled = YES;
    }
    
    [variableSection layerFilletWithRadius:2];
    variableSection.delegate = self;
    [self.contentView addSubview:variableSection];
    
    [constLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(switchSection).offset(-20);
        make.top.equalTo(lightSection.mas_bottom).offset(8);
        make.bottom.equalTo(switchSection.mas_top).offset(-8);
    }];
    [variableLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(variableSection).offset(-20);
        make.centerY.equalTo(constLabel);
    }];
    
    [ledControlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.left.right.equalTo(controlLabel);
        make.centerY.equalTo(switchSection);
    }];
    
    [switchSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.height.equalTo(lightSection);
    }];
    
    [variableSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(switchSection);
        make.centerY.equalTo(switchSection);
        make.left.equalTo(switchSection.mas_right).offset(8);
    }];
    
    
    self.lightSection = lightSection;
    self.lightColorSection = lightColorSection;
    self.switchSection = switchSection;
    self.variableSection = variableSection;
    
    
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if ([sectionTextField isEqual:self.variableSection]) {
        if ([sectionTextField.textField.text isEqualToString:_variableArray.firstObject]) {
            self.varItem = nil;
            self.switchSection.backgroundColor = [UIColor whiteColor];
            self.switchSection.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem = varItem.copy;
            self.switchSection.backgroundColor = [UIColor lightGrayColor];
            self.switchSection.userInteractionEnabled = NO;
        }
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];

    BOOL onOff = ![self.switchSection.textField.text isEqualToString:self.switchSection.sectionArray.firstObject];
    [dict setObject:@(onOff).stringValue forKey:@"Switch"];
    
    
    NSString *led = [self.lightSection.textField.text isEqualToString:self.lightSection.sectionArray.firstObject] ? @"Left_LED":@"Right_LED";
    [dict setObject:led forKey:@"LED"];
    
    NSString *color = [self.lightColorSection.textField.text isEqualToString:self.lightColorSection.sectionArray.firstObject] ? @"Blue":@"Red";
    
    [dict setObject:color forKey:@"Color"];
    
    if (self.varItem) {
        [dict setObject:self.varItem forKey:@"Var"];
        [self.elementModel.userVaritems addObject:self.varItem];
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}


@end
