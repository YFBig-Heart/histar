//
//  HSXunXianCAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSXunXianCAlertView : HSNorAlertView

@property (nonatomic,strong)HSSectionTextField *switchSection;
@property (nonatomic,strong)HSSectionTextField *variableSection;


@end

NS_ASSUME_NONNULL_END
