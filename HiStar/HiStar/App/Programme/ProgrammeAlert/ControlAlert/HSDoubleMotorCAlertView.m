//
//  HSDoubleMotorCAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSDoubleMotorCAlertView.h"

@interface HSDoubleMotorCAlertView ()<HSSectionTextFieldDelegate>

@property (nonatomic,strong)NSArray *variableArray;
/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem1;
/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem2;

@end

@implementation HSDoubleMotorCAlertView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        [self creatOtherAlertView];
        NSMutableArray *variableArrM = [NSMutableArray arrayWithObject:klocConstant];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:1]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}

- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    //    第一排
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Direction") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    UILabel *constLabel = [UILabel CreatLabelText:klocConstant bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:constLabel];
    UILabel *variableLabel = [UILabel CreatLabelText:klocVariable bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:variableLabel];
    
    HSSectionTextField *directionSection = [[HSSectionTextField alloc] init];
    
    directionSection.sectionArray = @[ElementString(@"Forward"),ElementString(@"Backward"),ElementString(@"Forward right"),ElementString(@"Forward left"),ElementString(@"Spin right"),ElementString(@"Spin left"),ElementString(@"Back right"),ElementString(@"Back left"),ElementString(@"Stop")];
    directionSection.textField.text = directionSection.sectionArray.firstObject;
    [directionSection layerFilletWithRadius:2];
    [self.contentView addSubview:directionSection];
    NSInteger index = [[HSParserHelper motor_configs][2] indexOfObject:originValue[@"Direction"]];
    directionSection.textField.text = directionSection.sectionArray[index];
    
    
    
    HSSectionTextField *directionVarSection = [[HSSectionTextField alloc] init];
    directionVarSection.sectionArray = @[klocConstant];
    [directionVarSection layerFilletWithRadius:2];
    directionVarSection.textField.text = directionVarSection.sectionArray.firstObject;
    [self.contentView addSubview:directionVarSection];
    directionVarSection.backgroundColor = [UIColor lightGrayColor];
    directionVarSection.userInteractionEnabled = NO;
    self.varItem1 = nil;
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(directionSection.mas_left).offset(-10);
        make.centerY.equalTo(directionSection);
    }];
    
    [constLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(directionSection).offset(-20);
        make.top.equalTo(self.contentView).offset(8);
        make.bottom.equalTo(directionSection.mas_top).offset(-8);
    }];
    [variableLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(directionVarSection).offset(-20);
        make.centerY.equalTo(constLabel);
    }];
    
    [directionSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(60));
        make.height.mas_equalTo(kAutoHei(40));
        make.top.equalTo(constLabel.mas_bottom).offset(8);
    }];
    
    [directionVarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.height.equalTo(directionSection);
        make.width.mas_equalTo(kAutoWid(65));
        make.left.equalTo(directionSection.mas_right).offset(8);
    }];
    //    第二排
    UILabel *twoLabel = [UILabel CreatLabelText:ElementString(@"Speed") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:twoLabel];
    
    HSSectionTextField *speedSection = [[HSSectionTextField alloc] init];
    speedSection.sectionArray = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
    speedSection.textField.text = [originValue objectForKey:@"Speed"];
    [speedSection layerFilletWithRadius:2];
    [self.contentView addSubview:speedSection];
    
    
    HSSectionTextField *speedVarSection = [[HSSectionTextField alloc] init];
    speedVarSection.sectionArray = _variableArray;
    [speedVarSection layerFilletWithRadius:2];
    [self.contentView addSubview:speedVarSection];
    
    speedVarSection.delegate = self;
    id var2 = [originValue objectForKey:@"Var_speed"];
    if ([var2 isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var2;
        speedVarSection.textField.text = varItem.name;
        self.varItem2 = varItem;
        speedSection.backgroundColor = [UIColor lightGrayColor];
        speedSection.userInteractionEnabled = NO;
    }else {
        self.varItem1 = nil;
        speedVarSection.textField.text = speedVarSection.sectionArray.firstObject;
        speedSection.backgroundColor = [UIColor whiteColor];
        speedSection.userInteractionEnabled = YES;
    }
    
    
    [twoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(oneLabel);
        make.centerY.equalTo(speedSection);
    }];
    
    [speedSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.height.equalTo(directionSection);
        make.top.equalTo(directionSection.mas_bottom).offset(15);
    }];
    
    [speedVarSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(directionVarSection);
        make.left.equalTo(speedSection.mas_right).offset(10);
        make.centerY.equalTo(speedSection);
    }];
    
    self.directionSection = directionSection;
    self.directionVarSection = directionVarSection;
    self.speedSection = speedSection;
    self.speedVarSection = speedVarSection;
    
   
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    
    if ([sectionTextField isEqual:self.speedVarSection]){
        if ([sectionTextField.textField.text isEqualToString:sectionTextField.sectionArray.firstObject]) {
            self.varItem2 = nil;
            self.speedSection.backgroundColor = [UIColor whiteColor];
            self.speedSection.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem2 = varItem.copy;
            self.speedSection.backgroundColor = [UIColor lightGrayColor];
            self.speedSection.userInteractionEnabled = NO;
        }
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];

    
    [dict setObject:[HSParserHelper motor_configs][2][self.directionSection.selectSectionIndex] forKey:@"Direction"];
    
    [dict setObject:CONSTANT forKey:@"Var"];
    [dict setObject:self.speedSection.textField.text forKey:@"Speed"];
    
    if (self.varItem2) {
        [dict setObject:self.varItem2 forKey:@"Var_speed"];
        [self.elementModel.userVaritems addObject:self.varItem2];
    }else {
        [dict setObject:CONSTANT forKey:@"Var_speed"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}



@end
