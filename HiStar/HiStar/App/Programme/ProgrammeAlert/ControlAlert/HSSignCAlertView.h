//
//  HSSignCAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSSignCAlertView : HSNorAlertView


@property (nonatomic,strong)HSCustomTextfield *dataTextField;
@property (nonatomic,strong)HSSectionTextField *varSection;


@end

NS_ASSUME_NONNULL_END
