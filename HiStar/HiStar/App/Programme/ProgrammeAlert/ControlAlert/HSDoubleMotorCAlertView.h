//
//  HSDoubleMotorCAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSDoubleMotorCAlertView : HSNorAlertView
/** 方向选择 */
@property (nonatomic,strong)HSSectionTextField *directionSection;

@property (nonatomic,strong)HSSectionTextField *directionVarSection;

/* 速度选择 */
@property (nonatomic,strong)HSSectionTextField *speedSection;
@property (nonatomic,strong)HSSectionTextField *speedVarSection;

@end

NS_ASSUME_NONNULL_END
