//
//  HSTimerCAlertView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSTimerCAlertView.h"

@interface HSTimerCAlertView ()<HSSectionTextFieldDelegate,HSCustomTextfieldDelegate>
/** 当前选中的变量 */
@property (nonatomic,strong)HSVariableItem *varItem;
@property (nonatomic,strong)NSArray *variableArray;

@end

@implementation HSTimerCAlertView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        [self creatOtherAlertView];
        NSMutableArray *variableArrM = [NSMutableArray arrayWithObject:klocConstant];
        [variableArrM addObjectsFromArray:[HSVariableItem variablesWithByteOrange:2]];
        self.variableArray = variableArrM.copy;
    }
    return self;
}

- (void)creatContentViewSubViews {
    NSDictionary *originValue = self.elementModel.convert_dict;
    
    UILabel *constLabel = [UILabel CreatLabelText:klocConstant bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:constLabel];
    
    UILabel *variableLabel = [UILabel CreatLabelText:klocVariable bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentCenter];
    [self.contentView addSubview:variableLabel];
    
    UILabel *oneLabel = [UILabel CreatLabelText:ElementString(@"Seconds") bgColor:nil textFont:[UIFont systemFontOfSize:12] textColor:[UIColor darkGrayColor] textAliment:NSTextAlignmentRight];
    [self.contentView addSubview:oneLabel];
    
    HSCustomTextfield *dataTextfield = [[HSCustomTextfield alloc] init];
    dataTextfield.textColor = RGB16TOCOLOR(0x030303);
    dataTextfield.font = [UIFont systemFontOfSize:14];
    dataTextfield.text = [originValue objectForKey:@"Word"];
    dataTextfield.keyboardType = UIKeyboardTypeNumberPad;
    dataTextfield.backgroundColor = RGB16TOCOLOR(0x8F82BC);
    dataTextfield.customDelegate = self;
    
    [self.contentView addSubview:dataTextfield];
    
   
    HSSectionTextField *varSection = [[HSSectionTextField alloc] init];
    varSection.sectionArray = _variableArray;
    [varSection layerFilletWithRadius:2];
    [self.contentView addSubview:varSection];
    
    varSection.delegate = self;
    id var = [originValue objectForKey:@"Var"];
    if ([var isKindOfClass:[HSVariableItem class]]) {
        HSVariableItem *varItem = var;
        varSection.textField.text = varItem.name;
        self.varItem = varItem;
        dataTextfield.backgroundColor = [UIColor lightGrayColor];
        dataTextfield.userInteractionEnabled = NO;
    }else {
        self.varItem = nil;
        varSection.textField.text = _variableArray.firstObject;
        dataTextfield.backgroundColor = [UIColor whiteColor];
        dataTextfield.userInteractionEnabled = YES;
    }
    
    
    [constLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(dataTextfield).offset(-20);
        make.top.equalTo(self.contentView).offset(8);
        make.bottom.equalTo(dataTextfield.mas_top).offset(-8);
    }];
    [variableLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(varSection).offset(-20);
        make.centerY.equalTo(constLabel);
    }];
    
    [oneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(dataTextfield.mas_left).offset(-10);
        make.centerY.equalTo(dataTextfield);
    }];
    
    [dataTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kAutoWid(50));
        make.height.mas_equalTo(kAutoHei(40));
        make.left.equalTo(oneLabel.mas_right).offset(10);
    }];
    
    [varSection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(dataTextfield);
        make.centerY.equalTo(dataTextfield);
        make.width.mas_equalTo(kAutoWid(65));
        make.left.equalTo(dataTextfield.mas_right).offset(8);
    }];
    
    self.dataTextField = dataTextfield;
    self.varSection = varSection;
    
}

- (void)sectionTextfieldValueChange:(HSSectionTextField *)sectionTextField {
    if (sectionTextField == self.varSection) {
        if ([sectionTextField.textField.text isEqualToString:_variableArray.firstObject]) {
            self.varItem = nil;
            self.dataTextField.backgroundColor = [UIColor whiteColor];
            self.dataTextField.userInteractionEnabled = YES;
        }else {
            HSVariableItem *varItem = [HSVariableItem getVariableItemWithName:sectionTextField.textField.text];
            self.varItem = varItem.copy;
            self.dataTextField.backgroundColor = [UIColor lightGrayColor];
            self.dataTextField.userInteractionEnabled = NO;
        }
    }
}

- (void)customTextFieldTextDidChange:(HSCustomTextfield *)textField {
    if ([textField isEqual:self.dataTextField]) {
        if ([textField.text integerValue] > 32767) {
            textField.text = [textField.text substringToIndex:textField.text.length-1];
        }else if ([textField.text integerValue] < 0){
            textField.text = @"0";
        }
    }
}

- (void)sureButtonClick:(UIButton *)sender {
    [self.elementModel.userVaritems removeAllObjects];
    // 修改配置值
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.elementModel.convert_dict];
    
    NSString *charStr = self.dataTextField.text;
    if (self.dataTextField.text.length == 0) {
        charStr = @"0";
    }
    
    [dict setObject:charStr forKey:@"Word"];
    
    if (self.varItem) {
        [dict setObject:self.varItem forKey:@"Var"];
        [self.elementModel.userVaritems addObject:self.varItem];
    }else {
        [dict setObject:CONSTANT forKey:@"Var"];
    }
    self.elementModel.convert_dict = dict.copy;
    [super sureButtonClick:sender];
}



@end
