//
//  HSMusicAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/23.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSNorAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSMusicCAlertView : HSNorAlertView

// 音符选择
@property (nonatomic,strong)HSSectionTextField *musicNoteSection;
// 音符--全音（1/16）
@property (nonatomic,strong)HSSectionTextField *musicNoteDBSection;

/** 第一排的选择按钮 */
@property (nonatomic,strong)UIButton *selectBtnOne;
/** 第二排的选择按钮 */
@property (nonatomic,strong)UIButton *selectBtnTwo;

@property (nonatomic,strong)HSCustomTextfield *musicTextfield;



@end

NS_ASSUME_NONNULL_END
