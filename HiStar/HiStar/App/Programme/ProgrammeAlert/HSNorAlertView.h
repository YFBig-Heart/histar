//
//  HSNorAlertView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/18.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "HSSectionTextField.h"
#import "HSCustomTextfield.h"
#import "HSElementModel.h"



NS_ASSUME_NONNULL_BEGIN

@class HSNorAlertView;
typedef void(^kSureButtonClickBlock)(HSNorAlertView *view,UIButton *sender);

@class HSElementModel;
@interface HSNorAlertView : UIView<HSCustomTextfieldDelegate>

@property (strong, nonatomic)  UILabel *leftTitleLabel;
@property (strong, nonatomic)  UILabel *rightTitleLabel;
@property (strong, nonatomic)  UITextView *textView;
@property (strong, nonatomic)  UIButton *sureButton;
@property (strong, nonatomic)  UIView *contentView;

@property (nonatomic,strong)HSElementModel *elementModel;
@property (nonatomic,copy)kSureButtonClickBlock sureBtnAction;

// 退出
- (void)disMissAlertView;

//重写这个方法绘制contentView上的子视图
- (void)creatContentViewSubViews;

- (void)sureButtonClick:(UIButton *)sender;

+ (instancetype)showNorAlertViewWithElementModel:(HSElementModel *)elementModel inVc:(UIViewController *)inVc sureBtnAction:(kSureButtonClickBlock)sureBtnBlock;



@end

NS_ASSUME_NONNULL_END
