//
//  HSElementModel+HSLoop.m
//  HiStar
//
//  Created by 晴天 on 2019/8/3.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel+HSLoop.h"

@implementation HSElementModel (HSLoop)

// 等待
- (NSString *)wait_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSString *time_buff = @"_main_time_buffer";
    if ([input[0] integerValue] == 0) {
        if ([input[5] boolValue]) {
            time_buff = @"_event_time_buffer";
        }else {
            time_buff = @"_main_time_buffer";
        }
        if ([input[2] isEqualToString:CONSTANT]) {
            NSInteger time = [HSParserHelper conv_to_timeWithString_in:input[1]];
            if (time == None) {
                return @"";
            }
            code_string = [code_string stringByAppendingFormat:@"movtime $%ld @%@\n",(long)time,time_buff];
        }else {
            code_string = [code_string stringByAppendingFormat:@"movtime @%@ @%@\n",input[2],time_buff];
        }
        NSString *label = [HSElementModel make_labelBric_id:bric_id index:0];
        code_string = [code_string stringByAppendingFormat:@"%@\n",label];
        code_string = [code_string stringByAppendingFormat:@"cmptime @%@\n",time_buff];
        code_string = [code_string stringByAppendingFormat:@"brle %@\n",label];
        
    }else {
        NSString *label = [HSElementModel make_labelBric_id:bric_id index:0];
        code_string = [code_string stringByAppendingFormat:@"%@\n",label];
        
        NSString *mod_alias = [HSParserHelper find_alias_from_event:input[4]];
        NSString *getStr = @"";
        [self creat_event_codeWithModAlias:mod_alias event:input[4] label:label codeStr:&getStr];
        code_string = [code_string stringByAppendingString:getStr];
    }
    return code_string;
}

- (NSString *)creat_event_codeWithModAlias:(NSString *)mod_alias event:(NSString *)event label:(NSString *)label codeStr:(NSString **)codeStr {
    
    NSInteger match = -1;
    NSInteger value = None;
    NSInteger bit = -1;
    NSString *mod = nil;
    BOOL clear_status = YES;
    
    NSString *getStr = @"";
    NSString *if_variant = @"";
    
    id module = [HSParserHelper module_remove_aliasWithAliasList:[HSParserHelper EVENT_ALIASES] alias:mod_alias];
    NSArray *events = [[HSParserHelper EVENT_DICT] objectForKey:mod_alias];
    NSInteger event_Index = match;
    for (NSInteger i = 0; i < events.count; i++) {
        NSArray *e_arr = events[i];
        NSString *title = e_arr[0];
        NSArray *details = e_arr[1];
        if_variant = e_arr[2];
        if ([title isEqualToString:event]) {
            mod = [details firstObject];
            bit = [[details lastObject] integerValue];
            event_Index = i;
            break;
        }
    }
    
    if (bit == -1) {
        NSLog(@"error: not a valid event in module ");
    }
    if (mod == nil || [mod integerValue] == None) {
        mod = module;
    }
    
    getStr = [getStr stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:mod reg_name:@"status"]];
    
    NSInteger mask = 0x0;
    if ([event isEqualToString:@"Pressed"]) {
        mask = 0x01;
        value = 0x01;
        clear_status = NO;
    }else if ([event isEqualToString:@"Released"]) {
        mask = 0x01;
        value = 0x00;
        clear_status = NO;
    }else if ([event isEqualToString:TRACKER_1_STATUS]){
        mask = 0x01;
        value = 0x01;
        clear_status = NO;
    }else if ([event isEqualToString:TRACKER_0_STATUS]){
        mask = 0x01;
        value = 0x00;
        clear_status = NO;
    }else if ([event hasPrefix:ElementString(@"Match #")]){
        mask = 0x02;
        match = event_Index;
        clear_status = NO;
    }else {
        mask = 1 << (bit);
    }
    
    getStr = [getStr stringByAppendingFormat:@"and $%ld\n",(long)mask];
    
    if (value != None) {
        getStr = [getStr stringByAppendingFormat:@"cmpb $%ld\n",(long)value];
        getStr = [getStr stringByAppendingFormat:@"brne %@\n",label];
    }else {
        getStr = [getStr stringByAppendingFormat:@"brz %@\n",label];
    }

    if (clear_status) {
        getStr = [getStr stringByAppendingFormat:@"bitclr $%ld %@\n",(long)bit,[HSElementModel make_mod_reg:mod reg_name:@"status"]];
    }
    
    if (match >= 0) {
        getStr = [getStr stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:mod reg_name:@"match"]];
        
        getStr = [getStr stringByAppendingFormat:@"cmpb $%ld\n",(long)match];
        getStr = [getStr stringByAppendingFormat:@"brne %@\n",label];
        getStr = [getStr stringByAppendingFormat:@"bitclr $%ld %@\n",(long)bit,[HSElementModel make_mod_reg:mod reg_name:@"status"]];
    }
    *codeStr = getStr;

    return if_variant;
    
}


// if
- (NSString *)if_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    
    NSArray *labels = [HSElementModel make_if_labelsWithBric_id:self.bric_id];
    
    if ([input[0] integerValue] == 0) {
        if ([input[1] isEqualToString:NO_VAR]) {
            code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[1]];
#warning program
            
        }else {
            NSString *size = [HSParserHelper vars_get_type_letter_form_name:input[1]];
            NSInteger number = [HSParserHelper conv_to_numberString_in:input[3] n_type:size n_min:None n_max:None];
            if (number == None) {
                return @"";
            }
#warning program
            code_string = [code_string stringByAppendingFormat:@"mov%@ $%ld %%_cpu:acc\n",size,(long)number];
            code_string = [code_string stringByAppendingFormat:@"cmp%@ @%@\n",size,input[1]];
            
            code_string = [code_string stringByAppendingString:[self creat_compare_codeWithInput:input[2] label:labels[0]]];
            
            code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[1]];
        }
    }else {
        NSString *mod_alias = [HSParserHelper find_alias_from_event:input[5]];
        NSString *getStr = @"";
        NSString *if_variant = [self creat_event_codeWithModAlias:mod_alias event:input[5] label:labels[1] codeStr:&getStr];
        NSLog(@"if_variant:%@",if_variant);
        code_string = [code_string stringByAppendingString:getStr];
        code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[0]];
    }
    return code_string;
}

- (NSString *)creat_compare_codeWithInput:(NSString *)inputCompare label:(NSString *)label {
    
    NSString *getCodeStr = @"";
    if ([inputCompare isEqualToString:@"="]) {
        getCodeStr = [getCodeStr stringByAppendingFormat:@"bre %@\n",label];
    }else if ([inputCompare isEqualToString:@"!="]){
        getCodeStr = [getCodeStr stringByAppendingFormat:@"brne %@\n",label];
    }else if ([inputCompare isEqualToString:@"<"]){
        getCodeStr = [getCodeStr stringByAppendingFormat:@"brl %@\n",label];
    }else if ([inputCompare isEqualToString:@"<="]){
        getCodeStr = [getCodeStr stringByAppendingFormat:@"brle %@\n",label];
    }else if ([inputCompare isEqualToString:@">"]){
        getCodeStr = [getCodeStr stringByAppendingFormat:@"brgr %@\n",label];
    }else {
        getCodeStr = [getCodeStr stringByAppendingFormat:@"brge %@\n",label];
    }
    return getCodeStr;
}


// loop
- (NSString *)loop_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    
    NSArray *labels = [HSElementModel make_loop_labelsWithBric_id:self.bric_id];
    
    code_string = [code_string stringByAppendingFormat:@"%@\n",labels[2]];
    
    if ([input[0] integerValue] == 0) {
        if ([input[1] isEqualToString:NO_VAR]) {
           code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[1]];
            
        }else {
            NSString *size = [HSParserHelper vars_get_type_letter_form_name:input[1]];
            
            NSInteger number = [HSParserHelper conv_to_numberString_in:input[3] n_type:size n_min:None n_max:None];
            if (number == None) {
                return @"";
            }
            code_string = [code_string stringByAppendingFormat:@"mov%@ $%ld %%_cpu:acc\n",size,(long)number];
            code_string = [code_string stringByAppendingFormat:@"cmp%@ @%@\n",size,input[1]];
            
            code_string = [code_string stringByAppendingString:[self creat_compare_codeWithInput:input[2] label:labels[1]]];
            
//            code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[1]];
            
        }
    }else if ([input[0] integerValue] == 1){
        
        NSString *mod_alias = [HSParserHelper find_alias_from_event:input[5]];
        NSString *getStr = @"";
        [self creat_event_codeWithModAlias:mod_alias event:input[5] label:labels[0] codeStr:&getStr];
        code_string = [code_string stringByAppendingString:getStr];
        code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[1]];
    }else {
        code_string = [code_string stringByAppendingFormat:@"bra %@\n",labels[0]];
    }
    return code_string;
}


// event
- (NSString *)event_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSInteger bit = -1;
    id mod = nil;
    
    NSString *event = input[1];
    NSString *mode_alias = [HSParserHelper find_alias_from_event:event];
    
    NSArray *events = [HSParserHelper EVENT_DICT][mode_alias];
//    @[@"Triangle button pressed", @[@"_devices", @(0)], @"button"],
    for (NSArray *sub_events in events) {
        if ([sub_events[0] isEqualToString:event]) {
            mod = [sub_events[1] objectAtIndex:0];
            bit = [sub_events[1][1] intValue];
            break;
        }
    }
    
    NSInteger mask = 0;
    NSInteger value = 0;
    
    if (bit == -1) {
        NSLog(@"%@ not a valid event in module %@",event,input[0]);
    }
    if (mod == nil || [mod  isEqual: @(None)]) {
        mod = input[0];
    }
    if ([event isEqualToString:@"Pressed"]) {
        mask = 0x81;
        value = mask;
    }else if ([event isEqualToString:@"Released"]){
        mask = 0x81;
        value = 0x80;
    }else if ([event isEqualToString:TRACKER_0_STATUS]){
        mask = 0x03;
        value = 0x02;
    }else if ([event isEqualToString:TRACKER_1_STATUS]){
        mask = 0x03;
        value = mask;
    }else {
        mask = 1<<bit;
        value = mask;
    }
    
    code_string = [code_string stringByAppendingFormat:@"BEGIN EVENT %@, %ld, %ld\n",[HSElementModel make_mod_reg:mod reg_name:@"status"],(long)mask,(long)value];
    
    code_string = [code_string stringByAppendingFormat:@"bitclr $%ld %@\n",bit,[HSElementModel make_mod_reg:mod reg_name:@"status"]];
    
    return code_string;
}

// email
- (NSString *)email_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSString *output = @"action";
    NSArray *letters = [HSParserHelper all_EmailTypes];
    int bit = 1;
    NSString *mod;
    for (NSArray *letterItem in letters) {
        if ([letterItem.firstObject isEqualToString:input[0]]) {
            mod = [[letterItem objectAtIndex:2] firstObject];
            bit = [[[letterItem objectAtIndex:2] lastObject] intValue];
            break;
        }
    }
   code_string = [code_string stringByAppendingFormat:@"bitset $%d %@\n",bit,[HSElementModel make_mod_reg:mod reg_name:output]];
    return code_string;
    
}


@end
