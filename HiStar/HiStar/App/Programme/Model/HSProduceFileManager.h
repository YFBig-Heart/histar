//
//  HSProduceFileManager.h
//  HiStar
//
//  Created by 晴天 on 2019/7/13.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSProduceFileManager : NSObject

// 编程协议生成入口
+ (NSString *)produceProgrameFileEntranceWithVc:(UIViewController *)vc isSave:(BOOL)isSave;

// 将协议文件转成二进制
+ (NSArray *)parperProgrameFileText:(NSString *)fileText isDownLoad:(BOOL)isDownLoad;


@end

NS_ASSUME_NONNULL_END
