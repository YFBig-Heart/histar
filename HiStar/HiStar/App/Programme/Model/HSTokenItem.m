//
//  HSTokenItem.m
//  HiStar
//
//  Created by 晴天 on 2019/7/20.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSTokenItem.h"
#import "HSTokenStreamItem.h"
#import "HSParserDataManager.h"
#import "HSWordItem.h"


@implementation HSTokenItem

- (instancetype)init {
    if (self = [super init]) {
        self.section_breaks = [NSMutableArray array];
        self.labels = [NSMutableDictionary dictionary];
        self.section_args = [NSMutableArray array];
        // 默认有三个元素
        self.name_space = [NSMutableArray arrayWithArray:@[@[].mutableCopy,@[].mutableCopy,@[].mutableCopy]];
        self.name_space_map = self.name_space.mutableCopy;
        self.limits = MAX_LIMITS;
        self.download_type = @[].mutableCopy;
        self.sector_size = -1;
        
    }
    return self;
}

- (NSMutableDictionary *)devices {
    if (!_devices) {
        _devices = [NSMutableDictionary dictionary];
    }
    return _devices;
}

- (NSMutableArray *)token_stream {
    if (!_token_stream) {
        _token_stream = [NSMutableArray array];
    }
    return _token_stream;
}

- (NSMutableArray *)current_sections {
    if (!_current_sections) {
        _current_sections = [NSMutableArray array];
    }
    return _current_sections;
}


- (void)assem_moveWithSize:(NSInteger)size words:(NSArray *)words special:(NSString *)special line:(NSString *)line {
    
    if (words.count != 2) {
        NSLog(@"error:Move needs 2 arguments");
        return;
    }
    HSTokenStreamItem *streamItem = [HSTokenStreamItem tokenStreamItemWithType:@"move" error_reporter:nil src:line];
    
    [streamItem add_bitsWithIndex:0 shift:6 mask:3 value:@(1)];
    
    NSInteger sdindex = 1;
    HSWordItem *word0 = words.firstObject;
    NSString *type = word0.type;
    id val = word0.value;
    
    if ([type isEqualToString:@"const"]) {
        if (special.length == 0 && [val integerValue] >= 0 && [val integerValue] <= 3) {
            [streamItem add_bitsWithIndex:0 shift:3 mask:0x03 value:val];
            [streamItem add_bitsWithIndex:0 shift:2 mask:0x01 value:@(size)];
        }else {
            if (size == 0) {
                [streamItem add_bitsWithIndex:0 shift:2 mask:0xf value:@(0xa)];
                [streamItem add_byteWithIndex:sdindex value:[val integerValue]];
                sdindex += 1;
            }else {
                if (special.length == 0 ) {
                    [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x0b)];
                }else if ([special isEqualToString:@"time"]){
                    [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x0f)];
                }else {
                    streamItem.error_reporter = @"Source for lcd moves must be byte sized";
                    return;
                }
                [streamItem add_wordWithIndex:sdindex value:[val integerValue]];
                sdindex += 2;
            }
        }
        
    }else if ([type isEqualToString:@"modreg"]){
        if (size == 0) {
            [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x08)];
        }else {
            [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x09)];
        }
        [streamItem add_byteWithIndex:sdindex value:[val integerValue]];
        sdindex += 1;
        
    }else if ([@[@"var",@"arg"] containsObject:type]){
        if (size == 0) {
            [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x0c)];
        }else {
            if (special.length == 0) {
                [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x0d)];
            }else if ([special isEqualToString:@"time"]){
                [streamItem add_bitsWithIndex:0 shift:2 mask:0x0f value:@(0x0e)];
            }else {
                streamItem.error_reporter = @"Source for lcd moves must be byte sized";
                return;
            }
        }
        
        if ([type isEqualToString:@"arg"]) {
            [streamItem add_byteWithIndex:sdindex value:word0.num];
        }else {
            [streamItem add_byteWithIndex:sdindex value:0];
            [streamItem add_vnameIndex:sdindex space:size name:val];
        }
        sdindex += 1;
    }
    
    if ([special isEqualToString:@"time"]) {
        size = 0;
    }
    
    // destination
    HSWordItem *word1 = words[1];
    NSString *type1 = word1.type;
    id var1 = word1.value;
    if ([type1 isEqualToString:@"modreg"]) {
        if (special.length > 0) {
            streamItem.error_reporter = @"Destination for lcd and time moves can NOT be mod/reg";
            return;
        }
        [streamItem add_bitsWithIndex:0 shift:0 mask:0x03 value:@(0x0)];
        [streamItem add_byteWithIndex:sdindex value:[var1 integerValue]];
        sdindex += 1;
        
    }else if ([@[@"var",@"arg"] containsObject:type1]) {
        if (size == 0) {
            if (![special isEqualToString:@"lcd"]) {
                [streamItem add_bitsWithIndex:0 shift:0 mask:0x03 value:@(0x01)];
            }else {
                [streamItem add_bitsWithIndex:0 shift:0 mask:0x03 value:@(0x03)];
            }
        }else {
            if (special.length > 0) {
                streamItem.error_reporter = @"Desination for lcd and time moves can not be in word variables";
                return;
            }
            [streamItem add_bitsWithIndex:0 shift:0 mask:0x03 value:@(0x02)];
        }
        if ([type1 isEqualToString:@"arg"]) {
            NSInteger numValue1 = [HSParserDataManager parse_baserWithsnum:var1 string:false];
            [streamItem add_byteWithIndex:sdindex value:numValue1];
        }else {
            [streamItem add_byteWithIndex:sdindex value:0];
            if ([special isEqualToString:@"lcd"]) {
                [streamItem add_vnameIndex:sdindex space:2 name:var1];
            }else {
                // 2,0,变量1
                [streamItem add_vnameIndex:sdindex space:size name:var1];
            }
        }
        sdindex += 1;
    }
    
    NSLog(@"xdjfortokeneeeeeee:%@",streamItem.token_info);
    NSLog(@"xdjfortokenfffffff:%@",streamItem.var_info);
    [self finishTokenStream:streamItem explicit_placement:-1];
    
}

- (void)assem_dataWithSize:(NSInteger)size words:(NSArray *)words line:(NSString *)line {
    if (words.count < 3) {
        NSLog(@"error:dat[bw] needs at least 3 arguments: var, len and value1");
        return;
    }
    HSWordItem *word0 = words.firstObject;
    NSString *type0 = word0.type;
    NSInteger start = 0;
    NSString *name = @"";
    
    if ([@[@"arg",@"var"] containsObject:type0] == NO) {
        NSLog(@"dat[bw] first argument must be a variable or location");
        return;
    }else if ([type0 isEqualToString:@"arg"]){
        start = word0.anum;
        name = @"*";
    }else {
        start = 0;
        name = word0.value;
    }
    
    int length = 1;
    HSWordItem *word1 = words[1];
    if ([word1.value isEqualToString:@"*"]) {
        length = -1;
    }else {
        length = (int)word1.anum;
    }
    
    words = [words subarrayWithRange:NSMakeRange(2, words.count-2)];
    
    NSMutableArray *values = [NSMutableArray array];
    for (HSWordItem *w in words) {
        if ([@[@"arg",@"string"] containsObject:w.type] == NO) {
            NSLog(@"error:Word should have been an argument or string");
            return;
        }else {
            if ([w.type isEqualToString:@"string"]) {
                for (int i = 0; i<[w.value length]; i++) {
                    int asc = [w.value characterAtIndex:i];
                    [values addObject:@(asc)];
                }
                
            }else {
                [values addObject:@(w.anum)];
            }
        }
    }
    
    //    Do space sanity checks
    if (length > 0 && values) {
        NSLog(@"error:Data has length of %d but more values (%lu) then length",length,(unsigned long)values.count);
        return;
    }
    int real_length = length;
    if (length <= 0) {
        real_length = (int)values.count;
    }
    
    //    zero up to the named length
    if (length > 0 && length > values.count) {
        while (values.count < length) {
            [values addObject:@(0)];
        }
    }
    //    make the tokens
    if (values.count > 0) {
        NSInteger tokens_to_create = (values.count + 14)/15;
        NSInteger last = values.count % 15;
        
        NSInteger val_index = 0;
        for (int i = 0; i < tokens_to_create; i++) {
            HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"data" error_reporter:nil src:line];
            [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(size+1)];
            NSInteger val_count  = 0;
            if ((i < (tokens_to_create - 1)) || last == 0) {
                val_count = 15;
            }else {
                val_count = last;
            }
            
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0xf value:@(val_count)];
            
            //            add the location
            if ([name isEqualToString:@"*"] == NO) {
                [tokenStream add_byteWithIndex:1 value:i*15];
                [tokenStream add_vnameIndex:1 space:size name:name];
            }else {
                [tokenStream add_byteWithIndex:1 value:start+(i*15)];
            }
            
            NSInteger token_index = 2;
            
            if (size == 0) {
                for (int j = 0; j<val_count; j++) {
                    [tokenStream add_byteWithIndex:token_index value:[values[val_index] integerValue]];
                    token_index += 1;
                    val_index += 1;
                }
            }else {
                for (int j = 0; j<val_count; j++) {
                    [tokenStream add_wordWithIndex:token_index value:[values[val_index] integerValue]];
                    token_index += 2;
                    val_index += 1;
                }
            }
            [self finishTokenStream:tokenStream explicit_placement:-1];
        }
    }
    
}




- (void)assem_uni_mathWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line {
    NSDictionary *uni_assem_map = @{@"not":@(0),@"inc":@(1),@"dec":@(2),};
    
    if (words.count > 1) {
        NSLog(@"error:Unary Math has at most one argument");
        return;
    }
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"uni-math" error_reporter:nil src:line];
    [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(2)];
    [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(size)];
    [tokenStream add_bitsWithIndex:0 shift:0 mask:3 value:uni_assem_map[op]];
    
    HSWordItem *wordItem = words.firstObject;
    
    if (words.count == 0 || ([wordItem.type isEqualToString:@"modreg"] && wordItem.value)) {
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(0)];
        [self finishTokenStream:tokenStream explicit_placement:-1];
    }else if ([@[@"var",@"arg"] containsObject:wordItem.type]){
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(1)];
        if ([tokenStream.type isEqualToString:@"arg"]) {
            [tokenStream add_byteWithIndex:1 value:wordItem.num];
        }else {
            [tokenStream add_byteWithIndex:1 value:0];
            [tokenStream add_vnameIndex:1 space:size name:wordItem.value];
        }
        [self finishTokenStream:tokenStream explicit_placement:-1];
    }else {
        NSLog(@"error:Unary Math - invalid argument type");
        return;
    }
    
}


- (void)assem_basic_mathWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line {
    
    if (words.count != 1) {
        NSLog(@"error:Basic Math needs one argument");
        return;
    }
    
    NSDictionary *basic_assem_map = @{@"add":@(0), @"sub":@(1), @"mul":@(2), @"cmp":@(3)};
    
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"basic-math" error_reporter:nil src:line];
    [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(2)];
    [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(1)];
    [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(size)];
    [tokenStream add_bitsWithIndex:0 shift:0 mask:3 value:basic_assem_map[op]];
    
    HSWordItem *wordItem = words.firstObject;
    
    if ([@[@"var",@"arg"] containsObject:wordItem.type]) {
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(0)];
        if ([wordItem.type isEqualToString:@"arg"]) {
            [tokenStream add_byteWithIndex:1 value:wordItem.num];
        }else {
            [tokenStream add_byteWithIndex:1 value:0];
            [tokenStream add_vnameIndex:1 space:size name:wordItem.value];
        }
        [self finishTokenStream:tokenStream explicit_placement:-1];
    }else if ([wordItem.type isEqualToString:@"const"]){
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(1)];
        if (size == 0) {
            [tokenStream add_byteWithIndex:1 value:[wordItem.value integerValue]];
        }else {
            [tokenStream add_wordWithIndex:1 value:[wordItem.value integerValue]];
        }
        [self finishTokenStream:tokenStream explicit_placement:-1];
    }else {
        NSLog(@"error:Basic Math - invalid argument type");
        return;
    }
}

- (void)assem_other_mathWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line {
    if (words.count != 1) {
        NSLog(@"error:Logic Math needs one arguement");
        return;
    }
    
    NSDictionary *other1_assem_map = @{@"shl":@(0), @"shr":@(1), @"div":@(2), @"mod":@(3)};
    NSDictionary *other2_assem_map = @{@"or":@(0), @"and":@(1), @"xor":@(2)};
    
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"log-math" error_reporter:nil src:line];
    [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(2)];
    [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(size)];
    
    
    if ([other1_assem_map.allKeys containsObject:op]) {
        [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(2)];
        [tokenStream add_bitsWithIndex:0 shift:0 mask:3 value:other1_assem_map[op]];
    }else {
        [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(3)];
        [tokenStream add_bitsWithIndex:0 shift:0 mask:3 value:other2_assem_map[op]];
    }
    
    HSWordItem *wordItem = words.firstObject;
    
    if ([@[@"var",@"arg"] containsObject:wordItem.type]) {
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(0)];
        
        if ([wordItem.type isEqualToString:@"arg"]) {
            [tokenStream add_byteWithIndex:1 value:[wordItem.value integerValue]];
        }else {
            [tokenStream add_byteWithIndex:1 value:0];
            [tokenStream add_vnameIndex:1 space:size name:wordItem.value];
        }
    }else if ([wordItem.type isEqualToString:@"const"]){
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(1)];
        [tokenStream add_byteWithIndex:1 value:[wordItem.value integerValue]];
    }else {
        NSLog(@"error:Logic Math - invalid argument type");
        return;
    }
    
    [self finishTokenStream:tokenStream explicit_placement:-1];
    
}

- (void)assem_convWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line {
    
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"conv" error_reporter:nil src:line];
    
    if ([op isEqualToString:@"cmptime"]) {
        if (words.count != 1) {
            NSLog(@"error:Cmptime needs one arguement");
            return;
        }else {
            [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(2)];
            [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(0)];
            [tokenStream add_bitsWithIndex:0 shift:0 mask:7 value:@(7)];
            HSWordItem *wordItem = words.firstObject;
            if ([wordItem.type isEqualToString:@"arg"]) {
                [tokenStream add_byteWithIndex:1 value:wordItem.num];
            }else if ([wordItem.type isEqualToString:@"var"]){
                [tokenStream add_byteWithIndex:1 value:0];
                [tokenStream add_vnameIndex:1 space:0 name:wordItem.value];
            }else {
                NSLog(@"error:Cmptime takes a variable as it's argument");
                return;
            }
        }
    }else {
        if (words.count != 0) {
            NSLog(@"error:Conversions don't that arguments");
            return;
        }else {
            [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(2)];
            [tokenStream add_bitsWithIndex:0 shift:0 mask:7 value:@(3)];
            if ([op isEqualToString:@"convm"]) {
                [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(1)];
            }
        }
    }
    [self finishTokenStream:tokenStream explicit_placement:-1];
    
}

- (void)assem_stackWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line {
    
    if (words.count != 1) {
        NSLog(@"error:stack ops need 1 argument");
        return;
    }
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"stack" error_reporter:nil src:line];
    
    [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(3)];
    [tokenStream add_bitsWithIndex:0 shift:5 mask:1 value:@(1)];
    [tokenStream add_bitsWithIndex:0 shift:4 mask:1 value:@(size)];
    
    HSWordItem *wordItem = words.firstObject;
    
    if ([op isEqualToString:@"push"]) {
        
        if ([wordItem.type isEqualToString:@"const"]) {
            if (size == 0) {
                [tokenStream add_byteWithIndex:1 value:[wordItem.value integerValue]];
            }else {
                [tokenStream add_wordWithIndex:1 value:[wordItem.value integerValue]];
            }
        }else if ([wordItem.type isEqualToString:@"modreg"]){
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0xf value:@(2)];
            [tokenStream add_byteWithIndex:1 value:[wordItem.value integerValue]];
        }else if ([@[@"var",@"arg"] containsObject:wordItem.type]){
            
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0x0f value:@(1)];
            if ([wordItem.type isEqualToString:@"arg"]) {
                [tokenStream add_byteWithIndex:1 value:wordItem.num];
            }else {
                [tokenStream add_byteWithIndex:1 value:0];
                [tokenStream add_vnameIndex:1 space:size name:wordItem.value];
            }
        }else {
            NSLog(@"error: Push - invalid operand:%@",wordItem.type);
            return;
        }
        
    }else { // pop
        
        if ([wordItem.type isEqualToString:@"modreg"]) {
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0x0f value:@(6)];
            [tokenStream add_byteWithIndex:1 value:[wordItem.value integerValue]];
            
        }else if ([@[@"var",@"arg"] containsObject:wordItem.type]){
            
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0x0f value:@(5)];
            if ([wordItem.type isEqualToString:@"arg"]) {
                [tokenStream add_byteWithIndex:1 value:wordItem.num];
            }else {
                [tokenStream add_byteWithIndex:1 value:0];
                [tokenStream add_vnameIndex:1 space:size name:wordItem.value];
            }
        }else {
            NSLog(@"error:Pop - invalid operand: %@",wordItem.type);
            return;
        }
    }
    [self finishTokenStream:tokenStream explicit_placement:-1];
    
}

- (void)assem_eventWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line {
    
    if (words.count != 0) {
        NSLog(@"error:Enable/disable don't take arguments");
        return;
    }
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"event" error_reporter:nil src:line];
    [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(2)];
    [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(3)];
    [tokenStream add_bitsWithIndex:0 shift:0 mask:3 value:@(3)];
    
    if ([op isEqualToString:@"enable"]) {
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(1)];
    }else {
        [tokenStream add_bitsWithIndex:0 shift:2 mask:1 value:@(0)];
    }
    
    [self finishTokenStream:tokenStream explicit_placement:-1];
}

- (void)assem_jumpWithOp:(NSString *)op cond:(NSString *)cond words:(NSArray *)words line:(NSString *)line {
    
    NSDictionary *jcond_assem_map = @{@"a":@(0), @"e":@(1), @"ne":@(2), @"gr":@(3), @"ge":@(4), @"l":@(5), @"le":@(6), @"z":@(1), @"nz":@(2)};
    
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"jump" error_reporter:nil src:line];
    [tokenStream add_bitsWithIndex:0 shift:6 mask:3 value:@(3)];
    
    
    if ([op isEqualToString:@"ret"]) {
        if (words.count != 0) {
            NSLog(@"error:Ret don't take an argument");
            return;
        } else {
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0x3f value:@(0x28)];
        }
    } else if (words.count != 1){
        NSLog(@"error:Jumps need a target to jump to");
        return;
    }else {
        
        if ([@[@"su",@"ds"] containsObject:[op substringToIndex:2]]) {
            [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(1)];
        } else {
            [tokenStream add_bitsWithIndex:0 shift:3 mask:1 value:@(0)];
        }
        
        if (cond.length == 0) {
            [tokenStream add_bitsWithIndex:0 shift:0 mask:7 value:@(7)];
        } else {
            [tokenStream add_bitsWithIndex:0 shift:0 mask:7 value:jcond_assem_map[cond]];
        }
        
        HSWordItem *wordItem = words.firstObject;
        
        if ([wordItem.type isEqualToString:@"const"]) {
            NSInteger offset = [wordItem.value integerValue];
            
            if (offset >= MIN_SBYTE && offset <= MAX_SBYTE) {
                if (offset < 0) {
                    offset += 256;
                }
                [tokenStream add_byteWithIndex:1 value:offset];
            }else if (offset >= MIN_WORD && offset <= MAX_WORD) {
                [tokenStream add_bitsWithIndex:0 shift:4 mask:1 value:@(1)];
                [tokenStream add_wordWithIndex:1 value:offset];
            }
            
        } else if ([wordItem.type isEqualToString:@"label"]) {
            
            if ([wordItem.value hasPrefix:@":"]) {
                [tokenStream add_bitsWithIndex:0 shift:4 mask:1 value:@(1)];
                tokenStream.jump_label = @[@(1),wordItem.value,@(YES)];
                [tokenStream add_wordWithIndex:1 value:0];
            } else {
                tokenStream.jump_label = @[@(1),wordItem.value,@(NO)];
                [tokenStream add_byteWithIndex:1 value:0];
            }
            
        }else {
            NSLog(@"Jumps need either a constant or a label as argument, not a:%@",wordItem.type);
            return;
        }
    }
    
    [self finishTokenStream:tokenStream explicit_placement:-1];
    
}

- (void)assem_miscWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line {
    if ([op isEqualToString:@"stop"]) {
        if (words.count != 0) {
            NSLog(@"error:Stop doesn't take arguments");
            return;
        }
        
        HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"misc" error_reporter:nil src:line];
        [tokenStream add_bitsWithIndex:0 shift:0 mask:0xff value:@(0xff)];
        [self finishTokenStream:tokenStream explicit_placement:-1];
        
    } else if ([@[@"bitset",@"bitclr"] containsObject:op]){
        
        if (words.count != 2) {
            NSLog(@"error:Bitset/bitclr needs 2 arguments: bit and mod/reg");
            return;
        }
        
        NSInteger bit = [words[0] anum];
        if (bit < 0 || bit > 7) {
            NSLog(@"Bitset/bitclr bit must be between 0 and 7 (not %ld)",(long)bit);
            return;
        }
        
        id modreg = [words[1] amodreg];
        HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"misc" error_reporter:nil src:line];
        
        [tokenStream add_bitsWithIndex:0 shift:4 mask:0x0f value:@(0)];
        if ([op isEqualToString:@"bitset"]) {
            [tokenStream add_bitsWithIndex:0 shift:3 mask:0x1 value:@(0x01)];
        }else {
            [tokenStream add_bitsWithIndex:0 shift:3 mask:0x1 value:@(0x00)];
        }
        [tokenStream add_bitsWithIndex:0 shift:0 mask:0x7 value:@(bit)];
        [tokenStream add_byteWithIndex:1 value:[modreg integerValue]];
        
        [self finishTokenStream:tokenStream explicit_placement:-1];
    }else {
        NSLog(@"error:Unknown misc operator:%@",op);
        return;
    }
}

- (void)assem_spec_labelWithLabel:(HSWordItem *)label words:(NSArray *)words line:(NSString *)line {
    if (words.count != 0) {
        NSLog(@"error:A label doesn't take any arguments");
        return;
    }
    [self add_labelWithName:label.value];
}

- (void)assem_spec_dataWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line {
    
    if (words.count < 2) {
        NSLog(@"DAT[BW] needs at least 2 arguments: name, start");
        return;
    }
    
    NSInteger space = [space_types[which] integerValue];
    NSString *name;
    NSInteger start = -1;
    
    HSWordItem *wordItem0 = words[0];
    HSWordItem *wordItem1 = words[1];
    
    if ([wordItem0.value isEqualToString:@"*"]) {
        name = @"*";
    }else {
        name = wordItem0.aStr;
    }
    
    if ([wordItem1.value isEqualToString:@"*"]) {
        start = -1;
        if ([name isEqualToString:@"*"]) {
            NSLog(@"error:DAT[BW] both name and loc can't both be '*'");
            return;
        }
    }else {
        start = wordItem1.anum;
    }
    
    words = [words subarrayWithRange:NSMakeRange(2, words.count-2)];
    NSInteger length = 1;
    if (words.count >= 1) {
        HSWordItem *wordItem2 = words.firstObject;
        if ([[wordItem2 value] isEqualToString:@"*"]) {
            length = -1;
        }else {
            length = [wordItem2 anum];
        }
        words = [words subarrayWithRange:NSMakeRange(1, words.count-1)];
    }
    NSMutableArray *values = [NSMutableArray array];
    for (HSWordItem *w in words) {
        if ([@[@"arg",@"string"] containsObject:w.type] == NO) {
            NSLog(@"error:Word should have been an argument or string!");
            return;
        }else {
            if ([w.type isEqualToString:@"string"]) {
                for (int i = 0; i<[w.value length]; i++) {
                    int asc = [w.value characterAtIndex:i];
                    [values addObject:@(asc)];
                }
            }else {
                [values addObject:@(w.anum)];
            }
        }
    }
    
    if  (length > 0 && values.count > length){
        NSLog(@"error:Data has length of %ld but more values (%lu) then length",(long)length,(unsigned long)values.count);
        return;
        
    }
    NSInteger real_Length = length;
    if (length > 0) {
        real_Length = length;
    }else {
        real_Length = values.count;
    }
    NSInteger test;
    if (start > 0) {
        test = start + real_Length;
    }else {
        test = real_Length;
    }
    
    if (test <= 0) {
        NSLog(@"error:There is no data for the space");
        return;
    }else if (test >= 255){
        NSLog(@"error:Data has overflowed the space");
    }
    
    if ([name isEqualToString:@"*"] && length > 0 && length > values.count) {
        while (values.count < length) {
            [values addObject:@(0)];
        }
    }
    
    if (values.count > 0) {
        
        NSInteger token_to_create = (values.count+14)/15;
        NSInteger last = values.count % 15;
        
        NSInteger var_index = 0;
        NSInteger val_count;
        for (int i = 0; i<token_to_create; i++) {
            HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"data" error_reporter:nil src:line];
            [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(space+1)];
            
            if ((i < (token_to_create-1)) || last == 0) {
                val_count = 15;
            }else {
                val_count = last;
            }
            
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0xf value:@(val_count)];
            
            if ([name isEqualToString:@"*"] == NO) {
                [tokenStream add_byteWithIndex:1 value:i*15];
                [tokenStream add_vnameIndex:1 space:space name:name];
            }else {
                [tokenStream add_byteWithIndex:1 value:start+(i*15)];
            }
            
            NSInteger token_index = 2;
            
            if ([which isEqualToString:@"B"]) {
                for (int j=0; j<val_count; j++) {
                    [tokenStream add_byteWithIndex:token_index value:[values[var_index] integerValue]];
                    token_index += 1;
                    var_index += 1;
                }
            }else {
                for (int j=0; j<val_count; j++) {
                    [tokenStream add_wordWithIndex:token_index value:[values[var_index] intValue]];
                    token_index += 2;
                    var_index += 1;
                }
            }
            
            [self finishTokenStream:tokenStream explicit_placement:-1];
        }
        
    }
    if ([name isEqualToString:@"*"] == NO) {
        [self add_variableWithSpace:space name:name start:start length:real_Length];
    }
}

- (void)assem_spec_data_lcdWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line {
    if (words.count < 4) {
        NSLog(@"error:DATA needs at least 4 arguments: row, col, len, val1");
        return;
    }
    HSWordItem *wordItem0 = words[0];
    NSInteger row = -1;
    NSInteger column = -1;
    NSInteger length = -1;
    
    if ([wordItem0.value isEqualToString:@"*"]) {
        NSLog(@"error:DATA needs a real row");
        return;
    }else {
        row = wordItem0.anum;
    }
    
    HSWordItem *wordItem1 = words[1];
    
    if ([wordItem1.value isEqualToString:@"*"]) {
        NSLog(@"error:DATA needs a real column");
        return;
    }else {
        column = wordItem1.anum;
    }
    
    NSInteger start = row * ROW_LENGTH + column;
    
    
    HSWordItem *wordItem2 = words[2];
    
    if ([wordItem2.value isEqualToString:@"*"]) {
        length = -1;
    }else {
        length = wordItem2.anum;
    }
    
    words = [words subarrayWithRange:NSMakeRange(3, words.count-3)];
    
    NSMutableArray *values = [NSMutableArray array];
    for (HSWordItem *w in words) {
        if ([@[@"arg",@"string"] containsObject:w.type] == NO) {
            NSLog(@"error:Word should have been an argument or string!");
            return;
        }else {
            if ([w.type isEqualToString:@"string"]) {
                for (int i = 0; i<[w.value length]; i++) {
                    int asc = [w.value characterAtIndex:i];
                    [values addObject:@(asc)];
                }
            }else {
                [values addObject:@(w.anum)];
            }
        }
    }
    if  (length > 0 && values.count > length){
        NSLog(@"error:Data has length of %ld but more values (%lu) then length",(long)length,(unsigned long)values.count);
        return;
        
    }
    NSInteger real_Length = length;
    if (length > 0) {
        real_Length = length;
    }else {
        real_Length = values.count;
    }
    
    if ((start + real_Length) > ROW_LENGTH*COL_LENGTH) {
        NSLog(@"error:Data has overflowed the space");
        return;
    }
    
    if (length > 0 && length > values.count) {
        while (values.count < length) {
            [values addObject:@" "];
        }
    }
    
    if (values.count > 0) {
        
        NSInteger token_to_create = (values.count+14)/15;
        NSInteger last = values.count % 15;
        
        NSInteger val_index = 0;
        NSInteger val_count;
        for (int i = 0; i<token_to_create; i++) {
            HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"data" error_reporter:nil src:line];
            [tokenStream add_bitsWithIndex:0 shift:4 mask:3 value:@(3)];
            
            if ((i < (token_to_create-1)) || last == 0) {
                val_count = 15;
            }else {
                val_count = last;
            }
            
            [tokenStream add_bitsWithIndex:0 shift:0 mask:0x0f value:@(val_count)];
            
            NSInteger loc = start + i * 15;
            [tokenStream add_byteWithIndex:1 value:loc/ROW_LENGTH];
            [tokenStream add_byteWithIndex:2 value:loc%ROW_LENGTH];
            
            
            NSInteger token_index = 3;
            
            for (int j=0; j<val_count; j++) {
                [tokenStream add_byteWithIndex:token_index value:[values[val_index] integerValue]];
                token_index += 1;
                val_index += 1;
            }
            [self finishTokenStream:tokenStream explicit_placement:-1];
        }
    }
    
}

- (void)assem_spec_binaryWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line {
    
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"binary" error_reporter:nil src:line];
    NSInteger token_index = 0;
    
    for (HSWordItem *w in words) {
        if ([@[@"arg",@"string",@"const"] containsObject:w.type] == NO) {
            NSLog(@"error:Word should have been an argument or string!");
            return;
        }else {
            if ([w.type isEqualToString:@"string"]) {
                for (int i = 0; i<[w.value length]; i++) {
                    int asc = [w.value characterAtIndex:i];
                    [tokenStream add_byteWithIndex:token_index value:asc];
                    token_index += 1;
                }
            }else {
                [tokenStream add_byteWithIndex:token_index value:w.anum];
                token_index += 1;
            }
        }
    }
    [self finishTokenStream:tokenStream explicit_placement:-1];
}

- (void)assem_spec_reserveWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line {
    if (words.count != 2) {
        NSLog(@"error:RESERV[ABW] needs 2 arguments: start, length");
        return;
    }
    [self reserve_name_space:[space_types[which] integerValue] start:[words[0] anum] length:[words[1] anum]];
}

- (void)assem_spec_versionWithWords:(NSArray *)words line:(NSString *)line {
    if (words.count != 2) {
        NSLog(@"error:VERSION needs 2 arguments: major, minor");
        return;
    }
    
    NSInteger major = [words[0] anum];
    NSInteger minor = [words[1] anum];
    if (major < 0 || major > 15) {
        NSLog(@"major version must be between 0 and 15 (not %ld)", (long)major);
    }
    if (minor < 0 || minor > 15) {
        NSLog(@"minor version must be between 0 and 15 (not %ld)", (long)major);
    }
    [self add_versionWithMajor:major minjor:minor];
}

- (void)assem_spec_begin_endWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line {
    if (words.count < 1) {
        NSLog(@"error:BEGIN/END need a type argument");
        return;
    }
    
    if ([[words[0] aStr] isEqualToString:@"FIRMWARE"]) {
        if ([op isEqualToString:@"BEGIN"]) {
            if (words.count != 1) {
                NSLog(@"error:FIRMWARE doesn't take any arguments. ");
                return;
            }
            [self add_beginWithType:@"firmware" arg1:-1 arg2:-1 arg3:-1];
        }else {
            [self add_endWithType:@"firmware"];
        }
    }else if ([[words[0] aStr] isEqualToString:@"EVENT"]){
        if ([op isEqualToString:@"BEGIN"]) {
            if (words.count != 4) {
                NSLog(@"error:EVENT needs 3 arguments: mod/reg, mask, value ");
                return;
            }
            NSInteger modreg = [[words[1] amodreg] integerValue];
            NSInteger mask = [words[2] anum];
            NSInteger value = [words[3] anum];
            
            [self add_beginWithType:@"event" arg1:modreg arg2:mask arg3:value];
        }else {
            [self add_endWithType:@"event"];
        }
    }else if ([[words[0] aStr] isEqualToString:@"MAIN"]){
        if ([op isEqualToString:@"BEGIN"]) {
            if (words.count != 1) {
                NSLog(@"error:MAIN doesn't take any arguments. ");
                return;
            }
            
            [self add_beginWithType:@"main" arg1:-1 arg2:-1 arg3:-1];
        }else {
            [self add_endWithType:@"main"];
        }
    }else {
        NSLog(@"error:BEGIN/END needs one of: MAIN, EVENT, FIRMWARE ");
        return;
    }
}

- (void)assem_spec_limitsWords:(NSArray *)words line:(NSString *)line {
    if (words.count != 5) {
        NSLog(@"error:LIMITS needs exactly 5 arguments");
        return;
    }
    
    NSInteger b_limit = [words[0] anum];
    NSInteger w_limit = [words[1] anum];
    NSInteger l_limit = [words[2] anum];
    NSInteger e_handlers = [words[3] anum];
    NSInteger t_bytes_limit = [words[4] anum];
    
    [self set_limitsWithb_lim:b_limit w_lim:w_limit l_lim:l_limit e_handlers:e_handlers t_lim:t_bytes_limit];
    
}

- (void)assem_spec_deviceWords:(NSArray *)words line:(NSString *)line {
    
    NSString *name = @"";
    
    if (words.count == 2) {
        name = @"";
    }else if (words.count == 3){
        name = [words[2] aStr];
    }else {
        NSLog(@"error:LIMITS needs exactly 5 arguments");
        return;
    }
    
    NSString *device_type = [words[0] aStr];
    NSInteger location = [words[1] anum];
    
    if ([[HSParserDataManager shareParserDataManager] add_deviceWithloc:location dtype:device_type name:name]) {
        [self add_devicesWithType:[HSParserDataManager shareParserDataManager].device_types[device_type] location:@(location).stringValue size:[[HSParserDataManager shareParserDataManager].device_storage[device_type] integerValue]];
    }
}

- (void)assem_spec_insertWords:(NSArray *)words line:(NSString *)line {
    if (words.count != 2) {
        NSLog(@"error:INSERT needs type and filename arguments");
        return;
    }
    
    NSString *type = [words[0] aStr];
    NSString *f_name = [words[1] aStr];
    
    if ([@[@"tokens", @"binary"] containsObject:type.lowercaseString] == NO) {
        NSLog(@"error:INSERT type must be one of: 'tokens', 'binary");
        return;
    }
    
    if ([@[@"tokens"] containsObject:type.lowercaseString]) {
        NSLog(@"error:INSERT TOKENS should have been consumed higher up! Eek!");
        return;
    }
    
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"binary" error_reporter:nil src:line];
    tokenStream.binary_file = f_name;
    [self finishTokenStream:tokenStream explicit_placement:-1];
    
}


- (void)assem_spec_commsWords:(NSArray *)words line:(NSString *)line {
    if (words.count != 1) {
        NSLog(@"error:FINISH doesn't have any arguments");
        return;
    }
    HSTokenStreamItem *tokenStream = [HSTokenStreamItem tokenStreamItemWithType:@"comms" error_reporter:nil src:line];
    [tokenStream add_bitsWithIndex:0 shift:0 mask:0xf value:@(0x8)];
    [tokenStream add_uwordWithIndex:1 value:[words[0] anum]];
    [self finishTokenStream:tokenStream explicit_placement:-1];
    
    [self set_commsWithSector_size:[words[0] anum]];
}

- (void)assem_spec_finishWords:(NSArray *)words line:(NSString *)line {
    if (words.count != 0) {
        NSLog(@"error:FINISH doesn't have any arguments");
        return;
    }
    // 啥也没有
    
}


//explicit_placement:默认传-1
- (void)finishTokenStream:(HSTokenStreamItem *)streamItem explicit_placement:(NSInteger)explicit_placement {
    if (streamItem == nil || streamItem.valid == NO) {
        return;
    }
    [self add_tokenStreamItem:streamItem explicit_placement:explicit_placement];
}
//explicit_placement:默认传-1
- (void)add_tokenStreamItem:(HSTokenStreamItem *)streamItem explicit_placement:(NSInteger)explicit_placement {
    
    NSString *spec_type = streamItem.type;
    if ([spec_type isEqualToString:@"finish"]) {
        if (self.current_sections.count > 0) {
            NSLog(@"error:FINISH must be after all sections.");
            return;
        }
    }else if ([spec_type isEqualToString:@"data"]) {
        if (self.current_sections.count == 0 || [@[@"main",@"event"] containsObject:self.current_sections.lastObject] == NO) {
            NSLog(@"error:DATA must be in a 'main' or 'event' section.");
            return;
        }
    }else if ([spec_type isEqualToString:@"binary"]) {
        if (self.current_sections.count == 0 || [@[@"firmware"] containsObject:self.current_sections.lastObject] == NO) {
            NSLog(@"error:INSERT BINARY must be in a 'firmware' section.");
            return;
        }
    }
    
    //    Make jump labels local to the section
    NSArray *jump = streamItem.jump_label;
    if (jump.count > 0) {
        if ([jump[1] hasPrefix:@":"] == NO) {
            // a local jump
            streamItem.jump_label = @[jump[0],[NSString stringWithFormat:@"%@_%d",jump[1],self.section_count],jump[2]];
        }
    }
    
    if (explicit_placement < 0) {
        [self.token_stream addObject:streamItem];
    }else {
        if (explicit_placement >= self.token_stream.count) {
            NSLog(@"error:Explicitly placing a token after the end of the stream.");
            return;
        }else {
            [self.token_stream insertObject:streamItem atIndex:explicit_placement];
        }
    }
}


- (void)add_labelWithName:(NSString *)name {
    
    if (self.current_sections.count == 0 || [@[@"main",@"event"] containsObject:self.current_sections.lastObject] == NO) {
        NSLog(@"error:Labels must be in a 'main' or 'event' section.");
        return;
    }
    NSString *new_name;
    if ([name hasPrefix:@":"]) {
        new_name = name;
    }else {
        new_name = [NSString stringWithFormat:@"%@_%d",name,self.section_count];
        NSLog(@"xdjxdjxdjmmmmmmmmmmmmmmm%@, %lu",new_name,(unsigned long)self.token_stream.count);
    }
    
    if ([self.labels.allKeys containsObject:new_name]) {
        NSLog(@"error:Label %@ defined twice in the same section.",new_name);
        return;
    }
    
    [self.labels setObject:@(self.token_stream.count) forKey:new_name];
    
}

- (void)add_variableWithSpace:(NSInteger)space name:(NSString *)name start:(NSInteger)start length:(NSInteger)length {
    if ([name isEqualToString:@"*"]) {
        name = @"";
    }
    if (length < 0) {
        NSLog(@"error:Negative length of data is not allowed");
        return;
    }
    [self.name_space[space] addObject:@[@"data",name,@(start),@(length)]];
}

// # reserve space in the variable name spaces
//# ARGS: space 0-2, start & length are numbers
- (void)reserve_name_space:(NSInteger)space  start:(NSInteger)start length:(NSInteger)length {
    if (self.section_count > 0) {
        NSLog(@"error:RESERV must be before all sections.");
        return;
    }
    
    if (start < 0 || length <= 0) {
        NSLog(@"error:Negative start or length is not allowed");
        return;
    }
    
    if ((start + length) > [self.limits[space] integerValue]) {
        NSLog(@"error:Reserve space extends beyond the max for %@ space.", space_names[space]);
        return;
    }
    [self.name_space[space] addObject:@[@"rsvd",@"",@(start),@(length)]];
}

- (void)add_versionWithMajor:(NSInteger)major minjor:(NSInteger)minjor {
    if (self.section_count > 0) {
        NSLog(@"error:VERSION must be before all sections.");
        return;
    }
    self.version = [NSString stringWithFormat:@"%ld_%ld",(long)major,(long)minjor];
}

- (void)add_beginWithType:(NSString *)type arg1:(NSInteger)arg1 arg2:(NSInteger)arg2 arg3:(NSInteger)arg3 {
    
    
    // do checks to see if this start is Ok
    if ([@[@"firmware", @"main", @"event"] containsObject:type]) {
        if (self.current_sections.count > 0) {
            NSLog(@"error:This section %@ must be the first section",type);
            return;
        }
        if (self.download_type.count > 0) {
            if ([@[@"main", @"event"] containsObject:self.download_type[0]] && [@[@"firmware"] containsObject:type]) {
                NSLog(@"error:Can't mix 'firmware' and 'main'/'event' sections");
                return;
            }
            else if ([self.download_type[0] isEqualToString:@"firmware"]){
                NSLog(@"error:Can't mix 'firmware' and 'main'/'event' sections or have multiple 'firmware' sections");
                return;
            }else if ([type isEqualToString:@"main"] && [self.download_type containsObject:@"main"]) {
                NSLog(@"error:Can't have multiple 'main' sections");
                return;
            }
            [self.download_type addObject:type];
        }else {
            [self.download_type addObject:type];
        }
    }
    
    [self.current_sections addObject:type];
    [self.section_breaks addObject:@[type,@(self.token_stream.count),@(-1)]];
    [self.section_args addObject:@[@(arg1),@(arg2),@(arg3)]];
    self.section_count += 1;
    
}

- (void)add_endWithType:(NSString *)type {
// check if we are actually in the section?
    if ([self.current_sections containsObject:type] == NO) {
        NSLog(@"error:Not in section %@, so can't end it",type);
        return;
    }
    int i = 0;
    while (i < self.section_breaks.count) {
        NSString *s_type = self.section_breaks[i][0];
        id start = self.section_breaks[i][1];
        id end = self.section_breaks[i][2];
        NSLog(@"%@",end);
        if ([s_type isEqualToString:type]) {
            self.section_breaks[i] = @[s_type,start,@(self.token_stream.count)];
            break;
        }
        i += 1;
    }
    [self.current_sections removeObject:type];
}

- (void)set_limitsWithb_lim:(NSInteger)b_lim w_lim:(NSInteger)w_lim l_lim:(NSInteger)l_lim e_handlers:(NSInteger)e_handlers t_lim:(NSInteger)t_lim {
    
    if (self.section_count > 0) {
        NSLog(@"error:LIMITS must be before all sections.");
        return;
    }
    NSMutableArray *new_limits = [NSMutableArray arrayWithArray:@[@(b_lim),@(w_lim),@(l_lim),@(e_handlers),@(t_lim)]];
    for (int i = 0; i< 5; i++) {
        if ([new_limits[i] integerValue] < 0) {
            new_limits[i] = @(0);
        }else if ([new_limits[i] integerValue] > [MAX_LIMITS[i] integerValue]) {
            new_limits[i] = MAX_LIMITS[i];
        }
    }
    self.limits = new_limits.copy;
}

- (void)set_commsWithSector_size:(NSInteger)sector_size {
    if (sector_size < 0) {
        NSLog(@"error:Comms sector_size can't be less then 0");
        return;
    }
    self.sector_size = sector_size;
}

- (void)add_devicesWithType:(NSString *)type location:(NSString *)location size:(NSInteger)size {
    if (self.section_count > 0) {
        NSLog(@"error:DEVICE must be before all sections");
        return;
    }
    [self.devices setObject:@[type,@(size)] forKey:location];
}

- (BOOL)fixup_jumps {
    
    NSLog(@"%@",self.labels);
    
    NSMutableArray *c_lengths = [self calc_cumulative_lengths];
    
    for (int i = 0; i< self.token_stream.count; i++) {
        HSTokenStreamItem *t = self.token_stream[i];
        if (t.jump_label.count > 0) {
            NSInteger index = [t.jump_label[0] integerValue];
            NSString *name = t.jump_label[1];
            BOOL big = [t.jump_label[2] boolValue];
            NSLog(@"xdjjump%ld,%@,%d",(long)index,name,big);
            if ([[self.labels allKeys] containsObject:name] == NO) {
                NSLog(@"error:Reference to an unknown label %@",name);
                return NO;
            }
        }
    }
    
    while (1) {
        c_lengths = [self calc_cumulative_lengths];
        NSInteger i = 0;
        
        while (i < self.token_stream.count) {
            HSTokenStreamItem *t = self.token_stream[i];
            if (t.jump_label.count > 0) {
                NSInteger index = [t.jump_label[0] integerValue];
                NSString *name = t.jump_label[1];
                BOOL big = [t.jump_label[2] boolValue];
                NSLog(@"xdjjump%ld,%@,%d",(long)index,name,big);
                NSInteger my_address = [c_lengths[i+1] integerValue];
                NSInteger target_addess = [c_lengths[[self.labels[name] integerValue]] integerValue];
                NSInteger offset = target_addess - my_address;
                
                if (big == NO) {
                    if (offset > MAX_SBYTE || offset < MIN_SBYTE) {
                        [t fixup_jumpBig:YES offset:offset];
                        break;
                    }else {
                        [t fixup_jumpBig:NO offset:offset];
                    }
                }else {
                    [t fixup_jumpBig:YES offset:offset];
                }
                
            }
            i += 1;
        }
        if (i >= self.token_stream.count) {
            // no more fixups needed
            break;
        }
    }
    return YES;
}

- (NSMutableArray *)calc_cumulative_lengths {
    
    NSInteger cumulative_length = 0;
    NSMutableArray *arrM = [NSMutableArray array];
    for (int i = 0; i< self.token_stream.count; i++) {
        HSTokenStreamItem *t = self.token_stream[i];
        [arrM addObject:@(cumulative_length)];
        cumulative_length += t.get_byte_len;
    }
    [arrM addObject:@(cumulative_length)];
    return arrM;
}


- (NSArray *)creat_header {
    NSArray *versionArr = [self.version componentsSeparatedByString:@"_"];
    if (self.version.length == 0) {
        NSLog(@"error:Version wasn't set");
        return nil;
    }else if ([versionArr.firstObject integerValue] < 0 || [versionArr.firstObject integerValue] > 2){
        NSLog(@"error:This assembler only handles major versions 0 to 2 (not %@)",versionArr.firstObject);
        return nil;
    }
    NSMutableArray *header_list = [NSMutableArray array];
    if ([self.download_type.firstObject isEqualToString:@"firmware"]) {
        NSMutableArray *bytes = [NSMutableArray array];
        for (int i = 0;i < self.token_stream.count ; i++) {
            HSTokenStreamItem *t = self.token_stream[i];
            [bytes addObjectsFromArray:[t get_token_bits]];
        }
        [header_list addObjectsFromArray:@[@(0),@(0),@(0),@(0),]];
        header_list[0] = [self word_to_bytesWithWord:bytes.count][0];
        header_list[1] = [self word_to_bytesWithWord:bytes.count][1];
        NSInteger crc = [self calculateWithBytes:bytes];
        header_list[2] = [self word_to_bytesWithWord:crc][0];
        header_list[3] = [self word_to_bytesWithWord:crc][1];
        return @[@"firmware",self.version,header_list];
    }else {
        NSMutableArray *bytes = [NSMutableArray array];
        for (int i = 0;i < self.token_stream.count ; i++) {
            HSTokenStreamItem *t = self.token_stream[i];
            [bytes addObjectsFromArray:[t get_token_bits]];
        }
        [header_list addObjectsFromArray:@[@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),]];
        NSMutableArray *module_list = [NSMutableArray array];
        NSMutableArray *event_list = [NSMutableArray array];
        NSInteger main_offset = 0;
        
        if ([versionArr.firstObject integerValue] == 1) {
            NSInteger offset = 0;
            NSInteger maxl = [self get_max_location];
            for (int i = 0; i < maxl+1; i++) {
                NSArray *dty = [self get_loc_type_and_sizeWithLoc:i];
                [module_list addObjectsFromArray:dty];
                offset += [dty[1] integerValue];
            }
            [module_list addObjectsFromArray:@[@(0xff),@(0)]];
        }
        
        if (module_list.count > 0) {
            header_list[4] = @(header_list.count);
            [header_list addObjectsFromArray:module_list];
        }
        
        NSMutableArray *c_lengths = [self calc_cumulative_lengths];
        for (int i = 0; i<self.section_breaks.count; i++) {
            NSArray *breakArr = self.section_breaks[i];
            NSLog(@"%@",breakArr);
            
            if ([breakArr.firstObject isEqualToString:@"main"]) {
                main_offset = [c_lengths[[breakArr[1] intValue]] integerValue];
            }else {
                NSArray *arg = self.section_args[i];
                [event_list addObject:@[c_lengths[[breakArr[1] intValue]],arg.firstObject,arg[1],arg[2]]];
            }
        }
        
        if (event_list.count > 0) {
            header_list[5] = @(header_list.count);
        }else {
            header_list[5]= @(0);
        }
        NSInteger final_header_bytes = header_list.count + event_list.count * 5;
        NSArray *header67Arr = [self word_to_bytesWithWord:main_offset+final_header_bytes];
        header_list[6] = header67Arr.firstObject;
        header_list[7] = header67Arr[1];
        
        
        for (NSArray *event in event_list) {
            NSInteger offset = [event[0] integerValue];
            
            NSArray *work_to_byte = [self word_to_bytesWithWord:offset+final_header_bytes];
            NSArray *work = @[work_to_byte.firstObject,work_to_byte.lastObject,event[1],event[2],event[3]];
            [header_list addObjectsFromArray:work];
        }
        
        // finally update the length and arc
        NSMutableArray *new_bytes = [NSMutableArray arrayWithArray:[header_list subarrayWithRange:NSMakeRange(4, header_list.count-4)]];
        [new_bytes addObjectsFromArray:bytes];
        
        header_list[0] = [self word_to_bytesWithWord:new_bytes.count].firstObject;
        header_list[1] = [self word_to_bytesWithWord:new_bytes.count].lastObject;
        
        NSInteger crc = [self calculateWithBytes:new_bytes];
        header_list[2] = [self word_to_bytesWithWord:crc].firstObject;
        header_list[3] = [self word_to_bytesWithWord:crc].lastObject;
        
        return @[@"program",self.version,header_list];
    }
    
}

- (NSArray *)word_to_bytesWithWord:(NSInteger)word {
    word = word & 0xffff;
    return @[@(((word >> 8) & 0xff)),@((word & 0xff))];
}


- (NSInteger)calculateWithBytes:(NSArray *)bytes {
    NSInteger crc = 0xffff;
    for (int i = 0; i < bytes.count; i++) {
        crc = crc ^ ([bytes[i] integerValue] << 8);
        for (int j = 0; j < 8; j++) {
            if ((crc & 0x8000) != 0) {
                crc = (crc << 1) ^ 0x1021;
            }else {
                crc = crc << 1;
            }
        }
    }
    return crc & 0xffff;
}

- (NSInteger)get_max_location {
    NSArray *keys = self.devices.allKeys;
    NSInteger max1;
    if (keys.count > 0) {
        keys = [keys sortedArrayUsingSelector:@selector(compare:)];
        max1 = [keys.lastObject integerValue];
    }else {
        max1 = -1;
    }
    return max1;
}


- (NSArray *)get_loc_type_and_sizeWithLoc:(NSInteger)loc {
    if ([self.devices.allKeys containsObject:@(loc)]) {
        return self.devices[@(loc).stringValue];
    }else {
        return @[@(0),@(0)];
    }
}

- (void)map_all_variables {
    
    self.name_space_map = [NSMutableArray arrayWithArray:@[@{}.mutableCopy,@{}.mutableCopy,@{}.mutableCopy]];
    for (int i = 0; i< 3; i++) {
         [self map_variables_in_space:i v_map:self.name_space_map[i]];
//        [self.name_space_map replaceObjectAtIndex:i withObject:dictM];
    }
    for (HSTokenStreamItem *t in self.token_stream) {
        NSLog(@"xdjkkkkkkk%@",t.token_info);
        for (NSArray *v_infoA in t.var_info) {
            NSInteger index = [v_infoA[0] integerValue];
            NSString *name = v_infoA[2];
            NSInteger space = [v_infoA[1] integerValue];
            if ([[self.name_space_map[space] allKeys] containsObject:name] == NO) {
                NSLog(@"error:Variable %@ not declared in %@ space. ",name,space_names[space]);
                return;
            }
            [t fixup_var_byteIndex:index value:[self.name_space_map[space][name][0] integerValue]];
        }
        NSLog(@"xdjsssssss%@",t.var_info);
        NSLog(@"xdjyyyyyyy%@",t.token_info);
    }
    
}

- (void)map_variables_in_space:(NSInteger)space v_map:(NSMutableDictionary *)v_map {
    
    NSInteger limit = [self.limits[space] integerValue];
    NSArray *variables = self.name_space[space];
    
    NSMutableArray *fixed_v_free = @[@[@(0),@(limit),@(limit)]].mutableCopy;
    
    for (NSArray *n_space in variables) {
        
        NSString *type = n_space.firstObject;
        NSString *name = n_space[1];
        NSInteger start = [n_space[2] integerValue];
        NSInteger length = [n_space[3] integerValue];
        
        if ([type isEqualToString:@"data"] && start<0) {
            continue;
        }
        
        NSInteger end = start+length;
        BOOL copy_to_end  = NO;
        NSArray *v_free = fixed_v_free;
        fixed_v_free = @[].mutableCopy;
        
        for (NSArray *freeArr in v_free) {
            
            NSInteger f_start = [freeArr[0] integerValue];
            NSInteger f_length = [freeArr[1] integerValue];
            NSInteger f_end = [freeArr[2] integerValue];
            
            if (copy_to_end) {
                [fixed_v_free addObject:@[@(f_start),@(f_length),@(f_end)]];
                continue;
            }
            if (start >= f_start && start < f_end) {
                if (end <= f_end) {
                    
                    if ([type isEqualToString:@"data"]) {
                        if ([v_map.allKeys containsObject:name]) {
                            if ([v_map[name][0] integerValue] != start || [v_map[name][1] integerValue] != length) {
                                NSLog(@"error:Data variable %@ declared twice and differently!",name);
                                return;
                            }
                        }else {
                            [v_map setObject:@[@(start),@(length)] forKey:name];
                        }
                    }
                    
                    if (start == f_start) {
                        if (end == f_end) {
                           // just remove this entry from the free list
                        }else {
                            // remove the front end of free block
                            [fixed_v_free addObject:@[@(end),@(f_length-length),@(f_end)]];
                        }
                    }else if (end == f_end){
                        [fixed_v_free addObject:@[@(f_start),@(f_length-length),@(start)]];
                    }else {
                        [fixed_v_free addObject:@[@(f_start),@(start-f_start),@(start)]];
                        [fixed_v_free addObject:@[@(end),@(f_end-end),@(f_end)]];
                    }
                    copy_to_end = YES;
                }else {
                    if ([type isEqualToString:@"data"]) {
                        NSLog(@"error:Fixed data variable %@ at %ld didn't fit!",name,(long)start);
                    }else {
                        NSLog(@"error:No room for Reserved data space at %ld",(long)start);
                    }
                    return;
                }
            }
        }
    }
    
//Now try to fit in the floating variables. Use a best-fit strategy where the smallest
//hole is used for each request. Also the block sizes are tried largest to smallest
    NSMutableArray *floats = [NSMutableArray array];
    for (NSArray *x in variables) {
        if ([x[0] isEqualToString:@"data"] && [x[2] integerValue] < 0) {
            [floats addObject:@[x[1],x[3]]];
        }
    }
    // 对数据按升序排序
    [floats sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1[1] compare:obj2[1]] == NSOrderedAscending;
    }];
    
    NSMutableArray *v_free = [NSMutableArray arrayWithArray:fixed_v_free];
    
    for (NSArray *f in floats) {
        
        NSInteger best_index = -1;
        NSInteger length = [f.lastObject integerValue];
        NSString *name = f.firstObject;
        
        for ( int i = 0; i<v_free.count; i++) {
            NSInteger f_length = [v_free[i][1] integerValue];
            if (f_length >= length) {
                if (f_length == length) {
                    best_index = i;
                    break;
                }else if (best_index == -1){
                    best_index = i;
                }else {
                    if (f_length < [v_free[best_index][1] integerValue]) {
                        best_index = i;
                    }
                }
            }
        }
        if (best_index == -1) {
            NSLog(@"error:Float data variable %@ (len:%ld) didn't fit!",name,(long)length);
            return ;
        }
        
        // add to the map and just free spaces
        NSInteger f_start = [v_free[best_index][0] intValue];
        NSInteger f_length = [v_free[best_index][1] intValue];
        NSInteger f_end = [v_free[best_index][2] intValue];
        
        if ([v_map.allKeys containsObject:name]) {
            NSLog(@"error:Data variable %@ declared twice",name);
            return ;
        }else {
            [v_map setObject:@[@(f_start),@(length)] forKey:name];
        }
        if (length == f_length) {
            [v_free removeObjectAtIndex:best_index];
        }else {
            v_free[best_index] = @[@(f_start+length),@(f_length-length),@(f_end)];
        }
//        NSLog(@"ashdfaskdgadfg%@---%@",v_map,v_free);
    }
}


@end
