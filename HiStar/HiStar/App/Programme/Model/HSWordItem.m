//
//  HSWordItem.m
//  HiStar
//
//  Created by 晴天 on 2019/7/21.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSWordItem.h"
#import "HSParserDataManager.h"

@implementation HSWordItem

+ (instancetype)wordItemType:(NSString *)type value:(id)value {
    HSWordItem *wordItem = [[HSWordItem alloc] init];
    wordItem.type = type;
    wordItem.value = value;
    return wordItem;
}


- (NSInteger)num {
   return [HSParserDataManager parse_baserWithsnum:self.value string:NO];
}

- (NSInteger)anum {
    if ([@[@"const",@"arg"] containsObject:self.type] == NO) {
        NSLog(@"error:Expected an argument or constant");
        return 0;
    }
    if ([self.type isEqualToString:@"const"]) {
        return [self.value integerValue];
    }else {
        return [self num];
    }
}


- (id)amodreg {
    if ([@[@"arg",@"modreg"] containsObject:self.type] == NO) {
        NSLog(@"error:Expected an argument or string");
        return @"";
    }else if ([self.type isEqualToString:@"modreg"]){
        return self.value;
    }else {
        return [HSParserDataManager parse_mod_reg:self.value];
    }
}

- (id)aStr {
    if ([@[@"arg",@"string"] containsObject:self.type] == NO) {
        NSLog(@"error: Expected an argument or string!");
        return @"";
    }else if ([self.value length] > 0){
        NSString *firstC = [self.value substringToIndex:1];
        if ([[ascii_letters stringByAppendingString:@"_"] containsString:firstC] == NO) {
//            NSLog(@"error:Word should have been a valid name!");
            return self.value;
        }else {
            return self.value;
        }
    }
    return @"";
}


- (NSString *)description {
    return [NSString stringWithFormat:@"wordItem type:%@ value:%@",self.type,self.value];
}

@end
