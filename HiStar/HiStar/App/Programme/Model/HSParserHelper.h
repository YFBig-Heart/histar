//
//  HSParserHelper.h
//  HiStar
//
//  Created by 晴天 on 2019/7/28.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define MAX_TUNE_STORE  17
//#define notes @"mMncCdDefFgGaAborR"
#define notes @"CDEFGABR"

#define MATH_PLUS  @"plus"
#define MATH_SUB  @"minus"
#define MATH_MULT @"multiply"
#define MATH_NOT @"not (bitwise)"
#define MATH_DIV @"divide"
#define MATH_MOD @"modulus"
#define MATH_LSHIFT @"left shift"
#define MATH_RSHIFT @"right shift"
#define MATH_AND @"and (bitwise)"
#define MATH_OR @"or (bitwise)"
#define MATH_XOR @"xor (bitwise)"


#define MOTOR_FWD @"Forward"
#define MOTOR_STP @"Stop"
#define MOTOR_BCK @"Backward"

#define MOTOR_P_RT @"Forward right"
#define MOTOR_P_LT @"Forward left"
#define MOTOR_P_BL @"Back left"
#define MOTOR_P_BR @"Back right"
#define MOTOR_P_SR @"Spin right"
#define MOTOR_P_SL @"Spin left"
#define MOTOR_P_RT_90 @"Turn right 90"
#define MOTOR_P_LT_90 @"Turn left 90"

#define MOTOR_CODE  @{@"F":@(0x80), @"B":@(0x40), @"S":@(0xc0), @"FD":@(0xa0), @"BD":@(0x60)}

#define DIRECTION_CODE @{MOTOR_FWD:@(0x80), MOTOR_BCK:@(0x40), MOTOR_STP:@(0xC0)}

#define DIRECTION_WITH_DIST_CODE @{MOTOR_FWD:@(0xa0), MOTOR_BCK:@(0x60), MOTOR_STP:@(0xC0)}

#define TRACKER_0_STATUS ElementString(@"On reflective surface")
#define TRACKER_1_STATUS ElementString(@"On non-reflective surface")



@interface HSParserHelper : NSObject

+ (instancetype)shareInstance;

+ (NSArray *)tone_notes;

+ (NSArray *)tone_durations;

+ (NSDictionary *)configuaration;

+ (NSDictionary *)config_origent;

+ (NSArray *)basic_ops;
+ (NSArray *)shift_ops;
+ (NSArray *)logical_ops;
+ (NSArray *)motor_configs;

+ (NSDictionary *)EVENT_DICT;
+ (NSArray *)get_event_modules;

+ (NSArray *)EVENT_ALIASES;
+ (NSArray *)EVENT_DICTAllkeysfilter:(NSArray *)keys;

+ (NSArray *)getEventValueFirstWithEventKey:(NSString *)event eventDict:(NSDictionary *)event_dict;

// 无效值为：-100000
+ (NSInteger)conv_to_tx_char:(NSString *)string_in;

+ (NSArray *)all_EmailTypes;


//字符串转成数字n_max和n_min 默认传None
+ (NSInteger)conv_to_numberString_in:(NSString *)string_in n_type:(NSString *)n_type n_min:(NSInteger)n_min n_max:(NSInteger)n_max;

+ (NSInteger)conv_to_timeWithString_in:(NSString *)string_in;

// 光照感应里面有用到
+ (id)module_remove_aliasWithAliasList:(NSArray *)aliaslist alias:(NSString *)alise;

+ (NSString *)vars_get_type_letter_form_name:(NSString *)name;


+ (id)config_loc_from_name:(NSString *)name;

+ (NSArray *)config_getWithLoc:(NSString *)loc;

+ (NSInteger)config_orient_from_loc:(NSInteger)loc;


+ (NSString *)find_alias_from_event:(NSString *)event;


@end

NS_ASSUME_NONNULL_END
