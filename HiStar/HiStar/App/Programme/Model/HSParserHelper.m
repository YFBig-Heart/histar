//
//  HSParserHelper.m
//  HiStar
//
//  Created by 晴天 on 2019/7/28.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSParserHelper.h"



@implementation HSParserHelper

+ (instancetype)shareInstance {
    static HSParserHelper *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[HSParserHelper alloc] init];
    });
    return _instance;
}
+ (NSArray *)tone_notes {
    //    return @[@"A (6th)", @"A# (6th)", @"B (6th)", @"C", @"C#", @"D", @"D#", @"E", @"F", @"F#",@"G", @"G#", @"A", @"A#", @"B", @"C (8th)", @"Rest"];
    return @[@"C",@"D",@"E",@"F",@"G",@"A",@"B",@"Rest"];
}
+ (NSArray *)tone_durations {
    return @[@"sixteenth",@"eighth", @"quarter", @"half", @"whole"];
}

+ (NSDictionary *)configuaration {
    return @{@"0": @[@"Line Tracker", @"LINE_TRACKER1"],
             @"1": @[@"LED", @"Right_LED"],
             @"3": @[@"Motor A", @"Right_Motor"],
             @"5": @[@"IR Receiver", @"IR_RECEIVER1"],
             @"6": @[@"Sounder", @"SOUNDER1"],
             @"7": @[@"IR Transmitter", @"IR_TRANSMITTER1"],
             @"8": @[@"Motor B", @"Left_Motor"],
             @"11": @[@"LED", @"Left_LED" ]};
}

+ (NSDictionary *)config_origent {
    return @{@"3": @[@"Motor A", @"MOTOR_A1"],@"8": @[@"Motor B", @"MOTOR_B1"]};
}
+ (NSArray *)basic_ops {
    return @[MATH_PLUS,MATH_SUB,MATH_MULT,MATH_NOT];
}
+ (NSArray *)shift_ops {
    return @[MATH_DIV,MATH_MOD,MATH_LSHIFT,MATH_RSHIFT];
}
+ (NSArray *)logical_ops {
    return @[MATH_AND,MATH_OR,MATH_XOR];
}

+ (NSArray *)motor_configs {
    return @[
             @[@"Left_Motor",@"Right_Motor"],
             @[@"Forward",@"Backward",@"Stop"],
             @[@"Forward",@"Backward",@"Forward right",@"Forward left",@"Spin right",@"Spin left",@"Back right",@"Back left",@"Stop"]];
}

+ (NSArray *)EVENT_DICTAllkeysfilter:(NSArray *)keys {
    NSMutableArray *arrM = [NSMutableArray arrayWithArray:[self EVENT_DICT].allKeys];
    for (NSString *key in keys) {
        if ([arrM containsObject:key]) {
            [arrM removeObject:key];
        }
    }
    return arrM.copy;
}

+ (NSArray *)EVENT_ALIASES {
    NSArray *EVENT_ALIASES = @[@[MOTHERBOARD, @"Keypad"], @[MOTHERBOARD, @"Countdown timer"],
                               @[@"Right_Motor", @"Drive"], @[@"SOUNDER1", @"Music"], @[@"SOUNDER1", @"Detect clap"],
                               @[@"IR_RECEIVER1", @"Data from another HiBot"], @[@"IR_RECEIVER1", @"Detect obstacle"],
                               @[@"IR_RECEIVER1", @"Data from TV remote"], @[@"LINE_TRACKER1", @"Line Tracker"]];
    return EVENT_ALIASES;
}

+ (NSDictionary *)EVENT_DICT {
    // Keypad
    NSDictionary *EVENT_DICT = @{ElementString(@"Keypad"): @[
                                         @[ElementString(@"up button pressed"), @[@"_devices", @(0)], @"button"],
                                         @[ElementString(@"left button pressed"), @[@"_devices", @(1)], @"button"],
                                         @[ElementString(@"down button pressed"), @[@"_devices", @(2)], @"button"],
                                         @[ElementString(@"right button pressed"), @[@"_devices", @3], @"button"],],
                                 ElementString(@"Countdown timer"): @[
                                         @[ElementString(@"timer finished"), @[@"_timers", @0], @"timer"],],
                                 //#@"Left Drive" : @[
                                 //#    @[@"Left Strain detected", @[@(None), 0], @"motor"],],
                                 ElementString(@"Drive") : @[
                                         @[ElementString(@"Strain detected"), @[@(None), @0], @"motor"],],
                                 ElementString(@"Music") : @[
                                         @[ElementString(@"Tune finished"), @[@(None), @0], @"timer"],],
                                 ElementString(@"Detect clap") : @[
                                         @[ElementString(@"Clap detected"), @[@(None), @2], @"clap"],],
                                 ElementString(@"Data from another HiBot"): @[
                                         @[ElementString(@"IR Character"), @[@(None), @0], @"irrx"],],
                                 ElementString(@"Detect obstacle") : @[
                                         @[ElementString(@"Obstacle at front"), @[@(None),@4], @"obstacle"],
                                         @[ElementString(@"Obstacle at left"), @[@(None),@5], @"obstacle"],
                                         @[ElementString(@"Obstacle at right"), @[@(None),@3], @"obstacle"],
                                         @[ElementString(@"Any Obstacle detected"), @[@(None),@6], @"obstacle"],],
                                 ElementString(@"Data from TV remote"): @[
                                         @[ElementString(@"Match #0 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #1 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #2 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #3 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #4 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #5 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #6 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #7 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Match #8 remote code"), @[@(None), @1], @"remote"],
                                         @[ElementString(@"Any match remote code"), @[@(None), @1],@"remote"],],
                                 ElementString(@"Line Tracker") : @[
                                         @[TRACKER_0_STATUS, @[@(None), @1], @"tracker"],
                                         @[TRACKER_1_STATUS, @[@(None), @1], @"tracker"],
                                         @[ElementString(@"Any change"), @[@(None), @1], @"tracker"],],
                                 
                                 ElementString(@"Receiving letter"):@[
                                         @[ElementString(@"red letter"),@[@"_emails",@(1)],@"red"],
                                         @[ElementString(@"blue letter"),@[@"_emails",@(2)],@"blue"],
                                         @[ElementString(@"green letter"),@[@"_emails",@(3)],@"green"],
                                         @[ElementString(@"purple letter"),@[@"_emails",@(4)],@"purple"],
                                         @[ElementString(@"gray letter"),@[@"_emails",@(5)],@"gray"],
                                         @[ElementString(@"pink letter"),@[@"_emails",@(5)],@"pink"]],
                                 };
    
    
    return EVENT_DICT;
}
+ (NSArray *)get_event_modules {
    return [[self EVENT_DICT] allKeys];
}

+ (NSArray *)getEventValueFirstWithEventKey:(NSString *)event eventDict:(NSDictionary *)event_dict {
    NSArray *array = [event_dict objectForKey:event];
    NSMutableArray *arrM = [NSMutableArray array];
    for (NSArray *valueArr in array) {
        [arrM addObject:valueArr.firstObject];
    }
    return arrM.copy;
}



+ (NSArray *)all_EmailTypes {
    return @[
             @[@"红色信件",ElementString(@"red letter"),@[@"_emails",@(1)],@"red"],
             @[@"蓝色信件",ElementString(@"blue letter"),@[@"_emails",@(2)],@"blue"],
             @[@"绿色信件",ElementString(@"green letter"),@[@"_emails",@(3)],@"green"],
             @[@"紫色信件",ElementString(@"purple letter"),@[@"_emails",@(4)],@"purple"],
             @[@"灰色信件",ElementString(@"grey letter"),@[@"_emails",@(5)],@"gray"],
             @[@"粉色信件",ElementString(@"pink letter"),@[@"_emails",@(6)],@"pink"]
             ];
}

+ (NSInteger)conv_to_tx_char:(NSString *)string_in {
    NSInteger value = 0;
    if (string_in.length > 1) {
        NSString *space = [string_in stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (space.length == 0) {
            string_in = @" ";
        }else {
            string_in = [string_in stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
    }
    if (string_in.length == 1){
        value = (int)[string_in characterAtIndex:0];
    }else if (string_in.length == 4 && [string_in hasPrefix:@"0x"] && [hexdigits  isEqualToString:[string_in substringWithRange:NSMakeRange(2, 1)]] && [hexdigits  isEqualToString:[string_in substringWithRange:NSMakeRange(3, 1)]]){
        value = strtoul([[string_in substringFromIndex:2] UTF8String], 0, 16);
    }else {
        NSLog(@"error: Invalid transmit character: %@. Must be a character or 0xhh where h is a hex digit.",string_in);
        return None;
    }
    return value;
}

+ (NSInteger)conv_to_timeWithString_in:(NSString *)string_in {
    string_in = [string_in stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    int time = (int)([string_in floatValue]*100.0);
    if (time<0) {
        NSLog(@"error:Time can't be negative");
        time = 0;
    }else if (time > MAX_WORD){
        NSLog(@"error:Time value is greater then 327.67 seconds");
        time = 327;// 最长327秒
    }else {
        time = time;
    }
    return time;
}

+ (id)module_remove_aliasWithAliasList:(NSArray *)aliaslist alias:(NSString *)alise {
    for (NSArray *rl in aliaslist) {
        if ([alise isEqualToString:ElementString(rl.lastObject)]) {
            return rl.firstObject;
        }
    }
    return alise;
}

+ (NSString *)vars_get_type_letter_form_name:(NSString *)name {
    HSVariableItem *var = [HSVariableItem getVariableItemWithName:name];
    if (var.isByteOrange) {
        return @"b";
    }else {
        return @"w";
    }
}

+ (NSInteger)conv_to_numberString_in:(NSString *)string_in n_type:(NSString *)n_type n_min:(NSInteger)n_min n_max:(NSInteger)n_max {
    
    if (string_in.length < 1) {
        NSLog(@"error:Empty number");
        return None;
    }
    NSInteger number = 0;
    if ([n_type isEqualToString:@"b"]) {
        if ([string_in isAllNum]) {
            number = [string_in integerValue];
        }
        if (number != None && (number < 0 || number > 255)) {
            NSLog(@"error:Number %ld is outside of the range: 0 -> 255" ,(long)number);
            return None;
        }
        
    }else if ([n_type isEqualToString:@"w"]){
        if ([string_in hasPrefix:@"-"]) {
            number = -1*([[string_in substringFromIndex:1] integerValue]);
        }else if ([string_in hasPrefix:@"+"]){
            number = ([[string_in substringFromIndex:1] integerValue]);
        }else {
            if ([string_in isAllNum]) {
                number = [string_in integerValue];
            }
        }
        
        if (number < -32767 || number > 32767) {
            NSLog(@"error: Number %ld is outside of the range: -32767 -> +32767",(long)number);
            return None;
        }
    }else {
        number = None;
    }
    
    if (number != None && n_min != None) {
        if (number < n_min) {
            NSLog(@"error: Number %ld is less than minimum %ld",(long)number,(long)n_min);
            //            number = None;
            number = 0;
        }
    }
    if (number != None && n_max != None) {
        if (number > n_max) {
            NSLog(@"error: Number %ld is greater than maximum %ld",(long)number,(long)n_min);
            //            number = None;
            number = n_max;
        }
    }
    
    return number;
}

+ (NSString *)config_loc_from_name:(NSString *)name {
    
    NSDictionary *configDict = [HSParserHelper configuaration];
    __block NSString *loc;
    [configDict enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSArray *_Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj[1] isEqualToString:name]) {
            loc = key;
        }
    }];
    return loc;
}

+ (NSArray *)config_getWithLoc:(NSString *)loc {
    NSDictionary *configDict = [HSParserHelper configuaration];
    if ([configDict.allKeys containsObject:loc]) {
        return [configDict objectForKey:loc];
    }else {
        return @[[NSNull null],[NSNull null]];
    }
}

+ (NSInteger)config_orient_from_loc:(NSInteger)loc {
    NSDictionary *configDict = [HSParserHelper config_origent];
    if ([configDict.allKeys containsObject:@(loc).stringValue]) {
        return [configDict[@(loc).stringValue] intValue];
    }else {
        return 0;
    }
    
}

+ (NSString *)find_alias_from_event:(NSString *)event {
    NSArray *modules = [HSParserHelper get_event_modules];
    for (NSString *m in modules) {
        NSArray *choices = [[HSParserHelper EVENT_DICT] objectForKey:m];
        for (NSArray *c in choices) {
            if ([c[0] isEqualToString:event]) {
                return m;
            }
        }
    }
    NSLog(@"error:Bad event %@",event);
    return @"";
}






@end
