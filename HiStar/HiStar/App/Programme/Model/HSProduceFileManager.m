    //
    //  HSProduceFileManager.m
    //  HiStar
    //
    //  Created by 晴天 on 2019/7/13.
    //  Copyright © 2019年 晴天. All rights reserved.
    //

#import "HSProduceFileManager.h"
#import "HSElementModel.h"
#import "HSProgrammerViewModel.h"
#import "YFFileManager.h"
#import "HSParserDataManager.h"

@implementation HSProduceFileManager

+ (void)get_code_streamWithItem:(HSElementModel *)item end_bric:(NSInteger)end_bric in_event:(BOOL)in_event codeLines:(NSMutableString *)codeLines varArray:(NSArray <NSMutableSet *>*)varArray {
        
    NSInteger bric_id = item.bric_id;
    do {
        // 记录所用到的变量
        for (HSVariableItem *varitem in item.userVaritems) {
            if (varitem.isByteOrange) {
                [varArray.firstObject addObject:@{@"name":varitem.name,@"value":@(varitem.value),@"isByteOrange":@(varitem.isByteOrange)}];
            }else {
                [varArray.lastObject addObject:@{@"name":varitem.name,@"value":@(varitem.value),@"isByteOrange":@(varitem.isByteOrange)}];
            }
        }
        [codeLines appendString:@"\n"];
        if ([item.codeName isEqualToString:@"Main"]) {
            bric_id += 1;
            continue;
        }
        NSString *digital = [item generate_codeWithEvent:item.sectionIndex != 0];
        [codeLines appendString:digital];
        
        if ([item.codeName isEqualToString:@"If"]) {
            NSArray *labels = [HSElementModel make_if_labelsWithBric_id:item.bric_id];
            [codeLines appendFormat:@"%@\n",labels[0]];
            
            for (HSElementModel *ifSubItem in item.subElementItems) {
                [self get_code_streamWithItem:ifSubItem end_bric:ifSubItem.end_bric_id in_event:in_event codeLines:codeLines varArray:varArray];
            }
            [codeLines appendFormat:@"bra %@\n",labels[2]];
            
            [codeLines appendFormat:@"%@\n",labels[1]];
            for (HSElementModel *if_noSubItem in item.if_noElementItems) {
                [self get_code_streamWithItem:if_noSubItem end_bric:if_noSubItem.end_bric_id in_event:in_event codeLines:codeLines varArray:varArray];
            }
            [codeLines appendFormat:@"%@\n",labels[2]];
            bric_id = item.end_bric_id;
        }else if ([item.codeName isEqualToString:@"Loop"]){
            NSArray *labels = [HSElementModel make_loop_labelsWithBric_id:item.bric_id];
            [codeLines appendFormat:@"%@\n",labels[0]];
            for (HSElementModel *loopSubItem in item.subElementItems) {
                [self get_code_streamWithItem:loopSubItem end_bric:loopSubItem.end_bric_id in_event:in_event codeLines:codeLines varArray:varArray];
            }
            [codeLines appendFormat:@"bra %@\n",labels[2]];
            [codeLines appendFormat:@"%@\n",labels[1]];
            bric_id = item.end_bric_id;
        }else {
            bric_id += 1;
        }
        
    } while (bric_id < end_bric);
    
}

    // 编程协议生成入口
+ (NSString *)produceProgrameFileEntranceWithVc:(UIViewController *)vc isSave:(BOOL)isSave {
    HSProgrammerViewModel *viewModel = [HSProgrammerViewModel shareProgrammerVM];
    if (viewModel.allElementArray.count == 0) {
        return @"";
    }
    NSString *fixhead = [HSProduceFileManager fixedHeadString];
    NSMutableString *get_code = [NSMutableString stringWithString:@"\n"];
    NSMutableSet *userVarByteM = [NSMutableSet set];
    NSMutableSet *userVarCharM = [NSMutableSet set];
    
    for (NSInteger j = 0; j<viewModel.vmElementTArray.count; j++) {
        NSMutableArray *sectionElementArr = [viewModel.vmElementTArray[j] mutableCopy];
        // 维持StartEvent 在前面
        HSElementModel *start_eventItem = [[sectionElementArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"funtction_type=%@",k_ft_startEvent]] firstObject];
        if (start_eventItem) {
            [sectionElementArr removeObject:start_eventItem];
            [sectionElementArr insertObject:start_eventItem atIndex:0];
        }
        BOOL in_event = j != 0;
        for (int i = 0; i<sectionElementArr.count; i++) {
            HSElementModel *item = sectionElementArr[i];
            if ([item.funtction_type isEqualToString:k_ft_addEvent]) {
                continue;
            }
            [self get_code_streamWithItem:item end_bric:item.end_bric_id in_event:in_event codeLines:get_code varArray:@[userVarByteM,userVarCharM]];
        }
        
        [get_code appendString:@"stop\n"];
        if (in_event == 0) {
            [get_code appendString:@"END MAIN\n"];
        }else {
            [get_code appendString:@"END EVENT\n"];
        }
    }
    
    // 拼接参数
    NSString *varString = @"";
    for (NSDictionary *dict in userVarByteM) {
        if ([[dict objectForKey:@"isByteOrange"] boolValue]) {
            varString = [varString stringByAppendingFormat:@"DATB %@ * 1 %@\n",[dict objectForKey:@"name"],[dict objectForKey:@"value"]];
        }else {
            varString = [varString stringByAppendingFormat:@"DATW %@ * 1 %@\n",[dict objectForKey:@"name"],[dict objectForKey:@"value"]];
        }
    }
    
    for (NSDictionary *dict in userVarCharM) {
        if ([[dict objectForKey:@"isByteOrange"] boolValue]) {
            varString = [varString stringByAppendingFormat:@"DATB %@ * 1 %@\n",[dict objectForKey:@"name"],[dict objectForKey:@"value"]];
        }else {
            varString = [varString stringByAppendingFormat:@"DATW %@ * 1 %@\n",[dict objectForKey:@"name"],[dict objectForKey:@"value"]];
        }
    }
    
    [get_code insertString:varString atIndex:0];
    [get_code insertString:fixhead atIndex:0];
    [get_code appendString:[self fixedFooterString]];
    
        // 写文件
    if (isSave) {
        UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:@"给程序取个名称" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UITextField *textfield = alertVc.textFields.firstObject;
            if (textfield.text.length > 0) {
                NSString *filePath = [YFFileManager getProgramProtolFilePath];
                filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",textfield.text]];
                NSError *error;
                BOOL suc = [get_code writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
                NSLog(@"%d",suc);
            }
        }];
        [alertVc addAction:cancelAction];
        [alertVc addAction:sureAction];
        [alertVc addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"请输入程序名称";
        }];
        [vc presentViewController:alertVc animated:YES completion:nil];
    }
    return get_code;
}

+ (NSArray *)parperProgrameFileText:(NSString *)fileText isDownLoad:(BOOL)isDownLoad {
        // 下载
    NSArray *downDataArr = [[HSParserDataManager shareParserDataManager] parserDatawithFileText:fileText];
    return downDataArr;
}
    
    // 头部固定的文件
+ (NSString *)fixedHeadString {
    NSString *fixedHeadFile = @"# Do not edit! Created automatically by EdWare.\n"
    "#\n"
    "# Edison code\n"
    "VERSION 2,0\n"
    "DEVICE tracker, 0, LINE_TRACKER1\n"
    "DEVICE led, 1, Right_LED\n"
    "DEVICE motor-a, 3, Right_Motor\n"
    "DEVICE irrx, 5, IR_RECEIVER1\n"
    "DEVICE beeper, 6, SOUNDER1\n"
    "DEVICE irtx, 7, IR_TRANSMITTER1\n"
    "DEVICE motor-b, 8, Left_Motor\n"
    "DEVICE led, 11, Left_LED\n"
    "BEGIN MAIN\n"
    "DATB _tune_store 0 17\n"
    "DATB _main_time_buffer * 8\n"
    "DATB _event_time_buffer * 8\n";
    
    return fixedHeadFile;
}
    // 结束固定文件
+ (NSString *)fixedFooterString {
        //    NSString *fixedFooterFile = @"stop\n"
        //    "END MAIN\n"
    return @"FINISH";
}



@end
