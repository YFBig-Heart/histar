//
//  HSElementModel+HSRead.m
//  HiStar
//
//  Created by 晴天 on 2019/7/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel+HSRead.h"

@implementation HSElementModel (HSRead)

- (NSString *)readsmall_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    
    NSString *code_string = @"";
    if ([name isEqualToString:@"Digital In"]) {
        code_string = [code_string stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        code_string = [code_string stringByAppendingFormat:@"and $1\n"];
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc @%@\n",input[1]];
    }else if ([name isEqualToString:@"Bumper"]){
        code_string = [code_string stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        code_string = [code_string stringByAppendingFormat:@"and $1\n"];
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc @%@\n",input[1]];
    }else if ([name isEqualToString:@"Infrared Data In"]){
        
        code_string = [code_string stringByAppendingFormat:@"movb %@ @%@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"char"],input[1]];
        
        code_string = [code_string stringByAppendingFormat:@"movb $0 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"char"]];
        
        code_string = [code_string stringByAppendingFormat:@"bitclr $0 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        
    }else if ([name isEqualToString:@"Remote"]){
        code_string = [code_string stringByAppendingFormat:@"movb %@ @%@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"match"],input[1]];
        
        code_string = [code_string stringByAppendingFormat:@"movb $0 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"match"]];
        
        code_string = [code_string stringByAppendingFormat:@"bitclr $1 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
    }else if ([name isEqualToString:@"Analogue In"]){
        
        code_string = [code_string stringByAppendingFormat:@"movw %@ @%@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"level"],input[1]];
    }else if ([name isEqualToString:@"Light Level"]){
        code_string = [code_string stringByAppendingFormat:@"movw %@ @%@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"level"],input[1]];
       
    }else if ([name isEqualToString:@"Read Clap Detect"]){
        
        code_string = [code_string stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        
        code_string = [code_string stringByAppendingFormat:@"bitclr $2 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        
        code_string = [code_string stringByAppendingString:@"and $4\n"];
        
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc @%@\n",input[1]];
        
    }else if([name isEqualToString:@"Read Obstacle Detect"]) {
        code_string = [code_string stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        
        code_string = [code_string stringByAppendingFormat:@"bitclr $3 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        code_string = [code_string stringByAppendingFormat:@"bitclr $4 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        code_string = [code_string stringByAppendingFormat:@"bitclr $5 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        code_string = [code_string stringByAppendingFormat:@"bitclr $6 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        
        code_string = [code_string stringByAppendingString:@"and $78/16\n"];
        
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc @%@\n",input[1]];
        
    }else {
        code_string = [code_string stringByAppendingFormat:@"movb %@ %%_cpu:acc\n",[HSElementModel make_mod_reg:input[0] reg_name:@"status"]];
        
        code_string = [code_string stringByAppendingString:@"and $1\n"];
        
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc @%@\n",input[1]];
    }
    return code_string;
}

- (NSString *)readinternal_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    
    NSString *code_string = @"";
    if ([name isEqualToString:@"Serial Data In"]) {
        
        code_string = [code_string stringByAppendingFormat:@"movb %%_devices:serrx @%@\n",input[0]];
        
        
    }else if ([name isEqualToString:@"Read Timer"]){
         code_string = [code_string stringByAppendingFormat:@"movw %%_timers:oneshot @%@\n",input[0]];
    }else {
        code_string = [code_string stringByAppendingFormat:@"movb %%_devices:button @%@\n",input[0]];
        code_string = [code_string stringByAppendingString:@"movb $0 %_devices:button\n"];
    }
    return code_string;
}

- (NSString *)readLight_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    NSArray *module_aliases = @[@[@"Left_LED", ElementString(@"Left light level")], @[@"Right_LED", ElementString(@"Right light level")],@[@"LINE_TRACKER1", ElementString(@"Line light level")]];
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSString *a = [HSParserHelper module_remove_aliasWithAliasList:module_aliases alias:input[0]];
    code_string = [code_string stringByAppendingFormat:@"movw %@ @%@\n",[HSElementModel make_mod_reg:a reg_name:@"lightlevel"],input[1]];
    return code_string;
    
}


@end
