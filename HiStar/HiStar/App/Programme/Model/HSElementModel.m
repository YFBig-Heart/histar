//
//  HSElementModel.m
//  HiStar
//
//  Created by petcome on 2019/6/5.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSElementModel.h"
#import <YYKit/YYKit.h>
#import "HSElementModel+HSControl.h"
#import "HSElementModel+HSRead.h"
#import "HSElementModel+HSData.h"
#import "HSElementModel+HSLoop.h"
#import "HSProgrammerViewModel.h"

#import "NSBundle+DAUtils.h"

@implementation HSElementModel

/*----------数据存储+模型转换----------**/
- (id)copyWithZone:(NSZone *)zone {
    HSElementModel *tempItem = [[HSElementModel allocWithZone:zone] init];
    NSDictionary *data = [self modelToJSONObject];
    BOOL suc = [tempItem modelSetWithJSON:data];
    tempItem.convert_dict = self.convert_dict;
    NSLog(@"%d",suc);
    return tempItem;
}

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"userVaritems":[HSVariableItem class],@"subElementItems":[HSElementModel class],@"convert_dict":[NSDictionary class],@"if_noElementItems":[HSElementModel class]};
}

+ (nullable NSArray<NSString *> *)modelPropertyBlacklist {
    return @[@"cellView"];
}
+ (NSArray *)bg_ignoreKeys {
    return @[@"cellView"];
}
+ (NSDictionary *)bg_objectClassInArray {
    return @{@"userVaritems":[HSVariableItem class],@"subElementItems":[HSElementModel class],@"convert_dict":[NSDictionary class],@"if_noElementItems":[HSElementModel class]};
}

/**
 如果模型中有自定义类变量,则实现该函数对应进行集合到模型的转换.
 注:字典转模型用.
 */
+(NSDictionary *)bg_objectClassForCustom{
    return @{@"userVaritems":[HSVariableItem class],@"subElementItems":[HSElementModel class],@"convert_dict":[NSDictionary class],@"if_noElementItems":[HSElementModel class]};
}
/**
 将模型中对应的自定义类变量转换为字典.
 模型转字典用.
 */
+(NSDictionary *)bg_dictForCustomClass{
    return @{@"userVaritems":[HSVariableItem class],@"subElementItems":[HSElementModel class],@"convert_dict":[NSDictionary class],@"if_noElementItems":[HSElementModel class]};
}

/*--------------------**/
- (NSMutableArray<HSVariableItem *> *)userVaritems {
    if (!_userVaritems) {
        _userVaritems = [NSMutableArray array];
    }
    return _userVaritems;
}

- (NSMutableArray *)subElementItems {
    if (!_subElementItems) {
        _subElementItems = [NSMutableArray array];
    }
    return _subElementItems;
}

- (NSMutableArray *)if_noElementItems {
    if (!_if_noElementItems) {
        _if_noElementItems = [NSMutableArray array];
    }
    return _if_noElementItems;
}

// 判断是否是 if 元素
-(BOOL)isIfConditionItem {
    BOOL isIfCondition = NO;
    if (self.moduleType == kModuleFlow) {
        isIfCondition = [self.codeName isEqualToString:@"If"];
    }
    return isIfCondition;
}
// 判断是否是 循环 元素
-(BOOL)isLoopItem {
    // condition
    BOOL isLoop = NO;
    if (self.moduleType == kModuleFlow) {
        isLoop =  [self.codeName isEqualToString:@"Loop"];
    }
    return isLoop;
}
- (void)setPropText:(NSString *)propText {
    _propText = [propText stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
}

- (CGSize)calculateHeightWidth {
    
    if (self.isLoopItem || self.self.isIfConditionItem) {
        UIImage *image = [UIImage imageNamed:@"flow_arrow"];
        CGFloat margin = 6;
        CGFloat maxheight = kElementWH;
        CGFloat width = kElementWH*2 + image.size.width+margin;
        for (int i = 0; i<self.subElementItems.count; i++) {
            HSElementModel *subItem = self.subElementItems[i];
            CGSize subSize = [subItem calculateHeightWidth];
            width += subSize.width + margin;
            width += image.size.width + margin;
            if (subSize.height > maxheight) {
                maxheight = subSize.height;
            }
        }
        if (self.if_noElementItems) {
            CGSize if_size = [self calculateif_noConditionHeightWidth];
            maxheight = maxheight + if_size.height + kElementWH;
            width = MAX(if_size.width, width);
        }else {
            maxheight = maxheight + kElementWH*2;
        }
        return CGSizeMake(width, maxheight);
    }else {
        return CGSizeMake(kElementWH, kElementWH);
    }
}

- (CGSize)calculateif_noConditionHeightWidth {
    UIImage *image = [UIImage imageNamed:@"flow_arrow"];
    CGFloat margin = 6;
    CGFloat maxheight_if = kElementWH;
    CGFloat width_if = kElementWH*2 + image.size.width+margin;
    for (int i = 0; i<self.if_noElementItems.count; i++) {
        HSElementModel *subItem = self.if_noElementItems[i];
        CGSize subSize = [subItem calculateHeightWidth];
        width_if += subSize.width + margin;
        width_if += image.size.width + margin;
        if (subSize.height > maxheight_if) {
            maxheight_if = subSize.height;
        }
    }
    
    return CGSizeMake(width_if, maxheight_if);
}


+(instancetype)elementItemWithMoudle:(kModuleType)moduleType funtionType:(NSString *)funtionType name:(NSString *)name norImage:(NSString *)norImageName {
    HSElementModel *model = [[HSElementModel alloc] init];
    model.funtction_type = funtionType;
    model.name = name;
    model.norImageName = norImageName;
    model.moduleType = moduleType;
    return model;
}

+ (NSDictionary *)getElementPlist {
    kLanaguageType lanType = [NSBundle isChineseLanguage];
    NSString *plistPath = @"elementModel";
    if (lanType == kLanaguageEn) {
        plistPath = @"elementModel_en";
    }else if (lanType == kLanaguageHans){
        plistPath = @"elementModel_jianti";
    }else if (lanType == kLanaguageHant){
        plistPath = @"elementModel_fanti";
    }
    NSString *pathString = [[NSBundle mainBundle] pathForResource:plistPath ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:pathString];
    return dict;
}

+ (NSArray <HSElementModel *>*)getElementItemsWithModuleType:(kModuleType)type {
    NSDictionary *dict = [self getElementPlist];
    NSArray *array = nil;
    switch (type) {
        case kModuleControl:{
            array = [dict objectForKey:@"control"];
        }
            break;
        case kModuleRead:{
            array = [dict objectForKey:@"read"];
        }
            break;
        case kModuleData:{
            array = [dict objectForKey:@"data"];
        }
            break;
        case kModuleFlow:{
            array = [dict objectForKey:@"flow"];
        }
            break;
        case kMoudleOther:{
            array = [dict objectForKey:@"other"];
        }
            break;
        default:
            return nil;
    }
    return [NSArray modelArrayWithClass:[HSElementModel class] json:array];
}

+ (NSArray *)make_if_labelsWithBric_id:(NSInteger)bric_id {
    return @[[NSString stringWithFormat:@":Bric%ld_If_True",(long)bric_id],
      [NSString stringWithFormat:@":Bric%ld_If_False",(long)bric_id],
      [NSString stringWithFormat:@":Bric%ld_If_Endif",(long)bric_id]];
}

+ (NSArray *)make_loop_labelsWithBric_id:(NSInteger)bric_id {
    return @[[NSString stringWithFormat:@":Bric%ld_Loop_body",(long)bric_id],
             [NSString stringWithFormat:@":Bric%ld_Loop_exit",(long)bric_id],
             [NSString stringWithFormat:@":Bric%ld_Loop_check",(long)bric_id]];
}

- (void)setCodeName:(NSString *)codeName {
    _codeName = codeName;
    [self creatInitatconvert_dict];// 创建初始值
}
// 默认值
- (void)creatInitatconvert_dict {
    if (self.convert_dict.allKeys.count != 0) {
        return;
    }
    if ([self.codeName isEqualToString:@"LED"]) {
        self.convert_dict = @{@"LED":@"Left_LED",@"Switch":@"0",@"Color":@"Blue",@"Var":CONSTANT};
    }else if ([self.codeName isEqualToString:@"Tone"]){
//        [0, 'SOUNDER1', u'C', u'sixteenth', u'', False]
//        [1, u'SOUNDER1', u'C', u'sixteenth', u'd1c3e5', False]
        self.convert_dict = @{@"ToneType":@(0),@"Tone":@"C",@"Time":@"sixteenth",@"Text":@""};
    }else if ([self.codeName isEqualToString:@"Obstacle Detection"]){
        self.convert_dict = @{@"Switch":@(0),@"Var":CONSTANT};
    }else if ([self.codeName isEqualToString:@"Motor"]){
        // 单边轮
        self.convert_dict = @{@"Motor_control":[HSParserHelper motor_configs][0][0],@"Direction":[HSParserHelper motor_configs][1][0],@"Var":CONSTANT,@"Speed":@"0",@"Var_speed":CONSTANT};
        
    }else if ([self.codeName isEqualToString:@"Motor Pair"]){
        self.convert_dict = @{@"Direction":[HSParserHelper motor_configs][2][0],@"Var":CONSTANT,@"Speed":@"0",@"Var_speed":CONSTANT};
        
    }else if ([self.codeName isEqualToString:@"LineTracker"]){
//        ['LINE_TRACKER1', u'On', '<-Constant', False]
        self.convert_dict = @{@"Switch":@(0),@"Var":CONSTANT};
    }else if ([self.codeName isEqualToString:@"Infrared Data Out"]){
        self.convert_dict = @{@"Char":@"0",@"Var":CONSTANT};
    }else if ([self.codeName isEqualToString:@"Set Timer"]){
        self.convert_dict = @{@"Word":@"0",@"Var":CONSTANT};
    }else if ([@[@"Read Obstacle Detect",@"Button",@"Read Clap Detect",@"Line Tracker",@"Infrared Data In",@"Remote"] containsObject:self.codeName]){
        HSVariableItem *variable1 = [HSVariableItem getVariable1];
        [self.userVaritems addObject:variable1];
        self.convert_dict = @{@"Var":variable1};
    }else if ([self.codeName isEqualToString:@"Read Timer"]){
        HSVariableItem *variable2 = [HSVariableItem getVariable2];
        [self.userVaritems addObject:variable2];
        self.convert_dict = @{@"Var":variable2};
    }else if ([self.codeName isEqualToString:@"Light Level"]){
        HSVariableItem *variable2 = [HSVariableItem getVariable2];
               [self.userVaritems addObject:variable2];
        self.convert_dict = @{@"Var":variable2,@"ReadLight":ElementString(@"Left light level")};
    }else if ([self.codeName isEqualToString:@"Increment"] || [self.codeName isEqualToString:@"Decrement"]){
        HSVariableItem *variable1 = [HSVariableItem getVariable1];
        [self.userVaritems addObject:variable1];
        self.convert_dict = @{@"Var":variable1};
    }else if ([self.codeName isEqualToString:@"Set Memory"]){
        HSVariableItem *variable1 = [HSVariableItem getVariable1];
        [self.userVaritems addObject:variable1];
        self.convert_dict = @{@"Var":variable1,@"Value":@(0)};
    }else if ([self.codeName isEqualToString:@"Copy"]){
        HSVariableItem *variable1 = [HSVariableItem getVariable1];
               [self.userVaritems addObject:variable1];
        self.convert_dict = @{@"Var":variable1,@"Var2":variable1};
    }else if ([self.codeName isEqualToString:@"Maths Basic"]){
        HSVariableItem *variable1 = [HSVariableItem getVariable1];
        [self.userVaritems addObject:variable1];
        self.convert_dict = @{@"Section":@"0",@"Var":variable1,@"Basic_op":[HSParserHelper basic_ops].firstObject,@"Basic_arg":@"0",@"Basic_var":CONSTANT,@"Divide_op":[HSParserHelper shift_ops].firstObject,@"Divide_arg":@"0",@"Divide_var":CONSTANT,@"Logical_op":[HSParserHelper logical_ops].firstObject,@"Logical_arg":@"0",@"Logical_var":CONSTANT};
    
    }else if ([self.codeName isEqualToString:@"Maths Advanced"]){
        HSVariableItem *variable2 = [HSVariableItem getVariable2];
        [self.userVaritems addObject:variable2];
        self.convert_dict = @{@"Section":@"0",@"Var":variable2,@"Basic_op":[HSParserHelper basic_ops].firstObject,@"Basic_arg":@"0",@"Basic_var":CONSTANT,@"Divide_op":[HSParserHelper shift_ops].firstObject,@"Divide_arg":@"0",@"Divide_var":CONSTANT,};
    }else if ([self.codeName isEqualToString:@"Wait"]) {
        NSString *eventType = [[HSParserHelper EVENT_DICT].allKeys firstObject];
        NSString *event_op = [HSParserHelper getEventValueFirstWithEventKey:eventType eventDict:[HSParserHelper EVENT_DICT]].firstObject;
        self.convert_dict = @{@"Section":@"0",@"Var":CONSTANT,@"Wait_time":@"0",@"Event_type":eventType,@"Event_op":[NSString checkIfNullWithString:event_op]};
        
    }else if ([self.codeName isEqualToString:@"If"]){
        NSString *eventType = [[HSParserHelper EVENT_DICT].allKeys firstObject];
        NSString *event_op = [HSParserHelper getEventValueFirstWithEventKey:eventType eventDict:[HSParserHelper EVENT_DICT]].firstObject;
        self.convert_dict = @{@"Section":@"0",@"Var":NO_VAR,@"Equation":@"=",@"Value_text":@"0",@"Event_type":eventType,@"Event_op":[NSString checkIfNullWithString:event_op]};
        
    }else if ([self.codeName isEqualToString:@"Loop"]){
        
        NSString *eventType = [[HSParserHelper EVENT_DICT].allKeys firstObject];
        NSString *event_op = [HSParserHelper getEventValueFirstWithEventKey:eventType eventDict:[HSParserHelper EVENT_DICT]].firstObject;
        
        self.convert_dict = @{@"Section":@"2",@"Var":NO_VAR,@"Equation":@"=",@"Value_text":@"0",@"Event_type":eventType,@"Event_op":[NSString checkIfNullWithString:event_op]};
    }else if ([self.codeName isEqualToString:@"Event"]){
        NSDictionary *event_dict = [[HSProgrammerViewModel shareProgrammerVM] getStartEvent_dictWithModel:nil filter:@[ElementString(@"Music"),ElementString(@"Detect clap")]];
        NSString *eventType = [event_dict allKeys].firstObject;
        NSString *event_op = [[[event_dict objectForKey:eventType] firstObject] firstObject];
        self.convert_dict = @{@"Event_type":eventType,@"Event_op":[NSString checkIfNullWithString:event_op]};
    }else if ([self.codeName isEqualToString:@"Email"]){
        self.convert_dict = @{@"Letter":@(0),};
    }
}

#pragma mark - 根据模型生成对应的编程语句


#pragma makr - 根据elementModel 类型生成对应的编程语句
- (NSString *)generate_codeWithEvent:(BOOL)in_event {
    NSString *string = [NSString stringWithFormat:@"# Bric id: %ld, name: %@\n",(long)self.bric_id,self.codeName];
    id var = [self.convert_dict objectForKey:@"Var"];
    HSVariableItem *varItem = var;
    if ([@[@"LED",@"LineTracker",@"Obstacle Detection"] containsObject:self.codeName]) {
        
        NSString *onoff = [[self.convert_dict objectForKey:@"Switch"] boolValue] ? @"On":@"Off";
        NSArray *inputArr;
        if ([var isKindOfClass:[HSVariableItem class]]) {
            if ([self.codeName isEqualToString:@"LED"]) {
                inputArr = @[[self.convert_dict objectForKey:@"LED"],onoff,varItem.name,@(varItem.value),self.convert_dict[@"Color"],@(in_event)];
            }else if ([self.codeName isEqualToString:@"Obstacle Detection"]){
                inputArr = @[@"IR_TRANSMITTER1",onoff,varItem.name,@(in_event)];
            }else {
                inputArr = @[@"LINE_TRACKER1",onoff,varItem.name,@(in_event)];
            }
        }else {
            if ([self.codeName isEqualToString:@"LED"]) {
                inputArr = @[[self.convert_dict objectForKey:@"LED"],onoff,CONSTANT,self.convert_dict[@"Color"],@(in_event)];
            }else if ([self.codeName isEqualToString:@"Obstacle Detection"]){
                inputArr = @[@"IR_TRANSMITTER1",onoff,CONSTANT,@(in_event)];
            }else {
                inputArr = @[@"LINE_TRACKER1",onoff,CONSTANT,@(in_event)];
            }
        }
        NSString *codeStr = [self digital_convertInput:inputArr command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
        
    }else if ([self.codeName isEqualToString:@"Beep"]){
        return [string stringByAppendingFormat:@"bitset 2 %%SOUNDER1:action\n"];
    }else if ([self.codeName isEqualToString:@"Tone"]){
        NSArray *inputArr = @[self.convert_dict[@"ToneType"],@"SOUNDER1",self.convert_dict[@"Tone"],self.convert_dict[@"Time"],self.convert_dict[@"Text"],@(in_event)];
        NSString *codeStr = [self tones_convertInput:inputArr command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Motor"]){
        NSString *varN = CONSTANT;
        NSString *varSpeedN = CONSTANT;
        if ([var isKindOfClass:[HSVariableItem class]]) {
            varN = varItem.name;
        }
        if ([self.convert_dict[@"Var_speed"] isKindOfClass:[HSVariableItem class]]) {
            varSpeedN = [self.convert_dict[@"Var_speed"] name];
        }
        NSArray *inputArr = @[self.convert_dict[@"Motor_control"],self.convert_dict[@"Direction"],varN,self.convert_dict[@"Speed"],varSpeedN,@"Unlimited - Forever",@"0",CONSTANT,@(in_event)];
        NSString *codeStr = [self motor_convertInput:inputArr command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Motor Pair"]){
        
        NSString *varN = CONSTANT;
        NSString *varSpeedN = CONSTANT;
        if ([var isKindOfClass:[HSVariableItem class]]) {
            varN = varItem.name;
        }
        if ([self.convert_dict[@"Var_speed"] isKindOfClass:[HSVariableItem class]]) {
            varSpeedN = [self.convert_dict[@"Var_speed"] name];
        }
        NSString *motorStr = [NSString stringWithFormat:@"%@+%@",[HSParserHelper motor_configs][0][1],[HSParserHelper motor_configs][0][0]];
        
        NSArray *inputArr = @[motorStr,self.convert_dict[@"Direction"],varN,self.convert_dict[@"Speed"],varSpeedN,@"Unlimited - Forever",@"0",CONSTANT,@(in_event)];
        NSString *codeStr = [self motor_convertInput:inputArr command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        
        return [string stringByAppendingString:codeStr];
        
    }else if ([self.codeName isEqualToString:@"Infrared Data Out"]){
        NSArray *inputArr = @[];
        if ([var isKindOfClass:[HSVariableItem class]]) {
            inputArr = @[@"IR_TRANSMITTER1",self.convert_dict[@"Char"],varItem.name,@(in_event)];
        }else {
            inputArr = @[@"IR_TRANSMITTER1",self.convert_dict[@"Char"],CONSTANT,@(in_event)];
        }
        NSString *codeStr = [self txir_convertInput:inputArr command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Set Timer"]){
        NSArray *inputArr = @[];
        if ([var isKindOfClass:[HSVariableItem class]]) {
            inputArr = @[self.convert_dict[@"Word"],varItem.name,@(in_event)];
        }else {
            inputArr = @[self.convert_dict[@"Word"],CONSTANT,@(in_event)];
        }
        NSString *codeStr = [self settimer_convertInput:inputArr command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
        
    }else if ([self.codeName isEqualToString:@"Read Obstacle Detect"]){
        NSString *codeStr = [self readsmall_convertInput:@[@"IR_RECEIVER1",varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Read Clap Detect"]){
        NSString *codeStr = [self readsmall_convertInput:@[@"SOUNDER1",varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Line Tracker"]){
        NSString *codeStr = [self readsmall_convertInput:@[@"LINE_TRACKER1",varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([@[@"Remote",@"Infrared Data In"] containsObject:self.codeName]){
        NSString *codeStr = [self readsmall_convertInput:@[@"IR_RECEIVER1",varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Button"]){
        
        NSString *codeStr = [self readinternal_convertInput:@[varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Read Timer"]){
        NSString *codeStr = [self readinternal_convertInput:@[varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Light Level"]){
        NSString *codeStr = [self readLight_convertInput:@[[self.convert_dict objectForKey:@"ReadLight"],varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Increment"] || [self.codeName isEqualToString:@"Decrement"]){
        NSString *codeStr =[self incdec_convertInput:@[varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Set Memory"]){
        NSString *codeStr = [self varset_convertInput:@[self.convert_dict[@"Value"],varItem.name,@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Copy"]){
        NSString *codeStr = [self varcopy_convertInput:@[varItem.name,[self.convert_dict[@"Var2"] name],@(in_event)] command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Maths Basic"]){
        HSVariableItem *varBasic = self.convert_dict[@"Basic_var"];
        HSVariableItem *varDivide = self.convert_dict[@"Divide_var"];
        HSVariableItem *varLogical = self.convert_dict[@"Logical_var"];
        NSString *basicN = CONSTANT;
        NSString *divideN = CONSTANT;
        NSString *logicalN = CONSTANT;
        if ([varBasic isKindOfClass:[HSVariableItem class]]) {
            basicN = varBasic.name;
        }
        if ([varDivide isKindOfClass:[HSVariableItem class]]) {
            divideN = varDivide.name;
        }
        if ([varLogical isKindOfClass:[HSVariableItem class]]) {
            logicalN = varLogical.name;
        }
        NSArray *input = @[[self.convert_dict objectForKey:@"Section"],varItem.name,self.convert_dict[@"Basic_op"],self.convert_dict[@"Basic_arg"],basicN,self.convert_dict[@"Divide_op"],self.convert_dict[@"Divide_arg"],divideN,self.convert_dict[@"Logical_op"],self.convert_dict[@"Logical_arg"],logicalN,@(in_event)];
        NSString *codeStr = [self math_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Maths Advanced"]){
        HSVariableItem *varBasic = self.convert_dict[@"Basic_var"];
        HSVariableItem *varDivide = self.convert_dict[@"Divide_var"];
        NSString *basicN = CONSTANT;
        NSString *divideN = CONSTANT;
        if ([varBasic isKindOfClass:[HSVariableItem class]]) {
            basicN = varBasic.name;
        }
        if ([varDivide isKindOfClass:[HSVariableItem class]]) {
            divideN = varDivide.name;
        }
        NSArray *input = @[[self.convert_dict objectForKey:@"Section"],varItem.name,self.convert_dict[@"Basic_op"],self.convert_dict[@"Basic_arg"],basicN,self.convert_dict[@"Divide_op"],self.convert_dict[@"Divide_arg"],divideN,@(in_event)];
        NSString *codeStr = [self math_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Wait"]){
        NSString *varN = CONSTANT;
        if ([varItem isKindOfClass:[HSVariableItem class]]) {
            varN = varItem.name;
        }
        NSArray *input = @[[self.convert_dict objectForKey:@"Section"],self.convert_dict[@"Wait_time"],varN,self.convert_dict[@"Event_type"],self.convert_dict[@"Event_op"],@(in_event)];
        NSString *codeStr = [self wait_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"If"]){
        NSString *varN = NO_VAR;
        if ([varItem isKindOfClass:[HSVariableItem class]]) {
            varN = varItem.name;
        }
        NSString *mod_aliaes = [HSParserHelper module_remove_aliasWithAliasList:[HSParserHelper EVENT_ALIASES] alias:self.convert_dict[@"Event_type"]];
        NSString *event_op = self.convert_dict[@"Event_op"];
        if (mod_aliaes == nil) {
            NSArray *arr = [[HSParserHelper EVENT_ALIASES] firstObject];
            mod_aliaes = arr.firstObject;
            event_op = [HSParserHelper getEventValueFirstWithEventKey:(NSString *)[arr lastObject] eventDict:[HSParserHelper EVENT_DICT]].firstObject;
        }
        
        NSArray *input = @[[self.convert_dict objectForKey:@"Section"],varN,self.convert_dict[@"Equation"],self.convert_dict[@"Value_text"],mod_aliaes,event_op,@(in_event)];
        
        NSString *codeStr = [self if_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Loop"]){
        NSString *varN = NO_VAR;
        if ([varItem isKindOfClass:[HSVariableItem class]]) {
            varN = varItem.name;
        }
        NSString *mod_aliaes = [HSParserHelper module_remove_aliasWithAliasList:[HSParserHelper EVENT_ALIASES] alias:self.convert_dict[@"Event_type"]];
        NSString *event_op = self.convert_dict[@"Event_op"];
        if (mod_aliaes == nil) {
            NSArray *arr = [[HSParserHelper EVENT_ALIASES] firstObject];
            mod_aliaes = arr.firstObject;
            event_op = [HSParserHelper getEventValueFirstWithEventKey:(NSString *)[arr lastObject] eventDict:[HSParserHelper EVENT_DICT]].firstObject;
        }
        NSArray *input = @[[self.convert_dict objectForKey:@"Section"],varN,self.convert_dict[@"Equation"],self.convert_dict[@"Value_text"],mod_aliaes,event_op,@(in_event)];
        
        NSString *codeStr = [self loop_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
        
    }else if ([self.codeName isEqualToString:@"Event"]) {
        
        NSString *mod_aliaes = [HSParserHelper module_remove_aliasWithAliasList:[HSParserHelper EVENT_ALIASES] alias:self.convert_dict[@"Event_type"]];
        NSString *event_op = self.convert_dict[@"Event_op"];
        if (mod_aliaes == nil) {
            NSArray *arr = [[HSParserHelper EVENT_ALIASES] firstObject];
            mod_aliaes = arr.firstObject;
            event_op = [HSParserHelper getEventValueFirstWithEventKey:(NSString *)[arr lastObject] eventDict:[HSParserHelper EVENT_DICT]].firstObject;
        }
        NSArray *input = @[mod_aliaes,event_op,@(in_event)];
        NSString *codeStr = [self event_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }else if ([self.codeName isEqualToString:@"Email"]){
         NSArray *letter = [[HSParserHelper all_EmailTypes] objectAtIndex:[self.convert_dict[@"Letter"] integerValue]];
         NSArray *input = @[letter.firstObject,@(in_event)];
        NSString *codeStr = [self email_convertInput:input command:@"gen_code" name:self.codeName bric_id:self.bric_id];
        return [string stringByAppendingString:codeStr];
    }

    return @"";
}

+ (NSString *)make_mod_reg:(NSString *)mod_name reg_name:(NSString *)reg_name {
    return [NSString stringWithFormat:@"%%%@:%@",[mod_name stringByReplacingOccurrencesOfString:@" " withString:@"_"],reg_name];
}

+ (NSArray <NSString *>*)make_labelsBric_id:(NSInteger)bric_id start:(NSInteger)start number:(NSInteger)number {
    NSMutableArray *result = [NSMutableArray array];
    for (int i = 0; i< number; i++) {
        [result addObject:[self make_labelBric_id:bric_id index:start+i]];
    }
    return result.copy;
}

+ (NSString *)make_labelBric_id:(NSInteger)bric_id index:(NSInteger)index {
    return [NSString stringWithFormat:@":Bric%ld_%ld",(long)bric_id,(long)index];
}




@end
