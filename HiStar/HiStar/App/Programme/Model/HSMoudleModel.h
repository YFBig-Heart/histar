//
//  HSMoudleModel.h
//  HiStar
//
//  Created by petcome on 2019/6/6.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class HSElementModel;
@interface HSMoudleModel : NSObject

@property (nonatomic,copy)NSString *title;
@property (nonatomic,assign)kModuleType type;
@property (nonatomic,strong)UIColor *bgColor;

@property (nonatomic,strong)NSArray <HSElementModel *>*elementItems;

+ (instancetype)moudleModellWithTitle:(NSString *)title type:(kModuleType)type;

+ (NSArray <HSMoudleModel *>*)getFunctionModels;

@end

NS_ASSUME_NONNULL_END
