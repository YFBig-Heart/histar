//
//  HSWordItem.h
//  HiStar
//
//  Created by 晴天 on 2019/7/21.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSWordItem : NSObject

@property (nonatomic,copy)NSString *type;
@property (nonatomic,strong)id value;


- (NSInteger)num;

- (NSInteger)anum;

- (id)amodreg;

- (id)aStr;

+ (instancetype)wordItemType:(NSString *)type value:(id)value;

@end

NS_ASSUME_NONNULL_END
