//
//  HSParserDataManager.m
//  HiStar
//
//  Created by 晴天 on 2019/7/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSParserDataManager.h"
#import "HSTokenItem.h"
#import "HSWordItem.h"
#import "HSTokenStreamItem.h"
#import <YYKit/YYKit.h>

#define special_names @[@"_index", @"_devices", @"_timers", @"_cpu"]
#define special_dtypes @[@"index", @"devices", @"timers", @"cpu"]

#define TOKEN_VERSION_STR  @"\x20"
#define TOKEN_VERSION_BYTE 0x20

#define TOKEN_DOWNLOAD_STR  @"\xf1"
#define TOKEN_DOWNLOAD_BYTE 0xf1

#define FIRMWARE_DOWNLOAD_STR  @"\xac"
#define FIRMWARE_DOWNLOAD_BYTE  0xac

#define FIRMWARE_VERSION_STR  @"\x20"
#define FIRMWARE_VERSION_BYTE  0x20

#define DOWNLOAD_BYTES_BETWEEN_PAUSES  1536
#define DOWNLOAD_PAUSE_MSECS  2000

#define WAVE_SAMPLE_RATE_HZ  44100


@interface HSParserDataManager()

/** json */
@property (nonatomic,strong)NSDictionary *jsonDict;

// 当前解析文件的 Token 数据集合模型
@property (nonatomic,strong)HSTokenItem *tokenItem;

@end

@implementation HSParserDataManager

+ (instancetype)shareParserDataManager {
    static dispatch_once_t onceToken;
    static HSParserDataManager *dataManager;
    dispatch_once(&onceToken, ^{
        dataManager = [[HSParserDataManager alloc] init];
    });
    return dataManager;
}

- (NSArray *)parserDatawithFileText:(NSString *)fileText {
    
    // 行拆成
    NSArray *allLines = [fileText componentsSeparatedByString:@"\n"];
    self.tokenItem = [[HSTokenItem alloc] init];
    
    for (int i = 0; i<allLines.count; i++) {
        NSString *line = allLines[i];
        // 对每一行进行处理
        NSArray *words = [HSParserDataManager chop_lineWithLineString:line];
        // 进一步转化
        [[HSParserDataManager shareParserDataManager] assem_lineWords:words lineString:line];
    }
    
    // 处理数据 
    [self.tokenItem map_all_variables];
    [self.tokenItem fixup_jumps];
    
    // 处理头部
    NSArray *creat_header = [self.tokenItem creat_header];
    NSLog(@"%@",creat_header);
    NSString *download_type = @"";
    NSString *version = @"";
    NSArray *header;
    if (creat_header) {
        download_type = header.firstObject;
        version = header[1];
        header = creat_header[2];
    }
    NSMutableArray *dowload_bytes = [NSMutableArray arrayWithObjects:@(TOKEN_DOWNLOAD_BYTE),@(TOKEN_VERSION_BYTE), nil];
    
    NSMutableString *dowload_str = [NSMutableString stringWithFormat:@"%@%@",TOKEN_DOWNLOAD_STR,TOKEN_VERSION_STR];
    
    for (id t in header) {
        [dowload_bytes addObject:t];
        [dowload_str appendFormat:@"%c",[t charValue]];
    }
    
    for (HSTokenStreamItem *t in self.tokenItem.token_stream) {
        NSMutableArray *bytes = [t get_token_bits];
        [dowload_bytes addObjectsFromArray:bytes];
        for (int i =0; i < bytes.count; i++) {
            [dowload_str appendFormat:@"%c",[bytes[i] charValue]];
        }
        NSLog(@"assdfasdfasdf%@",t.cached_bits);
    }
    NSLog(@"hahdhasd%@",dowload_bytes);
    [self clearData];
    Byte byte[dowload_bytes.count];
    for (int i =0; i < dowload_bytes.count; i++) {
        byte[i] = [dowload_bytes[i] integerValue];
    }
    NSData *data = [NSData dataWithBytes:byte length:dowload_bytes.count];
    if (data == nil) {
        data = [NSData data];
    }
    return @[dowload_bytes,data];
}
- (void)clearData {
    // 释放内存 数据还原
    _devices = nil;
    _locations = nil;
    _device_types = nil;
    _registers = nil;
}

- (void)assem_lineWords:(NSArray *)words lineString:(NSString *)line {
    
    if (words.count == 0) {
        return;
    }
    HSWordItem *t1 = words.firstObject;
    
    if ([t1.type isEqualToString:@"label"] == YES) {
        
        [self.tokenItem assem_spec_labelWithLabel:t1 words:[words subarrayWithRange:NSMakeRange(1, words.count-1)] line:line];
        
    }else if ([t1.type isEqualToString:@"op"]){
        NSString *op = t1.value;
        words = [words subarrayWithRange:NSMakeRange(1, words.count-1)];
        
        NSInteger size = 0;
        if ([op hasSuffix:@"b"] || [op hasSuffix:@"w"]) {
            size = 0;
            if ([op hasSuffix:@"w"]) {
                size = 1;
            }
            op = [op substringWithRange:NSMakeRange(0, op.length-1)];
            if ([op isEqualToString:@"mov"]) {
                [self.tokenItem assem_moveWithSize:size words:words special:@"" line:line];
            }else if ([op isEqualToString:@"dat"]){
                [self.tokenItem assem_dataWithSize:size words:words line:line];
            }else if ([@[@"not",@"dec",@"inc"] containsObject:op]){
                [self.tokenItem assem_uni_mathWithOp:op size:size words:words line:line];
            }else if ([@[@"add", @"sub", @"mul", @"cmp"] containsObject:op]){
                [self.tokenItem assem_basic_mathWithOp:op size:size words:words line:line];
            }else if ([@[@"shl", @"shr", @"div", @"mod"] containsObject:op]){
                [self.tokenItem assem_other_mathWithOp:op size:size words:words line:line];
            }else if ([@[@"push", @"pop"] containsObject:op]){
                [self.tokenItem assem_stackWithOp:op size:size words:words line:line];
            }else if ([@[@"or", @"and", @"xor"] containsObject:op]){
                [self.tokenItem assem_other_mathWithOp:op size:size words:words line:line];
            }else {
                
            }
            
        }else {
            if ([op isEqualToString:@"mova"]){
                [self.tokenItem assem_moveWithSize:0 words:words special:@"lcd" line:line];
            }else if ([op isEqualToString:@"movtime"]){
                [self.tokenItem assem_moveWithSize:1 words:words special:@"time" line:line];
            }else if ([@[@"conv", @"conv1", @"convm", @"cmptime"] containsObject:op]){
                // conv 8->16, conv 16->8 lsb, conv 16->8 msb, cmp time
                [self.tokenItem assem_convWithOp:op words:words line:line];
            }else if ([@[@"disable", @"enable"] containsObject:op]){
                [self.tokenItem assem_convWithOp:op words:words line:line];
            }else if ([@[@"ret", @"dbnz", @"dsnz"] containsObject:op]){
                [self.tokenItem assem_eventWithOp:op words:words line:line];
            }else if ([@[@"ret", @"dbnz", @"dsnz"] containsObject:op]){
                [self.tokenItem assem_jumpWithOp:op cond:@"" words:words line:line];
            }else if ([@[@"bra", @"bre", @"brne", @"brgr", @"brge", @"brl", @"brle", @"brz", @"brnz"] containsObject:op]){
               [self.tokenItem assem_jumpWithOp:@"branch" cond:[op substringFromIndex:2] words:words line:line];
            }else if ([@[@"suba", @"sube", @"subne", @"subgr", @"subge", @"subl", @"suble", @"subz", @"subnz"] containsObject:op]){
                [self.tokenItem assem_jumpWithOp:@"sub" cond:[op substringFromIndex:3] words:words line:line];
            }else if ([@[@"stop", @"bitset", @"bitclr"] containsObject:op]){
                [self.tokenItem assem_miscWithOp:op words:words line:line];
                
            }else if ([@[@"or", @"and", @"xor"] containsObject:op]){
                [self.tokenItem assem_other_mathWithOp:op size:0 words:words line:line];
            }else if ([@[@"DATB", @"DATW"] containsObject:op]){
                [self.tokenItem assem_spec_dataWithWhich:[op substringWithRange:NSMakeRange(3, 1)] words:words line:line];
            }else if ([op isEqualToString:@"DATA"]){
                [self.tokenItem assem_spec_data_lcdWithWhich:[op substringWithRange:NSMakeRange(3, 1)] words:words line:line];
            }else if ([@[@"BINB",] containsObject:op]){
                [self.tokenItem assem_spec_binaryWithWhich:[op substringWithRange:NSMakeRange(3, 1)] words:words line:line];
                
            }else if (op.length == 7 && [[op substringToIndex:6] isEqualToString:@"RESERV"]){
                [self.tokenItem assem_spec_reserveWithWhich:[op substringWithRange:NSMakeRange(6, 1)] words:words line:line];
                
            }else if ([@[@"BEGIN", @"END"] containsObject:op]){
                [self.tokenItem assem_spec_begin_endWithOp:op words:words line:line];
                
            }else if ([op isEqualToString:@"VERSION"]){
                [self.tokenItem assem_spec_versionWithWords:words line:line];
                
            }else if ([op isEqualToString:@"LIMITS"]){
                [self.tokenItem assem_spec_limitsWords:words line:line];
            }else if ([op isEqualToString:@"DEVICE"]){
                [self.tokenItem assem_spec_deviceWords:words line:line];
                
            }else if ([op isEqualToString:@"INSERT"]){
                [self.tokenItem assem_spec_insertWords:words line:line];
            }
//            else if ([op isEqualToString:@"COMMS"]){
//                [self.tokenItem assem_spec_commsWords:words line:line];
//            }
            else if ([op isEqualToString:@"FINISH"]){
                [self.tokenItem assem_spec_finishWords:words line:line];
            }else {
                NSLog(@"Unknown operator:%@",op);
                return;
            }
            
        }
        
    }
    
    return;
}

#pragma mark - assem 系列方法




#pragma mark - 转words 格式的数组
/*
 words:类似bitset $0 %Right_LED: -转成-> (( op,bitset),(arg,"$0"),(arg,"%Right_LED:output"))
 */
+ (NSArray *)chop_lineWithLineString:(NSString *)line {
    
    NSArray *segs = [self perchopLine:line];
    NSMutableArray *words = [NSMutableArray array];
    
    for (NSString *s in segs) {
        if ([s stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
            continue;
        }
        if ([s hasPrefix:@"#"]) {
            break;
        }
        if ([s hasPrefix:@":"]) {
            // a jump label
            HSWordItem *wordItem = [HSWordItem wordItemType:@"label" value:[s substringFromIndex:1]];
            [words addObject:wordItem];
        }else if (words.count == 0){
            // 第一个
            HSWordItem *wordItem = [HSWordItem wordItemType:@"op" value:s];
            [words addObject:wordItem];
        }else if (([s hasPrefix:@"\""] && [s hasSuffix:@"\""]) || ([s hasPrefix:@"‘"] && [s hasSuffix:@"’"])) {
            if (s.length <= 2) {
                continue;
            }else {
                HSWordItem *wordItem = [HSWordItem wordItemType:@"string" value:[s substringWithRange:NSMakeRange(1, s.length-2)]];
                [words addObject:wordItem];
            }
        }else if ([s hasPrefix:@"$"]){
            NSInteger num;
            if (s.length == 4 && [[s substringWithRange:NSMakeRange(1, 1)] isEqualToString:@"'"] && [[s substringWithRange:NSMakeRange(3, 1)] isEqualToString:@"'"]) {
                num = (NSInteger)[s characterAtIndex:2];
            }else {
                num = [HSParserDataManager parse_baserWithsnum:[s substringFromIndex:1] string:NO];
            }
            HSWordItem *wordItem = [HSWordItem wordItemType:@"const" value:@(num)];
            [words addObject:wordItem];
        }else if ([s hasPrefix:@"%"]){
            NSString *modreg = [self parse_mod_reg:[s substringFromIndex:1]];
            HSWordItem *wordItem = [HSWordItem wordItemType:@"modreg" value:modreg];
            [words addObject:wordItem];
            
        }else if ([s hasPrefix:@":"]){
            HSWordItem *wordItem = [HSWordItem wordItemType:@"label" value:[s substringFromIndex:1]];
            [words addObject:wordItem];
            
        }else if ([s hasPrefix:@"@"]){
            HSWordItem *wordItem = [HSWordItem wordItemType:@"var" value:[s substringFromIndex:1]];
            [words addObject:wordItem];
        }else {
            HSWordItem *wordItem = [HSWordItem wordItemType:@"arg" value:s];
            [words addObject:wordItem];
        }
    }
    NSLog(@"words:%@",words);
    
    return words;
}

+ (NSArray *)perchopLine:(NSString *)line {
    
    NSString *in_string = @"";
    BOOL in_escape = NO;
    NSString *comp = @"";
    NSMutableArray *components = [NSMutableArray array];
    
    for (int i = 0; i<line.length; i++) {
        NSString *c = [line substringWithRange:NSMakeRange(i, 1)];
        if ([c isEqualToString:@"\n"]) {
            break;
        }
        if (in_escape) {
            comp = [comp stringByAppendingString:c];
            in_escape = NO;
        }else if (in_string.length > 0 && [c isEqualToString:@"\\"]){
            in_escape = YES;
        }else if ([c isEqualToString:@"#"] && ![in_string containsString:c]){
            break;
        }else if (![in_string containsString:c] && [@[@"\""] containsObject:c]){
            in_string = c;
            comp = [comp stringByAppendingString:c];
        }else if (in_string.length > 0 && [c isEqualToString:in_string]){
            in_string = @"";
            comp = [comp stringByAppendingString:c];
        }else if (![in_string containsString:c] && [@[@" ",@"\t",@",",@"，"] containsObject:c]){
            if (comp.length > 0) {
                [components addObject:comp];
                comp = @"";
            }
        }else {
            comp = [comp stringByAppendingString:c];
        }
    }
    if (comp.length > 0) {
        [components addObject:comp];
    }
    return components.copy;
}


// 计算数值,string:YES 代表字节数组
+ (NSInteger)parse_baserWithsnum:(NSString *)snum  string:(BOOL)string {
    NSInteger num = 0;
    if ([snum hasPrefix:@"'"] && [snum hasSuffix:@"'"]) {
        num = [[snum substringWithRange:NSMakeRange(1, snum.length-1)] integerValue];
    }else if (string && [snum hasPrefix:@"\""] && [snum hasSuffix:@"\""]){
        NSLog(@"report_error:Error - use single quotes for an ascii constant");
        return num;
    }else if ([snum hasSuffix:@"/2"]){
        num = strtoul([[snum substringToIndex:snum.length-2] UTF8String], 0, 2);
    }else if ([snum hasSuffix:@"/10"]){
        num = strtoul([[snum substringToIndex:snum.length-3] UTF8String], 0, 10);
    }else if ([snum hasSuffix:@"/16"]){
        num = strtoul([[snum substringToIndex:snum.length-3] UTF8String], 0, 16);
    }else {
        num = [snum integerValue];
    }
    return num;
}

+ (NSString *)parse_mod_reg:(NSString *)smodreg {
    NSLog(@"parse_mod_reg%@",smodreg);
    NSInteger num = 0;
    if (smodreg.length == 2 && [hexdigits containsString:[smodreg substringWithRange:NSMakeRange(0, 1)]] && [hexdigits containsString:[smodreg substringWithRange:NSMakeRange(1, 1)]]) {
        num = strtoul([smodreg UTF8String], 0, 16);
    }else {
        if ([smodreg containsString:@":"]) {
            NSArray *part = [smodreg.lowercaseString componentsSeparatedByString:@":"];
            if (part.count != 2) {
                NSLog(@"error: Invalid mod/reg syntax: smodreg %@",smodreg);
                return @"";
            }
            NSDictionary *deviceDict = [HSParserDataManager shareParserDataManager].devices;
            NSString *dtype = @"";
            NSInteger loc;
            if ([deviceDict.allKeys containsObject:part[0]]) {
                NSLog(@"3333333");
                dtype = deviceDict[part[0]][0];
                loc = [deviceDict[part[0]][1] integerValue];
            }else if ([[HSParserDataManager shareParserDataManager].locations.allKeys containsObject:part[0]]){
                dtype = [HSParserDataManager shareParserDataManager].locations[part[0]][0];
                loc = [[HSParserDataManager shareParserDataManager].locations[part[0]][1] integerValue];
            }else {
                // 没有值
                NSLog(@"error:Didn't understand the module name/number: %@", part[0]);
                return @"";
            }
            
            NSDictionary *regs = [[HSParserDataManager shareParserDataManager].registers objectForKey:dtype];
            if ([regs.allKeys containsObject:part[1]]) {
                num = [regs[part[1]][0] integerValue] + (loc << 4);
            }else if ([hexdigits containsString:part[1]]){
                num = strtoul([part[1] UTF8String], 0, 16) + (loc << 4);
            }else {
                NSLog(@"error:Didn't understand the register name/number: %@",part[1]);
                return @"";
            }
        }else {
            NSLog(@"Error - can't parse %@ as a modreg", smodreg);
            return @"";
        }
    }
    NSLog(@"modred:%@-> %02ld",smodreg,(long)num);
    return @(num).stringValue;
}

- (BOOL)add_deviceWithloc:(NSInteger)loc dtype:(NSString *)dtype name:(NSString *)name {
    
    NSMutableDictionary *locations = self.locations.mutableCopy;
    NSMutableDictionary *devices = self.devices.mutableCopy;
    
    if ([self.devices.allKeys containsObject:dtype] == NO) {
        return NO;
    }
    if (loc < 0 || loc > 11) {
        return NO;
    }
    
    NSString *loc_hex = [hexdigits substringWithRange:NSMakeRange(loc, 1)];
    
    if ([self.locations.allKeys containsObject:loc_hex]) {
        return NO;
    }
    
    name = [name lowercaseString];
    
    if ([space_names containsObject:name]) {
        return NO;
    }else if ([self.devices.allKeys containsObject:name]){
        return NO;
    }
    
    if ([dtype isEqualToString:@"tracker"] && loc != 0) {
        return NO;
    }
    NSInteger motor_second_loc = 0;
    if ([dtype isEqualToString:@"motor-a"] || [dtype isEqualToString:@"motor-b"]) {
        if (loc == 11) {
            motor_second_loc = 0;
        }else {
            motor_second_loc = loc+1;
        }
        NSString *motor_second_hex = [hexdigits substringWithRange:NSMakeRange(motor_second_loc, 1)];
        if ([self.locations.allKeys containsObject:motor_second_hex]) {
            return NO;
        }
        locations[motor_second_hex] = @[@"motor_second",@(motor_second_loc)];
    }
    locations[loc_hex] = @[dtype,@(loc)];
    
    if (name.length > 0) {
        devices[name] = @[dtype,@(loc)];
    }
    self.devices = devices.copy;
    self.locations = locations.copy;
    return YES;
}



#pragma mark - 加載JSON 文件中的值
- (NSDictionary *)devices {
    if (!_devices) {
        _devices = [self.jsonDict objectForKey:@"devices"];
    }
    return _devices;
}

- (NSDictionary *)locations {
    if (!_locations) {
        _locations = [self.jsonDict objectForKey:@"locations"];
    }
    return _locations;
}
- (NSDictionary *)registers {
    if (!_registers) {
        _registers = [self.jsonDict objectForKey:@"registers"];
    }
    return _registers;
}

- (NSDictionary *)device_types {
    if (!_device_types) {
        _device_types = [self.jsonDict objectForKey:@"device_types"];
    }
    return _device_types;
}

- (NSDictionary *)device_storage {
    if (!_device_storage) {
        _device_storage = [self.jsonDict objectForKey:@"device_storage"];
    }
    return _device_storage;
}

- (NSDictionary *)jsonDict {
    if (!_jsonDict) {
         NSString *path = [[NSBundle mainBundle] pathForResource:@"devicesFixedValue.json" ofType:nil];
        NSData *data = [[NSData alloc] initWithContentsOfFile:path];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        _jsonDict = dict;
    }
    return _jsonDict;
}


@end
