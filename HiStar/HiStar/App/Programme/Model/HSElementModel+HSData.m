//
//  HSElementModel+HSData.m
//  HiStar
//
//  Created by 晴天 on 2019/7/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel+HSData.h"

@implementation HSElementModel (HSData)

- (NSString *)incdec_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    
    NSString *code_string = @"";
    NSString *size = [HSParserHelper vars_get_type_letter_form_name:input[0]];
    
    if ([name isEqualToString:@"Increment"]) {
        code_string = [code_string stringByAppendingFormat:@"inc%@ @%@\n",size,input[0]];
    }else {
        code_string = [code_string stringByAppendingFormat:@"dec%@ @%@\n",size,input[0]];
    }
    return code_string;
    
}

// 设置参数
- (NSString *)varset_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSString *size = [HSParserHelper vars_get_type_letter_form_name:input[1]];
    
    NSInteger number = [HSParserHelper conv_to_numberString_in:[input[0] stringValue] n_type:size n_min:None n_max:None];
    
    if (number == None){
        return @"";
    }
    code_string = [code_string stringByAppendingFormat:@"mov%@ $%@ @%@\n",size,input[0],input[1]];
    return code_string;
}

// 参数拷贝
- (NSString *)varcopy_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSString *size0 = [HSParserHelper vars_get_type_letter_form_name:input[0]];
    NSString *size1 = [HSParserHelper vars_get_type_letter_form_name:input[1]];
    
    
    if ([size0 isEqualToString:size1]) {
        code_string = [code_string stringByAppendingFormat:@"mov%@ @%@ @%@\n",size0,input[0],input[1]];
    }else if ([size0 isEqualToString:@"b"]){
        
        code_string = [code_string stringByAppendingFormat:@"movb @%@ %%_cpu:acc\n",input[0]];
        code_string = [code_string stringByAppendingString:@"conv\n"];
        
        code_string = [code_string stringByAppendingFormat:@"movw %%_cpu:acc @%@\n",input[1]];
        
    }else {
        code_string = [code_string stringByAppendingFormat:@"movw @%@ %%_cpu:acc\n",input[0]];
        code_string = [code_string stringByAppendingString:@"conv1\n"];
        
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc @%@\n",input[1]];
    }
    return code_string;
}


// 字/字节计算
- (NSString *)math_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    NSDictionary *op_dict = @{MATH_PLUS:@"add",
                              MATH_SUB:@"sub",
                              MATH_MULT:@"mul",
                              MATH_NOT:@"not",
                              MATH_DIV:@"div",
                              MATH_MOD:@"mod",
                              MATH_LSHIFT:@"shl",
                              MATH_RSHIFT:@"shr",
                              MATH_AND:@"and",
                              MATH_OR:@"or",
                              MATH_XOR:@"xor",
                              };
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    NSString *size = [HSParserHelper vars_get_type_letter_form_name:input[1]];
    
    code_string = [code_string stringByAppendingFormat:@"mov%@ @%@ %%_cpu:acc\n",size,input[1]];
    
    if ([input[0] integerValue] == 0) {
        if ([input[4] isEqualToString:CONSTANT]) {
            if ([input[2] isEqualToString:MATH_NOT]) {
                code_string = [code_string stringByAppendingFormat:@"not%@\n",size];
            }else {
                NSInteger number = [HSParserHelper conv_to_numberString_in:input[3] n_type:size n_min:None n_max:None];
                if (number == None) {
                    return @"";
                }
                code_string = [code_string stringByAppendingFormat:@"%@%@ $%ld\n",op_dict[input[2]],size,(long)number];
            }
        }else {
            if ([input[2] isEqualToString:MATH_NOT]) {
                code_string = [code_string stringByAppendingFormat:@"not%@\n",size];
            }else {
                code_string = [code_string stringByAppendingFormat:@"%@%@ @%@\n",op_dict[input[2]],size,input[4]];
            }
        }
        
    }else if ([input[0] integerValue] == 1){
        if ([input[7] isEqualToString:CONSTANT]) {
            NSInteger number = [HSParserHelper conv_to_numberString_in:input[6] n_type:size n_min:None n_max:None];
            if (number == None) {
                return @"";
            }
            code_string = [code_string stringByAppendingFormat:@"%@%@ $%@\n",op_dict[input[5]],size,@(number)];
        }else {
            code_string = [code_string stringByAppendingFormat:@"%@%@ @%@\n",op_dict[input[5]],size,input[7]];
        }
        
    }else {
        if ([input[10] isEqualToString:CONSTANT]) {
            NSInteger number = [HSParserHelper conv_to_numberString_in:input[9] n_type:@"b" n_min:None n_max:None];
            if (number == None) {
                return @"";
            }
            code_string = [code_string stringByAppendingFormat:@"%@ $%@\n",op_dict[input[8]],@(number)];
        }else {
            code_string = [code_string stringByAppendingFormat:@"%@ @%@\n",op_dict[input[8]],input[10]];
        }
    }
    code_string = [code_string stringByAppendingFormat:@"mov%@ %%_cpu:acc @%@\n",size,input[1]];
    
    return code_string;
}



@end
