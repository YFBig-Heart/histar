//
//  HSElementModel+HSData.h
//  HiStar
//
//  Created by 晴天 on 2019/7/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSElementModel (HSData)

- (NSString *)incdec_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 设置参数
- (NSString *)varset_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 参数拷贝
- (NSString *)varcopy_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 字/字节计算
- (NSString *)math_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

@end

NS_ASSUME_NONNULL_END
