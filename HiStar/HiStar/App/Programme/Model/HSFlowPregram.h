//
//  HSFlowPregram.h
//  HiStar
//
//  Created by 晴天 on 2019/8/4.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSFlowPregram : NSObject

// 这个代表一个程序，后期可能会增加 新事件
@property (nonatomic,strong)NSMutableArray *streams;
@property (nonatomic,strong)NSMutableDictionary *brics;
@property (nonatomic,strong)NSMutableArray *move_id;
@property (nonatomic,copy)NSString *start_id;
@property (nonatomic,assign)NSInteger *breic_count;


@end

NS_ASSUME_NONNULL_END
