//
//  HSElementModel+HSControl.h
//  HiStar
//
//  Created by 晴天 on 2019/7/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSElementModel (HSControl)

// command 暂时全部传 @"gen_code"
- (NSString *)digital_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 播放音乐
- (NSString *)tones_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 电动机
- (NSString *)motor_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 发射信号
- (NSString *)txir_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// 倒计时
- (NSString *)settimer_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

@end

NS_ASSUME_NONNULL_END
