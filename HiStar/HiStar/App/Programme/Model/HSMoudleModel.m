//
//  HSMoudleModel.m
//  HiStar
//
//  Created by petcome on 2019/6/6.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSMoudleModel.h"
#import "HSElementModel.h"

@implementation HSMoudleModel

+ (instancetype)moudleModellWithTitle:(NSString *)title type:(kModuleType)type {
    HSMoudleModel *model = [[HSMoudleModel alloc] init];
    model.title = title;
    model.type = type;
    switch (type) {
        case kModuleControl:
            model.bgColor = RGB16TOCOLOR(0x448ACA);
            break;
        case kModuleRead:
            model.bgColor = RGB16TOCOLOR(0x13B5B1);
            break;
        case kModuleData:
            model.bgColor = RGB16TOCOLOR(0x8F82BC);
            break;
        case kModuleFlow:
            model.bgColor = RGB16TOCOLOR(0xF8B551);
            break;
        default:
            break;
    }
    
    model.elementItems = [HSElementModel getElementItemsWithModuleType:type];
    if (type==kModuleFlow && model.elementItems.count >= 5) {
        model.elementItems = [model.elementItems subarrayWithRange:NSMakeRange(0, 5)];
    }else if (type == kModuleRead){
        model.elementItems = [model.elementItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"codeName!=%@",@"Read Clap Detect"]];
    }
    
    return model;
}

+ (NSArray <HSMoudleModel *>*)getFunctionModels {
    
    HSMoudleModel *control = [self moudleModellWithTitle:NSLocalizedString(@"Control module", nil) type:kModuleControl];
    HSMoudleModel *read = [self moudleModellWithTitle:NSLocalizedString(@"Control module", nil) type:kModuleRead];
    HSMoudleModel *data = [self moudleModellWithTitle:NSLocalizedString(@"Read module", nil) type:kModuleData];
    HSMoudleModel *flow = [self moudleModellWithTitle:NSLocalizedString(@"Flow module", nil) type:kModuleFlow];
    return @[control,read,data,flow];
}

@end
