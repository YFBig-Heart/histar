//
//  HSElementModel+HSRead.h
//  HiStar
//
//  Created by 晴天 on 2019/7/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSElementModel (HSRead)

- (NSString *)readsmall_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

- (NSString *)readinternal_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

- (NSString *)readLight_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;



@end

NS_ASSUME_NONNULL_END
