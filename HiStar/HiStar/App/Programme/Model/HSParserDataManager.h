//
//  HSParserDataManager.h
//  HiStar
//
//  Created by 晴天 on 2019/7/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN



@interface HSParserDataManager : NSObject

// json中对应的devices
@property (nonatomic,strong)NSDictionary *devices;
/** json中对应的locations */
@property (nonatomic,strong)NSDictionary *locations;

/** json中对应的registers */
@property (nonatomic,strong)NSDictionary *registers;

/** json中对应的device_types */
@property (nonatomic,strong)NSDictionary *device_types;

/** json中对应的device_storage */
@property (nonatomic,strong)NSDictionary *device_storage;

+ (instancetype)shareParserDataManager;
// 将文件转译成可执行的二进制文件
- (NSArray *)parserDatawithFileText:(NSString *)fileText;

+ (NSInteger)parse_baserWithsnum:(NSString *)snum  string:(BOOL)string;

- (BOOL)add_deviceWithloc:(NSInteger)loc dtype:(NSString *)dtype name:(NSString *)name;

+ (NSString *)parse_mod_reg:(NSString *)smodreg;


@end

NS_ASSUME_NONNULL_END
