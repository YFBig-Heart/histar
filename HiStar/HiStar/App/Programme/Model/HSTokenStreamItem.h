//
//  HSTokenStreamItem.h
//  HiStar
//
//  Created by 晴天 on 2019/7/20.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSTokenStreamItem : NSObject


// 默认Yes
@property (nonatomic,assign)BOOL valid;

@property (nonatomic,strong)NSMutableArray *token_info;
/** var info */
@property (nonatomic,strong)NSMutableArray *var_info;
// (1,'Bric_2_0',False)  类型的数组
@property (nonatomic,strong)NSArray *jump_label;

// 错误
@property (nonatomic,copy)NSString *error_reporter;

// 
@property (nonatomic,copy)NSString *type;

@property (nonatomic,strong)NSMutableArray *cached_bits;
/** 二进制文件 */
@property (nonatomic,copy)NSString *binary_file;

@property (nonatomic,copy)NSString *source_line;


@property (nonatomic,assign,readonly)NSInteger get_byte_len;

- (NSMutableArray *)get_token_bits;

+ (instancetype)tokenStreamItemWithType:(NSString *)type error_reporter:(NSString *__nullable)error_reporter src:(NSString *)src;


- (void)add_bitsWithIndex:(NSInteger)index shift:(NSInteger)shift mask:(NSInteger)mask value:(id)value;

- (void)add_byteWithIndex:(NSInteger)index value:(NSInteger)value;

- (void)add_wordWithIndex:(NSInteger)index value:(NSInteger)value;

- (void)add_uwordWithIndex:(NSInteger)index value:(NSInteger)value;

- (void)add_vnameIndex:(NSInteger)index space:(NSInteger)space name:(id)name;

- (void)fixup_jumpBig:(BOOL)big offset:(NSInteger)offset;

- (void)fixup_var_byteIndex:(NSInteger)index value:(NSInteger)value;

@end

NS_ASSUME_NONNULL_END
