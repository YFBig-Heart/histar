//
//  HSTokenItem.h
//  HiStar
//
//  Created by 晴天 on 2019/7/20.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class HSTokenStreamItem,HSWordItem;
@interface HSTokenItem : NSObject 

@property (nonatomic,strong)NSMutableDictionary *labels;
@property (nonatomic,strong)NSMutableArray *token_stream;
@property (nonatomic,strong)NSMutableArray *current_sections;
@property (nonatomic,strong)NSMutableArray *section_args;
@property (nonatomic,strong)NSMutableArray *section_breaks;
/** Variables requested in namespaces */
@property (nonatomic,strong)NSMutableArray *name_space;

@property (nonatomic,strong)NSMutableArray *name_space_map;

@property (nonatomic,strong)NSMutableDictionary *devices;

// 有默认值
@property (nonatomic,strong)NSArray *limits;

// major_minjor,用下划线隔断
@property (nonatomic,copy)NSString *version;

@property (nonatomic,strong)NSMutableArray *download_type;

@property (nonatomic,assign)int section_count;

// 默认-1
@property (nonatomic,assign)NSInteger sector_size;

- (void)assem_moveWithSize:(NSInteger)size words:(NSArray *)words special:(NSString *)special line:(NSString *)line;

- (void)assem_dataWithSize:(NSInteger)size words:(NSArray *)words line:(NSString *)line;

- (void)assem_uni_mathWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line;

- (void)assem_basic_mathWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line;

- (void)assem_other_mathWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line;

- (void)assem_convWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line;

- (void)assem_stackWithOp:(NSString *)op size:(NSInteger)size words:(NSArray *)words line:(NSString *)line;

- (void)assem_eventWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line;

- (void)assem_jumpWithOp:(NSString *)op cond:(NSString *)cond words:(NSArray *)words line:(NSString *)line;

- (void)assem_miscWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line;

/** specials in caps */
- (void)assem_spec_labelWithLabel:(HSWordItem *)label words:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_dataWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_data_lcdWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_binaryWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_reserveWithWhich:(NSString *)which words:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_versionWithWords:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_begin_endWithOp:(NSString *)op words:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_limitsWords:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_deviceWords:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_commsWords:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_insertWords:(NSArray *)words line:(NSString *)line;

- (void)assem_spec_finishWords:(NSArray *)words line:(NSString *)line;

- (void)add_labelWithName:(NSString *)name;

//explicit_placement:插入位置默认传-1
- (void)finishTokenStream:(HSTokenStreamItem *)streamItem explicit_placement:(NSInteger)explicit_placement;
//explicit_placement:插入位置默认传-1
- (void)add_tokenStreamItem:(HSTokenStreamItem *)streamItem explicit_placement:(NSInteger)explicit_placement;

- (void)add_variableWithSpace:(NSInteger)space name:(NSString *)name start:(NSInteger)start length:(NSInteger)length;

// # reserve space in the variable name spaces
//# ARGS: space 0-2, start & length are numbers
- (void)reserve_name_space:(NSInteger)space start:(NSInteger)start length:(NSInteger)length;

//arg1,arg2,arg3 默认为-1
- (void)add_beginWithType:(NSString *)type arg1:(NSInteger)arg1 arg2:(NSInteger)arg2 arg3:(NSInteger)arg3;

- (void)add_endWithType:(NSString *)type;

- (void)set_limitsWithb_lim:(NSInteger)b_lim w_lim:(NSInteger)w_lim l_lim:(NSInteger)l_lim e_handlers:(NSInteger)e_handlers t_lim:(NSInteger)t_lim;

- (void)add_devicesWithType:(NSString *)type location:(NSString *)location size:(NSInteger)size;

- (void)map_all_variables;

- (BOOL)fixup_jumps;

- (NSArray *)creat_header;

- (NSArray *)word_to_bytesWithWord:(NSInteger)word;

@end

NS_ASSUME_NONNULL_END
