//
//  HSElementModel+HSLoop.h
//  HiStar
//
//  Created by 晴天 on 2019/8/3.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSElementModel (HSLoop)


// 等待
- (NSString *)wait_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

- (NSString *)creat_event_codeWithModAlias:(NSString *)mod_alias event:(NSString *)event label:(NSString *)label codeStr:(NSString **)codeStr;


// if
- (NSString *)if_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

- (NSString *)creat_compare_codeWithInput:(NSString *)inputCompare label:(NSString *)label;

// loop
- (NSString *)loop_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// event
- (NSString *)event_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

// email
- (NSString *)email_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id;

@end

NS_ASSUME_NONNULL_END
