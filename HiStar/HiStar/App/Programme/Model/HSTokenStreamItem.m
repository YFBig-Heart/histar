//
//  HSTokenStreamItem.m
//  HiStar
//
//  Created by 晴天 on 2019/7/20.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSTokenStreamItem.h"



#define LIMIT_NAMES @[@"Bytes", @"Words", @"LCD chars", @"Event handlers", @"Token bytes"]


@implementation HSTokenStreamItem

- (instancetype)init {
    if (self = [super init]) {
        self.valid = YES;
        self.token_info = [NSMutableArray array];
        self.var_info = [NSMutableArray array];
        self.cached_bits = [NSMutableArray array];
    }
    return self;
}

- (NSInteger)get_byte_len {
    if (self.cached_bits.count == 0) {
        return [[self get_token_bits] count];
    }else {
        return self.cached_bits.count;
    }
}

- (NSMutableArray *)get_token_bits {
    if (self.cached_bits.count == 0) {
        if (self.binary_file.length > 0) {
#warning binary_file 这个不是很清楚，先不做
            return self.cached_bits;
        }else {
            NSInteger length = 1;
            self.cached_bits = [NSMutableArray arrayWithObject:@(0)];
            for (NSArray *info in self.token_info) {
                NSInteger index = [info[0] intValue];
                while ((index + 1) > length) {
                    [self.cached_bits addObject:@(0)];
                    length += 1;
                }
                NSInteger mask = [info[2] integerValue];
                NSInteger shift = [info[1] integerValue];
                NSInteger value = [info[3] integerValue];
                
                NSInteger work = [self.cached_bits[index] integerValue];
                work = work & ~(mask <<shift);
                work = work | ((value & mask) << shift);
                self.cached_bits[index] = @(work);
            }
            NSLog(@"asdgagasdgaghdfh%@",self.token_info);
        }
    }
    return self.cached_bits;
}


+ (instancetype)tokenStreamItemWithType:(NSString *)type error_reporter:(NSString *__nullable)error_reporter src:(NSString *)src {
    HSTokenStreamItem *streamItem = [[HSTokenStreamItem alloc] init];
    streamItem.type = type;
    streamItem.error_reporter = error_reporter;
    if (src && [src hasSuffix:@"\n"]) {
        streamItem.source_line = [src substringToIndex:src.length-2];
    }else {
        streamItem.source_line = src;
    }
    
    return streamItem;
}


- (void)add_bitsWithIndex:(NSInteger)index shift:(NSInteger)shift mask:(NSInteger)mask value:(id)value {
    if (value == nil) {
        value = @(0);
    }
    [self.token_info addObject:@[@(index),@(shift),@(mask),value]];
    self.cached_bits = [NSMutableArray array];
}
- (void)add_byteWithIndex:(NSInteger)index value:(NSInteger)value {
    if (value < MIN_BYTE || value > MAX_BYTE) {
        self.error_reporter = [NSString stringWithFormat:@"Out of range for a byte: %ld", (long)(value)];
        self.valid = NO;
        return;
    }
    [self.token_info addObject:@[@(index),@(0),@(0xff),@(value)]];
    self.cached_bits = [NSMutableArray array];
}

- (void)add_wordWithIndex:(NSInteger)index value:(NSInteger)value {
    if (value < MIN_WORD || value > MAX_WORD){
        self.error_reporter = [NSString stringWithFormat:@"Out of range for a word: %ld", (long)(value)];
        self.valid = NO;
    }else {
        [self.token_info addObject:@[@(index),@(0),@(0xff),@((value >> 8) & 255)]];
        [self.token_info addObject:@[@(index+1),@(0),@(0xff),@(value & 255)]];
        self.cached_bits = [NSMutableArray array];
    }
}

- (void)add_uwordWithIndex:(NSInteger)index value:(NSInteger)value {
    if (value < 0 || value > MAX_UWORD){
        self.error_reporter = [NSString stringWithFormat:@"Out of range for a unsigned word: %ld", (long)(value)];
        self.valid = NO;
    }else {
        [self.token_info addObject:@[@(index),@(0),@(0xff),@((value >> 8) & 255)]];
        [self.token_info addObject:@[@(index+1),@(0),@(0xff),@(value & 255)]];
        self.cached_bits = [NSMutableArray array];
    }
}

- (void)add_vnameIndex:(NSInteger)index space:(NSInteger)space name:(id)name {
    [self.var_info addObject:@[@(index),@(space),name]];
    self.cached_bits = [NSMutableArray array];
}

- (void)fixup_jumpBig:(BOOL)big offset:(NSInteger)offset {
//    self.jump_label;
    if (self.jump_label.count == 0) {
        return;
    }
    NSInteger j_index = [self.jump_label[0] integerValue];
    NSString *j_Name = self.jump_label[1];
    BOOL j_big = [self.jump_label[2] boolValue];
    
    if (big != j_big) {
        if (big == NO) {
            NSLog(@"Impossible - the jump size got SMALLER");
            self.valid = NO;
            return;
        }
        NSInteger find_index = [self find_index:j_index];
        if (find_index != -1) {
            [self.token_info removeObjectAtIndex:find_index];
        }
        [self add_bitsWithIndex:0 shift:4 mask:1 value:@(1)];
        [self add_wordWithIndex:j_index value:offset];
        self.jump_label = @[@(j_index),j_Name,@(big)];
        
    }else if (big == YES){
        NSInteger find_index = [self find_index:j_index];
        if (find_index != -1) {
            [self.token_info removeObjectAtIndex:find_index];
            [self.token_info removeObjectAtIndex:[self find_index:j_index+1]];
        }
        [self add_wordWithIndex:j_index value:offset];
    }else {
        NSInteger find_index = [self find_index:j_index];
        if (find_index != -1) {
            [self.token_info removeObjectAtIndex:find_index];
        }
        if (offset < 0) {
            offset += 256;
        }
        [self add_byteWithIndex:j_index value:offset];
        [self.token_info addObject:@[@(j_index),@(0),@(0xff),@(offset)]];
    }
    self.cached_bits = [NSMutableArray array];
    
}

- (NSInteger)find_index:(NSInteger)index {
    NSInteger found = -1;
    for (int i = 0; i < self.token_info.count; i++) {
        if ([self.token_info[i][0] integerValue] == index) {
            found = i;
            break;
        }
    }
    if (found == -1) {
        NSLog(@"error:Variable index: %ld invalid",(long)index);
        self.valid = NO;
        return -1;
    }
    return found;
}

- (void)fixup_var_byteIndex:(NSInteger)index value:(NSInteger)value {
    
    NSInteger i = [self find_index:index];
    NSInteger new_number = [self.token_info[i][3] integerValue] + value;
    
    if (new_number < MIN_BYTE || new_number > MAX_BYTE) {
        NSLog(@"error: Out of range for a byte: %ld",(long)new_number);
        self.valid = NO;
        return;
    }
    self.token_info[i] = @[@(index),@(0),@(0xff),@(new_number)];
    self.cached_bits = @[].mutableCopy;
}


@end
