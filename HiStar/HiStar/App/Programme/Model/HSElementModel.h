//
//  HSElementModel.h
//  HiStar
//
//  Created by petcome on 2019/6/5.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>

NS_ASSUME_NONNULL_BEGIN
// 元素模型
@interface HSElementModel : NSObject<NSCopying>

/** 数据模组类型 */
@property (nonatomic,assign)kModuleType moduleType;
// 功能类型
@property (nonatomic,copy)NSString *funtction_type;

@property (nonatomic,copy)NSString *norImageName;
@property (nonatomic,copy)NSString *seleteImageName;
/** 名称 */
@property (nonatomic,copy)NSString *name;
/** 编程上用到 */
@property (nonatomic,copy)NSString *codeName;

// 弹框的标题
@property (nonatomic,copy)NSString *proptitle;
@property (nonatomic,copy)NSString *propText;
// 弹框介绍文字
//配置参数
@property (nonatomic,strong)NSDictionary *convert_dict;

/** 子元素 */
@property (nonatomic,strong)NSMutableArray *subElementItems;
/** if元素专用,当条件为NO时,下面的元素 */
@property (nonatomic,strong)NSMutableArray *if_noElementItems;
/** 是否是 if 条件为NO 分支上的 */
@property (nonatomic,assign)BOOL isIf_noBranch;

// 转编程协议文件需要用到的东西
@property (nonatomic,assign)NSInteger bric_id;
// 普通的元素end_bric_id 等于bric_id，if和loop流程元素end_bric_id为子元素最后一个
@property (nonatomic,assign)NSInteger end_bric_id;

/** 使用到的变量 */
@property (nonatomic,strong)NSMutableArray <HSVariableItem *>*userVaritems;

/** 在第几组 */
@property (nonatomic,assign)NSInteger sectionIndex;
// 呈现的视图,存储到模型中，避免重复创建,可能是HSElememtView,HSConditionView
@property (nonatomic,weak)UIView *cellView;

// 判断是否是 if 元素
-(BOOL)isIfConditionItem;
// 判断是否是 循环 元素
-(BOOL)isLoopItem;

// 计算元素所需要的宽高
- (CGSize)calculateHeightWidth;
// 计算if 和loop 元素，下面宽高
- (CGSize)calculateif_noConditionHeightWidth;

+(instancetype)elementItemWithMoudle:(kModuleType)moduleType funtionType:(NSString *)funtionType name:(NSString *)name norImage:(NSString *)norImageName;

// 根据模组类型获取对应的元素
+ (NSArray <HSElementModel *>*)getElementItemsWithModuleType:(kModuleType)type;

+ (NSString *)make_mod_reg:(NSString *)mod_name reg_name:(NSString *)reg_name;

+ (NSArray <NSString *>*)make_labelsBric_id:(NSInteger)bric_id start:(NSInteger)start number:(NSInteger)number;

+ (NSString *)make_labelBric_id:(NSInteger)bric_id index:(NSInteger)index; 

+ (NSArray *)make_if_labelsWithBric_id:(NSInteger)bric_id;

+ (NSArray *)make_loop_labelsWithBric_id:(NSInteger)bric_id;

#pragma mark - 根据模型生成对应的编程语句
- (NSString *)generate_codeWithEvent:(BOOL)in_event;

@end

NS_ASSUME_NONNULL_END
