//
//  HSElementModel+HSControl.m
//  HiStar
//
//  Created by 晴天 on 2019/7/30.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElementModel+HSControl.h"

@implementation HSElementModel (HSControl)

// command 暂时全部传 @"gen_code"
- (NSString *)digital_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *output = @"output";
    int bit = 0;
    int mask = 1;
    if ([name isEqualToString:@"Obstacle Detection"]) {
        output = @"action";
        bit = 1;
        mask = 2;
    }else {
        output = @"output";
    }
    NSMutableString *codeString = [NSMutableString string];
    
    if ([input[2] isEqualToString:CONSTANT]) {
        if ([input[1] isEqualToString:@"Off"]) {
            [codeString appendFormat:@"bitclr $%ld %@\n",(long)bit,[HSElementModel make_mod_reg:input[0] reg_name:output]];
        }else {
            [codeString appendFormat:@"bitset $%ld %@\n",(long)bit,[HSElementModel make_mod_reg:input[0] reg_name:output]];
        }
        if ([name isEqualToString:@"LED"]) {
            if ([input[3] isEqualToString:@"Blue"]) {
                [codeString appendString:@"and $4\n"];
            }else {
                [codeString appendString:@"and $5\n"];
            }
        }
        
    }else {
        NSArray *labels = [HSElementModel make_labelsBric_id:bric_id start:0 number:2];
        [codeString appendFormat:@"movb @%@ %%_cpu:acc\n",input[2]];
        [codeString appendFormat:@"and $%ld\n",(long)mask];
        [codeString appendFormat:@"brz %@\n",labels[0]];
        [codeString appendFormat:@"bitset $%d %@\n",bit,[HSElementModel make_mod_reg:input[0] reg_name:output]];
        if ([name isEqualToString:@"LED"]) {
            if ([input[4] isEqualToString:@"Blue"]) {
                [codeString appendString:@"and $4\n"];
            }else {
                [codeString appendString:@"and $5\n"];
            }
        }
        [codeString appendFormat:@"bra %@\n",labels[1]];
        [codeString appendFormat:@"%@\n",labels[0]];
        [codeString appendFormat:@"bitclr $%d %@\n",bit,[HSElementModel make_mod_reg:input[0] reg_name:output]];
        if ([name isEqualToString:@"LED"]) {
            if ([input[4] isEqualToString:@"Blue"]) {
                [codeString appendString:@"and $4\n"];
            }else {
                [codeString appendString:@"and $5\n"];
            }
        }
        [codeString appendFormat:@"%@\n",labels[1]];
    }
    
    if ([name isEqualToString:@"Obstacle Detection"]) {
        [codeString appendFormat:@"bitclr $3 %@\n",[HSElementModel make_mod_reg:@"IR_RECEIVER1" reg_name:@"status"]];
        [codeString appendFormat:@"bitclr $4 %@\n",[HSElementModel make_mod_reg:@"IR_RECEIVER1" reg_name:@"status"]];
        [codeString appendFormat:@"bitclr $5 %@\n",[HSElementModel make_mod_reg:@"IR_RECEIVER1" reg_name:@"status"]];
        [codeString appendFormat:@"bitclr $6 %@\n",[HSElementModel make_mod_reg:@"IR_RECEIVER1" reg_name:@"status"]];
    }
    return codeString.copy;
}
// 播放音乐
- (NSString *)tones_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    
    NSString *mod = input[1];
    if ([input[0] integerValue] == 0) {
        NSInteger note_code = 0;
        note_code = [[HSParserHelper tone_notes] indexOfObject:input[2]];
        
        NSInteger dur_code = 1;
        for (NSString *t_dur in [HSParserHelper tone_durations]) {
            if ([t_dur isEqualToString:input[3]]) {
                dur_code = [[HSParserHelper tone_durations] indexOfObject:t_dur]+1;
                break;
            }
        }
        NSString *code = @"datb @_tune_store * ";
        
        if (note_code > 15) {
            code = [code stringByAppendingFormat:@"%ld ", ((0x80 + (dur_code << 4)))];
        }else {
            code = [code stringByAppendingFormat:@"%ld ", ((dur_code << 4) + note_code)];
        }
        
        code = [code stringByAppendingString:@"0\n"];
        code_string = [code_string stringByAppendingString:code];
    
        code_string = [code_string stringByAppendingFormat:@"movb $0 %@\n",[HSElementModel make_mod_reg:mod reg_name:@"tune"]];
        code_string = [code_string stringByAppendingFormat:@"bitset 0 %@\n",[HSElementModel make_mod_reg:mod reg_name:@"action"]];
    }else {
        NSString *code = @"datb @_tune_store * ";
        NSInteger pairs = [input[4] length]/2;
        if (pairs > MAX_TUNE_STORE-1) {
            NSLog(@"error: Too many tune notes/durations (max 16 pairs).");
            return @"";
        }
        
        for (int i = 0; i<pairs; i++) {
            NSString *note = [input[4] substringWithRange:NSMakeRange(i*2, 1)];
            int dur = [[input[4] substringWithRange:NSMakeRange(i*2+1, 1)] intValue];
            if (dur < 0) {
                dur = 0;
            }
            if (dur > 7) {
                dur = 7;
            }
            
            NSRange range = [notes rangeOfString:note];
            NSInteger note_code = range.location;
            if (note_code < 0 || note_code > 17 || dur < 0) {
                NSLog(@"error: Tune string is not valid. Check help for the format.");
                return @"";
            }else if (dur == 0 || note_code > 15){
                code = [code stringByAppendingFormat:@"%d ",((0x80 + (dur << 4)))];
            }else {
                code = [code stringByAppendingFormat:@"%ld ",((dur << 4) + note_code)];
            }
        }
        code = [code stringByAppendingString:@"0\n"];
        code_string = [code_string stringByAppendingString:code];
        code_string = [code_string stringByAppendingFormat:@"movb $0 %@\n", [HSElementModel make_mod_reg:mod reg_name:@"tune"]];
        code_string = [code_string stringByAppendingFormat:@"bitset 0 %@\n", [HSElementModel make_mod_reg:mod reg_name:@"action"]];
    }
    return code_string;
}

// 电动机
- (NSString *)motor_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    if ([name isEqualToString:@"Motor Pair"]) {
        NSArray *motors = [input[0] componentsSeparatedByString:@"+"];
        NSMutableArray *s_dirs = [self motor_pair_directionsWithMotors:motors command:input[1]].mutableCopy;
        
        for (int i = 0; i<2; i++) {
            NSInteger s_d = [s_dirs[i] integerValue];
            if (s_d == 0) {
                NSLog(@"error: -- motor pair didn't give a command for every command!");
                continue;
            }
            if ((s_d & 0x20) == 0x20) {
                //# rt/left 90% - so rotation by 12 revolutions
                code_string = [code_string stringByAppendingFormat:@"movw $%d %@\n",12,[HSElementModel make_mod_reg:motors[i] reg_name:@"distance"]];
             
                code_string = [code_string stringByAppendingFormat:@"movb $%@ %@\n",s_dirs[i],[HSElementModel make_mod_reg:motors[i] reg_name:@"control"]];
                
            }else {
                // not right or left by 90%!
                NSMutableArray *new_input = [NSMutableArray arrayWithArray:input];
                new_input[0] = motors[i];
                new_input[1] = s_dirs[i];
                NSString *getStr = @"";
                BOOL dist = [self single_motor_distanceWitnInput:new_input single:NO getString:&getStr];
                code_string = [code_string stringByAppendingString:getStr];
                
                if (dist) {
                    s_dirs[i] = @([s_dirs[i] intValue] | 0x20);
                }
                code_string = [code_string stringByAppendingFormat:@"movb $%d %%_cpu:acc\n",[s_dirs[i] intValue]];
                
                if ([input[4] isEqualToString:CONSTANT]) {
                    NSInteger number = [HSParserHelper conv_to_numberString_in:input[3] n_type:@"b" n_min:0 n_max:10];
                    
                    if (number == None) {
                        return @"";
                    }
                    if (number != 0) {
                        code_string = [code_string stringByAppendingFormat:@"or $%ld\n",(long)number];
                    }
                }else {
                    code_string = [code_string stringByAppendingFormat:@"or @%@\n",input[4]];
                }
                
                code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc %@\n",[HSElementModel make_mod_reg:motors[i] reg_name:@"control"]];
            }
        }
    }else {
        // 单边轮
        NSString *getStr = @"";
        [self single_motor_distanceWitnInput:input single:YES getString:&getStr];
        code_string = [code_string stringByAppendingString:getStr];
        
        if ([input[4] isEqualToString:CONSTANT]) {
            NSInteger number = [HSParserHelper conv_to_numberString_in:input[3] n_type:@"b" n_min:0 n_max:10];
            
            if (number == None) {
                return @"";
            }
            if (number != 0) {
                code_string = [code_string stringByAppendingFormat:@"or $%ld\n",(long)number];
            }
        }else {
            code_string = [code_string stringByAppendingFormat:@"or @%@\n",input[4]];
        }
        
        code_string = [code_string stringByAppendingFormat:@"movb %%_cpu:acc %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"control"]];
    }
    return code_string;
}

// 处理电动机方向
- (NSArray *)motor_pair_directionsWithMotors:(NSArray *)motors command:(NSString *)command {
    
    NSArray *locs = @[[NSString checkIfNullWithString:[HSParserHelper config_loc_from_name:motors.firstObject]],[NSString checkIfNullWithString:[HSParserHelper config_loc_from_name:motors[1]]]];
    
    NSArray *dtypes = @[[HSParserHelper config_getWithLoc:locs[0]][0],[HSParserHelper config_getWithLoc:locs[1]][0]];
    
    NSMutableArray *dirs = @[@(0),@(0)].mutableCopy;
    NSMutableArray *codes = @[@(0),@(0)].mutableCopy;
    NSInteger right_most = -1;
    
    for (int i = 0; i<locs.count; i++) {
        if ([dtypes[i] isEqualToString:@"Motor A"]) {
            
            if ([@[@"2",@"3",@"4",@"5",@"6"] containsObject:locs[i]]) {
                dirs[i] = @(1);
                right_most = i;
            }else if ([@[@"8",@"9",@"10",@"11",@"0"] containsObject:locs[i]]){
                dirs[i] = @(-1);
            }else if ([locs[i] integerValue] == 1){
            
                
                dirs[i] = @(-1*[HSParserHelper config_orient_from_loc:1]);
            }else {
                dirs[i] = @([HSParserHelper config_orient_from_loc:[locs[1] intValue]]);
            }
            
        }else {
            if ([@[@"2",@"3",@"4",@"5",@"6"] containsObject:locs[i]]) {
                dirs[i] = @(-1);
                right_most = i;
            }else if ([@[@"8",@"9",@"10",@"11",@"0"] containsObject:locs[i]]){
                dirs[i] = @(1);
            }else if ([locs[i] integerValue] == -1){
                dirs[i] = @([HSParserHelper config_orient_from_loc:i]);
            }else {
                dirs[i] = @(-1*[HSParserHelper config_orient_from_loc:[locs[i] intValue]]);
            }
        }
        
    }
    
    if (right_most < 0) {
        if ([locs[0] intValue] == 1 && [HSParserHelper config_orient_from_loc:1] == -1) {
            
            right_most = 0;
            
        }else if ([locs[0] intValue] == 7 && [HSParserHelper config_orient_from_loc:1] == 1){
            right_most = 0;
        }else if ([locs[1] intValue] == 1 && [HSParserHelper config_orient_from_loc:1] == 1){
            right_most = 1;
        }else {
            right_most = 1;
        }
    }
    
    NSInteger left_most = 0;
    if (right_most == 0) {
        left_most = 1;
    }
    if ([command isEqualToString:MOTOR_STP]) {
        codes = @[MOTOR_CODE[@"S"],MOTOR_CODE[@"S"]].mutableCopy;
    }else if ([command isEqualToString:MOTOR_FWD]){
        for (int i = 0;i < 2; i++) {
            if ([dirs[i] integerValue] == 1) {
                codes[i] = MOTOR_CODE[@"F"];
            }else {
                codes[i] = MOTOR_CODE[@"B"];
            }
        }
    }else if ([command isEqualToString:MOTOR_BCK]){
        for (int i = 0;i < 2; i++) {
            if ([dirs[i] integerValue] == 1) {
                codes[i] = MOTOR_CODE[@"B"];
            }else {
                codes[i] = MOTOR_CODE[@"F"];
            }
        }
    }else if ([command isEqualToString:MOTOR_P_RT]){
        codes[right_most] = MOTOR_CODE[@"S"];
        if ([dirs[left_most] integerValue] == 1) {
            codes[left_most] = MOTOR_CODE[@"F"];
        }else {
            codes[left_most] = MOTOR_CODE[@"B"];
        }
    }else if ([command isEqualToString:MOTOR_P_SR]){
        
        if ([dirs[left_most] integerValue] == 1) {
            codes[left_most] = MOTOR_CODE[@"F"];
            codes[right_most] = MOTOR_CODE[@"B"];
        }else {
            codes[left_most] = MOTOR_CODE[@"B"];
            codes[right_most] = MOTOR_CODE[@"F"];
        }
    }else if ([command isEqualToString:MOTOR_P_LT]){
        codes[left_most] = MOTOR_CODE[@"S"];
        if ([dirs[right_most] integerValue] == 1) {
            codes[right_most] = MOTOR_CODE[@"F"];
        }else {
            codes[right_most] = MOTOR_CODE[@"B"];
        }
    }else if ([command isEqualToString:MOTOR_P_SL]){
        if ([dirs[right_most] integerValue] == 1) {
            codes[right_most] = MOTOR_CODE[@"F"];
            codes[left_most] = MOTOR_CODE[@"B"];
        }else {
            codes[right_most] = MOTOR_CODE[@"B"];
            codes[left_most] = MOTOR_CODE[@"F"];
        }
    }else if ([command isEqualToString:MOTOR_P_BR]){
        codes[right_most] = MOTOR_CODE[@"S"];
        if ([dirs[left_most] integerValue] == 1) {
            codes[left_most] = MOTOR_CODE[@"B"];
        }else {
            codes[left_most] = MOTOR_CODE[@"F"];
        }
    }else if ([command isEqualToString:MOTOR_P_BL]){
        
        codes[left_most] = MOTOR_CODE[@"S"];
        if ([dirs[right_most] integerValue] == 1) {
            codes[right_most] = MOTOR_CODE[@"B"];
        }else {
            codes[right_most] = MOTOR_CODE[@"F"];
        }
    }else if ([command isEqualToString:MOTOR_P_RT_90]){
        if ([dirs[left_most] integerValue] == 1) {
            codes[left_most] = MOTOR_CODE[@"FD"];
            codes[right_most] = MOTOR_CODE[@"BD"];
        }else {
            codes[left_most] = MOTOR_CODE[@"BD"];
            codes[right_most] = MOTOR_CODE[@"FD"];
        }
    }else if ([command isEqualToString:MOTOR_P_LT_90]){
        if ([dirs[right_most] integerValue] == 1) {
            codes[right_most] = MOTOR_CODE[@"FD"];
            codes[left_most] = MOTOR_CODE[@"BD"];
        }else {
            codes[right_most] = MOTOR_CODE[@"BD"];
            codes[left_most] = MOTOR_CODE[@"FD"];
        }
    }
    return codes;
}

- (BOOL)single_motor_distanceWitnInput:(NSArray *)input single:(BOOL)single getString:(NSString **)getStr {
    BOOL distance_cmd = NO;
    NSString *getString = @"";
    if ([input[5] hasPrefix:@"Unlimited"] ||([input[2] isEqualToString:CONSTANT] && [@[MOTOR_STP] containsObject:input[1]])) {
        distance_cmd = NO;
    }else {
        distance_cmd = YES;
        if ([input[7] isEqualToString:CONSTANT]) {
            int dist = (int)[HSParserHelper conv_to_numberString_in:input[6] n_type:@"w" n_min:0 n_max:32000];
            int rotations = 0;
            if (dist == None) {
                return NO;
            }
            if ([input[5] hasPrefix:@"Millimetre"]) {
                rotations = (int)((dist/2.5)+0.5);
            }else if ([input[5] hasPrefix:@"Inch"]){
                rotations = dist*10;
            }else if ([input[5] hasPrefix:@"Degrees"]){
                rotations = (int)((dist/7.5)+0.5);
            }else if ([input[5] hasPrefix:@"Raw"]){
                rotations = dist;
            }else {
                rotations = dist;
            }
            
            getString = [getString stringByAppendingFormat:@"movw $%d %@\n",rotations,[HSElementModel make_mod_reg:input[0] reg_name:@"distance"]];
            
        }else {
            getString = [getString stringByAppendingFormat:@"movw $%@ %@\n",input[7],[HSElementModel make_mod_reg:input[0] reg_name:@"distance"]];
        }
    }
    if (single) {
        if ([input[2] isEqualToString:CONSTANT]) {
            id dir;
            if (distance_cmd) {
               dir = DIRECTION_WITH_DIST_CODE[input[1]];
            }else {
               dir = DIRECTION_CODE[input[1]];
            }
            getString = [getString stringByAppendingFormat:@"movb $%@ %%_cpu:acc\n",dir];
        }else {
            getString = [getString stringByAppendingFormat:@"movb @%@ %%_cpu:acc\n",input[2]];
        }
    }
    *getStr = getString;
    return distance_cmd;
}



// 发射信号
- (NSString *)txir_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    if ([input[2] isEqualToString:CONSTANT]) {
//        NSInteger character = [HSParserHelper conv_to_tx_char:input[1]];
        NSInteger character = [input[1] integerValue];
        if (character == None) {
            return @"";
        }
        code_string = [code_string stringByAppendingFormat:@"movb $%ld %@\n",(long)character,[HSElementModel make_mod_reg:input[0] reg_name:@"char"]];
    }else {
        code_string = [code_string stringByAppendingFormat:@"movb @%@ %@\n",input[2],[HSElementModel make_mod_reg:input[0] reg_name:@"char"]];
    }
    
    code_string = [code_string stringByAppendingFormat:@"bitset 0 %@\n",[HSElementModel make_mod_reg:input[0] reg_name:@"action"]];
    
    return code_string;
}

// 倒计时
- (NSString *)settimer_convertInput:(NSArray *)input command:(NSString *)command name:(NSString *)name bric_id:(NSInteger)bric_id {
    
    if ([command isEqualToString:@"gen_code"] == NO) {
        // 暂时没用不写
        return @"";
    }
    NSString *code_string = @"";
    if ([input[1] isEqualToString:CONSTANT]) {
        NSInteger time = [HSParserHelper conv_to_timeWithString_in:input[0]];
        if (time == None) {
            return @"";
        }
        code_string = [code_string stringByAppendingFormat:@"movw $%ld %%_timers:oneshot\n",(long)time];
    }else {
        code_string = [code_string stringByAppendingFormat:@"movw @%@ %%_timers:oneshot\n",input[1]];
    }
    
    code_string = [code_string stringByAppendingString:@"bitset 0 %_timers:action\n"];
    return code_string;
}


@end
