//
//  HSTypeElementView.m
//  HiStar
//
//  Created by petcome on 2019/6/4.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSTypeElementView.h"
#import "CustomSlider.h"
#import <Masonry/Masonry.h>
#import "HSElementModel.h"
#import "HSMoudleModel.h"
#import "UIImage+Extension.h"

@interface HSTypeElementView ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet CustomSlider *sliderView;
@property (nonatomic,strong)NSArray <HSElementModel *>*dataSourceArr;

@end
@implementation HSTypeElementView

+ (instancetype)typeElementView {
    HSTypeElementView *elementView = [[NSBundle mainBundle] loadNibNamed:@"HSTypeElementView" owner:nil options:0].lastObject;
    return elementView;
}
- (void)reloadDataWithMoudleModel:(HSMoudleModel *)moudleModel {
    self.dataSourceArr = moudleModel.elementItems;
    [self.collectionView reloadData];
    GCDAfter(0.15, ^{
        if (self.collectionView.contentSize.height <= self.collectionView.height) {
            self.sliderView.hidden = YES;
        }else {
            self.sliderView.hidden = NO;
            self.sliderView.thumbImage = [self.sliderView.thumbImage imageWithTintColor:moudleModel.bgColor alpha:1.0];
            [self.sliderView updateDrawUI];
        }
    });
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat value = (scrollView.contentOffset.y)/(scrollView.contentSize.height-scrollView.height);
    [self.sliderView setSliderValueAndCurrentTarget:(value * 100)];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpSliderView];
    [self setUpUiCollectionview];
    self.sliderView.hidden = YES;
}

- (void)setUpUiCollectionview {
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

    self.flowLayout.minimumLineSpacing = 16;
    self.flowLayout.minimumInteritemSpacing = 0;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(8, 2, 8, 17);
    self.flowLayout.itemSize = CGSizeMake(kElementWH, kElementWH);
    [self.collectionView registerClass:[HSTypeElementCell class] forCellWithReuseIdentifier:@"HSTypeElementCell"];
}
- (void)setUpSliderView {
    self.sliderView.cornerRadius = 2;
    self.sliderView.isHorizontalSlider = NO;
    self.sliderView.bgColor = RGB16TOCOLOR(0xffffff);
    self.sliderView.startColor = RGB16TOCOLOR(0xffffff);
    self.sliderView.endColor = RGB16TOCOLOR(0xffffff);
    self.sliderView.thumbImage = [UIImage imageNamed:@"green_slider"];
    [self.sliderView layerFilletWithRadius:self.sliderView.cornerRadius];
    [self.sliderView setSliderValueAndCurrentTarget:0];
    [self.sliderView updateDrawUI];
    WS(weakSelf);
    [self.sliderView setGesActionValueChange:^(CGFloat value) {
        CGFloat offsetY =value*((weakSelf.collectionView.contentSize.height-weakSelf.collectionView.height));
        weakSelf.collectionView.contentOffset = CGPointMake(0, offsetY);
    }];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HSTypeElementCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HSTypeElementCell" forIndexPath:indexPath];
    HSElementModel *model = self.dataSourceArr[indexPath.row];
    [cell configCellWithElementItem:model];
    [cell setLongGesActionBlock:^(HSTypeElementCell *cell, UILongPressGestureRecognizer *pan) {
        if ([self.delegate respondsToSelector:@selector(cellPanActionWithCell:longGes:)]) {
            [self.delegate cellPanActionWithCell:cell longGes:pan];
        }
    }];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(didSeleteCell:withelementModel:)]) {
        HSTypeElementCell *cell = (HSTypeElementCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [self.delegate didSeleteCell:cell withelementModel:nil];
    }
}

@end
@interface HSTypeElementCell ()
@property (nonatomic,strong)HSElementModel *elementModel;
@end
@implementation HSTypeElementCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imageView];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        self.typeNameLabel = [UILabel CreatLabelText:@"" bgColor:nil textFont:[UIFont systemFontOfSize:9] textColor:[UIColor whiteColor] textAliment:NSTextAlignmentCenter];
        [self.contentView addSubview:self.typeNameLabel];
        self.typeNameLabel.adjustsFontSizeToFitWidth = YES;
        self.typeNameLabel.minimumScaleFactor = 0.6;
        self.typeNameLabel.numberOfLines = 2;
        [self.typeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_bottom).offset(-2);
            make.left.equalTo(self).offset(4);
            make.right.equalTo(self).offset(-4);
            make.height.mas_equalTo(15);
        }];
        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.deleteBtn setImage:[UIImage imageNamed:@"delete_dot"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.deleteBtn];
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.equalTo(self.contentView);
        }];
        self.deleteBtn.hidden = YES;

        UILongPressGestureRecognizer *longGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGesAction:)];
        longGes.minimumPressDuration = 0.15;
        [self addGestureRecognizer:longGes];
    }
    return self;
}

- (void)configCellWithElementItem:(HSElementModel *)item {
    self.elementModel = item;
    self.imageView.image = [UIImage imageNamed:item.norImageName];
    self.typeNameLabel.text = item.name;
    
    if ([@[k_ft_start,k_ft_end,k_ft_addEvent,k_ft_startEvent,k_ft_endEvent] containsObject:item.funtction_type]) {
        self.deleteBtn.hidden = YES;
        self.typeNameLabel.textColor = RGB16TOCOLOR(0x3D88BE);
    }else if (_elementModel.moduleType == kModuleFlow) {
        self.typeNameLabel.textColor = RGB16TOCOLOR(0x434053);
    }else {
        self.typeNameLabel.textColor = [UIColor whiteColor];
    }
}

-(void)longGesAction:(UILongPressGestureRecognizer *)longGes {
    if (self.longGesActionBlock) {
        self.longGesActionBlock(self,longGes);
    }
}

@end
