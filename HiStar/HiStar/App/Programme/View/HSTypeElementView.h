//
//  HSTypeElementView.h
//  HiStar
//
//  Created by petcome on 2019/6/4.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>



@class HSElementModel,HSTypeElementCell,HSMoudleModel;
typedef void(^CellLongGesActionBlock)(HSTypeElementCell *cell,UILongPressGestureRecognizer *pan);

@protocol HSTypeElementViewDelegate <NSObject>


- (void)cellPanActionWithCell:(HSTypeElementCell *)cell longGes:(UILongPressGestureRecognizer *)longGes;

@optional;
- (void)didSeleteCell:(HSTypeElementCell *)cell withelementModel:(HSElementModel *)model;

@end

@interface HSTypeElementView : UIView
+ (instancetype)typeElementView;

@property (nonatomic,weak)id<HSTypeElementViewDelegate> delegate;

- (void)reloadDataWithMoudleModel:(HSMoudleModel *)moudleModel;

@end

@interface  HSTypeElementCell: UICollectionViewCell

@property (nonatomic,strong)UIImageView *imageView;
@property (nonatomic,strong)UILabel *typeNameLabel;
/** 删除小按钮 */
@property (nonatomic,strong)UIButton *deleteBtn;

@property (nonatomic,strong,readonly)HSElementModel *elementModel;

- (void)configCellWithElementItem:(HSElementModel *)item;

@property (nonatomic,copy)CellLongGesActionBlock longGesActionBlock;

@end

