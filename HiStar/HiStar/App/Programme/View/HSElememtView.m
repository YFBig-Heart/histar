//
//  HSElememtView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/10.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSElememtView.h"
#import <Masonry/Masonry.h>
#import "HSElementModel.h"
#import "HSProgrammerViewModel.h"



@implementation HSElememtView

- (instancetype)init {
    if (self = [super init]) {
        [self setUpUI];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUpUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBtnHiddenStateChange:) name:yfElementViewDeleteBtnHiddenStateChange object:nil];
    }
    return self;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)deleteBtnHiddenStateChange:(NSNotification *)note {
    if (self.isNotCanDelete) {
        self.deleteBtn.hidden = YES;
    }else {
        self.deleteBtn.hidden = ![note.object boolValue];
    }
}

- (void)setUpUI {
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.imageView = [[UIImageView alloc] init];
    [self addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    self.typeNameLabel = [UILabel CreatLabelText:@"" bgColor:nil textFont:[UIFont systemFontOfSize:9] textColor:[UIColor whiteColor] textAliment:NSTextAlignmentCenter];
    self.typeNameLabel.adjustsFontSizeToFitWidth = YES;
    self.typeNameLabel.minimumScaleFactor = 0.6;
    self.typeNameLabel.numberOfLines = 2;
    [self addSubview:self.typeNameLabel];
    [self.typeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-2);
        make.left.equalTo(self).offset(4);
        make.right.equalTo(self).offset(-4);
        make.height.mas_equalTo(15);
    }];
    self.typeNameLabel.adjustsFontSizeToFitWidth = YES;
    self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteBtn setImage:[UIImage imageNamed:@"delete_dot"] forState:UIControlStateNormal];
    [self addSubview:self.deleteBtn];
    [self.deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(self);
    }];
    
    if (self.isNotCanDelete) {
        self.deleteBtn.hidden = YES;
    }else {
       self.deleteBtn.hidden = ![HSProgrammerViewModel shareProgrammerVM].showDeleteBtn;
    }

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesAction:)];
    self.tapGes = tap;
    self.tapGes.enabled = YES;
    [self addGestureRecognizer:tap];
}

+ (instancetype)elementViewWithFrame:(CGRect)frame model:(HSElementModel *)model {
    HSElememtView *view = [[HSElememtView alloc] initWithFrame:frame];
    view.elementModel = model;
    return view;
}
- (void)setElementModel:(HSElementModel *)elementModel {
    _elementModel = elementModel;
    if ([elementModel.codeName isEqualToString:@"Email"] && [elementModel.norImageName isEqualToString:@"send_letter"]) {
        int index = [[elementModel.convert_dict objectForKey:@"Letter"] intValue];
        NSArray *items = [[HSParserHelper all_EmailTypes] objectAtIndex:index];
        _elementModel.norImageName = [NSString stringWithFormat:@"%@_letter",items[3]];
    }
    self.imageView.image = [UIImage imageNamed:elementModel.norImageName];
    self.typeNameLabel.text = elementModel.name;
    
    if ([@[k_ft_start,k_ft_end,k_ft_addEvent,k_ft_startEvent,k_ft_endEvent] containsObject:elementModel.funtction_type]) {
        if (![elementModel.funtction_type isEqualToString:k_ft_startEvent]) {
            self.isNotCanDelete = YES;
            self.deleteBtn.hidden = YES;
        }
        self.typeNameLabel.textColor = RGB16TOCOLOR(0x3D88BE);
    }else if (elementModel.moduleType == kModuleFlow) {
        self.typeNameLabel.textColor = RGB16TOCOLOR(0x434053);
    }
}

- (void)deleteBtnAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(deleteActionElementView:)]) {
        [self.delegate deleteActionElementView:self];
    }
}
- (void)tapGesAction:(UITapGestureRecognizer *)tapGes {
    if (self.deleteBtn.hidden) {
        if ([self.delegate respondsToSelector:@selector(tapGesActionElementView:)]) {
            [self.delegate tapGesActionElementView:self];
        }
    }else {
        if ([self.delegate respondsToSelector:@selector(deleteActionElementView:)]) {
            [self.delegate deleteActionElementView:self];
        }
    }
}

@end
