//
//  HSConditionView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/12.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSElememtView.h"

NS_ASSUME_NONNULL_BEGIN
@class HSElementModel,HSElementImageView,HSConditionView;

@protocol HSConditionViewDelegate <NSObject>

//删除本视图元素
- (void)deleteConitionView:(HSConditionView *)conditionView;
// 删除子元素视图或子流程视图，需要控制器重新构造页面
- (void)deleteOnlySubEelementViewOrSubConitionView;
// 点击HSConditionView 中的HSElememtView
- (void)conditionView:(HSConditionView *)conditionView tapElementView:(HSElememtView *)elementView;

// 长安手势的回调
- (void)conditionView:(HSConditionView *)conditionView longGes:(UILongPressGestureRecognizer *)longGes;

@end

@interface HSConditionView : UIView<HSElememtViewDelegate>
// 主model
@property (nonatomic,strong)HSElementModel *matserModel;
/** 主elementview */
@property (nonatomic,strong)HSElememtView *matserElementView;

// 所属第几组,主线程、新事件
//@property (nonatomic,assign)NSInteger sectionIndex;


+ (instancetype)conditionViewModel:(HSElementModel *)model;

@property (nonatomic,strong)NSMutableArray <HSElementImageView *>*arrowImageviews;

@property (nonatomic,weak)id<HSConditionViewDelegate> delegate;

/** 长按手势,用于放置后的移动操作 */
@property (nonatomic,strong)UILongPressGestureRecognizer *longGes;

@end

NS_ASSUME_NONNULL_END
