//
//  HSConditionView.m
//  HiStar
//
//  Created by 晴天 on 2019/6/12.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSConditionView.h"
#import "HSElementModel.h"
#import <YYKit/YYKit.h>
#import <Masonry/Masonry.h>
#import "HSElementImageView.h"
#import "HSProgrammerViewModel.h"

extern int staic_bric_id;

@interface HSConditionView ()<HSConditionViewDelegate,HSElememtViewDelegate>

@end
@implementation HSConditionView

+ (instancetype)conditionViewModel:(HSElementModel *)model {
    HSConditionView *view = [[HSConditionView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    view.matserModel = model;
    [view setUpUI];
    // 添加长按手势
    [view creatLongpressGesture];
    return view;
}

- (NSMutableArray<HSElementImageView *> *)arrowImageviews {
    if (!_arrowImageviews) {
        _arrowImageviews = [NSMutableArray array];
    }
    return _arrowImageviews;
}

- (void)setUpUI {
    HSElememtView *materView = [HSElememtView elementViewWithFrame:CGRectMake(0, 0, kElementWH, kElementWH) model:self.matserModel];
    materView.delegate = self;
    self.matserElementView = materView;
    self.matserModel.bric_id = staic_bric_id;
    self.matserModel.end_bric_id = self.matserModel.bric_id;
    staic_bric_id += 1;
    // 便于生成编译文件
    if ([@[k_ft_start,k_ft_end,k_ft_addEvent] containsObject:self.matserModel.funtction_type] == NO) {
        [[HSProgrammerViewModel shareProgrammerVM].allElementArray addObject:self.matserModel];
    }
    [self addSubview:materView];
    if (self.matserModel.isLoopItem == NO && self.matserModel.isIfConditionItem == NO) {
        self.bounds = CGRectMake(0, 0, materView.width, materView.height);
        return;
    }
    CGSize size = [self.matserModel calculateHeightWidth];
    self.size = size;
    UIImage *arrowImage = [UIImage imageNamed:@"flow_arrow"];
    CGFloat margin = 6;
    CGFloat subViewCenterY = (size.height - kElementWH*2)*0.5;
    materView.centerY = size.height - kElementWH*1.5;
    
    CGFloat subViewif_noX = materView.width + margin;
    CGFloat subViewCenterif_noY = size.height - kElementWH *0.5;
    
    if (self.matserModel.if_noElementItems.count != 0) {
        CGSize if_nosize = [self.matserModel calculateif_noConditionHeightWidth];
        subViewCenterY = (size.height - if_nosize.height - kElementWH)*0.5;
        materView.centerY = (size.height - if_nosize.height + kElementWH)*0.5;
        subViewCenterif_noY = (size.height - if_nosize.height*0.5);
    }
    
    materView.x = 0;
    // 创建上面的一个箭头
    CGFloat subViewX = materView.width + margin;
    HSElementImageView *topArrow = [[HSElementImageView  alloc] initWithImage:arrowImage];
    topArrow.x = subViewX;
    topArrow.centerY = subViewCenterY;
    topArrow.insetIndex = 0;
    topArrow.sectionIndex = self.matserModel.sectionIndex;
    [self.arrowImageviews addObject:topArrow];
    [self addSubview:topArrow];
    
    HSElementImageView *bottomArrow = [[HSElementImageView  alloc] initWithImage:arrowImage];
    bottomArrow.x = subViewX;
    bottomArrow.if_noCondition = YES;
    bottomArrow.centerY = subViewCenterif_noY;
    bottomArrow.insetIndex = 0;
    bottomArrow.sectionIndex = self.matserModel.sectionIndex;
    subViewif_noX += (margin + bottomArrow.width);
    
    if (self.matserModel.isLoopItem) {
        bottomArrow.image = [arrowImage imageByRotate180];
    }else {
        [self.arrowImageviews addObject:bottomArrow];
    }
    [self addSubview:bottomArrow];
    subViewX += (topArrow.width + margin);
    UIImageView *lastSubImg = topArrow;
    for (int i = 0; i<self.matserModel.subElementItems.count; i++) {
        HSElementModel *subItem = self.matserModel.subElementItems[i];
        HSConditionView *subConditionView = [HSConditionView conditionViewModel:subItem];
        subConditionView.centerY = subViewCenterY;
        subConditionView.x = subViewX;
        subViewX += subConditionView.width + margin;
        subConditionView.matserModel.sectionIndex = self.matserModel.sectionIndex;
        [self addSubview:subConditionView];
        subConditionView.delegate = self;
        [self.arrowImageviews addObjectsFromArray:subConditionView.arrowImageviews];
        
        // 创建一个箭头
        HSElementImageView *arrow = [[HSElementImageView  alloc] initWithImage:[UIImage imageNamed:@"flow_arrow"]];
        arrow.insetIndex = i+1;
        [self addSubview:arrow];
        [self.arrowImageviews addObject:arrow];
        arrow.x = subViewX;
        subViewX += arrow.width + margin;
        arrow.sectionIndex = self.matserModel.sectionIndex;
        arrow.centerY = subViewCenterY;
        if (i == self.matserModel.subElementItems.count - 1) {
            lastSubImg = arrow;
            self.matserModel.end_bric_id = subItem.end_bric_id;
        }
    }
    
    UIImageView *lastif_noSubImg = bottomArrow;
    for (int i = 0; i<self.matserModel.if_noElementItems.count; i++) {
        HSElementModel *subItem = self.matserModel.if_noElementItems[i];
        HSConditionView *subConditionView = [HSConditionView conditionViewModel:subItem];
        subConditionView.centerY = subViewCenterif_noY;
        subConditionView.x = subViewif_noX;
        subViewif_noX += subConditionView.width + margin;
        subConditionView.matserModel.sectionIndex = self.matserModel.sectionIndex;
        [self addSubview:subConditionView];
        subConditionView.delegate = self;
        
        [self.arrowImageviews addObjectsFromArray:subConditionView.arrowImageviews];
        
        // 创建一个箭头
        HSElementImageView *arrow = [[HSElementImageView  alloc] initWithImage:[UIImage imageNamed:@"flow_arrow"]];
        arrow.insetIndex = i+1;
        arrow.if_noCondition = YES;
        [self addSubview:arrow];
        [self.arrowImageviews addObject:arrow];
        arrow.x = subViewif_noX;
        subViewif_noX += arrow.width + margin;
        arrow.sectionIndex = self.matserModel.sectionIndex;
        arrow.centerY = subViewCenterif_noY;
        
        if (i == self.matserModel.if_noElementItems.count - 1) {
            lastif_noSubImg = arrow;
            self.matserModel.end_bric_id = subItem.end_bric_id;
        }
    }
    CGFloat lineWidth = 6;
    
    //第一条线
    UIView *oneLineView = [self creatAddlineView];
    [oneLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(lineWidth);
        make.top.equalTo(topArrow.mas_centerY);
        make.centerX.equalTo(materView);
        make.bottom.equalTo(materView.mas_top);
    }];
    
    
    UIView *twoLineView = [self creatAddlineView];
    [twoLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(lineWidth);
        make.right.equalTo(topArrow.mas_left).offset(-margin);
        make.left.equalTo(oneLineView);
        make.centerY.equalTo(topArrow);
    }];
    
    UIView *threeLineView = [self creatAddlineView];
    [threeLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(lineWidth);
        make.left.equalTo(lastSubImg.mas_right).offset(margin);
        make.right.equalTo(self);
        make.centerY.equalTo(lastSubImg);
    }];
    
    UIView *fourLineView = [self creatAddlineView];
    [fourLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(lineWidth);
        make.top.equalTo(threeLineView.mas_centerY);
        make.bottom.equalTo(lastif_noSubImg.mas_centerY);
        make.right.equalTo(self);
    }];
    
    UIView *fiveLineView = [self creatAddlineView];
    [fiveLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(lineWidth);
        make.left.equalTo(lastif_noSubImg.mas_right).offset(margin);
        make.centerY.equalTo(lastif_noSubImg);
        make.right.equalTo(self);
    }];
    
    UIView *sixLineView = [self creatAddlineView];
    [sixLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(lineWidth);
        make.right.equalTo(bottomArrow.mas_left).offset(-margin);
        make.centerY.equalTo(lastif_noSubImg);
        make.left.equalTo(materView.mas_centerX);
    }];
   
    UIView *sevenLineView = [self creatAddlineView];
    [sevenLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(lineWidth);
        make.top.equalTo(materView.mas_bottom);
        make.bottom.equalTo(sixLineView.mas_centerY);
        make.centerX.equalTo(materView.mas_centerX);
    }];
    
    if ([self.matserModel isIfConditionItem]) {
        // 打钩
        UIImageView *sureImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sure_icon"]];
        [self addSubview:sureImg];
        [sureImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(oneLineView.mas_centerX);
            make.centerY.equalTo(oneLineView.mas_top);
        }];
        UIImageView *errorImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error_icon"]];
        [self addSubview:errorImg];
        [errorImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(sevenLineView.mas_centerX);
            make.centerY.equalTo(sevenLineView.mas_bottom);
        }];
    }
}
- (UIView *)creatAddlineView {
    UIView *oneLineView = [[UIView alloc] init];
    oneLineView.backgroundColor = RGBCOLOR(112, 112, 112);
    [self addSubview:oneLineView];
    return oneLineView;
}

- (void)creatLongpressGesture {
    UILongPressGestureRecognizer *longGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGesAction:)];
    longGes.minimumPressDuration = 0.35;
    self.longGes = longGes;
    [self addGestureRecognizer:longGes];
}
-(void)longGesAction:(UILongPressGestureRecognizer *)longGes {
    if ([self.delegate respondsToSelector:@selector(conditionView:longGes:)]) {
        [self.delegate conditionView:self longGes:longGes];
    }
}

#pragma mark - HSConditionViewDelegate
- (void)deleteConitionView:(HSConditionView *)conditionView {
    if (conditionView.matserModel.isIf_noBranch) {
        [self.matserModel.if_noElementItems removeObject:conditionView.matserModel];
    }else {
       [self.matserModel.subElementItems removeObject:conditionView.matserModel];
    }
    if ([self.delegate respondsToSelector:@selector(deleteOnlySubEelementViewOrSubConitionView)]) {
        [self.delegate deleteOnlySubEelementViewOrSubConitionView];
    }
}
- (void)deleteOnlySubEelementViewOrSubConitionView {
    if ([self.delegate respondsToSelector:@selector(deleteOnlySubEelementViewOrSubConitionView)]) {
        [self.delegate deleteOnlySubEelementViewOrSubConitionView];
    }
}
- (void)conditionView:(HSConditionView *)conditionView tapElementView:(HSElememtView *)elementView {
    if ([self.delegate respondsToSelector:@selector(conditionView:tapElementView:)]) {
        [self.delegate conditionView:conditionView tapElementView:elementView];
    }
}
- (void)conditionView:(HSConditionView *)conditionView longGes:(UILongPressGestureRecognizer *)longGes {
    if ([self.delegate respondsToSelector:@selector(conditionView:longGes:)]) {
        [self.delegate conditionView:conditionView longGes:longGes];
    }
}

#pragma mark - HSElememtViewDelegate
- (void)tapGesActionElementView:(HSElememtView *)view {
    // 点击元素
    if ([self.delegate respondsToSelector:@selector(conditionView:tapElementView:)]) {
        [self.delegate conditionView:self tapElementView:view];
    }
}
- (void)deleteActionElementView:(HSElememtView *)view {
    if ([view.elementModel isEqual:self.matserModel]) {
        // 删自己
        if ([self.delegate respondsToSelector:@selector(deleteConitionView:)]) {
            [self.delegate deleteConitionView:self];
        }
    }else {
        if (self.matserModel.isIf_noBranch) {
            [self.matserModel.if_noElementItems removeObject:view.elementModel];
        }else {
            [self.matserModel.subElementItems removeObject:view.elementModel];
        }
        // 删除了普通元素
        if ([self.delegate respondsToSelector:@selector(deleteOnlySubEelementViewOrSubConitionView)]) {
            [self.delegate deleteOnlySubEelementViewOrSubConitionView];
        }
    }
}



@end
