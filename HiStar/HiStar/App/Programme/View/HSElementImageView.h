//
//  HSElementImageView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/14.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HSElementModel;
@interface HSElementImageView : UIImageView

// 插入的索引值
@property (nonatomic,assign)NSUInteger insetIndex;

// 第几组
@property (nonatomic,assign)NSInteger sectionIndex;

/** 是否插入到 if 条件，错误方向 */
@property (nonatomic,assign)BOOL if_noCondition;

@end

NS_ASSUME_NONNULL_END
