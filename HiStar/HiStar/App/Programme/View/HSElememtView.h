//
//  HSElememtView.h
//  HiStar
//
//  Created by 晴天 on 2019/6/10.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@class HSElementModel,HSElememtView;
@protocol HSElememtViewDelegate <NSObject>

@optional;
- (void)deleteActionElementView:(HSElememtView *)view;
- (void)tapGesActionElementView:(HSElememtView *)view;

@end

@interface HSElememtView : UIView

@property (nonatomic,strong)UIImageView *imageView;
@property (nonatomic,strong)UILabel *typeNameLabel;
/** 删除小按钮 */
@property (nonatomic,strong)UIButton *deleteBtn;

@property (nonatomic,strong)HSElementModel *elementModel;

/** 点击手势,enable默认为No */
@property (nonatomic,strong)UITapGestureRecognizer *tapGes;
/** 特殊元素,不允许删除 */
@property (nonatomic,assign)BOOL isNotCanDelete;

@property (nonatomic,weak)id<HSElememtViewDelegate> delegate;

+ (instancetype)elementViewWithFrame:(CGRect)frame model:(HSElementModel *)model;

@end

NS_ASSUME_NONNULL_END
