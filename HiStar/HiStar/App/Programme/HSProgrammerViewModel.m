//
//  HSProgrammerViewModel.m
//  HiStar
//
//  Created by 晴天 on 2019/6/3.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSProgrammerViewModel.h"
#import "HSMoudleModel.h"
#import "HSElementModel.h"

NSString * const yfElementViewDeleteBtnHiddenStateChange = @"yfElementViewDeleteBtnHiddenStateChange";
NSString * const yfElementViewTapActionNotice = @"yfElementViewTapActionNotice";

@implementation HSProgrammerViewModel

+ (instancetype)shareProgrammerVM {
    static HSProgrammerViewModel *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.elementWH = 55;
    }
    return self;
}

- (void)updateMoudleModels {
    _moudleModels = [HSMoudleModel getFunctionModels];
}

- (NSArray<HSMoudleModel *> *)moudleModels {
    if (!_moudleModels) {
        _moudleModels = [HSMoudleModel getFunctionModels];
    }
    return _moudleModels;
}
- (void)setShowDeleteBtn:(BOOL)showDeleteBtn {
    _showDeleteBtn = showDeleteBtn;
    [[NSNotificationCenter defaultCenter] postNotificationName:yfElementViewDeleteBtnHiddenStateChange object:@(showDeleteBtn)];
}

// 获取开始事件元素可用的事件集
- (NSArray *)getStartEventSectionsWithModel:(HSElementModel *__nullable)oldEventElement {
    // 去掉已经在使用的
    NSArray *allEventEelements = [HSProgrammerViewModel shareProgrammerVM].allElementArray;
    
    allEventEelements = [allEventEelements filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"codeName ==%@",@"Event"]];
    NSMutableArray *arrM = [NSMutableArray arrayWithArray:[HSParserHelper EVENT_DICT].allKeys];
    for (HSElementModel *element in allEventEelements) {
        NSString *ele_op = [element.convert_dict objectForKey:@"Event_type"];
        if (oldEventElement) {
            NSString *zj_op = [oldEventElement.convert_dict objectForKey:@"Event_type"];
            if ([ele_op isEqualToString:zj_op]) {
                continue;
            }
        }
        if ([arrM containsObject:ele_op]) {
            [arrM removeObject:ele_op];
        }
    }
    return arrM.copy;
}

// 获取开始事件元素可用的事件集
- (NSDictionary *)getStartEvent_dictWithModel:(HSElementModel *__nullable)oldEventElement filter:(NSArray *)filterKeys {
        
    // 去掉已经在使用的
    NSArray *allEventEelements = [HSProgrammerViewModel shareProgrammerVM].allElementArray;
    allEventEelements = [allEventEelements filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"codeName ==%@",@"Event"]];
    
    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
    [[HSParserHelper EVENT_DICT] enumerateKeysAndObjectsUsingBlock:^(NSString* _Nonnull key, NSArray *_Nonnull obj, BOOL * _Nonnull stop) {
        if (![filterKeys containsObject:key]) {
            [dictM setObject:[obj mutableCopy] forKey:key];
        }
    }];
    
    for (HSElementModel *element in allEventEelements) {
        NSString *ele_type = [element.convert_dict objectForKey:@"Event_type"];
        NSString *ele_op = [element.convert_dict objectForKey:@"Event_op"];
        if (oldEventElement) {
            NSString *zj_op = [oldEventElement.convert_dict objectForKey:@"Event_op"];
            if ([ele_op isEqualToString:zj_op]) {
                continue;
            }
        }
        NSMutableArray *ele_opArr = [dictM objectForKey:ele_type];
        for (NSArray *arr in [ele_opArr copy]) {
            if ([arr.firstObject isEqualToString:ele_op]) {
                [ele_opArr removeObject:arr];
                if ([[dictM objectForKey:ele_type] count] == 0) {
                    [dictM removeObjectForKey:ele_type];
                }
            }
        }
    }
    return dictM.copy;
}


@end
