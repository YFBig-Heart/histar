//
//  HSDesginCell.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSDesginCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HSMydesignItem.h"
#import "UIButton+ImageTitleStyle.h"
#import "HSTFAlertView.h"
#import "HSShareManager.h"


@implementation HSDesginCell

- (IBAction)closeBtnAction:(id)sender {
    self.editView.hidden = YES;
}
- (IBAction)deleteBtnAction:(id)sender {
    self.editView.hidden = YES;
    [YFAlertHelper showLeeAlertWithTitle:@"" message:NSLocalizedString(@"Confirm delete?", nil) cancelTitle:klocCancel defalutTitle:klocOk cancelAction:^{

    } defalutAction:^{
        if (self.deleteBtnClick) {
            self.deleteBtnClick(self.item);
        }
    }];
}
- (IBAction)renameBtnClick:(id)sender {
    self.editView.hidden = YES;
     __weak typeof(self) weakSelf = self;
    [HSTFAlertView tfAlertViewTitle:@"" placeholder:@"" tfText:self.item.designName textDidChange:^(UITextField * _Nonnull text) {
        if (text.text.length > 20) {
            [MBProgressHUD showTipMessageInView:[NSString stringWithFormat:NSLocalizedString(@"The nickname should be 1-%d characters long", nil),20]];
        }
    } btnClickBlock:^(NSInteger index, HSTFAlertView * _Nonnull view) {
        if (index == 1 && view.textField.text.length <= 20 && view.textField.text.length > 1) {
            weakSelf.item.designName = view.textField.text;
            [weakSelf.item bg_saveOrUpdate];
            weakSelf.nameLabel.text = weakSelf.item.designName;
            [view removeFromSuperview];
            if (weakSelf.changeNameBtnClick) {
                weakSelf.changeNameBtnClick(weakSelf.item);
            }
        }
        if (index == 0) {
            [view removeFromSuperview];
        }
    }];
}

- (IBAction)editBtnClick:(id)sender {
    self.editView.hidden = NO;
}
- (IBAction)shareBtnClick:(id)sender {
    // 分享按钮
    NSString *content = [NSString stringWithFormat:@"大家好，这是我的小勇士智慧绽放编程机器人作品‘%@’",self.item.designName];
    [[HSShareManager sharedInstance] shareUIWithContent:@"" withTitle:content images:self.designImg.image thumbImage:nil url:nil shareResultBlock:^(BOOL success) {
        self.editView.hidden = YES;
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.renameBtn setTitle:NSLocalizedString(@"Rename", nil) forState:UIControlStateNormal];
    [self.deleteBtn setTitle:NSLocalizedString(@"Delete", nil) forState:UIControlStateNormal];
    [self.shareBtn setTitle:NSLocalizedString(@"Share", nil) forState:UIControlStateNormal];
    [self.renameBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.deleteBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.shareBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    
    self.editView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"myDsignBg"]];

    self.editView.hidden = YES;
}

- (void)configCellIsAdd:(BOOL)isAdd item:(HSMydesignItem *__nullable)item {
    self.item = item;
    self.eidtBtn.hidden = isAdd;
    self.nameLabel.hidden = isAdd;
    self.designImg.hidden = isAdd;
    if (isAdd) {
        [self.downloadBtn setImage:[UIImage imageNamed:@"add_design_icon"] forState:UIControlStateNormal];
    }else {
        [self.downloadBtn setImage:[UIImage imageNamed:@"Camera_icon"] forState:UIControlStateNormal];
        [self.designImg sd_setImageWithURL:[NSURL fileURLWithPath:[item loadImagePath]]];
        self.nameLabel.text = item.designName;
    }
}

@end
