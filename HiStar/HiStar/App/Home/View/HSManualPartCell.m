//
//  HSManualPartCell.m
//  HiStar
//
//  Created by 晴天 on 2019/10/10.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSManualPartCell.h"
#import "NSAttributedString+RichText.h"
#import "UIFont+YFFont.h"

@interface HSManualPartCell ()

@property (weak, nonatomic) IBOutlet UIImageView *partImageView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numberLabelConW;

@end

@implementation HSManualPartCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    [self.numberLabel layerWithBorderColor:RGB16TOCOLOR(0x18B4F1) borderWidth:1 cornerRadius:10 masksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setPartItem:(HSPartItem *)partItem {
    _partItem = partItem;
    self.partImageView.image = [UIImage imageNamed:partItem.partImg];
    if (partItem.partCount.length == 0) {
        self.countLabel.text = @"";
    }else {
//        NSAttributedString *attr = [NSAttributedString attributedString:[NSString stringWithFormat:@"%@ ×",partItem.partCount] subString:@[partItem.partCount,@"×"] colors:@[RGB16TOCOLOR(0x18B4F1)] fonts:@[[UIFont pingFangSCFont:PingFangSCRegular size:18],[UIFont pingFangSCFont:PingFangSCRegular size:14]]];
//        self.countLabel.attributedText = attr;
        self.countLabel.text = [NSString stringWithFormat:@"%@×",partItem.partCount];
    }
   
    if (partItem.partNumber.length == 0) {
        self.numberLabel.text = @"";
        self.numberLabelConW.constant = 0;
        self.numberLabel.hidden = YES;
    }else {
        self.numberLabel.text = partItem.partNumber;
        self.numberLabelConW.constant = 20;
        self.numberLabel.hidden = NO;
    }
}

@end
