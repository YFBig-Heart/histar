//
//  HSManualPartCell.h
//  HiStar
//
//  Created by 晴天 on 2019/10/10.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSManualModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSManualPartCell : UITableViewCell

@property (nonatomic,strong)HSPartItem *partItem;

@end

NS_ASSUME_NONNULL_END
