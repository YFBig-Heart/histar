//
//  HSManualCell.h
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HSManualModel;
@interface HSManualCell : UICollectionViewCell


@property (nonatomic,strong)HSManualModel *manualModel;


@end

NS_ASSUME_NONNULL_END
