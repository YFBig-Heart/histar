//
//  HSBaseFounctionCell.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSBaseFounctionCell.h"

@interface HSBaseFounctionCell ()
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *modelNameLabel;

@end

@implementation HSBaseFounctionCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configCellWithModelName:(NSString *)modelName imageName:(NSString *)imageName {
    self.modelNameLabel.text = modelName;
    self.bgImageView.image = [UIImage imageNamed:imageName];
}

@end
