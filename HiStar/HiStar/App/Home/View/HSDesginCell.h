//
//  HSDesginCell.h
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HSMydesignItem;
@interface HSDesginCell : UICollectionViewCell
@property (nonatomic,weak)IBOutlet UIImageView *bgImageview;
@property (nonatomic,weak)IBOutlet UIButton *downloadBtn;
@property (weak, nonatomic) IBOutlet UIImageView *designImg;
@property (weak, nonatomic) IBOutlet UIButton *eidtBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *editView;
@property (weak, nonatomic) IBOutlet UIButton *renameBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;


@property (nonatomic,strong)HSMydesignItem *item;


- (IBAction)closeBtnAction:(id)sender;


- (void)configCellIsAdd:(BOOL)isAdd item:(HSMydesignItem *__nullable)item;

@property (nonatomic,copy)void (^deleteBtnClick)(HSMydesignItem *item);
@property (nonatomic,copy)void (^changeNameBtnClick)(HSMydesignItem *item);


@end

NS_ASSUME_NONNULL_END
