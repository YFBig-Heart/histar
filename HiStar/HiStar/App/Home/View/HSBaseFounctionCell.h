//
//  HSBaseFounctionCell.h
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSBaseFounctionCell : UICollectionViewCell

- (void)configCellWithModelName:(NSString *)modelName imageName:(NSString *)imageName;

@end

NS_ASSUME_NONNULL_END
