//
//  HSManualCell.m
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSManualCell.h"
#import "HSManualModel.h"

@interface HSManualCell ()
@property (weak, nonatomic) IBOutlet UIImageView *smpleImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation HSManualCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setManualModel:(HSManualModel *)manualModel {
    _manualModel = manualModel;
    self.smpleImg.image = [UIImage imageNamed:manualModel.sampleImg];
    self.nameLabel.text = NSLocalizedString(manualModel.manualName, nil);
}

@end
