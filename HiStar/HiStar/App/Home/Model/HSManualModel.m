//
//  HSManualModel.m
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSManualModel.h"
#import <YYKit/YYKit.h>

@implementation HSManualModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{};
}

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"segmentArray":[HSSegmentModel class]};
}

+ (NSArray *)getManualPlist {
    NSString *pathString = [[NSBundle mainBundle] pathForResource:@"Manual" ofType:@"plist"];
    NSArray *manual = [NSArray arrayWithContentsOfFile:pathString];
    return manual;
}

+ (NSArray <HSManualModel *>*)locationAllManuals {
    NSArray *array = [self getManualPlist];
    NSArray *results = [NSArray modelArrayWithClass:[HSManualModel class] json:array];
    return results;
}

// 需要用的时候设置一下
- (void)setSegmentName {
    if ([self.segmentArray.firstObject segmentName].length > 1) {
        return;
    }
    for (int i = 0; i< self.segmentArray.count; i++) {
        HSSegmentModel *seg = self.segmentArray[i];
        seg.segmentName = [NSString stringWithFormat:@"%@%d",self.segmentImgHead,(i+1)];
    }
}

@end

@implementation HSSegmentModel

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"partes":[HSPartItem class]};
}

@end

@implementation HSPartItem


@end
