//
//  HSProgramSaveModel.m
//  HiStar
//
//  Created by petcome on 2019/10/12.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSProgramSaveModel.h"
#import <YYKit/YYKit.h>

@implementation HSProgramSaveModel
    
+ (NSArray *)bg_unionPrimaryKeys {
    return @[@"timeStamp"];
}
+ (instancetype)programSaveModelWithName:(NSString *)name {
    HSProgramSaveModel *model = [[HSProgramSaveModel alloc] init];
    model.timeStamp = [[NSDate date] timeIntervalSince1970];
    model.saveName = name;
    model.saveId = [self getRandomStr];
    return model;
}
    
+ (NSString *)getRandomStr {
    char data[12];
    for (int x=0;x < 12;data[x++] = (char)('A' + (arc4random_uniform(26))));
    NSString *randomStr = [[NSString alloc] initWithBytes:data length:12 encoding:NSUTF8StringEncoding];
    NSString *string = [NSString stringWithFormat:@"%@",randomStr];
    NSLog(@"%@",string);
    return string;
}
    
+ (NSArray *)allProgramSaveModels {
    NSArray *array = [HSProgramSaveModel bg_find:nil where:nil];
    return array;
}

- (void)deleteSaveModel {
    [NSArray bg_clear:self.saveId];
    NSString *where = [NSString stringWithFormat:@"where %@=%@",bg_sqlKey(@"timeStamp"),bg_sqlValue(@(self.timeStamp))];
    [HSProgramSaveModel bg_delete:nil where:where];
}

- (NSMutableArray *)getHistoryProgramElements {
    NSMutableArray *arrM = (NSMutableArray *)[NSArray bg_arrayWithName:self.saveId];
    return arrM;
}

@end
