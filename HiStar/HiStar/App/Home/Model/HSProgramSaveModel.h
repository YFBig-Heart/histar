//
//  HSProgramSaveModel.h
//  HiStar
//
//  Created by petcome on 2019/10/12.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSProgramSaveModel : NSObject

/** 名称 */
@property (nonatomic,copy)NSString *saveName;
/** 存的时间戳 */
@property (nonatomic,assign)NSInteger timeStamp;

/** 存儲的唯一标识 */
@property (nonatomic,copy)NSString *saveId;

+ (instancetype)programSaveModelWithName:(NSString *)name;
+ (NSArray *)allProgramSaveModels;

// 删除
- (void)deleteSaveModel;

- (NSMutableArray *)getHistoryProgramElements;

@end

NS_ASSUME_NONNULL_END
