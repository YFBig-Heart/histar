//
//  HSManualModel.h
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class HSPartItem;
@interface HSManualModel : NSObject

/** 名称 */
@property (nonatomic,copy)NSString *manualName;
/** 样图 */
@property (nonatomic,copy)NSString *sampleImg;
/** 详细图 */
@property (nonatomic,copy)NSString *segmentImgHead;
/** 分部图 */
@property (nonatomic,strong)NSArray *segmentArray;

+ (NSArray <HSManualModel *>*)locationAllManuals;

// 需要用的时候设置一下
- (void)setSegmentName;

@end

@interface HSSegmentModel : NSObject

// sureImg:需要判断是否有
@property (nonatomic,copy)NSString *sureImg;
// 图片名称
@property (nonatomic,copy)NSString *segmentName;
/** 零件 */
@property (nonatomic,strong)NSArray <HSPartItem *>*partes;

@end

@interface HSPartItem : NSObject

// 零件图片名称
@property (nonatomic,copy)NSString *partImg;
// 需要多少个零件
@property (nonatomic,copy)NSString *partCount;
// 标号
@property (nonatomic,copy)NSString *partNumber;

@end


NS_ASSUME_NONNULL_END
