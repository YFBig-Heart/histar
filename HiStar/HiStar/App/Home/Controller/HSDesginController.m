//
//  HSDesginController.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSDesginController.h"
#import "HSDesginCell.h"
#import "HSMydesignItem.h"
#import "YFBtnAlertController.h"
#import "NSData+MD5Digest.h"
#import "HSTFAlertView.h"
#import "CustomSlider.h"
#import "TZImagePickerController.h"
#import "YYPhotoGroupView.h"

@interface HSDesginController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate, UIImagePickerControllerDelegate,TZImagePickerControllerDelegate>

@property (nonatomic,weak)IBOutlet UIButton *backBtn;
@property (nonatomic,weak)IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (nonatomic,strong)NSMutableArray *myDesignArray;

@property (weak, nonatomic) IBOutlet CustomSlider *sliderView;
@property (nonatomic,strong)TZImagePickerController *tzImagePicker;

@end

@implementation HSDesginController
+ (instancetype)desginController {
    HSDesginController *vc = [[HSDesginController alloc
                               ]initWithNibName:@"HSDesginController" bundle:nil];
    return vc;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    [self setUpUi];
    [self setUpSliderView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)setUpUi {
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.flowLayout.minimumLineSpacing = 20;
    CGFloat itemsizeH = 230.0*kYFScreenHeight/360;
    CGFloat collectionVW = itemsizeH*525/230;
    CGFloat itemsizeW = (collectionVW - self.flowLayout.minimumLineSpacing * 2)/3;

    self.flowLayout.itemSize = CGSizeMake(itemsizeW, itemsizeH);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"HSDesginCell" bundle:nil] forCellWithReuseIdentifier:@"HSDesginCell"];
    self.titleLabel.text = NSLocalizedString(@"My Creation", nil);
    
    self.sliderView.hidden = self.myDesignArray.count < 3;
}
- (void)setUpSliderView {
    self.sliderView.cornerRadius = 2;
    self.sliderView.isHorizontalSlider = YES;
    self.sliderView.bgColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.startColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.endColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.thumbImage = [UIImage imageNamed:@"white_slider"];
    [self.sliderView layerFilletWithRadius:self.sliderView.cornerRadius];
    [self.sliderView setSliderValueAndCurrentTarget:0];
    [self.sliderView updateDrawUI];
    WS(weakSelf);
    [self.sliderView setGesActionValueChange:^(CGFloat value) {
        CGFloat offsetX =value*((weakSelf.collectionView.contentSize.width-weakSelf.collectionView.width));
        weakSelf.collectionView.contentOffset = CGPointMake(offsetX, 0);
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat value = (scrollView.contentOffset.x)/(scrollView.contentSize.width-scrollView.width);
    [self.sliderView setSliderValueAndCurrentTarget:(value * 100)];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.myDesignArray.count + 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HSDesginCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HSDesginCell" forIndexPath:indexPath];
    if (indexPath.item == 0) {
        [cell configCellIsAdd:YES item:nil];
    }else {
        HSMydesignItem *item = [self.myDesignArray objectAtIndex:indexPath.item-1];
        [cell configCellIsAdd:NO item:item];
    }
     __weak typeof(self) weakSelf = self;
    [cell setDeleteBtnClick:^(HSMydesignItem * _Nonnull item) {
        [item deleteMydesign];
        [weakSelf.myDesignArray removeObject:item];
        [weakSelf.collectionView reloadData];
        weakSelf.sliderView.hidden = weakSelf.myDesignArray.count < 3;
    }];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        // 添加
//        [self takePhotoByPhotos];
        [self getAblumImage];
    }else {
        HSDesginCell *cell = (HSDesginCell *)[collectionView cellForItemAtIndexPath:indexPath];
        YYPhotoGroupItem *item = [[YYPhotoGroupItem alloc] init];
        item.thumbView = cell.designImg;
        HSMydesignItem *designItem = [self.myDesignArray objectAtIndex:indexPath.item-1];
        item.largeImageSize = [cell.designImg.image size];
        item.largeImageURL = [NSURL fileURLWithPath:[designItem loadImagePath]];
        YYPhotoGroupView *groupView = [[YYPhotoGroupView alloc] initWithGroupItems:@[item]];
        [groupView presentFromImageView:cell toContainer:self.view animated:YES completion:^{
            
        }];
        
    }
}

- (IBAction)backBtnClick:(id)sender {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (NSMutableArray *)myDesignArray {
    if (!_myDesignArray) {
        _myDesignArray = [NSMutableArray arrayWithArray:[HSMydesignItem getAllMyDesignes]];
    }
    return _myDesignArray;
}


- (void)getAblumImage {
    [self presentViewController:self.tzImagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    if (photos.count > 0) {
        UIImage *image = photos.firstObject;
        [self creatDesignItem:image];
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    _tzImagePicker = nil;
}

- (TZImagePickerController *)tzImagePicker {
    if (!_tzImagePicker) {
        _tzImagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
        _tzImagePicker.alwaysEnableDoneBtn = YES;
        _tzImagePicker.allowPickingVideo = NO;
        _tzImagePicker.allowTakePicture = YES;
        _tzImagePicker.allowPickingOriginalPhoto = NO;
        _tzImagePicker.sortAscendingByModificationDate = YES;
    }
    return _tzImagePicker;
}


#pragma mark - 添加照片
- (void)takePhotoByPhotos{
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        [MBProgressHUD showErrorMessage:NSLocalizedString(@"Can't get camera or photo information", nil) timer:2];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}


#pragma mark - UIImagePickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [self creatDesignItem:image];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)creatDesignItem:(UIImage *)image  {
     __weak typeof(self) weakSelf = self;
    [HSTFAlertView tfAlertViewTitle:@"" placeholder:@"" tfText:@"" textDidChange:^(UITextField * _Nonnull text) {
        if (text.text.length > 20) {
            [MBProgressHUD showTipMessageInView:[NSString stringWithFormat:NSLocalizedString(@"The nickname should be 1-%d characters long", nil),20]];
        }
    } btnClickBlock:^(NSInteger index, HSTFAlertView * _Nonnull view) {
        // 生成数据
        if (index == 1 && [NSString formatStr:view.textField.text len:20 hud:NO]) {
            HSMydesignItem *item = [[HSMydesignItem alloc] init];
            item.creat_time = @((NSInteger)[[NSDate date] timeIntervalSince1970]).stringValue;
            item.imagePath = [NSString stringWithFormat:@"%@.jpg",[NSData md5StringForString:item.creat_time]];
            item.designName = view.textField.text;
            NSString *path = [item loadImagePath];
            NSString *doc = [path stringByDeletingLastPathComponent];
            if (![[NSFileManager defaultManager] fileExistsAtPath:doc]) {
                [[NSFileManager defaultManager] createDirectoryAtPath:doc withIntermediateDirectories:YES attributes:nil error:nil];
            }
            NSData *data = UIImageJPEGRepresentation(image, 0.5);
            BOOL suc = [data writeToFile:path options:(NSDataWritingAtomic) error:nil];
            NSLog(@"%d",suc);
            [item bg_saveOrUpdate];
            [weakSelf.myDesignArray addObject:item];
            [weakSelf.collectionView reloadData];
            [view removeFromSuperview];
            weakSelf.sliderView.hidden = weakSelf.myDesignArray.count < 3;
        }
        if (index == 0) {
            [view removeFromSuperview];
        }
    }];

}


@end
