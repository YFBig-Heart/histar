//
//  HSHomeViewController.m
//  HiStar
//
//  Created by 晴天 on 2019/5/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSHomeViewController.h"
#import "UIButton+ImageTitleStyle.h"
#import "HSCustomBtn.h"
#import "HSSetViewController.h"
#import "HSManualController.h"
#import "HSDesginController.h"
#import "HSBaseFounctionVc.h"
#import "HSBlueConnectVc.h"
#import "HSProgrammeController.h"

@interface HSHomeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *baseFunctionBtn;
@property (weak, nonatomic) IBOutlet UILabel *baseFunctionLabel;

@property (weak, nonatomic) IBOutlet UIButton *programmeBtn;
@property (weak, nonatomic) IBOutlet UILabel *programeLabel;

@property (weak, nonatomic) IBOutlet UIButton *manualBtn;
@property (weak, nonatomic) IBOutlet UILabel *manualLabel;

@property (weak, nonatomic) IBOutlet UIButton *myDesginBtn;
@property (weak, nonatomic) IBOutlet UILabel *myDesginLabel;

@property (weak, nonatomic) IBOutlet UIButton *blueConnectBtn;

@end

@implementation HSHomeViewController

+(instancetype)homeViewController {
    HSHomeViewController *homevc = [[HSHomeViewController alloc] initWithNibName:@"HSHomeViewController" bundle:nil];

    return homevc;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    [self setUpUi];
    // 判断是否连接蓝牙
    if ([self checkBlueConnect] == NO) {
        [self blueToothBtnClick:self.blueConnectBtn];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}
- (BOOL)checkBlueConnect {
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    if ([peripheral isPeripheralConnected]==NO) {
        return NO;
    }
    return YES;
}

- (void)setUpUi {
    self.baseFunctionLabel.text = NSLocalizedString(@"Basic Functional", nil);
    self.programeLabel.text = NSLocalizedString(@"Programming Model", nil);
    self.manualLabel.text = NSLocalizedString(@"Build Handbook", nil);
    self.myDesginLabel.text = NSLocalizedString(@"My Creation", nil);
    
    [self.baseFunctionBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.programmeBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.manualBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.myDesginBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
}

#pragma mark - action
//基本功能
- (IBAction)baseFunctionBtnClick:(id)sender {
    HSBaseFounctionVc *basefounctionVc = [HSBaseFounctionVc baseFounctionVc];
    [self.navigationController pushViewController:basefounctionVc animated:YES];
}
- (IBAction)programmeBtnClick:(id)sender {
    HSProgrammeController *programmeVc = [HSProgrammeController programmeContrller];
    [self.navigationController pushViewController:programmeVc animated:YES];
}
- (IBAction)manualBtnClick:(id)sender {
    HSManualController *manualVc = [HSManualController manualController];
    [self.navigationController pushViewController:manualVc animated:YES];
}
- (IBAction)myDesginBtnClick:(id)sender {
    HSDesginController *desginVc = [HSDesginController desginController];
    [self.navigationController pushViewController:desginVc animated:YES];
}
- (IBAction)blueToothBtnClick:(id)sender {
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    if (peripheral.isPeripheralConnected) {
        [peripheral disconnectThePeripheralWithCompletionBlock:^(BOOL success, NSError *error) {
            HSBlueConnectVc *blueVc = [[HSBlueConnectVc alloc] initWithNibName:@"HSBlueConnectVc" bundle:nil];
            [self.navigationController pushViewController:blueVc animated:YES];
        }];
    }else {
        HSBlueConnectVc *blueVc = [[HSBlueConnectVc alloc] initWithNibName:@"HSBlueConnectVc" bundle:nil];
        [self.navigationController pushViewController:blueVc animated:YES];
    }
}
- (IBAction)settingBtnClick:(id)sender {
    HSSetViewController *setVc = [[HSSetViewController alloc] init];
    [self.navigationController pushViewController:setVc animated:YES];
}

@end
