//
//  HSRemoteController.h
//  HiStar
//
//  Created by petcome on 2019/5/28.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSRemoteController : YFBaseViewController

+ (instancetype)remoteController;

@end

NS_ASSUME_NONNULL_END
