//
//  HSNormalFunctionVc.m
//  HiStar
//
//  Created by petcome on 2019/5/28.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSNormalFunctionVc.h"
#import "HSBlueConnectVc.h"

@interface HSNormalFunctionVc ()

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *bubbleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImg;
@property (nonatomic,assign)BaseFunctionType functionType;

@end

@implementation HSNormalFunctionVc
/*
 不支持的模式：遥控、重力、编程
 */
+ (instancetype)normalFunctionVcWithType:(BaseFunctionType)functionType {
    HSNormalFunctionVc *vc = [[UIStoryboard storyboardWithName:@"BaseFounction" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    vc.functionType = functionType;
    return vc;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    [self updateUIWithFunctionType:self.functionType];
    [YFHomeHelper checkBlueConnectWithTargetVc:self];
}
- (void)dealloc {
    [self sendBlueCommend:BLEOperationTypeStopModel needCycle:NO];
}

- (void)updateUIWithFunctionType:(BaseFunctionType)functionType {
    NSString *titleName = @"";
    NSString *bubbleText=@"";
    NSString *bgImageName = @"yaokong_bg";
    switch (functionType) {
        case BaseFunctionRemote:{
            titleName = NSLocalizedString(@"Remote mode", nil);
            bgImageName = @"yaokong_bg";
            bubbleText = @"";
        }
            break;
        case BaseFunctionGravity:{
            titleName = NSLocalizedString(@"Gravity mode", nil);
            bgImageName = @"gravity_bg";
            bubbleText = @"";
        }
            break;
        case BaseFunctionBizhang:{
            titleName = NSLocalizedString(@"Obstacle avoidance mode", nil);
            bgImageName = @"bizhang_bg";
            bubbleText = NSLocalizedString(@"I will automatically avoid obstacles.", @"我会自动躲避障碍哦");
        }
            break;
        case BaseFunctionApplause:{
            titleName = NSLocalizedString(@"Applause mode", nil);
            bgImageName = @"zhangsheng_bg";
            bubbleText = @"可以向小星发出拍手声哦";
        }
            break;
        case BaseFunctionDance:{
            titleName = NSLocalizedString(@"Dancing mode", nil);
            bgImageName = @"tiaowu_bg";
            bubbleText = NSLocalizedString(@"You can post a dance instruction to the HiBot.",@"可以向小勇士发布跳舞指令哦");
        }
            break;
        case BaseFunctionXunXian:{
            titleName = NSLocalizedString(@"Line tracking mode", nil);
            bgImageName = @"xunXian_bg";
            bubbleText = NSLocalizedString(@"I will automatically follow the black line.",@"我会自动沿黑线前进哦");
        }
            break;
        case BaseFunctionFollowlight:{
            titleName = NSLocalizedString(@"Chasing light mode", nil);
            bgImageName = @"zhuiguang_bg";
            bubbleText = NSLocalizedString(@"I will follow the light.",@"我会跟着灯光前进哦");
        }
            break;
        case BaseFunctionSumo:{
            titleName = NSLocalizedString(@"Bullfighting mode", nil);
            bgImageName = @"xiangPu_bg";
            bubbleText = NSLocalizedString(@"You can issue a battle order to the HiBot.", @"可以向小勇士发布争斗指令哦");
        }
            break;
        case BaseFunctionCrawl:{
            titleName = NSLocalizedString(@"Fence mode", nil);
            bgImageName = @"weiLan_bg";
            bubbleText =NSLocalizedString(@"I will find the exit myself.", @"我会自己找到出口哦");
        }
            break;
        default:
            break;
    }
    self.titleLabel.text = titleName;
    self.bgImageView.image = [UIImage imageNamed:bgImageName];
    self.bubbleLabel.text = bubbleText;
}

- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playBtnClick:(UIButton *)sender {
    if (sender.selected) {
        //停止
        [self sendBlueCommend:BLEOperationTypeStopModel needCycle:NO];
        sender.selected = NO;
    }else {
        if ([YFHomeHelper checkBlueConnectWithTargetVc:self] == NO) {
            return;
        }
        sender.selected = YES;
        BLEOperationType operationType;
        BOOL needCycle = NO;
        switch (self.functionType) {
            case BaseFunctionBizhang:{
                operationType = BLEOperationTypeBiZhang;
            }
                break;
            case BaseFunctionApplause:{
                operationType = BLEOperationTypeApplause;
            }
                break;
            case BaseFunctionDance:{
                operationType = BLEOperationTypeDance;
            }
                break;
            case BaseFunctionXunXian:{
                operationType = BLEOperationTypeXunXian;
            }
                break;
            case BaseFunctionFollowlight:{
                operationType = BLEOperationTypeFollowlight;
            }
                break;
            case BaseFunctionSumo:{
                operationType = BLEOperationTypeSumo;
            }
                break;
            case BaseFunctionCrawl:{
                operationType = BLEOperationTypeCrawl;
            }
                break;
            default:
                return;
        }
        [self sendBlueCommend:operationType needCycle:needCycle];
    }
    
}

- (void)sendBlueCommend:(BLEOperationType)operationType needCycle:(BOOL)cycle {
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    if (peripheral.isPeripheralConnected == NO) {
        return;
    }
    [peripheral requestOperationType:operationType parmaObject:nil completionBlock:^(BOOL success, id response, NSError *error) {
        
    }];
}


@end
