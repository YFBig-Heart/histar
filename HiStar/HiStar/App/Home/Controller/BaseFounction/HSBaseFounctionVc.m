//
//  HSBaseFounctionVc.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSBaseFounctionVc.h"
#import "HSBaseFounctionCell.h"
#import "HSNormalFunctionVc.h"
#import "HSRemoteController.h"
#import "HSGravityController.h"
#import "CustomSlider.h"

@interface HSBaseFounctionVc ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet CustomSlider *sliderView;
@property (nonatomic,strong)NSArray *dataSource;


@end

@implementation HSBaseFounctionVc

+ (instancetype)baseFounctionVc {
    HSBaseFounctionVc *baseVc = [[UIStoryboard storyboardWithName:@"BaseFounction" bundle:nil] instantiateViewControllerWithIdentifier:@"HSBaseFounctionVc"];
    return baseVc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    [self setUpUi];
    [self setUpSliderView];
    [YFHomeHelper checkBlueConnectWithTargetVc:self];
}
- (void)setUpSliderView {
    self.sliderView.cornerRadius = 2;
    self.sliderView.isHorizontalSlider = YES;
    self.sliderView.bgColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.startColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.endColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.thumbImage = [UIImage imageNamed:@"white_slider"];
    [self.sliderView layerFilletWithRadius:self.sliderView.cornerRadius];
    [self.sliderView setSliderValueAndCurrentTarget:0];
    [self.sliderView updateDrawUI];
    WS(weakSelf);
    [self.sliderView setGesActionValueChange:^(CGFloat value) {
        CGFloat offsetX =value*((weakSelf.collectionView.contentSize.width-weakSelf.collectionView.width));
        weakSelf.collectionView.contentOffset = CGPointMake(offsetX, 0);
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat value = (scrollView.contentOffset.x)/(scrollView.contentSize.width-scrollView.width);
    [self.sliderView setSliderValueAndCurrentTarget:(value * 100)];
}
- (void)setUpUi {
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.flowLayout.minimumLineSpacing = 10;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(0, 30, 0, 30);
    CGFloat itemsizeH = 200.0*kYFScreenHeight/360;
    CGFloat itemsizeW = itemsizeH*153/200;
    self.flowLayout.itemSize = CGSizeMake(itemsizeW, itemsizeH);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"HSBaseFounctionCell" bundle:nil] forCellWithReuseIdentifier:@"HSBaseFounctionCell"];
    self.titleLabel.text = NSLocalizedString(@"Basic Functional", nil);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HSBaseFounctionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HSBaseFounctionCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    NSDictionary *dict = self.dataSource[indexPath.item];
    NSString *modelName = [dict objectForKey:@"name"];
    [cell configCellWithModelName:modelName imageName:[dict objectForKey:@"image"]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    NSDictionary *dict = self.dataSource[indexPath.item];
    BaseFunctionType type = [[dict objectForKey:@"model"] intValue];
    if (type == BaseFunctionRemote) {
        HSRemoteController *remoteVc = [HSRemoteController remoteController];
        [self.navigationController pushViewController:remoteVc animated:YES];
    }else if (type == BaseFunctionGravity){
        HSGravityController *gravityVc = [HSGravityController gravityController];
        [self.navigationController pushViewController:gravityVc animated:YES];
    }else if (type == BaseFunctionProgramme){
        
    }else {
        HSNormalFunctionVc *normalVc = [HSNormalFunctionVc normalFunctionVcWithType:type];
        [self.navigationController pushViewController:normalVc animated:YES];
    }

}

- (IBAction)backBtnClick:(id)sender {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (NSArray *)dataSource {
    if (!_dataSource) {
        _dataSource = @[
                        @{@"image":@"model_yaokong_bg",@"name":NSLocalizedString(@"Remote mode", nil),@"model":@(BaseFunctionRemote)},
                        @{@"image":@"model_gravity_bg",@"name":NSLocalizedString(@"Gravity mode", nil),@"model":@(BaseFunctionGravity)},

                        @{@"image":@"model_bizhang_bg",@"name":NSLocalizedString(@"Obstacle avoidance mode", nil),@"model":@(BaseFunctionBizhang)},
                        @{@"image":@"model_applause_bg",@"name":NSLocalizedString(@"Applause mode", nil),@"model":@(BaseFunctionApplause)},
                        @{@"image":@"model_dance_bg",@"name":NSLocalizedString(@"Dancing mode", nil),@"model":@(BaseFunctionDance)},
                        @{@"image":@"model_xunxian_bg",@"name":NSLocalizedString(@"Line tracking mode", nil),@"model":@(BaseFunctionXunXian)},
                        @{@"image":@"model_followlight_bg",@"name":NSLocalizedString(@"Chasing light mode", nil),@"model":@(BaseFunctionFollowlight)},
                        @{@"image":@"model_sumo_bg",@"name":NSLocalizedString(@"Bullfighting mode", nil),@"model":@(BaseFunctionSumo)},

                            @{@"image":@"model_ crawl_bg",@"name":NSLocalizedString(@"Fence mode", nil),@"model":@(BaseFunctionCrawl)},
                        ];
    }
    return _dataSource;
}

@end
