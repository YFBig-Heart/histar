//
//  HSRemoteController.m
//  HiStar
//
//  Created by petcome on 2019/5/28.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSRemoteController.h"
#import "HSBlueConnectVc.h"
#import "YFHomeHelper.h"


@interface HSRemoteController ()

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIButton *leftlightBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightLightBtn;
@property (weak, nonatomic) IBOutlet UIButton *remoteCycleBtn;
@property (weak, nonatomic) IBOutlet UIButton *soundBtn;
@property (weak, nonatomic) IBOutlet UIView *fangxiangPanView;
@property (weak, nonatomic) IBOutlet UIImageView *fnagxiangPanImg;

@property (nonatomic,assign)CGPoint originPoint;
/** 中间方向盘的宽高 */
@property (nonatomic,assign)CGFloat imgWH;
/** 底座的宽高 */
@property (nonatomic,assign)CGFloat bottomViewWH;
/** 防止指令发送过多 */
@property (nonatomic,assign)BOOL canSend;

@end

@implementation HSRemoteController {
    dispatch_source_t _timer;
    BLEOperationType _remoteOperationType; // 遥控方向
}

+ (instancetype)remoteController {
    HSRemoteController *vc = [[UIStoryboard storyboardWithName:@"BaseFounction" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    return vc;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    [self setWheelRemote];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didConnectPeripheral:) name:kLGPeripheralDidConnect object:nil];
    
    // 添加长按手势
//    UILongPressGestureRecognizer *remoteLongGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(remoteLongGesAction:)];
//    remoteLongGes.minimumPressDuration = 0.2;
//    [self.remoteCycleBtn addGestureRecognizer:remoteLongGes];
    
    UILongPressGestureRecognizer *tempLongGes = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tempLongGesAction:)];
    tempLongGes.minimumPressDuration = 0.2;
    [self.soundBtn addGestureRecognizer:tempLongGes];
    self.titleLabel.text = NSLocalizedString(@"Remote mode", nil);
    
    [YFHomeHelper checkBlueConnectWithTargetVc:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self sendBlueCommend:BLEOperationTypeYaokong needCycle:NO];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.fangxiangPanView layoutIfNeeded];
    self.originPoint = self.fnagxiangPanImg.center;
    self.imgWH = self.fnagxiangPanImg.width;
    self.bottomViewWH = self.fangxiangPanView.width;
}
- (void)dealloc {
    [self sendBlueCommend:BLEOperationTypeStopModel needCycle:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didConnectPeripheral:(NSNotification *)note {
    [self sendBlueCommend:BLEOperationTypeYaokong needCycle:NO];
}
// 长按按钮
//- (void)remoteLongGesAction:(UILongPressGestureRecognizer *)longGes {
//    switch (longGes.state) {
//        case UIGestureRecognizerStateBegan:
//            _canSend = YES;
//            self.remoteCycleBtn.selected = YES;
//            break;
//        case UIGestureRecognizerStateChanged:
//            if (_canSend) {
//                [self cycleBtnClick:self.remoteCycleBtn];
//                _canSend = NO;
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.08 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    self.canSend = YES;
//                });
//            }
//            break;
//        case UIGestureRecognizerStateCancelled:
//        case UIGestureRecognizerStateEnded:
//            self.remoteCycleBtn.selected = NO;
//            break;
//        default:
//            break;
//    }
//}
- (void)tempLongGesAction:(UILongPressGestureRecognizer *)longGes {
    switch (longGes.state) {
        case UIGestureRecognizerStateBegan:
            _canSend = YES;
            break;
        case UIGestureRecognizerStateChanged:
            if (_canSend) {
                [self soundBtnClick:self.soundBtn];
                _canSend = NO;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.08 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.canSend = YES;
                });
            }
            break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:
            
            break;
        default:
            break;
    }
}


- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)cycleBtnClick:(UIButton *)sender {
    if (sender.selected == YES) {
        [self sendBlueCommend:BLEOperationTypeStopAccelerate needCycle:NO];
    }else {
        [self sendBlueCommend:BLEOperationTypeAccelerate needCycle:NO];
    }
    sender.selected = !sender.isSelected;
}
- (IBAction)soundBtnClick:(UIButton *)sender {
    [self sendBlueCommend:BLEOperationTypeOpenSound needCycle:NO];
}

- (IBAction)leftLightBtnclick:( UIButton *)sender {
    if (sender.selected) {
        [self sendBlueCommend:BLEOperationTypeCloseLeftBlueLight needCycle:NO];
    }else {
        [self sendBlueCommend:BLEOperationTypeOpenLeftBlueLight needCycle:NO];
    }
    sender.selected = !sender.selected;
}
- (IBAction)rightLightBtnClick:(UIButton *)sender {
    if (sender.selected) {
        [self sendBlueCommend:BLEOperationTypeCloseRightRedLight needCycle:NO];
    }else {
        [self sendBlueCommend:BLEOperationTypeOpenRightRedLight needCycle:NO];
    }
    sender.selected = !sender.selected;
}

- (void)sendBlueCommend:(BLEOperationType)operationType needCycle:(BOOL)cycle {
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    [peripheral requestOperationType:operationType parmaObject:nil completionBlock:^(BOOL success, id response, NSError *error) {
        if (success) {
            
        }
    }];
}

#pragma mark - 设置方向盘
- (void)setWheelRemote {
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGetstureAction:)];
    self.fnagxiangPanImg.userInteractionEnabled = YES;
    [self.fnagxiangPanImg addGestureRecognizer:pan];
}

- (void)panGetstureAction:(UIPanGestureRecognizer *)pan {
    
    CGPoint point = [pan locationInView:self.fangxiangPanView];
    // 半径
    CGFloat maxXY = (self.bottomViewWH)*0.5;
    CGFloat cpointx = point.x- maxXY;
    CGFloat cpointy = point.y- maxXY;
    
    // 计算角度
    CGFloat panCurrentR = sqrt(pow(fabs(cpointx), 2) + pow(fabs(cpointy), 2));
    // 计算角度
    CGFloat angle = asin(fabs(cpointy)/panCurrentR);
    if (cpointx >=0 && cpointy < 0) {
        // 第一象限
        angle = angle;
    }else if (cpointx <=0 && cpointy < 0){
        // 第二象限
        angle = M_PI-angle;
    }else if (cpointy >=0 && cpointx <0){
        // 第三象限
        angle = M_PI+angle;
    }else {
        angle = M_PI*2 - angle;
    }
    
    BLEOperationType operationType;
    CGFloat degree = ((angle*360) / (2*M_PI));
    if (angle >= M_PI/8 && angle < M_PI*3/8){
        operationType= BLEOperationTypeGoUpRight;
        NSLog(@"右前方%0.1f",degree);
    }else if (angle >= M_PI*3/8 && angle < M_PI*5/8){
        operationType= BLEOperationTypeGoUp;
        NSLog(@"向前%0.1f",degree);
    }else if (angle >= M_PI*5/8 && angle < M_PI*7/8){
        operationType= BLEOperationTypeGoUpLeft;
        NSLog(@"左前方%0.1f",degree);
    }else if (angle >= M_PI*7/8 && angle < M_PI*9/8){
        operationType= BLEOperationTypeTurnLeft;
        NSLog(@"左转%0.1f",degree);
    }else if (angle >= M_PI*9/8 && angle < M_PI*11/8){
        operationType= BLEOperationTypeGoDownLeft;
        NSLog(@"左后方%0.1f",degree);
    }else if (angle >= M_PI*11/8 && angle < M_PI*13/8){
        operationType=BLEOperationTypeGodown ;
        NSLog(@"后退%0.1f",degree);
    }else if (angle >= M_PI*13/8 && angle < M_PI*15/8){
        operationType= BLEOperationTypeGoDownRight;
        NSLog(@"右后方%0.1f",degree);
    }else {
        // 向右
        operationType= BLEOperationTypeTurnRight;
        NSLog(@"向右%0.1f",degree);
    }

//    NSLog(@"%0.1f--%0.1f---%f",cpointx,cpointy,angle);
    if (panCurrentR > maxXY-self.imgWH*0.5) {
        cpointx = (cpointx*(maxXY-self.imgWH*0.5))/panCurrentR;
        cpointy = (cpointy*(maxXY-self.imgWH*0.5))/panCurrentR;
    }
    
    point = CGPointMake(cpointx + maxXY, maxXY + cpointy);
    _remoteOperationType = operationType;
    
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{
            [self dispathTimerUpdate];
        }
            break;
        case UIGestureRecognizerStateChanged:{
            self.fnagxiangPanImg.center = point;
        }
            break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded:{
            self.fnagxiangPanImg.center = self.originPoint;
            if (_timer) {
                 dispatch_source_cancel(_timer);
            }
        }
            break;
        default:{
            self.fnagxiangPanImg.center = self.originPoint;
        }
            break;
    }

}

// 固定间隔100毫秒触发一次
- (void)dispathTimerUpdate {
    __weak typeof(self) weakSelf = self;
    _timer = [YFHomeHelper interiorDCDTimeSpan:0.1 reduseTimeBlock:^(CGFloat reduseTime) {
        NSLog(@"%lu",(unsigned long)self->_remoteOperationType);
        [weakSelf sendBlueCommend:self->_remoteOperationType needCycle:NO];
    } cancelBlock:^{
        self->_timer = nil;
    }];
}







@end
