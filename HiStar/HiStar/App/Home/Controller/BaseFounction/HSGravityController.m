    //
    //  HSGravityController.m
    //  HiStar
    //
    //  Created by petcome on 2019/5/28.
    //  Copyright © 2019 晴天. All rights reserved.
    //

#import "HSGravityController.h"
#import <CoreMotion/CoreMotion.h>
#import "HSBlueConnectVc.h"

@interface HSGravityController ()
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (nonatomic, strong) CMMotionManager *motionManager;

@property (weak, nonatomic) IBOutlet UIView *tipView;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UIButton *skipCoverButton;
@property (weak, nonatomic) IBOutlet UILabel *skipLabel;

@end

@implementation HSGravityController

+ (instancetype)gravityController {
    HSGravityController *vc = [[UIStoryboard storyboardWithName:@"BaseFounction" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenNavBar = YES;
    self.tipLabel.text = NSLocalizedString(@"Please hold the phone as shown in the picture, turn left and right to control the HiBot!", nil);
    _skipLabel.text = NSLocalizedString(@"Skip", nil);
    self.titleLabel.text = NSLocalizedString(@"Operation prompt", nil);
    [YFHomeHelper checkBlueConnectWithTargetVc:self];
}
- (void)dealloc {
    [self sendBlueCommend:BLEOperationTypeStopModel needCycle:NO];
}
- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)playBtnClick:(UIButton *)sender {
    if (sender.selected) {
        [self.motionManager stopDeviceMotionUpdates];
    }else {
        [self startUpdateMoation];
    }
    sender.selected = !sender.selected;
}

- (IBAction)skipBtnClick:(id)sender {
    self.tipView.hidden = YES;
    self.titleLabel.text = NSLocalizedString(@"Gravity mode", nil);
}


- (void)sendBlueCommend:(BLEOperationType)operationType needCycle:(BOOL)cycle {
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    [peripheral requestOperationType:operationType parmaObject:nil completionBlock:^(BOOL success, id response, NSError *error) {
        if (success) {
            
        }
    }];
}

#pragma mark - 移动数据
- (void)startUpdateMoation {
    if (!self.motionManager) {
        self.motionManager = [[CMMotionManager alloc]init];
        if (!self.motionManager.deviceMotionAvailable) {
            [YFAlertHelper showLeeAlertWithTitle:@"" message:@"该设备的deviceMotion不可用" defalutTitle:klocOk defalutAction:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            return;
        }
    }
        // 更新频率
    self.motionManager.deviceMotionUpdateInterval = 0.1;
        //开始更新设备的动作信息
    __weak typeof(self) weakSelf = self;
    [self.motionManager startDeviceMotionUpdatesToQueue:[[NSOperationQueue alloc] init] withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf updateDisplay];
        });
        NSLog(@"ahsdhf");
    }];
}

- (void)updateDisplay {
    if (![self.motionManager isDeviceMotionAvailable]) {
        return;
    }
    //获取设备移动信息
    CMDeviceMotion* deviceMotion = self.motionManager.deviceMotion;
    NSMutableString* str = [NSMutableString stringWithString:@"deviceMotion信息为：\n"];
    [str appendFormat:@"gravity的X：%+.2f\n", deviceMotion.gravity.x];
    [str appendFormat:@"gravity的Y：%+.2f\n" , deviceMotion.gravity.y];
    [str appendFormat:@"gravity的Z：%+.2f\n" , deviceMotion.gravity.z];
    
    NSLog(@"%@",str);
    CGFloat y = deviceMotion.gravity.y;
    CGFloat z = deviceMotion.gravity.z;
    CGFloat x = deviceMotion.gravity.x;
    BLEOperationType operationType;
    // z大于0（0~1） 向后，小于0（0-~-1）向前；y小于0 （0~-1）向右，大于0（0~1）向左
    if (z < -0.1 && x > 0.3) {
        // 停止
         operationType = BLEOperationTypeStopModel;
    }else if (z < 0 && x > -0.65){
        // 向前
        if (y < 0.2 && y > -0.2) {
            operationType=BLEOperationTypeGoUp;
        }else if (y > 0.2 && y < 0.6){
            operationType = BLEOperationTypeGoUpLeft;
        }else if (y > 0.6) {
            operationType = BLEOperationTypeTurnLeft;
        }else if (y > -0.6 && y < -0.2){
            operationType = BLEOperationTypeGoUpRight;
        }else {
            operationType = BLEOperationTypeTurnRight;
        }
    }else {
        // 向后
        if (y < 0.2 && y > -0.2) {
            operationType=BLEOperationTypeGodown;
        }else if (y > 0.2 && y < 0.6){
            operationType = BLEOperationTypeGoDownLeft;
        }else if (y > 0.6) {
            operationType = BLEOperationTypeTurnLeft;
        }else if (y > -0.6 && y < -0.2){
            operationType = BLEOperationTypeGoDownRight;
        }else {
            operationType = BLEOperationTypeTurnRight;
        }
    }
    
//    if (z<0) {
//        if (y < 0.2 && y > -0.2) {
//            operationType=BLEOperationTypeGoUp;
//        }else if (y > 0.2 && y < 0.6){
//            operationType = BLEOperationTypeGoUpLeft;
//        }else if (y > 0.6) {
//            operationType = BLEOperationTypeTurnLeft;
//        }else if (y > -0.6 && y < -0.2){
//            operationType = BLEOperationTypeGoUpRight;
//        }else {
//            operationType = BLEOperationTypeTurnRight;
//        }
//    }else {
//        if (y < 0.2 && y > -0.2) {
//            operationType=BLEOperationTypeGodown;
//        }else if (y > 0.2 && y < 0.6){
//            operationType = BLEOperationTypeGoDownLeft;
//        }else if (y > 0.6) {
//            operationType = BLEOperationTypeTurnLeft;
//        }else if (y > -0.6 && y < -0.2){
//            operationType = BLEOperationTypeGoDownRight;
//        }else {
//            operationType = BLEOperationTypeTurnRight;
//        }
//    }
    [self sendBlueCommend:operationType needCycle:NO];
}


@end
