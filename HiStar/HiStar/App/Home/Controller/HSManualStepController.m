//
//  HSManualStepController.m
//  HiStar
//
//  Created by 晴天 on 2019/10/8.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSManualStepController.h"
#import "HSManualModel.h"
#import "HSManualPartCell.h"
#import "HSManualController.h"
#import <Masonry.h>

@interface HSManualStepController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *partView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *previousBtn;
@property (weak, nonatomic) IBOutlet UILabel *pageLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic,weak)IBOutlet UIView *scrollContentV;
@property (weak, nonatomic)IBOutlet UIImageView *segmentImg;
@property (weak, nonatomic)IBOutlet UIImageView *otherImg;

@property (nonatomic,strong)HSManualModel *model;
@property (nonatomic,strong)NSArray <HSPartItem *>*partArray;
@property (nonatomic,assign)int currentPage;

@end

@implementation HSManualStepController

+ (instancetype)manualStepControllerWithItem:(HSManualModel *)model {
    HSManualStepController *vc = [[HSManualStepController alloc] initWithNibName:@"HSManualStepController" bundle:nil];
    vc.model = model;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.model setSegmentName];
    [self setUpUi];
}

- (IBAction)backButtonClick:(id)sender {
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[HSManualController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}
- (IBAction)previousBtnClick:(id)sender {
    if (self.currentPage <= 1) {
        return;
    }
    self.currentPage -= 1;
}
- (IBAction)nextBtnClick:(id)sender {
    if (self.currentPage >= self.model.segmentArray.count) {
        return;
    }
    self.currentPage += 1;
}


- (void)setUpUi {
    self.titleLabel.text = NSLocalizedString(self.model.manualName, nil);
    if (self.model.segmentArray.count > 0) {
        self.currentPage = 1;
    }else {
        self.currentPage = 0;
    }
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"HSManualPartCell" bundle:nil] forCellReuseIdentifier:@"HSManualPartCell"];
    self.tableView.estimatedRowHeight = 40;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.scrollView.delegate = self;
    [self.scrollView setMinimumZoomScale:1];
    [self.scrollView setMaximumZoomScale:5];
}

- (void)setCurrentPage:(int)currentPage {
    _currentPage = currentPage;
    [self.scrollView setZoomScale:1 animated:YES];
    self.pageLabel.text = [NSString stringWithFormat:@"%d/%lu",_currentPage,(unsigned long)self.model.segmentArray.count];
    if (self.model.segmentArray.count == 0 || self.model.segmentArray.count < currentPage) {
        self.segmentImg.image = [UIImage new];
        return;
    }
    HSSegmentModel *segmodel = self.model.segmentArray[currentPage-1];
    if (segmodel.sureImg.length > 0) {
        self.otherImg.hidden = NO;
        self.otherImg.image = [UIImage imageNamed:segmodel.sureImg];
    }else {
        self.otherImg.hidden = YES;
    }
    self.segmentImg.image = [UIImage imageNamed:segmodel.segmentName];
    self.partView.hidden = segmodel.partes.count == 0;
    self.partArray = segmodel.partes;
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.partArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HSManualPartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HSManualPartCell" forIndexPath:indexPath];
    if (self.partArray.count > indexPath.row) {
        HSPartItem *partItem = [self.partArray objectAtIndex:indexPath.row];
        cell.partItem = partItem;
    }
    return cell;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.scrollContentV;
}


@end
