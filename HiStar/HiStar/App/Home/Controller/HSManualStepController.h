//
//  HSManualStepController.h
//  HiStar
//
//  Created by 晴天 on 2019/10/8.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "YFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class HSManualModel;
@interface HSManualStepController : YFBaseViewController

+ (instancetype)manualStepControllerWithItem:(HSManualModel *)model;

@end

NS_ASSUME_NONNULL_END
