//
//  HSHomeViewController.h
//  HiStar
//
//  Created by 晴天 on 2019/5/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "YFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSHomeViewController : YFBaseViewController

+ (instancetype)homeViewController;

@end

NS_ASSUME_NONNULL_END
