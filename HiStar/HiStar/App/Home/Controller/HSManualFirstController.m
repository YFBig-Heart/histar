//
//  HSManualFirstController.m
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSManualFirstController.h"
#import "HSManualStepController.h"
#import "HSManualModel.h"
#import "NSAttributedString+RichText.h"

@interface HSManualFirstController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *skipLabel;
@property (nonatomic,strong)HSManualModel *model;

@property (weak, nonatomic) IBOutlet UILabel *countTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *lengthTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *ectLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *numberLabels;

@end

@implementation HSManualFirstController

+ (HSManualFirstController *)manualFirstVcWithItem:(HSManualModel *)model {
    HSManualFirstController *vc = [[HSManualFirstController alloc] initWithNibName:@"HSManualFirstController" bundle:nil];
    vc.model = model;
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.titleLabel.text = NSLocalizedString(@"Construction tips", nil);
    self.skipLabel.text = NSLocalizedString(@"Skip", nil);

    [self setUpUi];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)setUpUi {
    for (UILabel *numLabel in self.numberLabels) {
        [numLabel layerWithBorderColor:RGB16TOCOLOR(0x18B4F1) borderWidth:1 cornerRadius:9 masksToBounds:YES];
    }
    self.ectLabel.text = NSLocalizedString(@"As an example:", nil);
    self.countTipLabel.text = NSLocalizedString(@"indicates the number of accessories", nil);
    NSMutableAttributedString *atterM = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"3 indicates the length of the accessories 3 vias", nil)];
    [atterM setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:24],NSForegroundColorAttributeName:RGB16TOCOLOR(0x18B4F1)} range:NSMakeRange(0, 1)];
    [atterM setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:RGB16TOCOLOR(0x18B4F1)} range:NSMakeRange(0, 1)];
    self.lengthTipLabel.attributedText = atterM;
    self.descLabel.text = NSLocalizedString(@"Description: Take the hole arm as References, other accessories come Compare, look at the length equivalent Several vias.", nil);
    self.ectLabel.text = NSLocalizedString(@"As an example:", nil);

}


- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)skipBtnClick:(id)sender {
    HSManualStepController *stepVc = [HSManualStepController manualStepControllerWithItem:self.model];
    [self.navigationController pushViewController:stepVc animated:YES];
}


@end
