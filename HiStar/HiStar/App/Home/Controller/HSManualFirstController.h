//
//  HSManualFirstController.h
//  HiStar
//
//  Created by petcome on 2019/10/8.
//  Copyright © 2019 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class HSManualModel;
@interface HSManualFirstController : YFBaseViewController

+ (HSManualFirstController *)manualFirstVcWithItem:(HSManualModel *)model;

@end

NS_ASSUME_NONNULL_END
