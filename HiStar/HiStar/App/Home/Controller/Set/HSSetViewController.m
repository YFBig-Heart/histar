//
//  HSSetViewController.m
//  HiStar
//
//  Created by petcome on 2019/5/17.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSSetViewController.h"
#import "HSLanaguageSetVc.h"
#import "HSVersionAboutVc.h"
#import "WKWebviewController.h"

@interface HSSetViewController ()

@property (nonatomic,strong)UIButton *rightBarButton;

@end

@implementation HSSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Set up", @"设置");
    [self loadDataItems];
    
    UIButton *barButton = [UIButton buttonWithType:UIButtonTypeCustom];
    barButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [barButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.rightBarButton = barButton;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:barButton];
    barButton.multipleTouchEnabled = YES;
    
    [barButton addTarget:self action:@selector(rightBarItem:event:) forControlEvents:UIControlEventTouchDownRepeat];
    
    self.rightBarButton.hidden = YES;
}

- (void)rightBarItem:(UIButton *)sender event:(UIEvent *)event {
    UITouch *touch = [event.allTouches anyObject];
    if (touch.tapCount == 5) {
        NSString *title = [YFUserDefault getTestMode] ? @"开启Joker🤡模式":@"关闭Joker🤡模式";
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
       
        
        UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
            UITextField *textfild = alert.textFields.firstObject;
            if ([textfild.text isEqualToString:@"xiaodezi"]) {
                [YFUserDefault setTestMode:![YFUserDefault getTestMode]];
                [self loadDataItems];
            }else {
                [MBProgressHUD showErrorMessage:@"没有🤡，继续猜😝" timer:1.5];
            }
            
        }];
        
        
        [alert addAction:cancel];
        [alert addAction:sure];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    YFPeripheral *peripheral = [YFCommunicationManager shareInstance].yfPeripheral;
    if (peripheral.isPeripheralConnected == NO) {
        [self.rightBarButton setTitle:@"" forState:UIControlStateNormal];
        self.rightBarButton.hidden = YES;
    }else {
        self.rightBarButton.hidden = NO;
        [self.rightBarButton setTitle:peripheral.lgPeripheral.name forState:UIControlStateNormal];
        [self.rightBarButton sizeToFit];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}



- (void)loadDataItems {
    [self.groups removeAllObjects];
    
    UserGroup *group0 = [[UserGroup alloc] init];
    UserArrowItem *item0_0 = [UserArrowItem itemWithIcon:@"set_lanaguage" andTitle:NSLocalizedString(@"Language",@"語言") andTarget:[HSLanaguageSetVc class]];
    [group0.items addObject:item0_0];

    UserGroup *group1 = [[UserGroup alloc] init];
    UserArrowItem *item1_0 = [UserArrowItem itemWithIcon:@"set_ver" andTitle:NSLocalizedString(@"Version number",@"版本號")];
    WS(weakSelf);
    [item1_0 setBlock:^(UserItem *itemT, NSIndexPath *indexpath) {
        HSVersionAboutVc *verisonVc = [HSVersionAboutVc verionAboutVc];
        verisonVc.modalPresentationStyle = UIModalPresentationCustom;
        [weakSelf presentViewController:verisonVc animated:YES completion:nil];
    }];
    UserArrowItem *item1_1 = [UserArrowItem itemWithIcon:@"set_lanaguage" andTitle:[YFUserDefault getTestMode] ? @"测试模式":@"正式模式"];
    [item1_1 setBlock:^(UserItem *itemT, NSIndexPath *indexpath) {
        if ([YFUserDefault getTestMode]) {
            itemT.title = @"正式模式";
            [YFUserDefault setTestMode:NO];
        }else {
            itemT.title = @"测试模式";
            [YFUserDefault setTestMode:YES];
        }
        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationFade];
    }];
    
    UserArrowItem *item1_2 = [UserArrowItem itemWithIcon:@"ic_more_service" andTitle:NSLocalizedString(@"Privacy Policy", nil)];
    [item1_2 setBlock:^(UserItem *itemT, NSIndexPath *indexpath) {
        WKWebviewController *webVc = [[WKWebviewController alloc] init];
        webVc.urlString = @"http://api.pet-come.com/html/privacy.html";
        [weakSelf.navigationController pushViewController:webVc animated:YES];
    }];
    if ([YFUserDefault getTestMode]) {
        [group1.items addObjectsFromArray:@[item1_0,item1_1,item1_2]];
    }else {
        [group1.items addObjectsFromArray:@[item1_0,item1_2]];
    }
    
    UserGroup *group2 = [[UserGroup alloc] init];
    UserArrowItem *item2_0 = [UserArrowItem itemWithIcon:@"set_logout" andTitle:NSLocalizedString(@"Logout",@"退出登录")];
    [item2_0 setBlock:^(UserItem *itemT, NSIndexPath *indexpath) {
        [YFAlertHelper showLeeAlertWithTitle:NSLocalizedString(@"prompt", nil) message:NSLocalizedString(@"Dear, confirm the exit?", nil) cancelTitle:NSLocalizedString(@"Cancel", nil) defalutTitle:NSLocalizedString(@"OK", nil) cancelAction:^{

        } defalutAction:^{
            // 退出登录
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kLoginAcionNotification" object:@(NO) userInfo:nil];
        }];
    }];
    [group2.items addObject:item2_0];
    [self.groups addObjectsFromArray:@[group0,group1,group2]];
    self.tableView.rowHeight = 50;
    
    [self.tableView reloadData];
}

@end
