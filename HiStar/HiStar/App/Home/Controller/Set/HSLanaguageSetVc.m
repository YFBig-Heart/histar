//
//  HSLanaguageSetVc.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSLanaguageSetVc.h"
#import "DAConfig.h"
#import "NSBundle+DAUtils.h"
#import "HSHomeViewController.h"
#import "MyNavigationViewController.h"

@interface HSLanaguageSetVc ()

@end

@implementation HSLanaguageSetVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Language",@"語言");
    [self loadDataItems];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)loadDataItems {
    UserGroup *group0 = [[UserGroup alloc] init];
    UserSaveItem *item0_0 = [UserSaveItem itemWithIcon:@"lanaguage_heart" andTitle:NSLocalizedString(@"Follow system", @"跟随系统")];
    [group0.items addObject:item0_0];

    UserGroup *group1 = [[UserGroup alloc] init];
    UserSaveItem *item1_0 = [UserSaveItem itemWithIcon:@"lanaguage_xuehua" andTitle:NSLocalizedString(@"Simplified Chinese", @"简体中文")];
    [group1.items addObject:item1_0];

    UserGroup *group2 = [[UserGroup alloc] init];
    UserSaveItem *item2_0 = [UserSaveItem itemWithIcon:@"lanaguage_star" andTitle:@"English"];
    [group2.items addObject:item2_0];

    UserGroup *group3 = [[UserGroup alloc] init];
    UserSaveItem *item3_0 = [UserSaveItem itemWithIcon:@"lanaguage_set" andTitle:NSLocalizedString(@"Traditional Chinese", @"繁體中文")];
    [group3.items addObject:item3_0];
    [self.groups addObjectsFromArray:@[group0,group1,group2,group3]];
    self.tableView.rowHeight = 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserCenterCell *cell = [UserCenterCell settingViewCellTableView:tableView];
    UserGroup *group = self.groups[indexPath.section];
    UserItem *item = group.items[indexPath.row];
    cell.item = item;
    if (indexPath.row == group.items.count - 1) {
        [cell showSeparationBottomLine:NO];
    }else {
        [cell showSeparationBottomLine:YES];
    }
    if (![DAConfig userLanguage].length) {
        cell.accessoryType = indexPath.section == 0 ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    } else {
        kLanaguageType lanType = [NSBundle isChineseLanguage];
        if (indexPath.section == lanType) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        return;
    }
    for (UITableViewCell *acell in tableView.visibleCells) {
        acell.accessoryType = acell == cell ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
//    [MBProgressHUD showActivityMessageInView:NSLocalizedString(@"Processing...", nil)];
    if (indexPath.section == 0) {
        [DAConfig setUserLanguage:nil];
    }else if (indexPath.section == 1) {
        [DAConfig setUserLanguage:@"zh-Hans"];
    }else if (indexPath.section ==  3){
        [DAConfig setUserLanguage:@"zh-Hant"];
    }else{
        [DAConfig setUserLanguage:@"en"];
    }
    
    [[HSProgrammerViewModel shareProgrammerVM] updateMoudleModels];
    //更新当前storyboard
    HSHomeViewController *hoemVc = [HSHomeViewController homeViewController];
    MyNavigationViewController *nav = [[MyNavigationViewController alloc] initWithRootViewController:hoemVc];
    nav.yfnavStyle = NavigationNomal;
    
    HSLanaguageSetVc *lanaguageVc = [[HSLanaguageSetVc alloc] init];
    [lanaguageVc setBaseBackBarItem];
    NSMutableArray *vcs = nav.viewControllers.mutableCopy;
    [vcs addObject:lanaguageVc];
    
    //解决奇怪的动画bug。异步执行
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].keyWindow.rootViewController = nav;
        nav.viewControllers = vcs;
    });
    
}




@end
