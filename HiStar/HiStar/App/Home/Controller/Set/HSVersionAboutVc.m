//
//  HSVersionAboutVc.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSVersionAboutVc.h"



@interface HSVersionAboutVc ()
@property (weak, nonatomic) IBOutlet UILabel *verionLaberl;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *appVersionLabel;

@end

@implementation HSVersionAboutVc

+ (instancetype)verionAboutVc {
   HSVersionAboutVc *about = [[HSVersionAboutVc alloc] initWithNibName:@"HSVersionAboutVc" bundle:nil];
    return about;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.titleLabel.text = NSLocalizedString(@"Version", nil);
    NSString *ver1 = [NSString stringWithFormat:NSLocalizedString(@"Little Warrior version:%@", nil),@"0201"];
    self.verionLaberl.text = ver1;
    NSString *ver2 = [NSString stringWithFormat:NSLocalizedString(@"Application version:%@", @"应用版本：%@"),[NSString appCurrentVersion]];
    self.appVersionLabel.text = [NSString stringWithFormat:@"%@",ver2];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    YFPeripheral *per = [YFCommunicationManager shareInstance].yfPeripheral;
    if (per.isPeripheralConnected) {
        [per requestOperationType:BLEOperationTypeGetVerison parmaObject:nil completionBlock:^(BOOL success, id response, NSError *error) {
            NSLog(@"%@",response);
            if (success) {
                if ([response isKindOfClass:[NSData class]]) {
                    NSString *ver = [YFBTHandler convertNewMacAddressWithData:response range:NSMakeRange(0, [(NSData *)response length]) isReversal:NO];
                    NSString *ver1 = [NSString stringWithFormat:NSLocalizedString(@"Little Warrior version:%@", nil),ver];
                    self.verionLaberl.text = ver1;
                }
            }
        }];
    }
}


- (IBAction)backBtnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
