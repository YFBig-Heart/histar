//
//  HSManualController.m
//  HiStar
//
//  Created by petcome on 2019/5/18.
//  Copyright © 2019 晴天. All rights reserved.
//

#import "HSManualController.h"
#import <Masonry/Masonry.h>
#import "HSManualCell.h"
#import "HSManualModel.h"
#import "HSManualFirstController.h"
#import "CustomSlider.h"

@interface HSManualController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>

@property (nonatomic,weak)IBOutlet UIButton *backBtn;
@property (nonatomic,weak)IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet CustomSlider *sliderView;

@property (nonatomic,strong)NSArray *manualArray;


@end

@implementation HSManualController

+ (instancetype)manualController {
    HSManualController *vc =  [[HSManualController alloc
                                ]initWithNibName:@"HSManualController" bundle:nil];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = NSLocalizedString(@"Build Handbook", nil);
    self.manualArray = [HSManualModel locationAllManuals];
    self.hiddenNavBar = YES;
    [self setUpUi];
    [self setUpSliderView];
}
- (IBAction)backBtnClick:(id)sender {
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)setUpUi {
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.flowLayout.minimumLineSpacing = 2;
    CGFloat itemsizeH = 230.0*kYFScreenHeight/360;
    CGFloat collectionVW = itemsizeH*568/230;
    CGFloat itemsizeW = (collectionVW - self.flowLayout.minimumLineSpacing * 2)/3;
    self.flowLayout.itemSize = CGSizeMake(itemsizeW, itemsizeH);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:@"HSManualCell" bundle:nil] forCellWithReuseIdentifier:@"HSManualCell"];
}

- (void)setUpSliderView {
    self.sliderView.cornerRadius = 2;
    self.sliderView.isHorizontalSlider = YES;
    self.sliderView.bgColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.startColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.endColor = RGB16TOCOLOR(0x00459E);
    self.sliderView.thumbImage = [UIImage imageNamed:@"white_slider"];
    [self.sliderView layerFilletWithRadius:self.sliderView.cornerRadius];
    [self.sliderView setSliderValueAndCurrentTarget:0];
    [self.sliderView updateDrawUI];
    WS(weakSelf);
    [self.sliderView setGesActionValueChange:^(CGFloat value) {
        CGFloat offsetX =value*((weakSelf.collectionView.contentSize.width-weakSelf.collectionView.width));
        weakSelf.collectionView.contentOffset = CGPointMake(offsetX, 0);
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat value = (scrollView.contentOffset.x)/(scrollView.contentSize.width-scrollView.width);
    [self.sliderView setSliderValueAndCurrentTarget:(value * 100)];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.manualArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HSManualCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HSManualCell" forIndexPath:indexPath];
    HSManualModel *model = self.manualArray[indexPath.item];
    cell.manualModel = model;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    HSManualModel *model = self.manualArray[indexPath.item];
    HSManualFirstController *firstVc = [HSManualFirstController manualFirstVcWithItem:model];
    [self.navigationController pushViewController:firstVc animated:YES];
}

@end
