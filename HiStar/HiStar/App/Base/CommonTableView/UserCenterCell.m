//
//  UserCenterCell.m
//  CoolTennisBall
//
//  Created by Coollang on 16/8/25.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import "UserCenterCell.h"
#import "UserItem.h"
#import "UserArrowItem.h"
#import "UIView+YYAdd.h"
#import "UserSaveItem.h"
#import "UserLabelItem.h"
#import "UserImageItem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import "UIImage+Extension.h"
#import <Masonry/Masonry.h>

@interface UserCenterCell ()

// 开关
@property (nonatomic, strong) UISwitch *accessorySwitch;
// 箭头
@property (nonatomic, strong) UIImageView *accessoryArrow;
// label
@property (nonatomic, strong) UILabel *accessoryLabel;

@property (nonatomic, strong)UIView *lineView;

/** 打钩 */
@property (nonatomic, strong) UIImageView *selectImageView;

@property (nonatomic, strong) UIImageView *rightImageView; // 右侧的图片

@property (nonatomic, strong) UIView *topLineView; // 顶部分割线
@property (nonatomic, strong) UIView *bottomLineView;

@end

@implementation UserCenterCell


+ (instancetype)settingViewCellTableView:(UITableView *)tableview withStyle:(UITableViewCellStyle)style {
    static NSString *ID = @"setting";
    UserCenterCell *cell = [tableview dequeueReusableCellWithIdentifier:ID];
    
    if (cell == nil) {
        cell = [[UserCenterCell alloc] initWithStyle:style reuseIdentifier:ID];
    }
    
    return cell;
}

+ (instancetype)settingViewCellTableView:(UITableView *)tableview {
    return [self settingViewCellTableView:tableview withStyle:UITableViewCellStyleValue1];
}

// 设置cell的样式
- (void)setupCell {
    self.textLabel.textColor = _item.titleColor;
    self.textLabel.font = [UIFont pingFangSCFont:PingFangSCMedium size:14];
    self.backgroundColor = [UIColor whiteColor];
    
    self.detailTextLabel.textColor = _item.subTitleColor;
    self.detailTextLabel.font =  [UIFont pingFangSCFont:PingFangSCRegular size:14];
    self.textLabel.numberOfLines = 0;
    self.detailTextLabel.numberOfLines = 0;
    
    if (_item.cellState == kCellStateUnenable) {
        self.imageView.image = [[UIImage imageNamed:_item.icon] imageWithTintColor:[UIColor grayColor] blendMode:kCGBlendModeSoftLight alpha:1.0];
        self.textLabel.textColor = [UIColor grayColor];
        self.detailTextLabel.textColor = RGBCOLOR(34, 34, 34);
        if(_accessoryArrow != nil){
            _accessoryArrow.image = [UIImage imageNamed:@"rightArrow_gray"];
        }
    }else {
        if(_accessoryArrow != nil){

            _accessoryArrow.image =[UIImage imageNamed:@"rightArrow_gray"];
        }
    }
}
- (void)setItem:(UserItem *)item {
    _item = item;
    // 设置数据
    if(item.icon) {
        UIImage *image = [UIImage imageNamed:item.icon];
        self.imageView.image = image;
    }
    // 如果是开关就设置成不能被选中
    self.selectionStyle = [item isKindOfClass:[UserLabelItem class]] ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleDefault;
   
    self.textLabel.text = item.title;
    self.detailTextLabel.text = item.subTitle;
    
    if ([item isKindOfClass:[UserArrowItem class]]) {
        _accessoryArrow.hidden = NO;
        self.accessoryView = [self accessoryArrow];
    }else {
        if (_accessoryArrow != nil) {
            _accessoryArrow.hidden = YES;
            [_accessoryArrow removeFromSuperview];
            _accessoryArrow = nil;
            self.accessoryArrow = nil;
        }
    }
    if ([item isKindOfClass:[UserSaveItem class]]){
        UserSaveItem *itemSave = (UserSaveItem *)item;
        [self updateSelectImageView];
        [itemSave addObserver:self forKeyPath:@"isSelected" options:NSKeyValueObservingOptionNew context:nil];
    }else {
        if (_selectImageView != nil) {
            [_selectImageView removeFromSuperview];
            _selectImageView = nil;
        }
    }
    if ([item isKindOfClass:[UserLabelItem class]]){
        // 给label赋值
        UserLabelItem *itemLabel = (UserLabelItem *)item;
        self.accessoryLabel.text = itemLabel.labelText;
    }else {
        if (_accessoryLabel != nil) {
            [_accessoryLabel removeFromSuperview];
            _accessoryLabel = nil;
        }
    }
    
    if ([item isKindOfClass:[UserImageItem class]]){
        [self rightImageView];
        UserImageItem *itemImage = (UserImageItem *)item;
        if (itemImage.image != nil) {
            self.rightImageView.image = itemImage.image;
        }else {
            [self.rightImageView sd_setImageWithURL:[NSURL URLWithString:itemImage.imagePath] placeholderImage:[UIImage imageNamed:@"user_camera"]];
        }
        _rightImageView.hidden = NO;

    }else {
        if (_rightImageView != nil) {
            _rightImageView.hidden = YES;
            [_rightImageView removeFromSuperview];
            _rightImageView = nil;
        }
    }
    [self setupCell];
}

- (UIImageView *)accessoryArrow {
    if (_accessoryArrow == nil) {
        _accessoryArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightArrow_gray"]];
    }
    return _accessoryArrow;
}
- (UILabel *)accessoryLabel {
    if (_accessoryLabel == nil) {
        _accessoryLabel = [[UILabel alloc] init];
        // 设置字体大小
        _accessoryLabel.font = [UIFont pingFangSCFont:PingFangSCRegular size:14];
        _accessoryLabel.textColor =RGBCOLOR(34, 34, 34);
        _accessoryLabel.textAlignment = NSTextAlignmentLeft;
        _accessoryLabel.numberOfLines = 0;
        [self.contentView addSubview:_accessoryLabel];
        [_accessoryLabel sizeToFit];
        [_accessoryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.contentView).offset(-16);
        }];
    }
    return _accessoryLabel;
}

- (UIImageView *)selectImageView {
    if (_selectImageView == nil) {
        _selectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_unselect_dot"]];
        [self.contentView addSubview:_selectImageView];
        [_selectImageView sizeToFit];
        [_selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.right.equalTo(self.contentView).offset(-16);
        }];
    }
    return _selectImageView;
}

- (void)updateSelectImageView {
    if ([self.item isKindOfClass:[UserSaveItem class]]) {
        UserSaveItem *itemSave = (UserSaveItem *)self.item;
        if (itemSave.isSelected) {
            self.selectImageView.image = [UIImage imageNamed:itemSave.selectImageName];
        }else {
            self.selectImageView.image = [UIImage imageNamed:itemSave.unSelectImageName];
        }
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"isSelected"]) {
        [self updateSelectImageView];
    }
}

- (void)dealloc {
//    YFLog(@"UsercenterCellDealloc");
    if ([self.item isKindOfClass:[UserSaveItem class]]) {
       [self.item removeObserver:self forKeyPath:@"isSelected"];
    }
}

- (UIImageView *)rightImageView {
    if (_rightImageView == nil) {
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.userInteractionEnabled = YES;
        [self.contentView addSubview:_rightImageView];
    }
    return _rightImageView;
}



#pragma mark 添加分隔线
// 分割线--x 对齐左侧第一个控件
- (UIView *)lineView {
    if(_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = kLightGrayBgColor;
        _lineView.hidden = YES;
        [self.contentView addSubview:_lineView];
    }
    return _lineView;
}
// 底部分割线--x 从0 开始
- (UIView *)bottomLineView {
    if (_bottomLineView == nil) {
        _bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 50 - 1, kYFScreenWidth, 1.0)];
        _bottomLineView.backgroundColor = kLightGrayBgColor;
        [self.contentView addSubview:_bottomLineView];
    }
    return _bottomLineView;
}
// 顶部分割线x 从0 开始
- (UIView *)topLineView {
    if (_topLineView == nil) {
        _topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kYFScreenWidth, 1)];
        _topLineView.backgroundColor = kLightGrayBgColor;
        [self.contentView addSubview:_topLineView];
    }
    return _topLineView;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.bottomLineView.top = self.bounds.size.height - 1;
    // 设置分隔线的frame
    if (self.item.icon) {
        self.lineView.left = self.imageView.left;
    }else {
        self.lineView.left = self.textLabel.left;
    }
    
    self.lineView.top = self.bounds.size.height - 1;
    self.lineView.width = self.bounds.size.width;
    self.lineView.height = 1;
    if ([self.item isKindOfClass:[UserImageItem class]]) {
        UserImageItem *imageItem = (UserImageItem *)self.item;
        _rightImageView.frame = imageItem.imageFrame;
        [_rightImageView sizeToFit];
        if (imageItem.imageFrame.size.width+imageItem.imageFrame.size.height == 0) {
            _rightImageView.y = (self.height - _rightImageView.height)*0.5;
            _rightImageView.x = self.width - _rightImageView.width - 20;
            _rightImageView.layer.cornerRadius = imageItem.cornerRadius;
            _rightImageView.layer.borderColor = imageItem.boderColor.CGColor;
            _rightImageView.layer.borderWidth = imageItem.boderWidth;
            [_rightImageView.layer setMasksToBounds:imageItem.cornerRadius > 0];
        }
    }
    [self adjustUI];
}

- (void)adjustUI {
    self.bottomLineView.height = (self.separationBottomLineHeight <= 0 || self.separationBottomLineHeight > self.height) ? 0.5:self.separationBottomLineHeight;
    
    if (![NSStringFromUIEdgeInsets(_separationBottomLinerightOffset) isEqualToString:NSStringFromUIEdgeInsets(UIEdgeInsetsZero)]) {
        self.bottomLineView.left= self.bottomLineView.left+ self.separationBottomLinerightOffset.left;
        self.bottomLineView.width = (self.bottomLineView.width - self.separationBottomLinerightOffset.right);
        self.bottomLineView.left= self.bottomLineView.left+ self.separationBottomLinerightOffset.left;
        self.bottomLineView.top = self.bottomLineView.top + self.separationBottomLinerightOffset.top - self.separationBottomLinerightOffset.bottom;
    }
    
    self.textLabel.top -= self.textLabelVOffset;
    self.detailTextLabel.top -= self.detailLabelVOffset;
    self.textLabel.left+= self.textLabelHOffset;
    self.detailTextLabel.left+= self.detailLabelHOffset;
    
}


- (void)showLine:(BOOL)show {
    self.lineView.hidden = !show;
    self.bottomLineView.hidden = show;
}
- (void)showSectionSepTopLineView:(BOOL)show {
    self.topLineView.hidden = !show;
}

// 分割线--x 对齐左侧第一个控件
- (void)showSeparationLine:(BOOL)show {
    self.lineView.hidden = !show;
    if (show && self.bottomLineView.hidden == NO) {
        self.bottomLineView.hidden = YES;
    }
}
// 顶部分割线x 从0 开始
- (void)showSeparationTopLine:(BOOL)show {
    self.topLineView.hidden = !show;
}
// 底部部分割线x 从0 开始
- (void)showSeparationBottomLine:(BOOL)show {
    self.bottomLineView.hidden = !show;
    if (show && self.lineView.hidden == NO) {
        self.lineView.hidden = YES;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
}


@end
