//
//  YFCommonBaseController.m
//  Petcome
//
//  Created by petcome on 2018/12/28.
//  Copyright © 2018 yunfei. All rights reserved.
//

#import "YFCommonBaseController.h"
#import <Masonry/Masonry.h>

#define kSectionHeadViewHeight 8

@interface YFCommonBaseController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation YFCommonBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    if( @available(iOS 11.0, *)){
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setTableviewStyle];
}
#pragma mark 设置tableView的样式
- (void)setTableviewStyle {
    self.tableView.delegate = self;
    self.tableView.dataSource =self;
    // 下面两个人一设置一个就可以
    self.tableView.sectionFooterHeight = 0;
    self.tableView.sectionHeaderHeight = 0;
    // 设置背景色
    self.tableView.backgroundColor = kLightGrayBgColor;
    // 去掉分割线
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.groups.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    UserGroup *group = self.groups[section];
    return group.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserCenterCell *cell = [UserCenterCell settingViewCellTableView:tableView];
    UserGroup *group = self.groups[indexPath.section];
    UserItem *item = group.items[indexPath.row];
    cell.item = item;
    if (indexPath.row == group.items.count - 1) {
        [cell showSeparationBottomLine:NO];
    }else {
        [cell showSeparationBottomLine:YES];
    }
    return cell;
}

// 选中的哪一行
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 取出对应的模型数据
    UserGroup *group = self.groups[indexPath.section];
    UserItem *item = group.items[indexPath.row];

    // 2.判断有没有block,有的话就执行
    if (item.block) {
        item.block(item, indexPath);
        return;
    }
    // 3.跳转
    if ([item isKindOfClass:[UserArrowItem class]]) {
        // 强转
        UserArrowItem *arrowItem = (UserArrowItem *)item;
        // 判断target是否有值
        if (arrowItem.target) {
            Class class = arrowItem.target;
            UIViewController *vc = [[class alloc] init];
            // 设置头部标题
            vc.title = item.title;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }

}

#pragma mark 返回组不或者时尾部标题
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return [self.groups[section] footerTitle];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UserGroup *group = self.groups[section];
    if (group.headerTitle != nil) {
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kYFScreenWidth, group.headHeight)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, kYFScreenWidth - 20, headView.bounds.size.height)];
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = RGB16TOCOLOR(0xaaaaaa);
        label.text = group.headerTitle;
        [headView addSubview:label];
        headView.backgroundColor = kLightGrayBgColor;
        return headView;
    }else {
        return [[UIView alloc] init];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    UserGroup *group = self.groups[section];
    if (group.headHeight <= 0) {
        return kSectionHeadViewHeight;
    }else {
        return group.headHeight;
    }
}


#pragma mark - setter & getter
- (NSMutableArray *)groups {
    if (_groups == nil) {
        _groups = [NSMutableArray array];
    }
    return _groups;
}
- (UITableView *)tableView {
    if (_tableView == nil) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [self.view addSubview:self.tableView];
        self.tableView.frame = self.view.bounds;
//        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.equalTo(self.view);
//        }];
    }
    return _tableView;
}


@end
