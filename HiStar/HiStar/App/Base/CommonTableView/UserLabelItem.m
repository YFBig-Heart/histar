//
//  UserLabelItem.m
//  CoolTennisBall
//
//  Created by Coollang on 16/8/29.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import "UserLabelItem.h"


@implementation UserLabelItem
+ (instancetype)itemWithIcon:(NSString *)icon labelItemWithTitle:(NSString *)title andValue:(NSString *)labelText {
    
    UserLabelItem *item = [UserLabelItem labelItemWithTitle:title andValue:labelText];
    
    item.icon = icon;
    
    return item;
}

+ (instancetype)labelItemWithTitle:(NSString *)title andValue:(NSString *)labelText {
    
    UserLabelItem *item = [UserLabelItem itemWithTitle:title];
    item.labelText = labelText;
    
    return item;
}


@end
