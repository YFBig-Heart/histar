//
//  UserItem.m
//  CoolTennisBall
//
//  Created by Coollang on 16/8/25.
//  Copyright © 2016年 CoolLang. All rights reserved.
//

#import "UserItem.h"

@implementation UserItem
- (instancetype)initWithTitle:(NSString *)title {
    if (self = [super init]) {
        self.title = title;
        self.titleColor = kTextDrakGrayColor;
        self.subTitleColor = kTextDrakGrayColor;
    }
    return self;
}
+ (instancetype)itemWithTitle:(NSString *)title {
    return [[self alloc] initWithTitle:title];
}

- (instancetype)initWithIcon:(NSString *)icon andTitle:(NSString *)title {
    if (self = [super init]) {
        self.title = title;
        self.icon = icon;
    }
    return self;
}
+ (instancetype)itemWithIcon:(NSString *)icon andTitle:(NSString *)title {
    
    return [[self alloc] initWithIcon:icon andTitle:title];
}

+ (instancetype)itemWithTitle:(NSString *)title andSubTitle:(NSString *)subTitle {
    UserItem *item = [self itemWithIcon:nil andTitle:title];
    item.subTitle = subTitle;
    return item;
}


@end
