//
//  YFCommonBaseController.h
//  Petcome
//
//  Created by petcome on 2018/12/28.
//  Copyright © 2018 yunfei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBaseViewController.h"
#import "UserCenterCell.h"
#import "UserItem.h"
#import "UserGroup.h"
#import "UserArrowItem.h"
#import "UserLabelItem.h"
#import "UserImageItem.h"
#import "UserSaveItem.h"
NS_ASSUME_NONNULL_BEGIN


@interface YFCommonBaseController : YFBaseViewController

@property (nonatomic, strong)NSMutableArray *groups;

// 可以拿到放置在想要的图层,也可以外界直接赋值,没有赋值的话就自己创建
@property (nonatomic,strong) UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
