//
//  YFBaseViewController.m
//  CoollangTennisBall
//
//  Created by Coollang on 2017/9/12.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFBaseViewController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "HSBlueConnectVc.h"

@interface YFBaseViewController ()

@end

@implementation YFBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.fd_interactivePopDisabled = YES;
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.hiddenNavBar) {
        [self.navigationController setNavigationBarHidden:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.hiddenNavBar) {
        [self.navigationController setNavigationBarHidden:NO];
    }
}
- (BOOL)shouldAutorotate {
    return YES;
}

//支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft |UIInterfaceOrientationMaskLandscapeRight;
}
//默认方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}


@end
