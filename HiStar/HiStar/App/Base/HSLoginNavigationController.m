//
//  HSLoginNavigationController.m
//  HiStar
//
//  Created by 晴天 on 2019/9/22.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSLoginNavigationController.h"

@interface HSLoginNavigationController ()

@end

@implementation HSLoginNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateNavApperanceWithNavStyle:NavigationLogin withViewController:self];
}
- (BOOL)shouldAutorotate {
    return YES;
}
//支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
//默认方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


@end
