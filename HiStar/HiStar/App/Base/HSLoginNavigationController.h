//
//  HSLoginNavigationController.h
//  HiStar
//
//  Created by 晴天 on 2019/9/22.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyNavigationViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSLoginNavigationController : MyNavigationViewController

@end

NS_ASSUME_NONNULL_END
