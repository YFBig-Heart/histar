//
//  UIViewController+FOXBaseVcCategory.h
//  FOXAISpeaker
//
//  Created by 刘云飞 on 2018/4/24.
//  Copyright © 2018年 answer. All rights reserved.
//

#import <UIKit/UIKit.h>
// UIViewController 分类，用于处理一些公用操作

// 导航栏样式
typedef NS_ENUM(NSUInteger, NavigationStyle) {
    NavigationNomal = 0,           // 白色
    NavigationLogin = 1,           // 登录是
};

typedef void(^NavBarItemActionBlock)(UIBarButtonItem *item);

@protocol UIViewControllerFoxBaseDelegate <NSObject>
@optional;
- (void)customBaseBackNavBarItemAction:(UIButton *)backItem;
@end

@interface UIViewController (FOXBaseVcCategory)<UIViewControllerFoxBaseDelegate>

/**
 * 便捷设置默认样式的返回BarItem
 *
 * 遵守yfBaseDelegate并实现customBaseBackNavBarItemAction自定义返回的方法
 */
- (UIButton *)setBaseBackBarItem;

/*
 设置导航栏返回按钮
 */
- (void)changeBackBarItemWithImage:(UIImage *)image;

/** 返回控制器的名称的字符串 */
+ (NSString *)defalutIndentify;

// 给导航栏添加navItem, imageName优先级大于 title
- (UIBarButtonItem *)addNavBarItemWithTitle:(NSString *)title orImageName:(UIImage *)image action:(NavBarItemActionBlock)barItemAction;

@property (nonatomic, weak) id <UIViewControllerFoxBaseDelegate> yfBaseDelegate;

/** 导航栏分割线颜色,默认没有分割线 */
@property (nonatomic, strong) UIColor *navSeparationLineColor;

/** 返回按钮 */
@property (nonatomic, strong) UIButton *yf_backBarButton;

/** 导航栏样式,默认值为0 */
@property (nonatomic, assign)NavigationStyle yfnavStyle;



@end

@interface UIBarButtonItem (YFBarItem)
@property (nonatomic, copy)NavBarItemActionBlock navBarBlock;
@end
