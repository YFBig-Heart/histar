//
//  PopupViewController.h
//  CoolMove
//
//  Created by CA on 14-8-23.
//  Copyright (c) 2014年 CA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFBaseViewController.h"

typedef NS_ENUM(NSUInteger, DetailViewControllerAnimationType) {
    DetailViewControllerAnimationTypeSlide,
    DetailViewControllerAnimationTypeFade,
    DetailViewControllerAnimationTypeSlideRight
};

typedef NS_ENUM(NSUInteger, DetailViewControllerPresentAnimationType) {
    DetailViewControllerPresentAnimationTypePop,
    DetailViewControllerPresentAnimationTypeUp,
    DetailViewControllerPresentAnimationTypeDown,
    DetailViewControllerPresentAnimationTypeFade,
    DetailViewControllerPresentAnimationTypeFadeUp,
    DetailViewControllerPresentAnimationTypeLeft
};

@interface PopupViewController : YFBaseViewController
@property (strong, nonatomic) IBOutlet UIView *popupView;
- (void)presentInParentViewController:(UIViewController *)parentViewController;

- (void)presentInParentViewController:(UIViewController *)parentViewController animationType:(DetailViewControllerPresentAnimationType)animationType;

- (void)dismissFromParentViewControllerWithAnimationType:(DetailViewControllerAnimationType)animationType;

@property (nonatomic,assign)DetailViewControllerAnimationType comeOutAnimation;


/** 是否开启点击黑色蒙版退出 */
@property (nonatomic, assign)BOOL isTapCoverViewDismiss;

@end


@interface GradientView : UIView

@end

