//
//  MyNavigationViewController.h
//  CoolRollerSkating
//
//  Created by 谢伟康 on 15/11/9.
//  Copyright © 2015年 Coollang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyNavigationViewController : UINavigationController

- (void)updateNavApperanceWithNavStyle:(NavigationStyle)navStyle withViewController:(UIViewController *)VC;

@end
