//
//  YFBaseViewController.h
//  CoollangTennisBall
//
//  Created by Coollang on 2017/9/12.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YFBaseViewController : UIViewController

/** 是否隐藏导航栏 */
@property (nonatomic,assign)BOOL hiddenNavBar;

@end
