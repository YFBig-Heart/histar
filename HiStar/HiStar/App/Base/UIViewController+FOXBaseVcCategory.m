//
//  UIViewController+FOXBaseVcCategory.m
//  FOXAISpeaker
//
//  Created by 刘云飞 on 2018/4/24.
//  Copyright © 2018年 answer. All rights reserved.
//

#import "UIViewController+FOXBaseVcCategory.h"
#import <objc/runtime.h>


@implementation UIViewController (FOXBaseVcCategory)

+ (NSString *)defalutIndentify {
    return NSStringFromClass([self class]);
}

#pragma mark - 设置导航栏
/*
 设置导航栏返回按钮
 */
- (void)changeBackBarItemWithImage:(UIImage *)image {
    [self.yf_backBarButton setImage:image forState:UIControlStateNormal];
}
- (UIButton *)setBaseBackBarItem {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:@"nav_setBack"] forState:UIControlStateNormal];
    [button sizeToFit];
    button.frame = CGRectMake(16, 0, 49, 49);
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button addTarget:self action:@selector(baseBackBarItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = backButton;
    self.yf_backBarButton = button;
    return button;
}
- (void)baseBackBarItemAction:(UIButton *)item {
    if ([self.yfBaseDelegate respondsToSelector:@selector(customBaseBackNavBarItemAction:)]) {
        [self.yfBaseDelegate customBaseBackNavBarItemAction:item];
    }else {
        if (self.presentingViewController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (UIBarButtonItem *)addNavBarItemWithTitle:(NSString *)title orImageName:(UIImage *)image action:(void(^)(UIBarButtonItem *))barItemAction {
    UIBarButtonItem *barItem = nil;
    if (image) {
        barItem = [[UIBarButtonItem alloc] initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(navBarItemAction:)];
    }else {
        barItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(navBarItemAction:)];
    }
    barItem.navBarBlock = barItemAction;
    return barItem;
}
- (void)navBarItemAction:(UIBarButtonItem *)barItem {
    if (barItem.navBarBlock) {
        barItem.navBarBlock(barItem);
    }
}
- (void)setYfBaseDelegate:(id<UIViewControllerFoxBaseDelegate>)yfBaseDelegate {
    objc_setAssociatedObject(self, @"yfBaseDelegate", yfBaseDelegate, OBJC_ASSOCIATION_ASSIGN);
}
- (id<UIViewControllerFoxBaseDelegate>)yfBaseDelegate {
    return objc_getAssociatedObject(self, @"yfBaseDelegate");
}

- (void)setNavSeparationLineColor:(UIColor *)navSeparationLineColor {
     objc_setAssociatedObject(self, @"navSeparationLineColor", navSeparationLineColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIColor *)navSeparationLineColor {
    return objc_getAssociatedObject(self, @"navSeparationLineColor");
}
- (void)setYf_backBarButton:(UIButton *)yf_backBarButton {
    objc_setAssociatedObject(self, @"yf_backBarButton",yf_backBarButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIButton *)yf_backBarButton {
    return objc_getAssociatedObject(self, @"yf_backBarButton");
}
- (void)setYfnavStyle:(NavigationStyle)yfnavStyle {
    objc_setAssociatedObject(self, @"yfnavStyle", @(yfnavStyle), OBJC_ASSOCIATION_ASSIGN);
}
- (NavigationStyle)yfnavStyle {
    return [objc_getAssociatedObject(self, @"yfnavStyle") integerValue];
}

@end

@implementation UIBarButtonItem (YFBarItem)

- (void)setNavBarBlock:(NavBarItemActionBlock)navBarBlock {
    /*
     OBJC_ASSOCIATION_COPY = 01403 //关联对象的属性是copy并且关联对象使用原子性
     */
    objc_setAssociatedObject(self, @"navBarBlock", navBarBlock, OBJC_ASSOCIATION_COPY);
}

- (NavBarItemActionBlock)navBarBlock {
    return objc_getAssociatedObject(self, @"navBarBlock");
}

@end
