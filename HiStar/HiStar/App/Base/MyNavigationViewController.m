//
//  MyNavigationViewController.m
//  CoolRollerSkating
//
//  Created by 谢伟康 on 15/11/9.
//  Copyright © 2015年 Coollang. All rights reserved.
//

#import "MyNavigationViewController.h"

@interface MyNavigationViewController ()<UINavigationControllerDelegate>

@end

@implementation MyNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.interactivePopGestureRecognizer.enabled = NO;
    [self updateNavApperanceWithNavStyle:self.yfnavStyle withViewController:self];
    self.delegate = self;
    self.view.backgroundColor = kLightGrayBgColor;
}
//topViewController是其最顶层的视图控制器，
-(BOOL)shouldAutorotate{
    return self.topViewController.shouldAutorotate;
}
//支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}
//默认方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return [self.topViewController preferredInterfaceOrientationForPresentation];
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.childViewControllers.count >= 1) {
        //返回按钮自定义
        [viewController setBaseBackBarItem];
//      [viewController setHidesBottomBarWhenPushed:YES];
    }
    [super pushViewController:viewController animated:animated];
}

//- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    if ([navigationController isKindOfClass:[MyNavigationViewController class]]) {
//        [self updateNavApperanceWithNavStyle:viewController.yfnavStyle withViewController:viewController];
//    }
//}
//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    if ([navigationController isKindOfClass:[MyNavigationViewController class]]) {
//        [self updateNavApperanceWithNavStyle:viewController.yfnavStyle withViewController:viewController];
//    }
//}

#pragma mark - 修改导航栏样式
- (void)updateNavApperanceWithNavStyle:(NavigationStyle)navStyle withViewController:(UIViewController *)VC {
    UIImage *backgroundImage = [[UIImage alloc] init];
    NSDictionary *textAttributes = nil;
    VC.yfnavStyle = navStyle;
    switch (navStyle) {
        case NavigationNomal:{
            textAttributes = @{
                               NSFontAttributeName : [UIFont pingFangSCFont:PingFangSCMedium size:16],
                               NSForegroundColorAttributeName : kNavTextDrakColor,
                               };
            [self.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            [self.navigationBar setShadowImage:[UIImage new]];
            [self.navigationBar setTitleTextAttributes:textAttributes];
            [self.navigationBar setBarTintColor:kNavColor ];
            [self.navigationBar setTintColor:kNavTextDrakColor];
            self.navigationBar.translucent = NO;
        }
            break;
        case NavigationLogin:{
            textAttributes = @{
                               NSFontAttributeName : [UIFont pingFangSCFont:PingFangSCMedium size:16],
                               NSForegroundColorAttributeName : [UIColor whiteColor],
                               };
            [self.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            [self.navigationBar setShadowImage:[UIImage new]];
            [self.navigationBar setTitleTextAttributes:textAttributes];
            [self.navigationBar setBarTintColor:kNavLoginColor];
            [self.navigationBar setTintColor:[UIColor whiteColor]];
            self.navigationBar.translucent = NO;
        }
            break;
        default:
            break;
    }
}


@end
