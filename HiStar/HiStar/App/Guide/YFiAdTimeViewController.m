//
//  YFiAdTimeViewController.m
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/12.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "YFiAdTimeViewController.h"

@interface YFiAdTimeViewController ()
@property (nonatomic,strong)UIImageView *iadimageView;

@end

@implementation YFiAdTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.iadimageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LaunchScreen_V"]];
    self.iadimageView.frame = CGRectMake(0, 0, kYFScreenWidth, kYFScreenHeight);
    [self.view addSubview:self.iadimageView];
}

- (BOOL)shouldAutorotate {
    return YES;
}
//支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
//默认方向
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}




@end
