//
//  YFUIDefine.h
//  Petcome
//
//  Created by petcome on 2018/12/28.
//  Copyright © 2018 yunfei. All rights reserved.
//

#ifndef YFUIDefine_h
#define YFUIDefine_h

/// 屏幕的长宽
#define kYFScreenWidth  [UIScreen mainScreen].bounds.size.width
#define kYFScreenHeight [UIScreen mainScreen].bounds.size.height

// 判断是否为iPhone X 系列  这样写消除了在Xcode10上的警告。
#define IPHONE_X \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

#define kYFsafeAreaInsets \
({UIEdgeInsets safeAreaInsets = UIEdgeInsetsZero;\
if (@available(iOS 11.0, *)) {\
safeAreaInsets = [[UIApplication sharedApplication] delegate].window.safeAreaInsets;\
}\
(safeAreaInsets);})


/**
 *导航栏高度
 */
#define SafeAreaTopHeight (IPHONE_X ? 88 : 64)


/**
 *tabbar高度
 */
#define SafeAreaBottomHeight (IPHONE_X ? (49 + 34) : 49)

/// 导航栏默认高度
#define kStatusBarheght [[UIApplication sharedApplication] statusBarFrame].size.height

/// 纯代码适配等比例拉伸(以iPhone6/6s计算)
#define kSCREEN_WIDTH_RATIO (kYFScreenWidth/375.0)
#define kSCREEN_HEIGHT_RATIO (kYFScreenHeight<500?1:kYFScreenHeight/667.0) /// 5/5s以下不做等比例缩放
#define kAutoWid(w) (w*kSCREEN_WIDTH_RATIO)
#define kAutoHei(h) (h*kSCREEN_HEIGHT_RATIO)


#define kElementWH [HSProgrammerViewModel shareProgrammerVM].elementWH // 元素宽高

#endif /* YFUIDefine_h */
