//
//  YFStarEnum.h
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/11.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YFStarEnum : NSObject
// 性别
typedef NS_ENUM(NSInteger, Gender){
    GenderUnknown = 0,  ///未知
    GenderMale = 1,     ///男
    GenderFemale = 2,   ///女
};

// 蓝牙设备
typedef NS_ENUM(NSInteger, BlueOemType) {
    BlueOemTypeHiStar = 0, //小海星
};


// 基本功能
typedef NS_ENUM(NSInteger, BaseFunctionType) {
    BaseFunctionRemote = 0,    //遥控模式
    BaseFunctionGravity = 1,   //重力模式
    BaseFunctionBizhang = 2,   //避障模式
    BaseFunctionApplause = 3,  //掌声模式
    BaseFunctionDance = 4,     //跳舞模式
    BaseFunctionXunXian = 5,   //巡线模式
    BaseFunctionFollowlight=6, //追光模式
    BaseFunctionSumo=7,        //相扑模式
    BaseFunctionCrawl =8 ,     //围栏模式
    BaseFunctionProgramme=9,   //编程模式
};

typedef enum : NSUInteger {
    kModuleControl=0, //控制模组
    kModuleRead = 1, // 读取模组
    kModuleData = 2, // 数据模组
    kModuleFlow = 3, // 流程模组
    kMoudleOther = 4, //其他模组
} kModuleType;


// 控制元素
typedef NS_ENUM(NSInteger, ElementTypeId) {
    ElementTypeStart = 0,// 开始
    ElementTypeEnd = 1, // 结束
    
};

@end

NS_ASSUME_NONNULL_END
