//
//  YFConfigBase.h
//  Petcome
//
//  Created by petcome on 2018/12/24.
//  Copyright © 2018 yunfei. All rights reserved.
//

#ifndef YFConfigBase_h
#define YFConfigBase_h

#import "UIView+Layer.h"
#import "UIView+Frame.h"
#import "UIButton+YFHitRect.h"
#import "YFUIDefine.h"
#import "YFStarEnum.h"
#import "YFColorDefine.h"


#define kCurrentLanguageIschinese [[[NSLocale preferredLanguages] objectAtIndex:0] rangeOfString:@"zh-Hans"].location != NSNotFound


// Bugly
#define kBuglyAppid @"238db648df"
#define kBuglyAppKey @"7c01214c-c7ab-477c-82a8-88f6e5b86b33"

#define kwechatAppkey @"wx2008720536bacf82"
#define kwechatAppscreat @"e3e36e34a795998d285966bc37494557"

//应用名称
#define kAPPName                [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]
//版本号
#define kAppVersion             [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
//开发版本号
#define kBuildVersion           [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
//Bundle identifier
#define kAppBundleID            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]

///系统版本
#define kSystemVersion   ([[UIDevice currentDevice] systemVersion])
#define IOS_Version ([kSystemVersion floatValue])

#define IOS_OR_LATER(version)   (IOS_Version >= version)
#define IOS7_OR_LATER   (IOS_Version >= 7.0) //ios 7
#define IOS8_OR_LATER   (IOS_Version >= 8.0) //ios 8
#define IOS9_OR_LATER    (IOS_Version >= 9.0)  //ios 9
#define IOS10_OR_LATER   (IOS_Version >= 10.0) //ios 10
#define IOS11_OR_LATER   (IOS_Version >= 11.0) //ios 11


/** 强弱引用 **/
#define WS(weakSelf)    __weak __typeof(&*self)   weakSelf  = self;
#define SS(strongSelf)  __strong __typeof(&*self) strongSelf = weakSelf;


//主线程
#define GCDMain(block)       dispatch_async(dispatch_get_main_queue(),block)
///延迟处理
#define GCDAfter(seconds, block) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)( seconds * NSEC_PER_SEC)),dispatch_get_main_queue(), block)



/* 功能类型 */
#define k_ft_start @"start" // 开始事件
#define k_ft_end @"end" // 结束事件

#define k_ft_addEvent @"addEvent" // 添加新事件
#define k_ft_startEvent @"startEvent" // 开始事件
#define k_ft_endEvent @"endEvent" // 结束事件

#define k_ft_trumpet @"trumpet" // 喇叭
#define k_ft_light @"light" // 提示灯
#define k_ft_music @"music" // 音乐
#define k_ft_obstacle @"obstacle" // 障碍检测
#define k_ft_singleMotor @"singleMotor" // 单边轮
#define k_ft_doubleMotor @"doubleMotor" // 双边轮
#define k_ft_xunXian @"xunXian" // 巡线模式
#define k_ft_sign @"sign" // 发射信号
#define k_ft_timer @"timer" // 倒计时

#define k_ft_keyBtn @"keyBtn" // 按键
#define k_ft_applouse @"applouse" // 拍手声
#define k_ft_yaoKong @"yaoKong" // 遥控
#define k_ft_receiveData @"receiveData" // 接收数据
#define k_ft_sensitiveLight @"sensitiveLight" // 感光

#define k_ft_add @"add" // 加1
#define k_ft_reduce @"reduce" // 减1
#define k_ft_variable @"variable" // 变量赋值
#define k_ft_copy @"copy" // 拷贝变量
#define k_ft_character @"character"
#define k_ft_byte @"byte" 


#define CONSTANT klocConstant
#define NO_VAR ElementString(@"-No Variable-")
#define MOTHERBOARD @"*Motherboard*"

#define MIN_BYTE 0
#define MAX_BYTE 0xff

#define MIN_SBYTE -0x7f
#define MAX_SBYTE 0x7f

#define MIN_WORD -0x7fff
#define MAX_WORD 0X7FFF
#define MAX_UWORD 0xffff

#define None -1000000

#define digits @"0123456789"
#define hexdigits @"0123456789abcdefABCDEF"

#define octdigits @"01234567"
#define punctuation @"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
#define whitespace @" \t\n\r\v\f"
#define lowercase @"abcdefghijklmnopqrstuvwxyz"
#define lowercase @"abcdefghijklmnopqrstuvwxyz"


#define ascii_letters @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

#define MAX_LIMITS @[@(256),@(256),@(16*4),@(16),@(4096)]
#define space_names @[@"Byte",@"word",@"LCD"]
#define space_types @{@"b":@(0),@"B":@(0),@"w":@(1),@"W":@(1),@"a":@(2),@"A":@(2)}


#define ElementString(key) NSLocalizedStringFromTable(key, @"ElementString", nil)

#define klocOk NSLocalizedString(@"OK", nil)
#define klocCancel NSLocalizedString(@"Cancel", nil)
#define klocVariable NSLocalizedString(@"Variable", nil) // 变量
#define klocConstant NSLocalizedString(@"Constant", nil) // 常量


#define ROW_LENGTH 14
#define COL_LENGTH 6


#endif /* YFConfigBase_h */
